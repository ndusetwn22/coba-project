import React, { Component } from "react";
import Loading, { setUrl, getUrl,setProfile, getProfile,decrypt} from "../../APIProp/ApiConstants";
import {onLoginValidation, insertTokenFcm} from './LoginAxios';
import { View } from "native-base";
import LoginPage from './LoginPage';
import styles from "./css/style.js";
import SweetAlert from 'react-native-sweet-alert';
import messaging from '@react-native-firebase/messaging';
import { AppState,Platform,Dimensions, Image, FlatList , TouchableOpacity,ScrollView, ImageBackground, BackHandler, Alert, PixelRatio, StyleSheet, TextInput, Picker } from "react-native";



class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      paramLogin:{
        username:'',
        password:'',
      },
      profile:null,
      isLoading:false,
      url:null
    };
  }

  componentDidMount= async()=>{
    await setUrl();
    await getUrl().then(result=>{
        this.setState({url:result})
    });
    await getProfile().then(result=>{
      this.setState({profile:result})
    })
    if(this.state.profile!=null){
      this.props.navigation.replace('MainRouter', {screen: 'Dashboard'})
    }
  }

  onLogin=async()=>{
    await this.setState({isLoading:true});

    try {
      
    
      var result = await onLoginValidation(this.state.url, this.state.paramLogin)
      if(result!=null){
        if(result.data.status=="success"){
          // console.log('sukses ')
          if(result.data.data[0].pass == this.state.paramLogin.password){
            // await setProfile(result.data.data[0]);
            let tempProfile = result.data.data[0]

                  // await setProfile(tempProfile);
                  // // await insertTokenFcm(this.state.url, result.data.data[0].id_user, token )
                  // this.showSweetAlert(true, "Let's Dive In!")
                  // this.props.navigation.replace('MainRouter')

                  await console.log('id user = ', result.data.data[0].id_user)
                  var fcm_token = await messaging().getToken().then(async(token) => {

                    tempProfile.token_fcm = token
                    await setProfile(tempProfile);
                    await insertTokenFcm(this.state.url, result.data.data[0].id_user, token )
                    this.showSweetAlert(true, "Let's Dive In!")
                    this.props.navigation.replace('MainRouter')
                    // return token;

                }).catch(error=>{
                  this.showSweetAlert(false,"FCM Token Error!")
                });
          }else{
            this.showSweetAlert(false,"Wrong Password!")
          }
        }else{
          this.showSweetAlert(false,"Unknown User!")
        }
      }else{
        this.showSweetAlert(false,"Connection Failure")
      }

    } catch (error) {
      alert('ERRORRRR'+ error.message.toString())
    }

    await this.setState({isLoading:false});
  }

  showSweetAlert=(param, textInfo)=>{
    if(param){
      SweetAlert.showAlertWithOptions({
        title: 'Success Login!',
        subTitle: textInfo,
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'success',
        cancellable: true
      });
    }else{
      SweetAlert.showAlertWithOptions({
        title: 'Fail Login!',
        subTitle: textInfo,
        dangerMode: true,
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#cb3837',
        style: 'error',
        cancellable: true
      });
    }
  }

  onInputChange=async(factor,value)=>{
    var tmpParamLogin = this.state.paramLogin;
    if(factor=='U'){
      tmpParamLogin.username=value;
    }else{
      tmpParamLogin.password=value;
    }
    await this.setState({paramLogin:tmpParamLogin});
  }

  render() {
    return (
      <View style={styles.container}>
        <LoginPage 
          {...this.state}
          onInputChange={this.onInputChange}
          onLogin={this.onLogin}
        />
        {this.state.isLoading ? <Loading/> : false}
      </View>
    );
  }
}

export default Login;
