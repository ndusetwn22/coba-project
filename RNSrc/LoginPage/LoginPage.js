import React, { Component } from "react";
import styles from "./css/style.js";
import { View, Container } from "native-base";
import {Text,TextInput,Image,TouchableOpacity, ImageBackground, SafeAreaView, Dimensions} from "react-native";
import Logo from "./component/Logo.js";
import LinearGradient from "react-native-linear-gradient";
import {version} from "../../APIProp/ApiConstants";
const windowHeight = Dimensions.get('window').height;

export default class LoginPage extends Component {
    render(){
        return(
            <>
                {/* <View style={{width: '100%', height: 200, position: 'absolute', bottom: 0}}>
                    <Image resizeMode='cover' source={require('../../Assets/Background1.png')} style={{flex: 1, width: undefined, height: undefined, opacity: 0.5}}/>
                </View> */}
                <View style={{
                        width: '100%',
                        height: '100%',
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: 0
                }}>
                    <Image source={require('../../Assets/Background2a.png')} style={[ {      flex: 1,
                    width: '100%',
                    right: -10,
                    opacity: 0.4}]}/>
                </View>
                <Logo />
                <View style={styles.loginForm}>
                <Text style={styles.signTitle}>Transporter Login</Text>
                <Text style={{fontSize:15, fontStyle:"italic"}}>v{version.version}</Text>
                <LinearGradient
                    colors={["#ffffff", "#ffffff", "rgba(255, 255, 255, 0.1)"]}
                    style={styles.inputBox}
                >
                    <View style={styles.SectionStyle}>
                    <Image
                        source={require("../../Assets/people.png")}
                        style={styles.ImageStyle}
                    />
                    <TextInput
                        style={{ flex: 1 }}
                        underlineColorAndroid="rgba(0,0,0,0)"
                        placeholder="Email | No Handphone"
                        placeholderTextColor="#919191"
                        selectionColor="#1e2427"
                        keyboardType="email-address"
                        value={this.props.paramLogin.username}
                        onChangeText={input => this.props.onInputChange('U',input)}
                    />
                    </View>
                </LinearGradient>
                <LinearGradient
                    colors={["#ffffff", "#ffffff", "rgba(255, 255, 255, 0.1)"]}
                    style={styles.inputBox}
                >
                    <View style={styles.SectionStyle}>
                    <Image
                        source={require("../../Assets/lock.png")}
                        style={styles.ImageStyle}
                    />
                    <TextInput
                        style={{ flex: 1 }}
                        underlineColorAndroid="rgba(0,0,0,0)"
                        placeholder="Password"
                        secureTextEntry={true}
                        placeholderTextColor="#919191"
                        value={this.props.paramLogin.password}
                        onChangeText={input => this.props.onInputChange('P',input)}
                    />
                    </View>
                </LinearGradient>
                <TouchableOpacity style={[styles.button,{borderColor:"#008c45",
                                                shadowOffset: {
                                                    width: 0,
                                                    height: 2,
                                                },
                                                shadowOpacity: 0.1,
                                                shadowRadius: 3.84,                                                
                                                elevation:1
                                                }]} onPress={()=>this.props.onLogin()}>
                    <Text style={styles.buttonText}>MASUK</Text>
                </TouchableOpacity>
                    <View style={{marginBottom: 50}}/>
                </View>
            </>
        );
    }
}