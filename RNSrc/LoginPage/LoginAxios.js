import Axios from 'axios';
import {encrypt} from '../../APIProp/ApiConstants.js';

export const onLoginValidation = async (url, param) =>{
    // var strSql = `
    //     select 
    //         mmu.id as id_user,
    //         mmu.nama,
    //         mmt.id as id_transporter,
    //         mmt.nama_transporter,
    //         mmt.foto_logo,
    //         mmt.kode_transporter,
    //         to_char(mmt.startdate_epod, 'YYYY-MM-DD') startdate_epod,
    //         mmt.skb_isvalid,
    //         mmt.phone as phone_transporter,
    //         mmu.email,
    //         mmu.hp,
    //         mmu.hak_akses, 
    //         epm_decrypt("password") as pass
    //     from 
    //         mt_master_user mmu,
    //         mt_master_transporter mmt
    //     where 
    //         (mmu.email = '`+param.username+`' or mmu.hp = '`+param.username+`') and
    //         mmu.active= 'A' and
    //         mmu.role = 'TRANSPORTER' and
    //         mmu.transporter_id = mmt.id
    // `

    var strSql = `
        select * from mobiletra_onLoginValidationWithLaut('`+param.username+`')
    `
    // alert('strSQL'+ strSql)
    // console.log('param login', param)
    let x = await encrypt(strSql);
    var result = await Axios.post(url.select,{
        query: x
    }).then(value=>{
        return value;
    }).catch(err=>{
        return null;
    });
    return result;
}

export const insertTokenFcm = async (url, id_user, token) =>{
    // var strSql = `
    //     insert into mt_fcm_token (user_id, token_fcm, create_date, create_by, update_date, update_by)
    //     values (`+id_user+`, '`+token+`', now(), `+id_user+`, now(), `+id_user+`)
    // `

    var strSql = `
        select * from mobiletra_inserttokenfcm(`+id_user+`, '`+token+`')
      `
    console.log('query insert token fcm : ', strSql)
    let x = await encrypt(strSql);
    var result = await Axios.post(url.select,{
        query: x
    }).then(value=>{
        console.log('sukses token')
        return value;
    }).catch(err=>{
        console.log('gagal token', err)
        return null;
    });
    return result;
}

export const deleteTokenFcm = async (url, id_user, token) =>{
    // var strSql = `
    //     DELETE FROM mt_fcm_token 
    //     WHERE user_id = `+id_user+`
    //     and token_fcm = '`+token+`';
    // `

    var strSql = `
        select * from mobiletra_deletetokenfcm(`+id_user+`, '`+token+`')
    `
    console.log('delete token : ', strSql)
    let x = await encrypt(strSql);
    var result = await Axios.post(url.select,{
        query: x
    }).then(value=>{
        console.log('sukses delete token')
        return value;
    }).catch(err=>{
        console.log('gagal delete token', err)
        return null;
    });
    return result;
}