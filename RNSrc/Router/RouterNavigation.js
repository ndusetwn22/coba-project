import React from 'react'
import { setUrl, getUrl, version, checkVersion} from '../../APIProp/ApiConstants.js';

import { NavigationContainer  } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { useNavigation  } from '@react-navigation/native';


import Splash from '../SplashScreen/Splash.js'
import Login from '../LoginPage/Login.js'
import {MainRouter} from '../MainPage/MainRouter'


import { Text } from 'native-base';
import {BurgerMenu} from '../MainPage/Component/BurgerMenu'

const Stack = createStackNavigator();

class Routes extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          curPage:''
        };
    }
    componentDidMount=async()=>{
        await setUrl();
        await getUrl().then(result=>{
            url =result;
        });
        await checkVersion(url,version.version,version.app);
        // console.log('router', this.props)
    }

    changeHeaderTitle=(title)=>{
        this.setState({curPage:title})
    }

    render(){
        return(
            <NavigationContainer>
                <Stack.Navigator initialRouteName="Splash">
                    <Stack.Screen options={{headerShown: false}} name="Splash" component={Splash} />
                    <Stack.Screen options={{headerShown: false}} name="Login" component={Login} />
                    <Stack.Screen options={{
                        headerShown: false,
                        headerTitle:props =><Text>{this.state.curPage}</Text>,
                        headerLeft: props => <BurgerMenu {...props} />
                    }} name="MainRouter">
                        {props => <MainRouter changeHeaderTitle={this.changeHeaderTitle}/>}
                    </Stack.Screen>
                </Stack.Navigator>
                {/* <MyDrawer/> */}
            </NavigationContainer>
        )
    }
   
}

  
  
export default Routes