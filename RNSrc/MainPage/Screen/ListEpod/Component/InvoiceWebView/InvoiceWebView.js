import React,{Component} from 'react'
import {
    View,
    CheckBox,
    Image, 
    TextInput,
    ScrollView,
    FlatList,
    TouchableHighlight
} from "react-native";
import {
    Text,  
    Card,
    CardItem,
    Body,
    Left,
    Right,
    Button,
    Icon,
    Thumbnail,
    Content,
} from 'native-base'
import { WebView } from 'react-native-webview';
import RNHTMLtoPDF from 'react-native-html-to-pdf';




class InvoiceWebView extends Component {
    constructor(props) {
        super(props);
        this.state = {
          myUrl:'',
        };
      }
    componentDidMount= async()=>{
        // console.log('value', this.props.value)
        // this.props.funcSweetAlert('status', 'Sukses download PDF', 'success')
        await this.setState({myUrl: this.props.urlWebView})
        console.log('propsurl webview', this.props.urlWebView)

    } 

    onNavigationStateChange(navState) {
        this.setState({
          height: navState.title,
        });
      } 

      async createPDF(urlSaya) {
        //   console.log('urlweb', this.props.urlWebView)
        console.log('url saya', urlSaya)
        // var my_url = `<iframe width="100%" height="100%" src=`+urlSaya+` frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>`
        var my_url = urlSaya
        let options = {
            html: my_url,
            fileName: 'test153',
            directory: 'Documents',
        };

        let file = await RNHTMLtoPDF.convert(options)
        // console.log(file.filePath);
        alert(file.filePath);
    }

    render(){
        
        var html = '<!DOCTYPE html><html><body>' + 
                    '<h1>JUDUL</h1><p> paragraph </p>' + 
                    '<script>window.location.hash = 1;document.title = document.height;</script></body></html>';

        var html_2 = `<!-- #######  YAY, I AM THE SOURCE EDITOR! #########-->
        <h1 style="color: #5e9ca0;">You can edit <span style="color: #2b2301;">this demo</span> text!</h1>
        <h2 style="color: #2e6c80;">How to use the editor:</h2>
        <p>Paste your documents in the visual editor on the left or your HTML code in the source editor in the right. <br />Edit any of the two areas and see the other changing in real time.&nbsp;</p>
        <p>Click the <span style="background-color: #2b2301; color: #fff; display: inline-block; padding: 3px 10px; font-weight: bold; border-radius: 5px;">Clean</span> button to clean your source code.</p>
        <h2 style="color: #2e6c80;">Some useful features:</h2>
        <ol style="list-style: none; font-size: 14px; line-height: 32px; font-weight: bold;">
        <li style="clear: both;"><img style="float: left;" src="https://html-online.com/img/01-interactive-connection.png" alt="interactive connection" width="45" /> Interactive source editor</li>
        <li style="clear: both;"><img style="float: left;" src="https://html-online.com/img/02-html-clean.png" alt="html cleaner" width="45" /> HTML Cleaning</li>
        <li style="clear: both;"><img style="float: left;" src="https://html-online.com/img/03-docs-to-html.png" alt="Word to html" width="45" /> Word to HTML conversion</li>
        <li style="clear: both;"><img style="float: left;" src="https://html-online.com/img/04-replace.png" alt="replace text" width="45" /> Find and Replace</li>
        <li style="clear: both;"><img style="float: left;" src="https://html-online.com/img/05-gibberish.png" alt="gibberish" width="45" /> Lorem-Ipsum generator</li>
        <li style="clear: both;"><img style="float: left;" src="https://html-online.com/img/6-table-div-html.png" alt="html table div" width="45" /> Table to DIV conversion</li>
        </ol>
        <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
        <h2 style="color: #2e6c80;">Cleaning options:</h2>
        <table class="editorDemoTable">
        <thead>
        <tr>
        <td>Name of the feature</td>
        <td>Example</td>
        <td>Default</td>
        </tr>
        </thead>
        <tbody>
        <tr>
        <td>Remove tag attributes</td>
        <td><img style="margin: 1px 15px;" src="images/smiley.png" alt="laughing" width="40" height="16" /> (except <strong>img</strong>-<em>src</em> and <strong>a</strong>-<em>href</em>)</td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td>Remove inline styles</td>
        <td><span style="color: green; font-size: 13px;">You <strong style="color: blue; text-decoration: underline;">should never</strong>&nbsp;use inline styles!</span></td>
        <td><strong style="font-size: 17px; color: #2b2301;">x</strong></td>
        </tr>
        <tr>
        <td>Remove classes and IDs</td>
        <td><span id="demoId">Use classes to <strong class="demoClass">style everything</strong>.</span></td>
        <td><strong style="font-size: 17px; color: #2b2301;">x</strong></td>
        </tr>
        <tr>
        <td>Remove all tags</td>
        <td>This leaves <strong style="color: blue;">only the plain</strong> <em>text</em>. <img style="margin: 1px;" src="images/smiley.png" alt="laughing" width="16" height="16" /></td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td>Remove successive &amp;nbsp;s</td>
        <td>Never use non-breaking spaces&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;to set margins.</td>
        <td><strong style="font-size: 17px; color: #2b2301;">x</strong></td>
        </tr>
        <tr>
        <td>Remove empty tags</td>
        <td>Empty tags should go!</td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td>Remove tags with one &amp;nbsp;</td>
        <td>This makes&nbsp;no sense!</td>
        <td><strong style="font-size: 17px; color: #2b2301;">x</strong></td>
        </tr>
        <tr>
        <td>Remove span tags</td>
        <td>Span tags with <span style="color: green; font-size: 13px;">all styles</span></td>
        <td><strong style="font-size: 17px; color: #2b2301;">x</strong></td>
        </tr>
        <tr>
        <td>Remove images</td>
        <td>I am an image: <img src="images/smiley.png" alt="laughing" /></td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td>Remove links</td>
        <td><a href="https://html-online.com">This is</a> a link.</td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td>Remove tables</td>
        <td>Takes everything out of the table.</td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td>Replace table tags with structured divs</td>
        <td>This text is inside a table.</td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td>Remove comments</td>
        <td>This is only visible in the source editor <!-- HELLO! --></td>
        <td><strong style="font-size: 17px; color: #2b2301;">x</strong></td>
        </tr>
        <tr>
        <td>Encode special characters</td>
        <td><span style="color: red; font-size: 17px;">&hearts;</span> <strong style="font-size: 20px;">☺ ★</strong> &gt;&lt;</td>
        <td><strong style="font-size: 17px; color: #2b2301;">x</strong></td>
        </tr>
        <tr>
        <td>Set new lines and text indents</td>
        <td>Organize the tags in a nice tree view.</td>
        <td>&nbsp;</td>
        </tr>
        </tbody>
        </table>
        <p><strong>&nbsp;</strong></p>
        <p><strong>Save this link into your bookmarks and share it with your friends. It is all FREE! </strong><br /><strong>Enjoy!</strong></p>
        <p><strong>&nbsp;</strong></p>`

        var html_3 = `<div id="tableform" style="width:21cm;margin:auto;padding:1rem;">
        <div class="row p-1" style="text-align:left">
            @<input id="urlFoto" value="@m.foto_logo" style="display:none;" />@
            <img style="width:15%" id="fotologo" />
        </div>
        <br />
        <br />
        <table>
            <tr>
                <td style="width:95%">
                    <div class="namaTrans">@m.nama_trans</div>
                    <div class="alamatTrans col-6 p-0">@m.alamat</div>
                </td>
                <td style="width:5%;">
                    <h1>INVOICE</h1>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <table>
            <tr>
                <td style="width:50%">
                    <label>No Pelanggan</label>
                    <input value="@m.kode" name="nopel" type="text" id="nopel" class="form-control">
                </td>
                <td style="width:50%">
                    <label>No Invoice</label>
                    <input value="@m.no_invoice" name="noinv" type="text" id="noinv" class="form-control">
                </td>
            </tr>
            <tr>
                <td style="width:50%">
                    <label>Kode & No. Seri Faktur Pajak</label>
                    <input value="@m.nofaktur" name="nofaktur" type="text" id="nofaktur" class="form-control">
                </td>
                <td style="width:50%">
                    <label>Kode & No. Seri Faktur Pajak yang Diganti</label>
                    <input value="@m.nofaktur2" name="nofaktur2" type="text" id="nofaktur2" class="form-control">
                </td>
            </tr>
            <tr>
                <td>
                    <label>Pemesan</label><br />
                    PT Enseval Putera Megatrading Tbk.
                    <div class=" col-6 p-0">Jl. Pulo Lentut No. 10 Kawasan Industri Pulo Gadung, Jakarta Timur</div>
                </td>
                <td>
                    <label>Pembayar</label><br />
                    PT Enseval Putera Megatrading Tbk.
                    <div class=" col-6 p-0">Jl. Pulo Lentut No. 10 Kawasan Industri Pulo Gadung, Jakarta Timur</div>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Pembeli Barang Kena Pajak</label><br />
                    PT Enseval Putera Megatrading Tbk.
                    <div class=" col-6 p-0">Jl. Pulo Lentut No. 10 Kawasan Industri Pulo Gadung, Jakarta Timur</div>
                </td>
                <td>
                    <label>Pengusaha Kena Pajak</label><br />
                    PT Enseval Putera Megatrading Tbk.
                    <div class=" col-6 p-0">Jl. Pulo Lentut No. 10 Kawasan Industri Pulo Gadung, Jakarta Timur</div>
                </td>
            </tr>
        </table>

        <table style="width:100%;text-align:center" class="table table-striped">
            <thead>
                <tr class="trborder">
                    <td style="width:5%"><label>No.</label></td>
                    <td style="width:20%"><label>Keterangan</label></td>
                    <td style="width:15%"><label>Biaya Trip</label></td>
                    <td style="width:40%"><label>Biaya Lain-Lain</label></td>
                    <td style="width:5%"><label>Jumlah Order</label></td>
                    <td style="width:15%"><label>Harga</label></td>
                </tr>
            </thead>

            <tbody id="invoicetable">
                @{
                    int idx = 1;
                    foreach (var j in m.ListTripInvoice)
                    {
                        <tr>
                            <td style="width:5%"><label>@idx</label></td>
                            <td style="width:20%"><label>@j.id_trip</label></td>
                            <td style="width:15%;text-align:right;"><label id="btrip">@j.biaya_trip</label></td>
                            <td style="width:15%;text-align:right;"><label id="blain">@j.biaya_lain</label></td>
                            <td style="width:5%"><label>@j.jum_order</label></td>
                            <td style="width:10%;text-align:right;"><label id="btriptot">@j.biaya_triptot</label></td>
                        </tr>
                    }
                    idx++; }
            </tbody>
        </table>
        <table style="width:100%;text-align:right;" class="table table-striped">
            <tr>
                <td style="width:70%;"></td>
                <td style="width:20%;text-align:left">
                    <label>Jumlah Total (Rp.)</label>
                </td>
                <td style="width:10%;text-align:right;" id="btot">@m.total</td>
            </tr>
        </table>
        <table style="width:100%;" class="table table-striped">
            <tr>
                <td style="width:70%;"></td>
                <td style="width:20%;text-align:left">
                    <label>PPN (Rp.)</label>
                </td>
                <td style="width:10%;text-align:right" id="bppn">@m.ppn</td>
            </tr>
        </table>
        <table style="width:100%;" class="table table-striped">
            <tr>
                <td style="width:70%;"></td>
                <td style="width:20%;text-align:left">
                    <label>Grand Total (Rp.)</label>
                </td>
                <td style="width:10%;text-align:right" id="bgrandtot">@m.grandtotal</td>
            </tr>
        </table>
        <div class="row p-0">
            * Kwitansi diterbitkan secara komputerisasi
        </div>
    </div>`


        var test_link = 'https://sandboxdev.enseval.com/transporter/MInvoiceEpod?param=eyJJZFRyYW5zIjoiMiIsIklkVXNlciI6IjE4IiwiS29kZVRyYW5zcG9ydGVyIjoiSVRTIiwiRm90b0xvZ28iOiJodHRwczovL2ZpcmViYXNlc3RvcmFnZS5nb29nbGVhcGlzLmNvbS92MC9iL21vc3RyYW5zLXN0b3JhZ2UuYXBwc3BvdC5jb20vby9Gb3RvVHJhbnNwb3J0ZXIlMkYyJTJGQXNzZXQlMjAxbWRwaS5wbmc/YWx0PW1lZGlhJnRva2VuPThjYmJkNjdkLTQzYzktNGEwZS05ZjA5LTIxOTgwODQ1MjU2MiIsIkFsYW1hdFRyYW5zIjoiSmwuIFB1bG8gTGVudHV0LCBSVC4yL1JXLjEsIFJhd2EgVGVyYXRlLCBFYXN0IEpha2FydGEgQ2l0eSwgSmFrYXJ0YSwgSW5kb25lc2lhIiwiTmFtYVRyYW5zIjoiUFQgSU5UUkFOUyIsIk5vSW52b2ljZSI6IklOVi1JVFMyMDIwMDgxMjExMjEiLCJGYWt0dXJQYWphayI6IkpndWdmdWZ1ZyIsIkZha3R1clBhamFrRGlnYW50aSI6IiIsIlRyaXAiOlt7Ik5vbW9yVHJpcCI6IjY3IiwiQmlheWFUcmlwIjowLCJCaWF5YUxhaW4iOjAsIkJpYXlhS3VsaSI6MCwiQmlheWFJbmFwIjowLCJKdW1PcmRlciI6IjEifV0sInVybEZha3R1ciI6Imh0dHBzOi8vZmlyZWJhc2VzdG9yYWdlLmdvb2dsZWFwaXMuY29tL3YwL2IvbW9zdHJhbnMtZGV2LmFwcHNwb3QuY29tL28vSU5WLUlUUzIwMjAwODEyMTEyMSUyRkludm9pY2VFcG9kMS5wZGY/YWx0PW1lZGlhJnRva2VuPWYyNTc4NTc0LTg2NmYtNGFhOS04MjNjLTZmN2I4MTdhYmJlZCIsIlRvdGFsIjowLCJQUE4iOjB9'

        // var dummy = '<iframe width="100%" height="100%" src="https://sandboxdev.enseval.com/transporter/MInvoiceEpod?param=eyJJZFRyYW5zIjoiMiIsIklkVXNlciI6IjE4IiwiS29kZVRyYW5zcG9ydGVyIjoiSVRTIiwiRm90b0xvZ28iOiJodHRwczovL2ZpcmViYXNlc3RvcmFnZS5nb29nbGVhcGlzLmNvbS92MC9iL21vc3RyYW5zLXN0b3JhZ2UuYXBwc3BvdC5jb20vby9Gb3RvVHJhbnNwb3J0ZXIlMkYyJTJGQXNzZXQlMjAxbWRwaS5wbmc/YWx0PW1lZGlhJnRva2VuPThjYmJkNjdkLTQzYzktNGEwZS05ZjA5LTIxOTgwODQ1MjU2MiIsIkFsYW1hdFRyYW5zIjoiSmwuIFB1bG8gTGVudHV0LCBSVC4yL1JXLjEsIFJhd2EgVGVyYXRlLCBFYXN0IEpha2FydGEgQ2l0eSwgSmFrYXJ0YSwgSW5kb25lc2lhIiwiTmFtYVRyYW5zIjoiUFQgSU5UUkFOUyIsIk5vSW52b2ljZSI6IklOVi1JVFMyMDIwMDgxMjExMjEiLCJGYWt0dXJQYWphayI6IkpndWdmdWZ1ZyIsIkZha3R1clBhamFrRGlnYW50aSI6IiIsIlRyaXAiOlt7Ik5vbW9yVHJpcCI6IjY3IiwiQmlheWFUcmlwIjowLCJCaWF5YUxhaW4iOjAsIkJpYXlhS3VsaSI6MCwiQmlheWFJbmFwIjowLCJKdW1PcmRlciI6IjEifV0sInVybEZha3R1ciI6Imh0dHBzOi8vZmlyZWJhc2VzdG9yYWdlLmdvb2dsZWFwaXMuY29tL3YwL2IvbW9zdHJhbnMtZGV2LmFwcHNwb3QuY29tL28vSU5WLUlUUzIwMjAwODEyMTEyMSUyRkludm9pY2VFcG9kMS5wZGY/YWx0PW1lZGlhJnRva2VuPWYyNTc4NTc0LTg2NmYtNGFhOS04MjNjLTZmN2I4MTdhYmJlZCIsIlRvdGFsIjowLCJQUE4iOjB9" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'
        var my_url = `<iframe width="100%" height="100%" src=`+this.props.urlWebView+` frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>`
        // my_url = `<iframe src=`+this.props.urlWebView+` title="title">
        //                 Presss me: <a href=`+this.props.urlWebView+`>Download PDF</a>
        //             </iframe>`
        var my_url_second = this.props.urlWebView
        return(
            <>
            
                <WebView 
                // source={{ uri: this.props.urlWebView }} 
                // automaticallyAdjustContentInsets={false}
                // scrollEnabled={false}
                // onNavigationStateChange={this.onNavigationStateChange.bind(this)}
                // html={html}
                // style={{resizeMode: 'contain', resizeMethod: 'scale', flex: 1 }}
                // style={{flex:1 , resizeMode: 'cover'}}
                // automaticallyAdjustContentInsets={false}
                originWhitelist={['*']}
                source={{uri: my_url}}
                // source={{ uri: test_link }}
                />
                {/* <Text  onPress={this.createPDF(html_3)}>Testtingg</Text> */}
            </>
        );
    }
}

export default InvoiceWebView;