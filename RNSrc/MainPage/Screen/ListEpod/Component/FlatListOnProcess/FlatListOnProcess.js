import React,{Component} from 'react'
import {Text, View,Icon, Content, Button} from 'native-base'
import {
    TouchableOpacity,
    ScrollView,FlatList, RefreshControl, ToastAndroid
} from "react-native";
import styles from './style'
import CardOnProcess from './CardOnProcess/CardOnProcess'
import SearchInput,{ createFilter } from 'react-native-search-filter';
import { getListOnProcess, getListPod, getListInvoice, getListBayar, getListReject } from '../../ListEpodAxios';
const KEYS_TO_FILTERS = ['id_trip', 'asal', 'tujuan', 'harga', 'biaya_kuli', 'kendaraan', 'tanggal_pengiriman', 'jam_pengiriman', 'status'];

class FlatListOnProcess extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paramSearch:'',
            refreshing: false,
        };
    }

    componentDidMount= async()=>{
        // console.log('props', this.props)
    } 

    onRefresh= async() => {
        this.setState({refreshing: true})

        var _listOnProcess = await getListOnProcess(this.props.url, this.props.profile.id_transporter, this.props.profile.id_user, this.props.startDate, this.props.endDate);
        var _listPod = await getListPod(this.props.url, this.props.profile.id_transporter, this.props.profile.id_user, this.props.startDate, this.props.endDate, this.props.profile.startdate_epod);
        var _listInvoice = await getListInvoice(this.props.url, this.props.profile.id_transporter, this.props.profile.id_user, this.props.startDate, this.props.endDate);
        var _listBayar = await getListBayar(this.props.url, this.props.profile.id_transporter, this.props.profile.id_user, this.props.startDate, this.props.endDate);
        var _listReject = await getListReject(this.props.url, this.props.profile.id_transporter, this.props.profile.id_user, this.props.startDate, this.props.endDate);
  
        await this.props.refreshContent(_listOnProcess, _listPod, _listInvoice, _listBayar, _listReject)
  
  
          ToastAndroid.show('Refreshing', ToastAndroid.SHORT);
          this.setState({refreshing: false})
  
      };

    render(){
        // console.log('list user', this.props.profile)
        const filteredOnProcess = this.props.listOnProcess.filter(createFilter(this.state.paramSearch, KEYS_TO_FILTERS))
        const hak_akses = this.props.hak_akses
        return(
            <>
                
                    <View style={{flexDirection: 'row', backgroundColor: 'white', top: 1}}>
                        <SearchInput 
                            onChangeText={(term) => { this.setState({paramSearch:term}) }} 
                            style={{
                                paddingLeft:10,
                                paddingTop:5,
                                marginTop: 10,
                                marginLeft:10,
                                marginRight:10,
                                marginBottom:5,
                                paddingBottom:5,
                                borderColor: '#008c45',
                                borderWidth: 1,
                                backgroundColor:"white",
                                borderRadius: 10,
                                width: '350%',
                            }}
                            placeholder="Search ... "
                            inputFocus={true}
                            removeClippedSubviews={true} // Unmount components when outside of window 
                            initialNumToRender={2} // Reduce initial render amount
                            maxToRenderPerBatch={1} // Reduce number in each render batch
                            maxToRenderPerBatch={10} // Increase time between renders
                            windowSize={7} // Reduce the window size
                        />
                    </View>
                <Content
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this.onRefresh.bind(this)}
                        />}
                >
                    <ScrollView>
                            <FlatList
                                data={
                                    filteredOnProcess
                                }
                                style={{marginBottom:10}}
                                renderItem={({ item: value, index }) => {
                                    return (
                                        <CardOnProcess {...this.props} index={index} value={value} />
                                    );
                                }}
                                keyExtractor={(item, index) => index.toString()}
                            /> 
                    </ScrollView>
                </Content>
            </>
        );
    }
}

export default FlatListOnProcess;