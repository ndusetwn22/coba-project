import React,{Component} from 'react'
import {Text, View,Icon, Content, Button} from 'native-base'
import {
    TouchableOpacity,
    ScrollView,FlatList,
    Alert, RefreshControl, ToastAndroid
} from "react-native";
import styles from './style'
import CardPod from './CardPod/CardPod'
import SearchInput,{ createFilter } from 'react-native-search-filter';
import ModalUploadFaktur from './ModalUploadFaktur/ModalUploadFaktur';
import { getListOnProcess, getListPod, getListInvoice, getListBayar, getListReject } from '../../ListEpodAxios';
const KEYS_TO_FILTERS = ['id_trip', 'asal', 'tujuan', 'harga', 'biaya_kuli', 'jenis_kendaraan', 'status'];
import DocumentPicker from 'react-native-document-picker';
import storage from '@react-native-firebase/storage';
import moment from 'moment'




class FlatListPod extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paramSearch:'',
            refreshing: false,
            podList: this.props.listPod,
            modalUploadFakturVisible: false,
            documentFileNameFaktur: 'Pilih Dokumen Faktur',
            documentSourceFaktur: null,
            urlFaktur: '',
            kodeTransporter_: '',
            noInvoice_: '',
            cekSkb: '',

        };
    }

    componentDidMount= async()=>{

        // console.log('id transporter', this.props.profile.skb_isvalid)

        await this.setState({
            podList: this.props.listPod
        })

    } 

    onRefresh= async() => {
        this.setState({refreshing: true})

        var _listOnProcess = await getListOnProcess(this.props.url, this.props.profile.id_transporter, this.props.profile.id_user, this.props.startDate, this.props.endDate);
        var _listPod = await getListPod(this.props.url, this.props.profile.id_transporter, this.props.profile.id_user, this.props.startDate, this.props.endDate, this.props.profile.startdate_epod);
        var _listInvoice = await getListInvoice(this.props.url, this.props.profile.id_transporter, this.props.profile.id_user, this.props.startDate, this.props.endDate);
        var _listBayar = await getListBayar(this.props.url, this.props.profile.id_transporter, this.props.profile.id_user, this.props.startDate, this.props.endDate);
        var _listReject = await getListReject(this.props.url, this.props.profile.id_transporter, this.props.profile.id_user, this.props.startDate, this.props.endDate);
  
        await this.props.refreshContent(_listOnProcess, _listPod, _listInvoice, _listBayar, _listReject)
  
  
          ToastAndroid.show('Refreshing', ToastAndroid.SHORT);
          this.setState({refreshing: false})
  
      };

    prosesInvoice=async()=>{
        let count=0;
        let listTrip=[];
        let total=0;
        let datacheckout=[];
        for(let i=0;i<this.state.podList.length;i++){
            //ngecek setiap data apakah ceklist atau tidak
          if(this.state.podList[i].isChecked == true) {
            count = +count + +1;
            //bikin list trip untuk masuk ke invoice, ada nomor, keterangan, biaya trip, biaya lain" (kuli(muat+bongkar) + biaya inap(tambah))
            // console.log('udh dipilih', count)
            
            // console.log('datanya', this.state.podList[i])
            let x={
                "No": count,
                "id_trip": this.state.podList[i].id_trip,
                "biaya_trip": parseInt(this.state.podList[i].harga),
                "biaya_kuli": this.state.podList[i].biaya_kuli,
                "biaya_inap": 0,
                "jum_order" : this.state.podList[i].jumlah_order,
              }
            //   total = +total + +this.state.dataList[i].amount_due_remaining;
              listTrip.push(x);
          }
        }

        if(count==0) {
            this.props.funcSweetAlert('status', 'Anda belum memilih trip!','warning')
        }
        else{
            //do something
            console.log('hasil push', listTrip)
            // await this.setState({modalUploadFakturVisible: !this.state.modalUploadFakturVisible})

            await this.lanjutBuatInvoiceNew()
            // ganti disini --> revisi upload faktur

            
            //lanjut ke page ini kalo udh sukses upload faktur
            // this.props.pageBuatInvoice(listTrip)
        }
      }

      toggleModalFaktur = async() => {
        this.setState({modalUploadFakturVisible: !this.state.modalUploadFakturVisible, documentFileNameFaktur: 'Pilih Dokumen Faktur', documentSourceFaktur: null, urlFaktur: ''});
      };

      pilihDokumenFaktur = async() => {
          try {
              const res = await DocumentPicker.pick({
              type: [DocumentPicker.types.pdf],
              });
              await this.setState({documentFileNameFaktur: res.name, documentSourceFaktur: res.uri})
              console.log(
              res.uri,
              res.type, // mime type
              res.name,
              res.size
              );
          } catch (err) {
              if (DocumentPicker.isCancel(err)) {
              // User cancelled the picker, exit any dialogs or menus and move on
              } else {
              this.props.funcSweetAlert('status', 'Failed upload Faktur','warning')
              throw err;
              }
          }

    }

    //ini salah, liat dari invoiceview
    funcCombinedUploadFaktur = async() => {
        
        var namaDokumen = this.state.documentFileNameFaktur
        var uriDokumen = this.state.documentSourceFaktur

        if (namaDokumen != 'Pilih Dokumen Faktur' && uriDokumen != null) {

            //generate no invoice(24jam)--> utk ref folder firebase -->
            let tgl_invoice = moment().format('YYYYMMDDHHmm')
            let no_invoice = 'INV-'+this.props.profile.kode_transporter+tgl_invoice
            // this.setState({noInvoice_: no_invoice, kodeTransporter_: this.props.profile.kode_transporter, cekSkb: this.props.profile.skb_isvalid})
            this.setState({noInvoice_: no_invoice})
            //   insertFoto
            const FireBaseStorage = storage();
            //Insert firebase
            const documentSource = uriDokumen
            //pake ini untuk bikin folder ref
            const documentStorageRef = FireBaseStorage.ref(`${no_invoice}/${namaDokumen}`)

            Promise.resolve(documentStorageRef.putFile(documentSource))
                .then((data)=>{
                    Promise.resolve(documentStorageRef.getDownloadURL())
                        .then((url)=> {
                            console.log('URL STORAGE', url)
                            this.setState({urlFaktur: url})
                        }).catch((error)=>{
                            throw error
                        })
                    this.props.funcSweetAlert('Status', 'Success Upload Faktur, \n Silahkan Klik Lanjut' , 'success')
                }).catch((error) => {   
                    console.log('catch error', error)          
                    this.props.funcSweetAlert('Status', 'Kesalahan Upload Faktur', 'warning')
            })


            // this.props.funcSweetAlert('Status', 'Success Upload Faktur, \n Silahkan Klik Lanjut' , 'success')
        }else{
            this.props.funcSweetAlert('Status', 'Failed Upload Faktur, Harap Pilih File Faktur Dahulu', 'warning')
        }        
        // this.setState({modalUploadFakturVisible: !this.state.modalUploadFakturVisible, documentFileNameFaktur: 'Pilih Dokumen Faktur', documentSourceFaktur: null});
    }

    lanjutBuatInvoice = async() => {
        if (this.state.urlFaktur == "") {
            this.props.funcSweetAlert('Status', 'Gagal proses lanjut, \n harap upload faktur dahulu', 'warning')
        }else{
            let count=0;
            let listTrip=[];
            let total=0;
            let datacheckout=[];
            for(let i=0;i<this.state.podList.length;i++){
                //ngecek setiap data apakah ceklist atau tidak
                if(this.state.podList[i].isChecked == true) {
                    count = +count + +1;
                    //bikin list trip untuk masuk ke invoice, ada nomor, keterangan, biaya trip, biaya lain" (kuli(muat+bongkar) + biaya inap(tambah))
                    // console.log('udh dipilih', count)
                    
                    // console.log('datanya', this.state.podList[i])
                    let x={
                        // "No": count,
                        "NomorTrip" : this.state.podList[i].id_trip,
                        "BiayaTrip" : parseInt(this.state.podList[i].harga),
                        "BiayaLain" : 0,
                        "BiayaKuli" : parseInt(this.state.podList[i].biaya_kuli),
                        "BiayaInap" : 0,
                        "JumOrder"  : this.state.podList[i].jumlah_order,
                        "IdTrip"    : this.state.podList[i].id,
                        // "id_trip": this.state.podList[i].id_trip,
                        // "biaya_trip": parseInt(this.state.podList[i].harga),
                        // "biaya_kuli": this.state.podList[i].biaya_kuli,
                        // "biaya_inap": 0,
                        // "jum_order" : this.state.podList[i].jumlah_order,
                    }
                    //   total = +total + +this.state.dataList[i].amount_due_remaining;
                    listTrip.push(x);
                }
            }

            if(count==0) {
                this.props.funcSweetAlert('status', 'Anda belum memilih trip!','warning')
            }
            else{
                this.setState({modalUploadFakturVisible: !this.state.modalUploadFakturVisible, documentFileNameFaktur: 'Pilih Dokumen Faktur', documentSourceFaktur: null, urlFaktur: ''});
                this.props.pageBuatInvoice(listTrip, this.state.noInvoice_, this.props.profile.kode_transporter, this.props.profile.skb_isvalid, this.state.urlFaktur)
            }
        }
    }

    lanjutBuatInvoiceNew = async() => {
            //generate no invoice(24jam)--> utk ref folder firebase -->
            let tgl_invoice = moment().format('YYYYMMDDHHmm')
            let no_invoice = 'INV-'+this.props.profile.kode_transporter+tgl_invoice
            // this.setState({noInvoice_: no_invoice, kodeTransporter_: this.props.profile.kode_transporter, cekSkb: this.props.profile.skb_isvalid})
            await this.setState({noInvoice_: no_invoice})

            let count=0;
            let listTrip=[];
            let total=0;
            let datacheckout=[];
            for(let i=0;i<this.state.podList.length;i++){
                if(this.state.podList[i].isChecked == true) {
                    count = +count + +1;
                    let x={
                        "NomorTrip" : this.state.podList[i].id_trip,
                        "BiayaTrip" : parseInt(this.state.podList[i].harga),
                        "BiayaLain" : 0,
                        "BiayaKuli" : parseInt(this.state.podList[i].biaya_kuli),
                        "BiayaInap" : 0,
                        "JumOrder"  : this.state.podList[i].jumlah_order,
                        "IdTrip"    : this.state.podList[i].id,
                        // "id_trip": this.state.podList[i].id_trip,
                        // "biaya_trip": parseInt(this.state.podList[i].harga),
                        // "biaya_kuli": this.state.podList[i].biaya_kuli,
                        // "biaya_inap": 0,
                        // "jum_order" : this.state.podList[i].jumlah_order,
                    }
                    //   total = +total + +this.state.dataList[i].amount_due_remaining;
                    listTrip.push(x);
                }
            }
            if(count==0) {
                this.props.funcSweetAlert('status', 'Anda belum memilih trip!','warning')
            }
            else{
                this.setState({documentFileNameFaktur: 'Pilih Dokumen Faktur', documentSourceFaktur: null, urlFaktur: ''});
                this.props.pageBuatInvoice(listTrip, this.state.noInvoice_, this.props.profile.kode_transporter, this.props.profile.skb_isvalid, this.state.urlFaktur)
            }
    }


    render(){
        // console.log('list user', this.props.profile)
        const filteredPod = this.props.listPod.filter(createFilter(this.state.paramSearch, KEYS_TO_FILTERS))
        const hak_akses = this.props.hak_akses
        return(
            <>
                
                    <View style={{flexDirection: 'row', backgroundColor: 'white', top: 1}}>
                        <SearchInput 
                            onChangeText={(term) => { this.setState({paramSearch:term}) }} 
                            style={{
                                paddingLeft:10,
                                paddingTop:5,
                                marginTop: 10,
                                marginLeft:10,
                                marginRight:10,
                                marginBottom:5,
                                paddingBottom:5,
                                borderColor: '#008c45',
                                borderWidth: 1,
                                backgroundColor:"white",
                                borderRadius: 10,
                                width: '350%',
                            }}
                            placeholder="Search ... "
                            inputFocus={true}
                            removeClippedSubviews={true} // Unmount components when outside of window 
                            initialNumToRender={2} // Reduce initial render amount
                            maxToRenderPerBatch={1} // Reduce number in each render batch
                            maxToRenderPerBatch={10} // Increase time between renders
                            windowSize={7} // Reduce the window size
                        />
                    </View>
                <Content
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this.onRefresh.bind(this)}
                        />}
                >
                    <ScrollView>
                            <FlatList
                                data={
                                    filteredPod
                                }
                                style={{marginBottom:10}}
                                renderItem={({ item: value, index }) => {
                                    return (
                                        <CardPod {...this.props} index={index} value={value} gantiValue={this.props.gantiValue}/>
                                    );
                                }}
                                keyExtractor={(item, index) => index.toString()}
                            /> 
                    </ScrollView>
                </Content>

                <TouchableOpacity onPress={this.prosesInvoice}>
                    <View style={styles.flexGridNoPadding}>
                        <View style={styles.labelPriceBoxStyle}>
                            <View style={{backgroundColor: '#E2ECFF', height: 40, width: 40, borderRadius: 30, alignItems:'center', alignSelf: 'center', justifyContent: 'center'}}>
                                <Icon type='FontAwesome5' name='file-invoice' style={{color: '#719DF0', fontSize: 20}}/>
                            </View>
                            <View style={{marginLeft: 10}}>
                                <Text style={{fontSize: 12, textAlign: 'center', marginTop: 0, color: '#7d8d8c'}}>Buat Invoice</Text>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
                <ModalUploadFaktur {...this.state} {...this.props} 
                                toggleModalFaktur={this.toggleModalFaktur} 
                                pilihDokumenFaktur={this.pilihDokumenFaktur} 
                                funcCombinedUploadFaktur={this.funcCombinedUploadFaktur} 
                                lanjutBuatInvoice={this.lanjutBuatInvoice}>
                    </ModalUploadFaktur>                


                {/* {this.state.isLoading ? <Loading/> : false} */}

            </>
        );
    }
}

export default FlatListPod;