import React,{Component} from 'react'
import {
    View,
    CheckBox
} from "react-native";
import styles from './style'
import {
    Text,  
    Card,
    CardItem,
    Body,
    Left,
    Right,
    Button,
    Icon,
    Thumbnail
} from 'native-base'
import { TouchableOpacity } from 'react-native-gesture-handler';

class CardPod extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount= async()=>{

    } 

    numberWithCommas=(x)=>{
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      }

    render(){
        // console.log('profile di card', this.props.profile.hak_akses)
        const hak_akses = this.props.hak_akses
        return(
            <Card style={{marginLeft: 10, marginRight: 10}}>
                <CardItem>
                    <Body>
                    <CheckBox   value={this.props.value.isChecked}
                                onValueChange={(hasil)=>this.props.gantiValue(hasil, this.props.index)}
                                style={{marginBottom: -30}}
                    />
                        <View style={{flex: 1, flexDirection: 'column', marginBottom: 5, justifyContent: 'center', alignItems: 'center', alignContent:'center', alignSelf: 'center'}}>
                            <Text note style={{fontSize: 8, textAlign: 'center'}}>
                                ID TRIP
                            </Text>
                            <Text style={{fontSize: 16, fontWeight: 'bold', textAlign: 'center'}}>
                                {this.props.value.id_trip}
                            </Text>
                            <Text note style={{fontSize: 8, textAlign: 'center'}}>
                                {this.props.value.jadwal} ( {this.props.value.status} )
                            </Text>
                            <Text note style={{fontSize: 8, textAlign: 'center'}}>
                                Kendaraan : {this.props.value.jenis_kendaraan}
                            </Text>
                        </View>
                        <View style={{flex: 1, flexDirection: 'row'}}>
                            <View style={{flex:1, marginLeft: 10}}>
                                    <Text note style={{width: "100%", fontSize: 12, textAlign: 'center'}}>
                                        Asal kota :{'\n'}{this.props.value.asal}
                                    </Text>
                                <View style={{flexDirection: 'column'}}>
                                    <Text note style={{width: "100%", fontSize: 12, textAlign: 'center'}}>
                                        Harga :
                                    </Text>
                                    <Text note style={{width: "100%", fontSize: 12, color: '#78e4a9', textAlign: 'center'}}>
                                        Rp. {this.props.value.harga == null ? 0 : this.numberWithCommas(this.props.value.harga)} ,-
                                    </Text>
                                </View>
                            </View>
                            <View style={{flex:1, justifyContent: 'center'}}>
                                    <Text note style={{width: "100%", fontSize: 12, textAlign: 'center'}}>
                                        Tujuan kota :{'\n'}{this.props.value.tujuan}
                                    </Text>
                                <View style={{flexDirection: 'column'}}>
                                    <Text note style={{width: "100%", fontSize: 12, textAlign: 'center'}}>
                                        Biaya kuli :
                                    </Text>
                                    <Text note style={{width: "100%", fontSize: 12, color: '#f6b247', textAlign: 'center'}}>
                                        Rp. {this.props.value.biaya_kuli == null ? 0 : this.numberWithCommas(this.props.value.biaya_kuli)} ,-
                                    </Text>
                                </View>
                            </View>
                            
                        </View>

                        <View style={{ marginTop: 10, justifyContent: 'center', alignItems: 'center', alignContent:'center', alignSelf: 'center'}}>
                            <Button small style={{backgroundColor: '#5a8dee', height: 20}} onPress={()=>this.props.onPodSelected(this.props.value)}>
                                <Text style={{fontSize: 8}}>Detail Order</Text>
                            </Button>
                        </View>
                        
                    </Body>
                </CardItem>
            </Card>
        );
    }
}

export default CardPod;