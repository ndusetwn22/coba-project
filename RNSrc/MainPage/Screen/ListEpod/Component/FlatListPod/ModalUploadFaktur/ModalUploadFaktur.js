import React,{Component} from 'react'
import {View, ScrollView,TouchableOpacity, Image} from "react-native";
import styles from './style.js'
import {Text,Button, Card,CardItem,Body, Icon, Label} from 'native-base'
import Timeline from 'react-native-timeline-flatlist'
import Modal from 'react-native-modal';

class ModalUploadFaktur extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        };
    }

    componentDidMount= async()=>{
        // console.log('modalupload', this.props.modalUploadFakturVisible)

    }

    
    
    render(){
        return(
            <>
                <Modal animationType="slide" isVisible={this.props.modalUploadFakturVisible} onBackdropPress={this.props.toggleModalFaktur}>
                    <View style={styles.circleViewStyle}>
                        <Text style={{textAlign: 'center', marginTop: 20}}>Upload Faktur</Text>
                        <Text note style={{textAlign: 'center'}}>extensi : .pdf</Text>
                        <View style={styles.viewButton}>   
                            <Button
                            style={styles.button}
                            onPress={this.props.pilihDokumenFaktur}  
                            >
                            <Text uppercase={false} style={{fontSize: 14}} numberOfLines={1}>{this.props.documentFileNameFaktur}</Text>
                            <Icon type='MaterialCommunityIcons' name='file-document' />
                            </Button>
                        </View>
                        {/* <TouchableOpacity style={{marginTop: 10}} onPress={this.props.lihatDokumen}>
                            <Text note style={{textAlign: 'center'}}>lihat dokumen</Text>
                        </TouchableOpacity> */}
                        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                            <Button style={{backgroundColor: '#CE3C3E', height: 30, marginLeft: 10, marginRight: 10, marginTop: 20}} onPress={this.props.toggleModalFaktur}>
                                <Text style={{fontSize: 8}}>  Hide  </Text>
                            </Button>
                            <Button style={{backgroundColor: '#5a8dee', height: 30, marginLeft: 10, marginRight: 10, marginTop: 20}} onPress={this.props.funcCombinedUploadFaktur}>
                                <Text style={{fontSize: 8}}>Upload</Text>
                            </Button>
                            {/* <Button style={{backgroundColor: '#008c45', height: 30, marginLeft: 10, marginRight: 10, marginTop: 20}} onPress={this.props.lanjutBuatInvoice}>
                                <Text style={{fontSize: 8}}>Lanjut</Text>
                            </Button> */}
                        </View>
                    </View>
                </Modal>
            </>
        );
    }
}

export default ModalUploadFaktur;