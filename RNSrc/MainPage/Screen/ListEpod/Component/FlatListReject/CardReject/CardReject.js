import React,{Component} from 'react'
import {
    View
} from "react-native";
import styles from './style'
import {
    Text,  
    Card,
    CardItem,
    Body,
    Left,
    Right,
    Button,
    Icon,
    Thumbnail
} from 'native-base'
import { TouchableOpacity } from 'react-native-gesture-handler';

class CardReject extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount= async()=>{

    } 

    numberWithCommas=(x)=>{
        if (x == null) {
            return 0
        } else{
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        }
      }

    render(){
        // console.log('profile di card', this.props.profile.hak_akses)
        const hak_akses = this.props.hak_akses
        return(
            <Card style={{marginLeft: 10, marginRight: 10}}>
                <CardItem>
                    <Body>
                        <View style={{flex: 1, flexDirection: 'column', marginBottom: 5, justifyContent: 'center', alignItems: 'center', alignContent:'center', alignSelf: 'center'}}>
                            <Text note style={{fontSize: 8, textAlign: 'center'}}>
                                INVOICE
                            </Text>
                            <Text style={{fontSize: 16, fontWeight: 'bold', textAlign: 'center'}}>
                                {this.props.value.nomor_invoice}
                            </Text>
                            <Text note style={{fontSize: 8, textAlign: 'center', color: '#ee595b'}}>
                                ( {this.props.value.status_epod} ) {this.props.value.keterangan}
                            </Text>
                        </View>
                        <View style={{flex: 1, flexDirection: 'row'}}>
                            <View style={{flex:1, marginLeft: 10}}>
                                    <Text note style={{width: "100%", fontSize: 12, textAlign: 'center'}}>
                                        Request date :{'\n'}
                                        {this.props.value.create_date == null ? 
                                            null
                                        : 
                                            this.props.value.create_date2
                                            //  + ' - ' + this.props.value.create_date.substr(11,8)
                                        } 
                                    </Text>
                                <View style={{flexDirection: 'column'}}>
                                    <Text note style={{width: "100%", fontSize: 12, textAlign: 'center'}}>
                                        Harga :
                                    </Text>
                                    <Text note style={{width: "100%", fontSize: 12, color: '#78e4a9', textAlign: 'center'}}>
                                        Rp. {this.props.value.total == null ? 0 : this.numberWithCommas(this.props.value.total)} ,-
                                    </Text>
                                </View>
                            </View>
                            <View style={{flex:1, justifyContent: 'center'}}>
                                    <Text note style={{width: "100%", fontSize: 12, textAlign: 'center'}}>
                                        Jatuh tempo :{'\n'}
                                        {this.props.value.jatuh_tempo == null ? 
                                            null
                                        : 
                                            this.props.value.jatuh_tempo
                                            // .substr(0,10) + ' - ' + this.props.value.jatuh_tempo.substr(11,8) 
                                        }
                                    </Text>
                                <View style={{flexDirection: 'column'}}>
                                    <Text note style={{width: "100%", fontSize: 12, textAlign: 'center'}}>
                                        Jumlah trip :
                                    </Text>
                                    <Text note style={{width: "100%", fontSize: 12, color: '#f6b247', textAlign: 'center'}}>
                                        {this.props.value.jumlah_trip}
                                    </Text>
                                </View>
                            </View>
                            
                        </View>
                    </Body>
                </CardItem>
            </Card>
        );
    }
}

export default CardReject;