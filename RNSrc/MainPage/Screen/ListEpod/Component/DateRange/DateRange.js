import React,{Component} from 'react'
import {Text, View} from 'native-base'
import styles from './style'
import Icon from 'react-native-vector-icons/FontAwesome';
import { TouchableOpacity } from 'react-native-gesture-handler';

class DateRange extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount= async()=>{

    }

    render(){
        return(
            <>
            {this.props.startDate!=null && this.props.displayedDate!=null? 
                    <TouchableOpacity style={styles.headerViewStyleGradientDate} onPress={this.props.openModalCalendar}>
                        {
                            this.props.startDate!=null && this.props.endDate!=null?
                        this.props.startDate.format('YYYY-MM-DD')==this.props.endDate.format('YYYY-MM-DD')? 
                            <Text style={styles.textStyle}>
                                <Icon 
                                    name='calendar' 
                                    size={15} 
                                    style={{marginRight:10
                                    }}
                                    color='#6d6d6e' 
                                />
                                {" "+this.props.startDate.format('D MMMM YYYY')}
                            </Text>
                        :
                            <Text style={styles.textStyle}>
                                <Icon 
                                    name='calendar' 
                                    size={15} 
                                    style={{marginRight:10
                                    }}
                                    color='#6d6d6e' 
                                />{"  "}
                                {this.props.startDate.format('D MMMM YYYY')} 
                                {" "} s/d {" "}
                                {this.props.endDate.format('D MMMM YYYY')}
                            </Text>
                        :null
                        }
                    </TouchableOpacity>
                :null}
            </>
        );
    }
}

export default DateRange;