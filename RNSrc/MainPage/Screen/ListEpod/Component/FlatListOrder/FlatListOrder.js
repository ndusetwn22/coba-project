import React,{Component} from 'react'
import {Text, View,Icon, Content, Button} from 'native-base'
import {
    TouchableOpacity,
    ScrollView,FlatList
} from "react-native";
import styles from './style'
import CardOrder from './CardOrder/CardOrder'
import SearchInput,{ createFilter } from 'react-native-search-filter';
import Loading,{getProfile, setUrl, getUrl, decrypt} from '../../../../../../APIProp/ApiConstants'
const KEYS_TO_FILTERS = ['id_order', 'jadwal_penjemputan', 'status_order'];

class FlatListOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paramSearch:'',
            isLoading: false,
            orderList: this.props.listOrder
        };
    }

    componentDidMount= async()=>{
        await this.setState({
            orderList: this.props.listOrder
        })

        console.log('props flatlist:', this.props)

    } 

    render(){
        // console.log('list user', this.props.profile)
        const filteredOrder = this.props.listOrder.filter(createFilter(this.state.paramSearch, KEYS_TO_FILTERS))
        const hak_akses = this.props.hak_akses
        return(
            <>
                
                    <View style={{flexDirection: 'row', backgroundColor: 'white', top: 1}}>
                        <Button onPress={()=>this.props.backToListPod()} light style={{marginTop: 10, marginLeft: 10, borderRadius: 10, padding: 0, height: 40, backgroundColor: '#5a8dee'}}>
                            <Icon type='MaterialCommunityIcons' name='arrow-left' style={{color: 'white'}}/>
                        </Button>
                        <SearchInput 
                            onChangeText={(term) => { this.setState({paramSearch:term}) }} 
                            style={{
                                paddingLeft:10,
                                paddingTop:5,
                                marginTop: 10,
                                marginLeft:10,
                                marginRight:10,
                                marginBottom:5,
                                paddingBottom:5,
                                borderColor: '#008c45',
                                borderWidth: 1,
                                backgroundColor:"white",
                                borderRadius: 10,
                                width: '285%',
                            }}
                            placeholder="Search ... "
                            inputFocus={true}
                            removeClippedSubviews={true} // Unmount components when outside of window 
                            initialNumToRender={2} // Reduce initial render amount
                            maxToRenderPerBatch={1} // Reduce number in each render batch
                            maxToRenderPerBatch={10} // Increase time between renders
                            windowSize={7} // Reduce the window size
                        />
                    </View>
                <Content>
                    <ScrollView>
                            <FlatList
                                data={
                                    filteredOrder
                                }
                                style={{marginBottom:10}}
                                renderItem={({ item: value, index }) => {
                                    return (
                                        <CardOrder {...this.props} index={index} value={value} onDokumenPODSelected={this.props.onDokumenPODSelected} onUpdateDokumenPOD={this.props.onUpdateDokumenPOD} onDokumenKwitansiSelected={this.props.onDokumenKwitansiSelected} onUpdateKwitansi={this.props.onUpdateKwitansi}/>
                                    );
                                }}
                                keyExtractor={(item, index) => index.toString()}
                            /> 
                    </ScrollView>
                </Content>
                {this.state.isLoading ? <Loading/> : false}
            </>
        );
    }
}

export default FlatListOrder;