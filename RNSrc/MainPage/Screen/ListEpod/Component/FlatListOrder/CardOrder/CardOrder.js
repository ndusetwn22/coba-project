import React,{Component} from 'react'
import {
    View,
    CheckBox,
    Image,
    Linking
} from "react-native";
import styles from './style'
import {
    Text,  
    Card,
    CardItem,
    Body,
    Left,
    Right,
    Button,
    Icon,
    Thumbnail,
    Content
} from 'native-base'
import Modal from 'react-native-modal';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { SliderBox } from "react-native-image-slider-box";
import SweetAlert from 'react-native-sweet-alert';
import Carousel from 'react-native-snap-carousel';
import {firebase} from '../../../../../../../APIProp/ApiConstants';
//image picker
import ImagePicker from 'react-native-image-picker';
import { updateDokumenPOD, lihatDokumenPod } from '../../../ListEpodAxios';



class CardOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fotoFileName: 'Pilih foto pengemudi',
            fotoSource: null,
            modalVisible: false,
            modalVisibleKwitansi: false,
            textKwitansiMuat: '',
            textkwitansiBongkar: '',
            // images: [
            //     firebase.linkStorage+"image-edf62fc1-2dd2-4c24-9f7d-95204d1441c5.jpg?alt=media",
            //     firebase.linkStorage+"image-878d7faa-fece-44d3-89f3-ca11288fa4d7.jpg?alt=media",
            //     firebase.linkStorage+"image-9b0ece5c-2442-4e7b-81e9-b996f9aa19f5.jpg?alt=media",
            //     firebase.linkStorageFotoDriver%2F13%2FDriver.jpg?alt=media",
            //     firebase.linkStorageFotoDriver%2F11%2FDriver.png?alt=media",
            //     // ""
            //     // "https://source.unsplash.com/1024x768/?tree", // Network image
            //   ],
            // images: this.props.value.dokumenPOD,
            images: [],
            // images: this.props.value.dokumenGambar,
            // images:['https://firebasestorage.googleapis.com/v0/b/chatting-ed3db.appspot.com/o/FotoKwitansi%2FEPM1910100004%2FBongkar%2Foutput-onlinepngtools%20(2).png?alt=media&token=331fcc9b-9a50-424e-881a-503d0c2b6d86'],
            indexCaption: 0,
            caption: ['Dokumen POD', 'Dokumen POD 2', 'Dokumen POD 3', 'Dokumen POD 4', 'Dokumen POD 5'],
            // indexCaptionKwitansi: 0,
            // captionKwitansi: ['Kwitansi'],
            activeIndex:0,
            carouselItemsNew:this.props.imagesPod,
            carouselItems: [
                {
                    title:"Item 1",
                    text: "Text 1",
                },
                {
                    title:"Item 2",
                    text: "Text 2",
                },
                {
                    title:"Item 3",
                    text: "Text 3",
                },
                {
                    title:"Item 4",
                    text: "Text 4",
                },
                {
                    title:"Item 5",
                    text: "Text 5",
                },
            ],
        };
    }

    componentDidMount= async()=>{
        console.log('PROPSSS', this.props)
        console.log('carousel', this.state.carouselItems)
    } 

    _renderItem({item,index}){
        // this.selectFotoTapped = this.selectFotoTapped.bind(this);
        return (
          <View style={{
            //   backgroundColor:'white',
              borderRadius: 5,
              height: '80%',
              justifyContent: 'center'
            //   padding: 50,
            //   marginLeft: 25,
            //   marginRight: 25, 
              }}>
            {/* <Text style={{fontSize: 30}}>{item.title}</Text>
            <Text>{item.text}</Text> */}
            <Text>{index}</Text>
            <Image source={{uri: item.dokumen_pod}} style={{flex: 1, resizeMode: 'contain'}}/>
            {/* <Button style={{backgroundColor: '#008c45', justifyContent: 'center', height: 30, marginLeft: 10, marginRight: 10}} onPress={this.selectFotoTapped.bind(this,this.state.indexCaption)}>
                <Text style={{fontSize: 8, textAlign: 'center'}}>Ganti Dokumen POD {index}</Text>
            </Button> */}
          </View>

        )
    }

    selectFotoTapped = async(dokumenPOD) => {
        console.log('index dokumenPOD : ', dokumenPOD+1)
        let dokumenPOD_new = parseInt(dokumenPOD)+1
        const options = {
          quality: 1.0,
          maxWidth: 500,
          maxHeight: 500,
          storageOptions: {
            skipBackup: true,
          },
        };

        ImagePicker.launchImageLibrary(options, async(response) => {
          console.log('Response = ', response);
    
          if (response.didCancel) {
            console.log('User cancelled photo picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            let source = {uri: response.uri};
    
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
            
            console.log('options : ', options)
            console.log('uri :', response.uri)

            await this.setState({modalVisible: false})

            let _temp = await this.props.onUpdateDokumenPOD(this.props.url, this.props.value.id, this.props.profile.id_user, this.props.value.id+'_'+dokumenPOD_new,source, dokumenPOD_new)

            // let _temp = await updateDokumenPOD(this.props.url, this.props.value.id, this.props.profile.id_user, this.props.value.id+'_'+dokumenPOD_new,source, dokumenPOD_new)

            // await this.props.onPodSelected(this.props.value)
            return this.funcSweetAlert(_temp[0], _temp[1], _temp[2])
            // this.setState({
            //   // avatarSource: response.uri,
            //   fotoSource: source,
            //   fotoFileName: 'Driver'
            // });
    
          }
        });
      }

    toggleModal = async() => {
        
        if (this.state.modalVisible == false) {
            // console.log('props value', this.props.value)

            // let _temp = await lihatDokumenPod(this.props.url, this.props.value.id)
            // console.log('temp', _temp[0].dokumenPOD)
            // await this.setState({images: _temp[0].dokumenPOD})

            let _temp = await this.props.onDokumenPODSelected(this.props.value.id)
            await this.setState({images: _temp[0].dokumenPOD})
            await console.log('PROPS NEW', this.props)
        }

        // if (this.state.modalVisible == false) {
        //     this.props.onDokumenPODSelected(this.props.value.id)
        // }

        await this.setState({modalVisible: !this.state.modalVisible});
      };

    funcSweetAlert = async(title, subTitle, style) =>{
        SweetAlert.showAlertWithOptions({
            title: title,
            subTitle: subTitle,
            confirmButtonTitle: 'OK',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: style,
            cancellable: true
          },
            callback => console.log('callback'));
    }
    
    toggleModalKwitansi = async() => {

        if (this.state.modalVisibleKwitansi == false) {
            let _temp = await this.props.onDokumenKwitansiSelected(this.props.value.id)

            var kwitansiMuat = _temp[0].kwitansi_kuli_muat
            var kwitansiBongkar = _temp[0].kwitansi_kuli_bongkar

            await this.setState({textKwitansiMuat: kwitansiMuat, textkwitansiBongkar: kwitansiBongkar})
        }else{
            await this.setState({textKwitansiMuat: '', textkwitansiBongkar: ''})
        }

        await this.setState({modalVisibleKwitansi: !this.state.modalVisibleKwitansi});
    };

    selectFotoTappedKwitansi = async(jenisKwitansi) => {
        console.log('jenis Kwitansi : ', jenisKwitansi)
        const options = {
          quality: 1.0,
          maxWidth: 500,
          maxHeight: 500,
          storageOptions: {
            skipBackup: true,
          },
        };

        ImagePicker.launchImageLibrary(options, async(response) => {
          console.log('Response = ', response);
    
          if (response.didCancel) {
            console.log('User cancelled photo picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            let source = {uri: response.uri};
    
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
            
            console.log('options : ', options)
            console.log('uri :', response.uri)

            await this.setState({modalVisibleKwitansi: false})

            let _temp = await this.props.onUpdateKwitansi(this.props.url, this.props.value.id, this.props.value.id_order, this.props.profile.id_user, jenisKwitansi, source)

            // let _temp = await this.props.onUpdateDokumenPOD(this.props.url, this.props.value.id, this.props.profile.id_user, this.props.value.id+'_'+dokumenPOD_new,source, dokumenPOD_new)

            return this.funcSweetAlert(_temp[0], _temp[1], _temp[2])
    
          }
        });
      }

    numberWithCommas=(x)=>{
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      }

    render(){

        // this.selectFotoTapped = this.selectFotoTapped.bind(this);


        // console.log('profile di card', this.props.profile.hak_akses)
        const hak_akses = this.props.hak_akses
        return(
            <>
            {/* <Button style={{backgroundColor: '#008c45', justifyContent: 'center', height: 30, marginLeft: 10, marginRight: 10, marginTop: 30}} onPress={this.selectFotoTapped.bind(this)}>
                <Text style={{fontSize: 8, textAlign: 'center'}}>Ganti {this.state.caption[this.state.indexCaption]}</Text>
            </Button> */}
            <Card style={{marginLeft: 10, marginRight: 10}}>
                <CardItem>
                    <Body>
                        <View style={{flex: 1, flexDirection: 'column', marginBottom: 5, justifyContent: 'center', alignItems: 'center', alignContent:'center', alignSelf: 'center'}}>
                            <Text note style={{fontSize: 8, textAlign: 'center'}}>
                                ID ORDER
                            </Text>
                            <Text style={{fontSize: 16, fontWeight: 'bold', textAlign: 'center'}}>
                                {this.props.value.id_order}
                            </Text>
                            <Text note style={{fontSize: 8, textAlign: 'center'}}>
                                Jadwal penjemputan : {this.props.value.jadwal_penjemputan}
                            </Text>
                            <Text note style={{fontSize: 8, textAlign: 'center'}}>
                                 Status : {this.props.value.status_order}
                            </Text>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                            <View style={{flex: 1, marginTop: 0, justifyContent: 'center', alignItems: 'center', alignContent:'center', alignSelf: 'center'}}>
                                <Button small style={{backgroundColor: '#5a8dee', height: 20}} onPress={this.toggleModal}>
                                    <Text style={{fontSize: 8}}>Lihat Dokumen</Text>
                                </Button>
                            </View>
                            <View style={{flex: 1,  marginTop: 0, justifyContent: 'center', alignItems: 'center', alignContent:'center', alignSelf: 'center'}}>
                                <Button small style={{backgroundColor: '#f5ac41', height: 20}} onPress={this.toggleModalKwitansi}>
                                    <Text style={{fontSize: 8}}>Lihat Kwitansi</Text>
                                </Button>
                            </View>
                        </View>
                    </Body>
                </CardItem>
            </Card>

            {/* Modal Dokumen POD */}
                <Modal isVisible={this.state.modalVisible} animationType='slide' onBackdropPress={this.toggleModal}>
                    <View style={styles.circleViewStyle}>
                        <Text style={{textAlign: 'center', marginTop: 20}}>{this.state.caption[this.state.indexCaption]}</Text>

                        <View style={{flexDirection: 'row', margin: 5, marginTop: 10, justifyContent: 'center'}}>
                            {/* <Carousel
                                layout={"default"}
                                ref={ref => this.carousel = ref}
                                // data={this.state.carouselItems}
                                data={this.props.imagesPod}
                                sliderWidth={300}
                                itemWidth={300}
                                renderItem={this._renderItem}
                                onSnapToItem = { index => this.setState({indexCaption:index}) } /> */}


                            <SliderBox
                                // images={this.props.imagesPod[0].dokumenPOD == undefined ? this.props.imagesPod : this.props.imagesPod[0].dokumenPOD}
                                images={this.state.images}
                                // autoplay
                                circleLoop
                                resizeMethod={'scale'}
                                resizeMode={'contain'}
                                // sliderBoxHeight={200}
                                ImageComponentStyle={{borderRadius: 15, width: '100%', marginTop: 5}}
                                // disableOnPress={true}
                                onCurrentImagePressed={index => Linking.openURL(this.state.images[index])}
                                currentImageEmitter={index => this.setState({indexCaption: index})}
                                // dotColor="#008c45"
                                inactiveDotColor="#90A4AE"
                                />
                                
                        </View>
                        {/* <TouchableOpacity> */}
                        {/* <Button style={{backgroundColor: '#008c45', justifyContent: 'center', height: 30, marginLeft: 10, marginRight: 10}} onPress={()=>this.carousel.snapToNext()}>
                                <Text style={{fontSize: 8, textAlign: 'center'}}>Ganti {this.state.caption[this.state.indexCaption]}</Text>
                            </Button> */}

                            <Button style={{backgroundColor: '#008c45', justifyContent: 'center', height: 30, marginLeft: 10, marginRight: 10, marginTop: 20}} onPress={this.selectFotoTapped.bind(this,this.state.indexCaption)}>
                                <Text style={{fontSize: 8, textAlign: 'center'}}>Ganti {this.state.caption[this.state.indexCaption]}</Text>
                            </Button>
                        {/* </TouchableOpacity> */}
                        {/* <Text>{this.state.caption[this.state.indexCaption]}</Text> */}
                    </View>
                </Modal>



            {/* Modal Kwitansi */}
                <Modal isVisible={this.state.modalVisibleKwitansi} animationType='slide' onBackdropPress={this.toggleModalKwitansi}>
                    <View style={styles.circleViewStyle2}>
                    <Text style={{textAlign: 'center', marginTop: 15, fontSize: 12}}>Kwitansi</Text>
                        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                                {/* <Button style={{backgroundColor: '#008c45', height: 30, marginLeft: 10, marginRight: 10, marginTop: 15}} onPress={()=>this.props.value.kwitansi_kuli_muat == null || this.props.value.kwitansi_kuli_muat == "" ? this.funcSweetAlert('Kwitansi Kosong', 'Kwitansi Kuli Muat Kosong', 'warning') : Linking.openURL(this.props.value.kwitansi_kuli_muat)}>
                                    <Text style={{fontSize: 8}}>Kwitansi Kuli Muat</Text>
                                </Button>
                                <Button style={{backgroundColor: '#008c45', height: 30, marginLeft: 10, marginRight: 10, marginTop: 15}} onPress={()=>this.props.value.kwitansi_kuli_bongkar == null || this.props.value.kwitansi_kuli_bongkar == "" ? this.funcSweetAlert('Kwitansi Kosong', 'Kwitansi Kuli Bongkar Kosong', 'warning') : Linking.openURL(this.props.value.kwitansi_kuli_bongkar)}>
                                    <Text style={{fontSize: 8}}>Kwitansi Kuli Bongkar</Text>
                                </Button> */}

                            <View style={{flexDirection: 'column'}}>
                                <Button style={{justifyContent: 'center', backgroundColor: '#008c45', height: 30, marginLeft: 10, marginRight: 10, marginTop: 15}} onPress={()=>this.state.textKwitansiMuat == null || this.state.textKwitansiMuat == "" ? this.funcSweetAlert('Kwitansi Kosong', 'Kwitansi Kuli Muat Kosong', 'warning') : Linking.openURL(this.state.textKwitansiMuat)}>
                                    <Text style={{fontSize: 8}}>Lihat Kwitansi Kuli Muat</Text>
                                </Button>
                                <Button style={{justifyContent: 'center', backgroundColor: '#f5ac41', height: 30, marginLeft: 10, marginRight: 10, marginTop: 15}} onPress={this.selectFotoTappedKwitansi.bind(this, 'Kwitansi Muat')}>
                                    <Text style={{fontSize: 8}}>Ganti Kwitansi Kuli Muat</Text>
                                </Button>
                            </View>
                            <View style={{flexDirection: 'column'}}>
                                <Button style={{justifyContent: 'center', backgroundColor: '#008c45', height: 30, marginLeft: 10, marginRight: 10, marginTop: 15}} onPress={()=>this.state.textkwitansiBongkar == null || this.state.textkwitansiBongkar == "" ? this.funcSweetAlert('Kwitansi Kosong', 'Kwitansi Kuli Bongkar Kosong', 'warning') : Linking.openURL(this.state.textkwitansiBongkar)}>
                                    <Text style={{fontSize: 8}}>Lihat Kwitansi Kuli Bongkar</Text>
                                </Button>
                                <Button style={{justifyContent: 'center', backgroundColor: '#f5ac41', height: 30, marginLeft: 10, marginRight: 10, marginTop: 15}} onPress={this.selectFotoTappedKwitansi.bind(this, 'Kwitansi Bongkar')}>
                                    <Text style={{fontSize: 8}}>Ganti Kwitansi Kuli Bongkar</Text>
                                </Button>
                            </View>
                        </View>
                    </View>
                </Modal>
            </> //Catatan, ini butuh
        );
    }
}

export default CardOrder;