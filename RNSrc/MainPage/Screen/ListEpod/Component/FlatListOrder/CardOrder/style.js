import { StyleSheet, PixelRatio } from 'react-native';


// sebelumnya elevation: 1
const styles = StyleSheet.create({
    circleViewStyle:{
        backgroundColor: '#fafafa', 
        borderRadius:10, 
        shadowColor: "#000",
        height: '60%'   
    },
    circleViewStyle2:{
        backgroundColor: '#fafafa', 
        borderRadius:10, 
        shadowColor: "#000",
        height: '25%'   
    },
    textStyleCircle:{
        textAlign: 'center', 
        marginBottom: 20, 
        marginTop: 5, 
        fontSize: 12, 
        color: '#7d8d8c'
    },
});

export default styles;
