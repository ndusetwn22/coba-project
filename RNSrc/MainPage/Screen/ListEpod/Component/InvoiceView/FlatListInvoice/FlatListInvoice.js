import React,{Component} from 'react'
import {Text, View,Icon, Content, Button} from 'native-base'
import {
    TouchableOpacity,
    ScrollView,FlatList,
    Alert
} from "react-native";
import CardInvoice from './CardInvoice/CardInvoice'

class FlatListInvoice extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount= async()=>{


    } 


    render(){

        return(
            <>
                <Content style={{marginTop: -40}}>
                        <ScrollView>
                            <FlatList
                                data={
                                    this.props.listTripInvoice
                                }
                                renderItem={({ item: value, index }) => {
                                    return(

                                    <CardInvoice {...this.props} index={index} value={value} gantiValue={this.props.gantiValue}/>
                                )
                                }}
                                keyExtractor={(item, index) => index.toString()}
                            /> 
                        </ScrollView>
                    </Content>


            </>
        );
    }
}

export default FlatListInvoice;