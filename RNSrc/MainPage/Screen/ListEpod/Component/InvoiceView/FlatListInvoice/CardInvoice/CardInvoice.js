import React,{Component} from 'react'
import {
    View,
    CheckBox,
    Image, 
    TextInput,
    ScrollView,
    FlatList
} from "react-native";
import styles from './style'
import {
    Text,  
    Card,
    CardItem,
    Body,
    Left,
    Right,
    Button,
    Icon,
    Thumbnail,
    Content,
} from 'native-base'
import Modal from 'react-native-modal'
import { TouchableOpacity } from 'react-native-gesture-handler';
import ModalDetailOrder from './ModalDetailOrder/ModalDetailOrder';
import {getListDetailOrder} from '../../../../ListEpodAxios'
import Loading,{getProfile, setUrl, getUrl, decrypt} from '../../../../../../../../APIProp/ApiConstants'



class CardInvoice extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            modalVisible: false,
            modalVisibleKwitansi: false,
            listDetailOrder: [],
        };
    }

    componentDidMount= async()=>{
        console.log('value', this.props.value)

    } 

    toggleModal = () => {
        this.setState({modalVisible: !this.state.modalVisible});
      };
    


    toggleModalKwitansi = async(index) => {
        await this.setState({modalVisibleKwitansi: !this.state.modalVisibleKwitansi, isLoading: true});
        console.log('index ke',index)

        if (this.state.modalVisibleKwitansi) {
            this.getListDetailOrder(index)
        }else{
            this.setState({listDetailOrder: []})
        }

        this.setState({isLoading: false})
    };

    toggleModalKwitansi2 = async() => {
        await this.setState({modalVisibleKwitansi: !this.state.modalVisibleKwitansi, isLoading: true, listDetailOrder: []});
        this.setState({isLoading: false})
    };

    functionCombined = (value) => {
        this.setState({modalVisible: !this.state.modalVisible});

        console.log('value', value)
        // this.props.assignBiaya(this.props.value)

    }

    numberWithCommas=(x)=>{
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      }

    getListDetailOrder = async(id_trip) => {
        let _getListDetailOrder = await getListDetailOrder(this.props.url, id_trip)

        this.setState({listDetailOrder: _getListDetailOrder})
    }

    render(){

        return(
            <>
            <Card style={{marginLeft: 10, marginRight: 10}}>
                <CardItem>
                    <Body>
                        <View style={{flex: 1, flexDirection: 'column', marginBottom: 5, alignSelf: 'center'}}>
                            <Text note style={{fontSize: 8, textAlign: 'center'}}>
                                ID TRIP
                            </Text>
                            <Text style={{fontSize: 16, fontWeight: 'bold', textAlign: 'center'}}>
                                {this.props.value.NomorTrip}
                            </Text>
                            <View style={{flexDirection: 'row'}}>
                                <Text note style={{fontSize: 8, textAlign: 'center'}}>
                                    Biaya Trip : Rp. {this.props.value.BiayaTrip == null ? 0 : this.numberWithCommas(this.props.value.BiayaTrip)},-
                                </Text>
                                <Text note style={{fontSize: 8, textAlign: 'center'}}>
                                    Biaya Kuli : Rp. {this.props.value.BiayaKuli == null ? 0 : this.numberWithCommas(this.props.value.BiayaKuli)},-
                                </Text>
                                <Text note style={{fontSize: 8, textAlign: 'center'}}>
                                    Biaya Inap : Rp. {this.props.value.BiayaInap == null ? 0 : this.numberWithCommas(this.props.value.BiayaInap)},-
                                </Text>
                            </View>


                            <View style={{flexDirection: 'row'}}>
                                <View style={{flex: 1, marginTop: 0, justifyContent: 'center', alignItems: 'center', alignContent:'center', alignSelf: 'center'}}>
                                    <Button small style={{backgroundColor: '#F6B759', height: 20, alignSelf: 'center', marginLeft: -10, marginTop: 5}} 
                                        // onPress={()=>this.props.assignBiaya(this.props.value)}
                                        onPress={this.toggleModal}>
                                        <Text style={{fontSize: 8, textAlign: 'center'}}>Assign Biaya</Text>
                                    </Button>
                                </View>
                                <View style={{flex: 1, marginTop: 0, justifyContent: 'center', alignItems: 'center', alignContent:'center', alignSelf: 'center'}}>
                                    <Button small style={{backgroundColor: '#008c45', height: 20, alignSelf: 'center', marginLeft: -10, marginTop: 5}} 
                                        // onPress={()=>this.props.assignBiaya(this.props.value)}
                                        onPress={()=>this.toggleModalKwitansi(this.props.value.IdTrip)}>
                                        <Text style={{fontSize: 8, textAlign: 'center'}}>Upload Kwitansi</Text>
                                    </Button>
                                </View>
                            </View>
                            {/* <Button small style={{backgroundColor: '#F6B759', height: 20, alignSelf: 'center', marginLeft: -10, marginTop: 5}} 
                            // onPress={()=>this.props.assignBiaya(this.props.value)}
                            onPress={this.toggleModal}
                            >
                                <Text style={{fontSize: 8, textAlign: 'center'}}>Assign Biaya</Text>
                            </Button> */}
                        </View>
                    </Body>
                </CardItem>
            </Card>

        {/* ModalDetailOrder */}
        <ModalDetailOrder {...this.state} {...this.props} listDetailOrder={this.state.listDetailOrder} modalVisibleKwitansi={this.state.modalVisibleKwitansi} toggleModalKwitansi2={this.toggleModalKwitansi2}/>

        <Modal isVisible={this.state.modalVisible} animationType='slide' onBackdropPress={this.toggleModal}>
            <View style={styles.circleViewStyle}>
                {/* <Text style={{textAlign: 'center', marginTop: 20}}>Kwitansi</Text> */}
                <View style={{flexDirection: 'column', margin: 5, marginTop: 10, justifyContent: 'center'}}>
                    <Text style={{textAlign: 'center', marginTop: 10}}>Biaya Inap</Text>
                    <TextInput
                        style={styles.textInput}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder="Biaya Inap"
                        placeholderTextColor = "#CACCD7"
                        selectionColor="#008c45"
                        keyboardType="number-pad"
                        // onChangeText={input=>this.setState({email: input})}
                        onChangeText={(input)=>this.props.gantiValueBiayaInap(input, this.props.index)}
                        value={this.props.value.BiayaInap.toString()}
                        />
                    <Text style={{textAlign: 'center', marginTop: 10}}>Biaya Kuli</Text>
                    <TextInput
                        style={styles.textInput}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder="Biaya Inap"
                        placeholderTextColor = "#CACCD7"
                        selectionColor="#008c45"
                        keyboardType="number-pad"
                        // onChangeText={input=>this.setState({email: input})}
                        onChangeText={(input)=>this.props.gantiValueBiayaKuli(input, this.props.index)}
                        value={this.props.value.BiayaKuli.toString()}
                        />
                </View>
                
                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                    <Button style={{backgroundColor: '#CE3C3E', height: 30, marginLeft: 10, marginRight: 10, marginTop: 10}} onPress={this.toggleModal}>
                        <Text style={{fontSize: 8}}>  Hide  </Text>
                    </Button>
                    <Button style={{backgroundColor: '#5a8dee', height: 30, marginLeft: 10, marginRight: 10, marginTop: 10}} onPress={()=>this.functionCombined(this.props.value)}>
                        <Text style={{fontSize: 8}}> Assign </Text>
                    </Button>
                </View>
            </View>
        </Modal>

        {this.state.isLoading ? <Loading/> : false}
            </>
        );
    }
}

export default CardInvoice;