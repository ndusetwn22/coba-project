import React,{Component} from 'react'
import {
    View, ScrollView, FlatList, Linking
} from "react-native";
import styles from './style'
import {
    Text,  
    Card,
    CardItem,
    Body,
    Left,
    Right,
    Button,
    Icon,
    Thumbnail,
    Content
} from 'native-base'
import { TouchableOpacity } from 'react-native-gesture-handler';
import Modal from 'react-native-modal'
//image picker
import ImagePicker from 'react-native-image-picker';
import { updateKwitansiMuat_, updateKwitansiBongkar_ } from '../../../../../ListEpodAxios';
import SweetAlert from 'react-native-sweet-alert';
import Axios from 'axios';
import storage from '@react-native-firebase/storage';
import {encrypt} from '../../../../../../../../../APIProp/ApiConstants';

class ModalDetailOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fotoFileName: '',
            fotoSource: null,
            fotoFileName2: '',
            fotoSource2: null,
        };
    }

    componentDidMount= async()=>{
        // console.log('url', this.props.url)
        // console.log(this.props.profile.id_user)
    } 

    funcSweetAlert = async(title, subTitle, style) =>{
        SweetAlert.showAlertWithOptions({
            title: title,
            subTitle: subTitle,
            confirmButtonTitle: 'OK',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: style,
            cancellable: true
          },
            callback => console.log('callback'));
    }

    selectFotoTapped = (id_order) => {
        const options = {
          quality: 1.0,
          maxWidth: 500,
          maxHeight: 500,
          storageOptions: {
            skipBackup: true,
          },
        };

        console.log('id',id_order)
    
        ImagePicker.showImagePicker(options, response => {
          console.log('Response = ', response);
    
          if (response.didCancel) {
            console.log('User cancelled photo picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            let source = {uri: response.uri};
    
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
            var temp
            console.log('options : ', options)
            console.log('uri :', response.uri)
            // let _updateKwitansiMuat = updateKwitansiMuat_(this.props.url, id_order, this.props.profile.id_user, this.props.profile.id_transporter, response.fileName, source)
            // this.funcSweetAlert('status', 'Success Upload Kwitansi','success')

            // this.funcSweetAlert(_updateKwitansiMuat[0], _updateKwitansiMuat[1], _updateKwitansiMuat[2])


            this.setState({
                fotoSource: source,
                fotoFileName: response.fileName
              });

            this.uploadFirebase('muat', id_order)

          }
        });
      }


      selectFotoTapped2 = (id_order) => {
        const options = {
          quality: 1.0,
          maxWidth: 500,
          maxHeight: 500,
          storageOptions: {
            skipBackup: true,
          },
        };

        console.log('id',id_order)
    
        ImagePicker.showImagePicker(options, response => {
          console.log('Response = ', response);
    
          if (response.didCancel) {
            console.log('User cancelled photo picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            let source = {uri: response.uri};
    
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
            var temp
            console.log('options : ', options)
            console.log('uri :', response.uri)
            
            // let _updateKwitansiMuat = updateKwitansiBongkar_(this.props.url, id_order, this.props.profile.id_user, this.props.profile.id_transporter, response.fileName, source)
            // this.funcSweetAlert('status', 'Success Upload Kwitansi','success')

            // this.funcSweetAlert(_updateKwitansiMuat[0], _updateKwitansiMuat[1], _updateKwitansiMuat[2])


            this.setState({
                fotoSource: source,
                fotoFileName: response.fileName
              });
            this.uploadFirebase('bongkar', id_order)

          }
        });
      }

      uploadFirebase = async(tipe, id_order) => {
        if (tipe == 'muat') {
            let _updateKwitansiMuat = await updateKwitansiMuat_(this.props.url, id_order, this.props.profile.id_user, this.props.profile.id_transporter, this.state.fotoFileName, this.state.fotoSource)
            await this.funcSweetAlert(_updateKwitansiMuat[0], _updateKwitansiMuat[1], _updateKwitansiMuat[2])
        }else{
            let _updateKwitansiBongkar = await updateKwitansiBongkar_(this.props.url, id_order, this.props.profile.id_user, this.props.profile.id_transporter, this.state.fotoFileName, this.state.fotoSource)
            await this.funcSweetAlert(_updateKwitansiBongkar[0], _updateKwitansiBongkar[1], _updateKwitansiBongkar[2])
        }

        await this.setState({fotoFileName: '', fotoSource: null})
        this.props.toggleModalKwitansi2()
      }

    render(){
        this.selectFotoTapped = this.selectFotoTapped.bind(this);
        this.selectFotoTapped2 = this.selectFotoTapped2.bind(this);


        return(
            <>
        <Modal isVisible={this.props.modalVisibleKwitansi} animationType='slide' onBackdropPress={this.props.toggleModalKwitansi2}>
            {/* <View style={styles.circleViewStyle2}>
            <Text style={{textAlign: 'center', marginTop: 15, fontSize: 12}}>Kwitansi</Text>
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                    <Button style={{backgroundColor: '#008c45', height: 30, marginLeft: 10, marginRight: 10, marginTop: 15}} onPress={()=>this.props.value.kwitansi_kuli_muat == null || this.props.value.kwitansi_kuli_muat == "" ? this.funcSweetAlert('Kwitansi Kosong', 'Kwitansi Kuli Muat Kosong', 'warning') : Linking.openURL(this.props.value.kwitansi_kuli_muat)}>
                        <Text style={{fontSize: 8}}>Kwitansi Kuli Muat</Text>
                    </Button>
                    <Button style={{backgroundColor: '#008c45', height: 30, marginLeft: 10, marginRight: 10, marginTop: 15}} onPress={()=>this.props.value.kwitansi_kuli_bongkar == null || this.props.value.kwitansi_kuli_bongkar == "" ? this.funcSweetAlert('Kwitansi Kosong', 'Kwitansi Kuli Bongkar Kosong', 'warning') : Linking.openURL(this.props.value.kwitansi_kuli_bongkar)}>
                        <Text style={{fontSize: 8}}>Kwitansi Kuli Bongkar</Text>
                    </Button>
                </View>
            </View> */}

                <Content>
                        <ScrollView>
                            <FlatList
                                data={
                                    this.props.listDetailOrder
                                }
                                renderItem={({ item: value, index }) => {
                                    return(

                                    <Card style={{marginLeft: 10, marginRight: 10}}>
                                        <CardItem>
                                            <Body>
                                                {/* <View style={styles.circleViewStyle3}> */}
                                                <View style = {styles.lineStyle} />
                                                    <View style={{alignContent: 'center', alignSelf: 'center'}}>
                                                        <Text style={{textAlign: 'center', margin: 5, fontSize: 12, fontWeight: 'bold'}}>{value.id_order}</Text>
                                                    </View>
                                                <View style = {styles.lineStyle} />
                                                    <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                                                        {value.kwitansi_kuli_muat == null || value.kwitansi_kuli_muat == "" ?
                                                            <Button style={{backgroundColor: '#F6B759', height: 30, marginLeft: 10, marginRight: 10, marginTop: 15}} onPress={this.selectFotoTapped.bind(this, value.id)}>
                                                                <Text style={{fontSize: 8}}>Pilih Kwitansi Kuli Muat</Text>
                                                            </Button>
                                                        : 
                                                            <Button style={{backgroundColor: '#008c45', height: 30, marginLeft: 10, marginRight: 10, marginTop: 15}} onPress={()=>Linking.openURL(value.kwitansi_kuli_muat)}>
                                                                <Text style={{fontSize: 8}}>Kwitansi Kuli Muat</Text>
                                                            </Button>
                                                        }
                                                        {value.kwitansi_kuli_bongkar == null || value.kwitansi_kuli_bongkar == "" ?
                                                            <Button style={{backgroundColor: '#F6B759', height: 30, marginLeft: 10, marginRight: 10, marginTop: 15}} onPress={this.selectFotoTapped2.bind(this, value.id)}>
                                                                <Text style={{fontSize: 8}}>Pilih Kwitansi Kuli Bongkar</Text>
                                                            </Button>
                                                        : 
                                                            <Button style={{backgroundColor: '#008c45', height: 30, marginLeft: 10, marginRight: 10, marginTop: 15}} onPress={()=>Linking.openURL(value.kwitansi_kuli_bongkar)}>
                                                                <Text style={{fontSize: 8}}>Kwitansi Kuli Bongkar</Text>
                                                            </Button>
                                                        }
                                                    </View>
                                                {/* </View> */}
                                            </Body>
                                        </CardItem>
                                    </Card>
                                    
            //                         <Card>
            //     <TouchableOpacity 
            //         onPress={()=>{this.props.closeModal();this.props.onTripSelected(this.props.value)}}
            //     >
            //         <CardItem >
            //             <Body >
            //                 <Text style={[styles.listTripSize,{width:"100%", backgroundColor:"#008c45", color:"white", textAlign:"center",borderRadius:4, marginBottom:10}]}>
            //                     {this.props.value.id_trip}
            //                 </Text>
            //                 <View style = {styles.lineStyle} />
            //                 <Text style={{ width:'100%', color:"#008c45", fontSize:12, fontWeight:'bold', textAlign:"center", fontStyle:"italic"}}>
            //                     {this.props.value.jenis_kendaraan}
            //                 </Text>
            //                 <View style = {styles.lineStyle} />
            //                 <View style={[styles.flexGrid,{marginBottom:5, marginTop:5}]}>
            //                     <Text style={styles.superMediumFontSize}>
            //                         {this.props.value.kota_asal}
            //                     </Text>
            //                     <Text style={styles.superMediumFontSize}>
            //                         {this.props.value.kota_tujuan}
            //                     </Text>
            //                 </View>
                            
            //             </Body>
            //         </CardItem>
            //     </TouchableOpacity>
            // </Card>
                                )
                                }}
                                keyExtractor={(item, index) => index.toString()}
                            /> 
                        </ScrollView>
                    </Content>
        </Modal>
            </>
        );
    }
}

export default ModalDetailOrder;