import { StyleSheet, PixelRatio } from 'react-native';


// sebelumnya elevation: 1
const styles = StyleSheet.create({
    circleViewStyle:{
        backgroundColor: '#fafafa', 
        borderRadius:10, 
        shadowColor: "#000",
        // flex: 1
        height: '70%'   
    },
    circleViewStyle2:{
      backgroundColor: '#fafafa', 
      borderRadius:10, 
      shadowColor: "#000",
      height: '20%'   
  },
  circleViewStyle3:{
    backgroundColor: '#fafafa', 
    borderRadius:10, 
    shadowColor: "#000",
    height: '100%'   
},
    textStyleCircle:{
        textAlign: 'center', 
        marginBottom: 20, 
        marginTop: 5, 
        fontSize: 12, 
        color: '#7d8d8c'
    },
    title:{
        fontSize: 18,
        fontWeight: 'bold', 
        textAlign: 'center', 
        marginTop: 15, 
        // marginBottom: 5, 
        color: '#7d8d8c'
      },
    
      viewTextInput:{
        flex: 1, 
        flexDirection: 'column'
      },
    
      label:{
        fontSize: 14, 
        marginLeft: 10, 
        marginTop: 10, 
        alignSelf: 'flex-start', 
        alignItems: 'flex-start'
      },
    
      textInputTransporter:{
        flex:1, 
        borderColor: 'gray', 
        borderWidth: 2, 
        borderRadius: 5, 
        marginTop: 10, 
        marginLeft: 20, 
        marginRight: 20, 
        height: 40, 
        backgroundColor: '#E9EDF2', 
        color: '#494A4A', 
        paddingLeft: 10, 
        paddingRight: 10, 
        fontSize: 14 
      },
    
      textInput:{
        // flex:1, 
        borderColor: 'gray', 
        borderWidth: 2, 
        borderRadius: 5, 
        marginTop: 10, 
        marginLeft: 20, 
        marginRight: 20, 
        height: 40, 
        color: '#494A4A', 
        paddingLeft: 10, 
        paddingRight: 10, 
        fontSize: 12
      },
    
      viewButton:{
        flex: 1, 
        flexDirection: 'column'
      },
    
      button:{
        width: '89%', 
        // borderColor: 'gray', 
        // borderWidth: 2, 
        borderRadius: 5, 
        marginTop: 10, 
        marginLeft: 20, 
        marginRight: 20, 
        height: 40, 
        // color: '#494A4A',
        backgroundColor: '#008c45', 
        padding: 10 
      },
      listTripSize:{
        fontSize:15
    },
    flexGrid:{
        flex: 1,
        flexDirection: 'row'
    },
    mediumFontSize:{
        fontSize:14,
        padding:10
    },
    superMediumFontSize:{
        fontSize:13,
        width:"50%",
        fontStyle:"italic"
    },
    assignedStyle:{
        fontSize:13,
        width:"50%",
        fontWeight:"bold",
        fontStyle:"italic",
        color:"#595959"
    },
    unAssignedStyle:{
        fontSize:13,
        width:"50%",
        color:"red",
        fontStyle:"italic"
    },
    detailTripFontSize:{
        fontSize:11,
        width:"50%"
    },
    lineStyle:{
        borderWidth: 0.5,
        width:"100%",
        borderColor:'#008c45'
    },
});

export default styles;