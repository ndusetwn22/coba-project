import React,{Component} from 'react'
import {Text, View,Icon, Content, Button, Thumbnail, Label, Card, CardItem, Body} from 'native-base'
import {
    TouchableOpacity,
    ScrollView,FlatList,
    Alert, Image, Dimensions, TextInput, Linking
} from "react-native";
import styles from './style'
import FlatListInvoice from './FlatListInvoice/FlatListInvoice';
import RNHTMLtoPDF from 'react-native-html-to-pdf';
import SweetAlert from 'react-native-sweet-alert';
import { cekKwitansiOrder } from '../../ListEpodAxios';
import Loading,{getProfile, setUrl, getUrl, decrypt} from '../../../../../../APIProp/ApiConstants'
import base64 from 'react-native-base64'
import Axios from 'axios';
import ModalUploadFaktur from '../FlatListPod/ModalUploadFaktur/ModalUploadFaktur';
import DocumentPicker from 'react-native-document-picker';
import storage from '@react-native-firebase/storage';
import moment from 'moment'
// import {WebView} from 'react-native-web'


class InvoiceView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            kodeTransporterInvoice: this.props.kodeTransporterInvoice,
            noInvoice: this.props.noInvoice,
            kodeSeri: '',
            kodeSeriDiganti: '',
            modalUploadFakturVisible: false,
            documentFileNameFaktur: 'Pilih Dokumen Faktur',
            documentSourceFaktur: null,
            urlFaktur: '',
            bolehProsesInvoice: false,
            //faktur revisi
            // listTripInvoice: this.props.listTripInvoice

        };
    }

    componentDidMount= async()=>{
        // console.log('id trans', this.props.profile.id_transporter)
        // console.log('id user', this.props.profile.id_user)
        // console.log('list tripnya', this.props.listTripInvoice)
        // console.log(this.props.kodeTransporterInvoice, this.props.noInvoice, this.props.cekSkbInvoice)
        this.funcSweetAlert('status', 'Harap assign biaya dahulu sebelum proses Invoice!', 'warning')
        // this.props.funcSweetAlert('status', 'Harap assign biaya dahulu sebelum proses Invoice!', 'warning')
        this.setState({kodeTransporterInvoice: this.props.kodeTransporterInvoice, noInvoice: this.props.noInvoice, cekSkb: this.props.cekSkbInvoice})
        // console.log('transporter', this.props.profileTransporter)
        //cek kalo skb == y --> boleh proses ,karena ga wajib upload faktur
        if (this.props.profileTransporter.skb_isvalid == 'Y') {
            this.setState({bolehProsesInvoice: true})
        }else{
            this.setState({bolehProsesInvoice: false})
        }
    } 

    numberWithCommas=(x)=>{
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      }

    funcSweetAlert = async(title, subTitle, style) =>{
        SweetAlert.showAlertWithOptions({
            title: title,
            subTitle: subTitle,
            confirmButtonTitle: 'OK',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: style,
            cancellable: true
          },
            callback => console.log('callback'));
    }

    prosesInvoice = async(jumlahTotal, ppn) => {
        if (this.state.kodeSeri == null || this.state.kodeSeri == "") { this.funcSweetAlert('status', 'Kode & No Seri Faktur Pajak Harus Diisi !', 'warning')}
        else if(this.state.bolehProsesInvoice == false) {this.funcSweetAlert('status','Harap upload faktur dahulu!','warning')}
        else{
        this.setState({isLoading: true})
        console.log('proses invoice')
        var _listTripInvoice = [];
        var _idTrip = []; //untuk cek query kwitansi di setiap order yg ada di trip;(((


        for (let i = 0; i < this.props.listTripInvoice.length; i++) {
            const element = this.props.listTripInvoice[i];
        
            // biayaLain = +biayaLain+ +this.props.listTripInvoice[i].BiayaInap == 0 ? 0 : parseInt(this.props.listTripInvoice[i].BiayaInap)+ +this.props.listTripInvoice[i].BiayaKuli == 0 ? 0 : parseInt(this.props.listTripInvoice[i].BiayaKuli)
            let x = {
                "NomorTrip" : this.props.listTripInvoice[i].IdTrip,
                "BiayaTrip" : this.props.listTripInvoice[i].BiayaTrip == 0 ? 0 : parseInt(this.props.listTripInvoice[i].BiayaTrip),
                "BiayaLain" : parseInt(this.props.listTripInvoice[i].BiayaKuli) + parseInt(this.props.listTripInvoice[i].BiayaInap) == 0 ? 0 : (parseInt(this.props.listTripInvoice[i].BiayaKuli) + parseInt(this.props.listTripInvoice[i].BiayaInap)),
                "BiayaKuli" : this.props.listTripInvoice[i].BiayaKuli == 0 ? 0 : parseInt(this.props.listTripInvoice[i].BiayaKuli),
                "BiayaInap" : this.props.listTripInvoice[i].BiayaInap == 0 ? 0 : parseInt(this.props.listTripInvoice[i].BiayaInap),
                "JumOrder"  : this.props.listTripInvoice[i].JumOrder,
            }
            _listTripInvoice.push(x)

            //id_ordernya dong;(
            let cekKwitansi = await cekKwitansiOrder(this.props.url, this.props.listTripInvoice[i].IdTrip)

            // console.log('cekKwitansi', cekKwitansi)
            await _idTrip.push(cekKwitansi)
        }

        //pake yg dari getProfile aja
        var invoice = {
            "IdTrans" : this.props.profile.id_transporter,
            "IdUser" : this.props.profile.id_user,
            "KodeTransporter" : this.props.kodeTransporterInvoice,
            "FotoLogo" : this.props.profileTransporter.foto_logo,
            "AlamatTrans" : this.props.profileTransporter.alamat,
            "NamaTrans" : this.props.profileTransporter.nama_transporter,
            "NoInvoice" : this.props.noInvoice,
            "FakturPajak" : this.state.kodeSeri,
            "FakturPajakDiganti" : this.state.kodeSeriDiganti,
            "Trip" : _listTripInvoice,
            "urlFaktur" : this.state.urlFaktur,
            "Total" : jumlahTotal,
            "PPN" : ppn,
        }
        // console.log('invoice', invoice)
        // console.log('y', _idTrip)
        // console.log('cekLanjut ga, kalo false lanjut', cekLanjut)
        let cekLanjut = _idTrip.includes(true)
        

        if (cekLanjut){
            // console.log('Alert Kwitansi ada yg kosong')
            this.funcSweetAlert('status', 'Harap upload semua kwitansi order!', 'warning')            
        }else{
            console.log('Lakukan request')
            let objJsonStr = JSON.stringify(invoice);
            let requestInvoice = base64.encode(objJsonStr);
            console.log(requestInvoice)
            console.log(objJsonStr)
            console.log('https://sandboxdev.enseval.com/transporter/MInvoiceEpod?param='+requestInvoice)

            var param = 'https://sandboxdev.enseval.com/transporter/MInvoiceEpod?param='+requestInvoice
    
            // Linking.openURL
            // aktivin disini kalo mau testing
            // this.props.pageInvoice(param)

            Linking.openURL('https://sandboxdev.enseval.com/transporter/MInvoiceEpod?param='+requestInvoice)
            this.props.backToMenu()

        }
        this.setState({isLoading: false})
        }   
    }

    toggleModalFaktur = async() => {
        this.setState({modalUploadFakturVisible: !this.state.modalUploadFakturVisible, documentFileNameFaktur: 'Pilih Dokumen Faktur', documentSourceFaktur: null, urlFaktur: ''});
    };

    pilihDokumenFaktur = async() => {
        try {
            const res = await DocumentPicker.pick({
            type: [DocumentPicker.types.pdf],
            });
            await this.setState({documentFileNameFaktur: res.name, documentSourceFaktur: res.uri})
            console.log(
            res.uri,
            res.type, // mime type
            res.name,
            res.size
            );
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
            // User cancelled the picker, exit any dialogs or menus and move on
            } else {
            this.funcSweetAlert('status', 'Failed upload Faktur','warning')
            throw err;
            }
        }

    }

    funcCombinedUploadFaktur = async() => {
        
        var namaDokumen = this.state.documentFileNameFaktur
        var uriDokumen = this.state.documentSourceFaktur

        if (namaDokumen != 'Pilih Dokumen Faktur' && uriDokumen != null) {
            //   insertFoto
            const FireBaseStorage = storage();
            //Insert firebase
            const documentSource = uriDokumen
            //pake ini untuk bikin folder ref
            // const documentStorageRef = FireBaseStorage.ref(`${no_invoice}/${namaDokumen}`)
            const documentStorageRef = FireBaseStorage.ref(`${this.state.noInvoice}/${namaDokumen}`)

            Promise.resolve(documentStorageRef.putFile(documentSource))
                .then((data)=>{
                    Promise.resolve(documentStorageRef.getDownloadURL())
                        .then((url)=> {
                            console.log('URL STORAGE', url)
                            this.setState({urlFaktur: url})
                            this.funcSweetAlert('Status', 'Sukses Upload Faktur, \n Silahkan Klik Proses Invoice' , 'success')
                            //kalo sukses state bolehProsess --> true
                            this.setState({modalUploadFakturVisible: !this.state.modalUploadFakturVisible, bolehProsesInvoice: true})
                        }).catch((error)=>{
                            this.funcSweetAlert('Status', 'Kesalahan Upload Faktur' , 'warning')
                            //kalo sukses state bolehProsess --> true
                            this.setState({modalUploadFakturVisible: !this.state.modalUploadFakturVisible, bolehProsesInvoice: false})
                            throw error
                        })
                    // this.funcSweetAlert('Status', 'Sukses Upload Faktur, \n Silahkan Klik Proses Invoice' , 'success')
                    // //kalo sukses state bolehProsess --> true
                    // this.setState({modalUploadFakturVisible: !this.state.modalUploadFakturVisible, bolehProsesInvoice: true})
                }).catch((error) => {   
                    console.log('catch error', error)          
                    this.funcSweetAlert('Status', 'Kesalahan Upload Faktur', 'warning')
                    this.setState({modalUploadFakturVisible: !this.state.modalUploadFakturVisible, bolehProsesInvoice: false})
            })
        }else{
            this.funcSweetAlert('Status', 'Failed Upload Faktur, Harap Pilih File Faktur Dahulu', 'warning')
        }        
    }

    render(){

        var widthPhone = Dimensions.get('window').width
        var heightPhone = Dimensions.get('window').height
        // console.log('skb', this.props.profileTransporter.skb_isvalid)

        var jumlahTotal = 0
        if (this.props.profileTransporter.skb_isvalid == 'Y') {
            var ppn = 0
        }else{
            var ppn = 0.1
        }
        var grandTotal = 0
        // console.log('render invoice view')

        for (let i = 0; i < this.props.listTripInvoice.length; i++) {
            const element = this.props.listTripInvoice[i];
            jumlahTotal = parseInt(jumlahTotal) + parseInt(this.props.listTripInvoice[i].BiayaTrip) + parseInt(this.props.listTripInvoice[i].BiayaKuli) + parseInt(this.props.listTripInvoice[i].BiayaInap)
        }
        ppn = ppn * parseInt(jumlahTotal)
        grandTotal = parseInt(jumlahTotal)+parseInt(ppn)
        // console.log('jumlahTotal', jumlahTotal)
        // console.log('ppn', ppn)
        // console.log('grandTotal', jumlahTotal+ppn)
       
        return(
            <>
                {/* <View>
                    <TouchableOpacity onPress={this.createPDF}>
                    <Text>Create PDF</Text>
                    </TouchableOpacity>
                </View> */}
                {/* <Content> */}
                    <View style={{flexDirection: 'column', marginLeft: 10}}>
                        <Image source={{uri: this.props.profileTransporter.foto_logo}} style={{width: '20%', height: '20%', resizeMode: 'contain', marginBottom: -15}}></Image>
                        <View style={{flexDirection: 'row'}}>
                            <View style={{flex: 1, marginTop: 5}}>
                                <Text note style={{fontSize: 12}}>{this.props.profileTransporter.nama_transporter}</Text>  
                            </View>
                            <View style={{flex:1, marginLeft: widthPhone-250, marginTop: -20}}>
                                <Text style={{fontSize: 20, fontWeight: 'bold'}}>INVOICE</Text>
                            </View>
                            {/* <Text style={{fontWeight: 'bold', fontSize: 20, marginLeft: widthPhone-200, marginTop: -20}}>INVOICE</Text> */}
                        </View> 

                        {/* <View style={{flexDirection: 'row'}}>
                            <View style={{flex: 1}}>
                                <Text note style={{fontSize: 12}}>{this.props.profileTransporter.alamat}</Text>
                            </View>
                            <View style={{flex: 1}}>
                                <Text></Text>
                            </View>
                        </View> */}

                        <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                            <View style={{flexDirection: 'column', margin: 5}}>
                                <Text note style={{textAlign: 'center', marginBottom: 5, fontSize: 10}}>No Pelanggan</Text>
                                <TextInput
                                    style={styles.textInputDisabled}
                                    underlineColorAndroid='rgba(0,0,0,0)'
                                    caretHidden={true}
                                    editable ={false}
                                    selectTextOnFocus = {false}
                                    // placeholder="PT INSTRANS"
                                    placeholderTextColor = "#CACCD7"
                                    selectionColor="#008c45"
                                    value = {this.state.kodeTransporterInvoice}
                                    
                                    />
                                {/* <Text style={{textAlign: 'center'}}>{this.props.profileTransporter.kode_transporter}</Text> */}
                            </View>
                            <View style={{flexDirection: 'column', margin: 5}}>
                                <Text note style={{textAlign: 'center', marginBottom: 5, fontSize: 10}}>No Invoice</Text>
                                <TextInput
                                    style={styles.textInputDisabled}
                                    caretHidden={true}
                                    editable ={false}
                                    selectTextOnFocus = {false}
                                    underlineColorAndroid='rgba(0,0,0,0)'
                                    // placeholder="Nama Pengguna"
                                    placeholderTextColor = "#CACCD7"
                                    selectionColor="#008c45"
                                    value = {this.state.noInvoice}
                        
                                />
                                {/* <Text style={{textAlign: 'center'}}>{this.props.profileTransporter.kode_transporter}</Text> */}
                            </View>
                        </View>

                        <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                            <View style={{flexDirection: 'column', margin: 5}}>
                            <Text note style={{textAlign: 'center', marginBottom: 5, fontSize: 10}}>Kode & No. Seri Faktur {'\n'}Pajak</Text>
                                <TextInput
                                    style={styles.textInput}
                                    underlineColorAndroid='rgba(0,0,0,0)'
                                    // placeholder="Kode & No Seri Faktur Pajak"
                                    placeholderTextColor = "#CACCD7"
                                    selectionColor="#008c45"
                                    onChangeText={input=>this.setState({kodeSeri: input})}
                                    // value = {this.props.profileTransporter.kode_transporter}
                                    />
                            </View>
                            <View style={{flexDirection: 'column', margin: 5}}>
                                <Text note style={{textAlign: 'center', marginBottom: 5, fontSize: 10}}>Kode & No. Seri Faktur {'\n'}Pajak Yang Diganti</Text>
                                <TextInput
                                    style={styles.textInput}
                                    underlineColorAndroid='rgba(0,0,0,0)'
                                    // placeholder="Kode & No Seri Faktur Pajak yang Diganti"
                                    placeholderTextColor = "#CACCD7"
                                    selectionColor="#008c45"
                                    onChangeText={input=>this.setState({kodeSeriDiganti: input})}
                                    // value = 'INV-ITS2020120831'
                                />
                            </View>
                        </View>

                        <View style={{flexDirection: 'row', marginTop: 10, padding: 2}}>
                            <View style={{flex: 1, flexDirection: 'column'}}>
                                {/* <Text note style={{fontWeight: 'bold', fontSize: 10}}>PEMESAN {'\n'}( {this.props.profileTransporter.nama_transporter} )</Text> */}
                                {/* <Text note style={{fontSize: 12}}>{this.props.profileTransporter.alamat}</Text> */}
                                <Text note style={{fontWeight: 'bold', fontSize: 10}}>PEMESAN {'\n'}PT Enseval Putera Megatrading Tbk.</Text>
                                {/* <Text note style={{fontSize: 10}}>Jl. Pulo Lentut No. 10 Kawasan Industri Pulo Gadung, Jakarta Timur</Text> */}
                            </View>
                            <View style={{flex: 1, flexDirection: 'column'}}>
                                {/* <Text note style={{fontWeight: 'bold', fontSize: 10}}>PEMBAYAR {'\n'}( {this.props.profileTransporter.nama_transporter} )</Text> */}
                                {/* <Text note style={{fontSize: 12}}>{this.props.profileTransporter.alamat}</Text> */}
                                <Text note style={{fontWeight: 'bold', fontSize: 10}}>PEMBAYAR {'\n'}PT Enseval Putera Megatrading Tbk.</Text>
                                {/* <Text note style={{fontSize: 10}}>Jl. Pulo Lentut No. 10 Kawasan Industri Pulo Gadung, Jakarta Timur</Text> */}
                            </View>
                        </View>

                        <View style={{flexDirection: 'row', marginTop: 5, padding: 2}}>
                            <View style={{flex: 1, flexDirection: 'column'}}>
                                {/* <Text note style={{fontWeight: 'bold', fontSize: 10}}>PEMBELI BARANG KENA PAJAK {'\n'}( {this.props.profileTransporter.nama_transporter} )</Text> */}
                                {/* <Text note style={{fontSize: 12}}>{this.props.profileTransporter.alamat}</Text> */}
                                <Text note style={{fontWeight: 'bold', fontSize: 10}}>PEMBELI BARANG KENA PAJAK {'\n'}PT Enseval Putera Megatrading Tbk.</Text>
                                {/* <Text note style={{fontSize: 10}}>Jl. Pulo Lentut No. 10 Kawasan Industri Pulo Gadung, Jakarta Timur</Text> */}
                            </View>
                            <View style={{flex: 1, flexDirection: 'column'}}>
                                {/* <Text note style={{fontWeight: 'bold', fontSize: 10}}>PENGUSAHA KENA PAJAK {'\n'}( {this.props.profileTransporter.nama_transporter} )</Text> */}
                                {/* <Text note style={{fontSize: 12}}>{this.props.profileTransporter.alamat}</Text> */}
                                <Text note style={{fontWeight: 'bold', fontSize: 10}}>PENGUSAHA KENA PAJAK {'\n'}PT Enseval Putera Megatrading Tbk.</Text>
                                {/* <Text note style={{fontSize: 10}}>Jl. Pulo Lentut No. 10 Kawasan Industri Pulo Gadung, Jakarta Timur</Text> */}
                            </View>
                        </View>
                   

                    </View>

                        <FlatListInvoice {...this.state} {...this.props}></FlatListInvoice>
                
                        <View style={{flexDirection: 'row', margin: 20, marginBottom: 0}}>
                            <View style={{flex: 1, flexDirection: 'column'}}>
                                <Text note style={{fontWeight: 'bold', fontSize: 10}}>Jumlah Total (Rp.) {jumlahTotal == null ? 0 : this.numberWithCommas(jumlahTotal)},-</Text>
                                <Text note style={{fontWeight: 'bold', fontSize: 10}}>Grand Total (Rp.) {grandTotal == null ? 0 : this.numberWithCommas(grandTotal)},-</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'column'}}>
                                <Text note style={{fontWeight: 'bold', fontSize: 10}}>PPN 10% (Rp.) {ppn == null ? 0 : this.numberWithCommas(ppn)},-</Text>
                            </View>
                            {/* <View style={{flex: 1, flexDirection: 'column'}}>
                                    <TouchableOpacity style={styles.labelPriceBoxStyle2} onPress={()=>this.prosesInvoice(jumlahTotal, ppn)}>
                                                <Text style={{fontSize: 10, textAlign: 'center', marginTop: 0, color: 'white'}}>Proses Invoice 
                                                </Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.labelPriceBoxStyle2} onPress={()=>this.prosesInvoice(jumlahTotal, ppn)}>
                                                <Text style={{fontSize: 10, textAlign: 'center', marginTop: 0, color: 'white'}}>Proses Invoice 
                                                </Text>
                                    </TouchableOpacity>
                            </View> */}
                        </View>

                        <View style={{flexDirection: 'row', margin: 5, marginLeft: 10, marginRight: 10}}>
                            {/* <View style={{flex: 1, flexDirection: 'column'}}> */}
                                    <TouchableOpacity style={styles.labelPriceBoxStyle3} onPress={()=>this.setState({modalUploadFakturVisible: !this.setState.modalUploadFakturVisible})}>
                                                <Text style={{fontSize: 10, textAlign: 'center', marginTop: 0, color: 'white'}}>Upload Faktur 
                                                </Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.labelPriceBoxStyle4} onPress={()=>this.prosesInvoice(jumlahTotal, ppn)}>
                                                <Text style={{fontSize: 10, textAlign: 'center', marginTop: 0, color: 'white'}}>Proses Invoice 
                                                </Text>
                                    </TouchableOpacity>
                            {/* </View> */}
                        </View>

                        <ModalUploadFaktur
                                {...this.state} {...this.props} 
                                toggleModalFaktur={this.toggleModalFaktur} 
                                pilihDokumenFaktur={this.pilihDokumenFaktur} 
                                funcCombinedUploadFaktur={this.funcCombinedUploadFaktur} 
                                lanjutBuatInvoice={this.lanjutBuatInvoice}
                        />
                    
                {/* </Content> */}
                {this.state.isLoading ? <Loading/> : false}
            </>
        );
    }
}

export default InvoiceView;