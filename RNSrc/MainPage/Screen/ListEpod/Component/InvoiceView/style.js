import { StyleSheet, Platform } from 'react-native';

const styles = StyleSheet.create({
  
    
  title:{
    fontSize: 18,
    fontWeight: 'bold', 
    textAlign: 'center', 
    marginTop: 15, 
    // marginBottom: 5, 
    color: '#7d8d8c'
  },

  viewTextInput:{
    flex: 1, 
    flexDirection: 'column'
  },

  label:{
    fontSize: 14, 
    marginLeft: 10, 
    marginTop: 10, 
    alignSelf: 'flex-start', 
    alignItems: 'flex-start'
  },

  textInputTransporter:{
    flex:1, 
    borderColor: 'gray', 
    borderWidth: 2, 
    borderRadius: 5, 
    marginTop: 10, 
    marginLeft: 20, 
    marginRight: 20, 
    height: 40, 
    backgroundColor: '#E9EDF2', 
    color: '#494A4A', 
    paddingLeft: 10, 
    paddingRight: 10, 
    fontSize: 14 
  },

  textInputDisabled:{
    borderColor: 'gray', 
    borderWidth: 2, 
    borderRadius: 5, 
    height: 35, 
    color: '#494A4A', 
    backgroundColor: '#E9EDF2', 
    paddingLeft: 10, 
    paddingRight: 10, 
    fontSize: 10,
    textAlign: 'center'
  },

  textInput:{
    borderColor: 'gray', 
    borderWidth: 2, 
    borderRadius: 5, 
    height: 35, 
    color: '#494A4A', 
    paddingLeft: 10, 
    paddingRight: 10, 
    fontSize: 10,
    textAlign: 'center'
  },

  viewButton:{
    flex: 1, 
    flexDirection: 'column'
  },

  button:{
    width: '89%', 
    // borderColor: 'gray', 
    // borderWidth: 2, 
    borderRadius: 5, 
    marginTop: 10, 
    marginLeft: 20, 
    marginRight: 20, 
    height: 40, 
    // color: '#494A4A',
    backgroundColor: '#008c45', 
    padding: 10 
  },
  labelPriceBoxStyle2:{
    // flex: 1,
    flexDirection: 'row',
    // backgroundColor: '#58d0de',
    backgroundColor: '#008c45',  
    // margin: 10, 
    marginTop: 5,
    borderRadius:10, 
    height: 20,
    // width: '50%',
    // width: 100, 
    shadowColor: "#000",
    alignItems:'center', 
    // alignSelf: 'center', 
    justifyContent: 'center',

    shadowOffset: {
        width: 0,
        height: 9,
    },
    shadowOpacity: 0.48,
    shadowRadius: 11.95,
    elevation: 1, 
    zIndex: 0
},

  labelPriceBoxStyle3:{
    flex: 1,
    flexDirection: 'row',
    // backgroundColor: '#58d0de',
    backgroundColor: '#F6B759',  
    // margin: 10, 
    marginLeft: 5, 
    marginRight: 5,
    borderRadius:10, 
    height: 30,
    // width: '50%',
    // width: 100, 
    shadowColor: "#000",
    alignItems:'center', 
    // alignSelf: 'center', 
    justifyContent: 'center',

    shadowOffset: {
        width: 0,
        height: 9,
    },
    shadowOpacity: 0.48,
    shadowRadius: 11.95,
    elevation: 1, 
    zIndex: 0
  },

  labelPriceBoxStyle4:{
    flex: 1,
    flexDirection: 'row',
    // backgroundColor: '#58d0de',
    backgroundColor: '#008c45',  
    // margin: 10, 
    marginLeft: 5, 
    marginRight: 5,
    borderRadius:10, 
    height: 30,
    // width: '50%',
    // width: 100, 
    shadowColor: "#000",
    alignItems:'center', 
    // alignSelf: 'center', 
    justifyContent: 'center',

    shadowOffset: {
        width: 0,
        height: 9,
    },
    shadowOpacity: 0.48,
    shadowRadius: 11.95,
    elevation: 1, 
    zIndex: 0
  },





});

export default styles;