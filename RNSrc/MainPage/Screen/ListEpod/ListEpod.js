import React,{Component} from 'react'
import {BackHandler,TouchableOpacity, Text,View} from "react-native";
import { useNavigation  } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
import CustomNavbar from '../../Header/Navbar/Navbar'
import Loading,{getProfile, setUrl, getUrl, decrypt} from '../../../../APIProp/ApiConstants'
import {Container} from 'native-base'
import {getListOnProcess, getListPod, getListInvoice, getListBayar, getListReject, getListOrder, getProfileTransporter, lihatDokumenPod, updateDokumenPOD, lihatDokumenKwitansi, updateKwitansi} from './ListEpodAxios'
import moment from 'moment';
import SweetAlert from 'react-native-sweet-alert';
import ListEpodPage from './ListEpodPage'
import FlatListOrder from './Component/FlatListOrder/FlatListOrder';
import InvoiceView from './Component/InvoiceView/InvoiceView';
import InvoiceWebView from './Component/InvoiceWebView/InvoiceWebView';
import messaging from '@react-native-firebase/messaging';
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import PushNotification from 'react-native-push-notification'
import NetInfo from "@react-native-community/netinfo";
import LostConnection from '../../Component/LostConnection';


class ListEpod extends Component {
    constructor(props) {
        super(props);
        this.state = {
          paramLogin:{
            username:'',
            password:'',
          },
          isLoading:false,
          url:null,
          profile: null,
          profileTransporter: null,
          profileStr: null,
          isConnected: null,
          isConnectedChange: null,
          listOnProcess: [],
          listPod: [],
          listInvoice: [],
          listBayar: [],
          listReject: [],
          startDate: moment().startOf('month'),
          endDate: moment().endOf('month'),
          displayedDate: moment(),
          pageListOrder: false,
          listOrder: [],
          listTripInvoice: [],
          pageBuatInvoice: false,
          kodeTransporterInvoice: '',
          noInvoice: '',
          cekSkbInvoice: '',
          urlFakturInvoice: '',
          pagePdfInvoice: false,
          urlWebView: '',
          imagesPod: []
          // modalUploadFakturVisible: false,

        };
      }
    
      componentDidMount= async()=>{
        await this.setState({isLoading:true});
        await setUrl();
        await getUrl().then(async(result)=>{
          this.setState({url:result})
        });
        await this.funcGetProfile();
        await this.cekConnection();
        console.log('re run')
        // console.log('url select', this.state.url.select)
        var startOfMonth = moment().startOf('month')
        var endOfMonth   = moment().endOf('month')

        // var _listOnProcess = []
        var _listOnProcess = await getListOnProcess(this.state.url, this.state.profile.id_transporter, this.state.profile.id_user, this.state.startDate, this.state.endDate)
        var _listPod = await getListPod(this.state.url, this.state.profile.id_transporter, this.state.profile.id_user, this.state.startDate, this.state.endDate, this.state.profile.startdate_epod)
        var _listInvoice = await getListInvoice(this.state.url, this.state.profile.id_transporter, this.state.profile.id_user, this.state.startDate, this.state.endDate)
        var _listBayar = await getListBayar(this.state.url, this.state.profile.id_transporter, this.state.profile.id_user, this.state.startDate, this.state.endDate)
        var _listReject = await getListReject(this.state.url, this.state.profile.id_transporter, this.state.profile.id_user, this.state.startDate, this.state.endDate)
        await this.setState({isLoading:false, listOnProcess: _listOnProcess, listPod: _listPod, listInvoice: _listInvoice, listBayar: _listBayar, listReject: _listReject});
        
        // await this.pushNotif();

      }

      cekConnection = async()=>{
        // Cek Connection
        NetInfo.addEventListener(state => {
          if (this.state.isConnected == null) {
            this.setState({isConnected: state.isConnected})
          }
          if (state.isConnected == false) {
            this.setState({isConnected: state.isConnected, isConnectedChange: state.isConnected})
          }
          if (state.isConnected == true) {
            this.setState({isConnectedChange: state.isConnected})
          }      
          console.log("Is connected?", state.isConnected);
        });
      }

      restartConnection = async() =>{
        console.log('Restart Connection', this.state.isConnected)
        if (this.state.isConnected == false && this.state.isConnectedChange == true) {
          console.log('HARUSNYA INI OFF', this.state.isConnected)
          this.props.navigation.replace('MainRouter', {screen: 'ListEpod'})
        }
      }

      funcGetProfile=async()=>{
        await getProfile().then(result=>{
          if(result!=null){
            this.setState({profileStr:result})
          }
        })
        if(this.state.profileStr==null){
          this.props.navigation.replace('Login')
        }
        var tmpStr = await decrypt(this.state.profileStr);
        await this.setState({profile:JSON.parse(tmpStr)})
        await this.setState({hak_akses: JSON.parse(tmpStr).hak_akses}) // tambah ini utk hak_akses dan state
        // console.log('test', JSON.parse(tmpStr).startdate_epod)
        // console.log('profile', this.state.profile)
        // console.log('profile', JSON.parse(tmpStr).hak_akses)
      }

      funcSweetAlert = async(title, subTitle, style) =>{
        SweetAlert.showAlertWithOptions({
            title: title,
            subTitle: subTitle,
            confirmButtonTitle: 'OK',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: style,
            cancellable: true
          },
            callback => {
              console.log('callback') 
              this.backToMenu()
            }
          );
    }


      setStartEndDate=async(s,e)=>{
        await this.setState({
          startDate:moment(s, 'YYYY-MM-DD'),
          endDate:moment(e, 'YYYY-MM-DD')
        })
      }
  
      onSearchButtonClicked=async()=>{
        await this.setState({isLoading:true});
        
        var startDate =this.state.startDate;
        var endDate;
  
        if(this.state.endDate==null){
          endDate = this.state.startDate;
        }else{
          endDate = this.state.endDate;
        }

        //ada perubahan date range, jadi query ulangnya di Tab Bayar dan Reject aja

        // var _listOnProcess = await getListOnProcess(this.state.url, this.state.profile.id_transporter, this.state.profile.id_user, this.state.startDate, this.state.endDate)
        // var _listPod = await getListPod(this.state.url, this.state.profile.id_transporter, this.state.profile.id_user, this.state.startDate, this.state.endDate)
        // var _listInvoice = await getListInvoice(this.state.url, this.state.profile.id_transporter, this.state.profile.id_user, this.state.startDate, this.state.endDate)
        var _listBayar = await getListBayar(this.state.url, this.state.profile.id_transporter, this.state.profile.id_user, this.state.startDate, this.state.endDate)
        var _listReject = await getListReject(this.state.url, this.state.profile.id_transporter, this.state.profile.id_user, this.state.startDate, this.state.endDate)

        await this.setState({
          isLoading:false,
          // listOnProcess: _listOnProcess,
          // listPod: _listPod,
          // listInvoice: _listInvoice,
          listBayar: _listBayar,
          listReject: _listReject
        });
      }

      backToList=async()=>{
        await this.setState({isLoading:true});
        // var _listUser = await getlistUser(this.state.url, this.state.profile.id_transporter)
        await this.setState({isLoading:false});
      }

      backToListPod=async()=>{
        await this.setState({isLoading: true, pageListOrder:true});
        // var _listUser = await getlistUser(this.state.url, this.state.profile.id_transporter)
        setTimeout(() => {       
          this.setState({isLoading: false, pageListOrder:false, listOrder: []});
        }, 100)

        // await this.setState({isLoading: false, pageListOrder:false, listOrder: []});
      }

      onPodSelected=async(value)=>{
        await this.setState({isLoading:true, pageListOrder: false});
        var _listOrder = await getListOrder(this.state.url, this.state.profile.id_transporter, this.state.profile.id_user, value.id)
        await this.setState({isLoading:false, pageListOrder: true, listOrder: _listOrder});
      }

      onDokumenPODSelected=async(value)=>{
        await this.setState({isLoading:true});
        var _temp = await lihatDokumenPod(this.state.url, value)
        console.log('TEMP', _temp)

        // var _tempArray=[]
        // if (_temp[0].dokumen_pod != null && _temp[0].dokumen_pod != '') {
        //   var _temp1 = {}
        //   _temp1.dokumen_pod = _temp[0].dokumen_pod
        //   _tempArray.push(_temp1)
        // }
        // if (_temp[0].dokumen_pod_2 != null && _temp[0].dokumen_pod_2 != '') {
        //   var _temp2 = {}
        //   _temp2.dokumen_pod = _temp[0].dokumen_pod_2
        //   _tempArray.push(_temp2)
        // }
        // if (_temp[0].dokumen_pod_3 != null && _temp[0].dokumen_pod_3 != '') {
        //   var _temp3 = {}
        //   _temp3.dokumen_pod = _temp[0].dokumen_pod_3
        //   _tempArray.push(_temp3)
        // }
        // if (_temp[0].dokumen_pod_4 != null && _temp[0].dokumen_pod_4 != '') {
        //   var _temp4 = {}
        //   _temp4.dokumen_pod = _temp[0].dokumen_pod_4
        //   _tempArray.push(_temp4)
        // }
        // if (_temp[0].dokumen_pod_5 != null && _temp[0].dokumen_pod_5 != '') {
        //   var _temp5 = {}
        //   _temp5.dokumen_pod = _temp[0].dokumen_pod_5
        //   _tempArray.push(_temp5)
        // }

        

        console.log('new temp', _temp)
        await this.setState({isLoading:false, imagesPod: _temp});
        return _temp
      }


      onUpdateDokumenPOD = async(url, id, user_id, fileName, Source) =>{
        await this.setState({isLoading: true})
        let _temp = await updateDokumenPOD(url, id, user_id, fileName, Source)
        await this.setState({isLoading: false})
        return await _temp
      }

      onDokumenKwitansiSelected = async(value) =>{
        await this.setState({isLoading: true})
        var _temp = await lihatDokumenKwitansi(this.state.url, value)
        console.log('temp', _temp)
        await this.setState({isLoading: false})
        return _temp

      }

      onUpdateKwitansi = async(url, id, id_order, user_id, fileName, Source) =>{
        await this.setState({isLoading: true})
        let _temp = await updateKwitansi(url, id, id_order, user_id, fileName, Source)
        // let _temp = await updateDokumenPOD(url, id, user_id, fileName, Source)
        await this.setState({isLoading: false})
        return await _temp
      }

      gantiValue = async(value, index)=>{
        // ganti value checkbox
        let temp = this.state.listPod
        temp[index].isChecked = value
        this.setState({
            listPod: temp
        })
        console.log('podList', this.state.listPod)
    }

      gantiValueBiayaInap = async(value, index)=>{
      // ganti value checkbox
      let temp = this.state.listTripInvoice
      temp[index].BiayaInap = value
      this.setState({
          listTripInvoice: temp
      })
      console.log('ganti value biaya inap', this.state.listTripInvoice)
    }

      gantiValueBiayaKuli = async(value, index)=>{
        // ganti value checkbox
        let temp = this.state.listTripInvoice
        temp[index].BiayaKuli = value
        this.setState({
            listTripInvoice: temp
        })
        console.log('ganti value biaya kuli', this.state.listTripInvoice)
      }


    pageBuatInvoice = async(listTrip, noInvoice, kodeTransporter, cekSkb, urlFaktur) =>{
      await this.setState({isLoading: true, pageListOrder: false, pageBuatInvoice: false })
      var _profileTransporter = await getProfileTransporter(this.state.url, this.state.profile.id_transporter)
      // console.log('master transporter', _profileTransporter)
      // console.log('listTrip dari parent:', listTrip)
      await this.setState({isLoading: false, pageListOrder: false, pageBuatInvoice: true, profileTransporter: _profileTransporter, listTripInvoice: listTrip, noInvoice: noInvoice, kodeTransporterInvoice: kodeTransporter, cekSkbInvoice: cekSkb, urlFakturInvoice: urlFaktur})
    }

    backToMenu = async() =>{
      console.log('back to menu')
      await this.setState({isLoading:true});
      await setUrl();
      await getUrl().then(async(result)=>{
        this.setState({url:result})
      });
      await this.funcGetProfile();

      // console.log('url select', this.state.url.select)
      var startOfMonth = moment().startOf('month')
      var endOfMonth   = moment().endOf('month')

      await this.setState({listOnProcess: [], listPod: [], listInvoice: [], listBayar: [], listReject: []});
      var _listOnProcess = await getListOnProcess(this.state.url, this.state.profile.id_transporter, this.state.profile.id_user, this.state.startDate, this.state.endDate)
      var _listPod = await getListPod(this.state.url, this.state.profile.id_transporter, this.state.profile.id_user, this.state.startDate, this.state.endDate, this.state.profile.startdate_epod)
      var _listInvoice = await getListInvoice(this.state.url, this.state.profile.id_transporter, this.state.profile.id_user, this.state.startDate, this.state.endDate)
      var _listBayar = await getListBayar(this.state.url, this.state.profile.id_transporter, this.state.profile.id_user, this.state.startDate, this.state.endDate)
      var _listReject = await getListReject(this.state.url, this.state.profile.id_transporter, this.state.profile.id_user, this.state.startDate, this.state.endDate)
      await this.setState({pageListOrder: false, pageBuatInvoice: false})
      await this.setState({isLoading:false, listOnProcess: _listOnProcess, listPod: _listPod, listInvoice: _listInvoice, listBayar: _listBayar, listReject: _listReject});
    }

    pageInvoice = async(url) => {
      await this.setState({isLoading: true, pageBuatInvoice: false, urlWebView: url})
      await this.setState({isLoading: false, pageBuatInvoice: false, pagePdfInvoice: true})
    }

    backToListWithoutLoad = async() => {
      await this.setState({pageBuatInvoice: false, pageListOrder: false, pagePdfInvoice: false})
    }

    refreshContent = async(_listOnProcess, _listPod, _listInvoice, _listBayar, _listReject) => {
      await this.setState({isLoading: true})
      await this.setState({isLoading:false, listOnProcess: _listOnProcess, listPod: _listPod, listInvoice: _listInvoice, listBayar: _listBayar, listReject: _listReject});
    }

    pushNotif = async () => {
      // Must be outside of any component LifeCycle (such as `componentDidMount`).
      PushNotification.configure({
        // (optional) Called when Token is generated (iOS and Android)
        onRegister: function (token) {
          console.log("TOKEN:", token);
        },
      
        // (required) Called when a remote is received or opened, or local notification is opened
        onNotification: async (notification) => {
          console.log("NOTIFICATION:", notification);
          //Utk dpt Notif
          // process the notification
          //mau apa disini
          //disini setelah ada userInteraction/di klik
          if (notification.userInteraction) {
            console.log('DO SOMETHING NOTIFICATION IN EPOD') 
            this.props.navigation.navigate('ListTrip',{
              screen: 'ListTrip',
              id_trip: notification.data.id_trip,
              id: notification.data.id
            })
          }
      
          // (required) Called when a remote is received or opened, or local notification is opened
          notification.finish(PushNotificationIOS.FetchResult.NoData);
        },

        
        // // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
        // onAction: function (notification) {
        //   console.log("ACTION:", notification.action);
        //   console.log("NOTIFICATION ACTION:", notification);
      
        //   // process the action

        //   console.log('waiwawa')
        // },
      
        // // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
        // onRegistrationError: function(err) {
        //   console.error(err.message, err);
        // },
      
        // // IOS ONLY (optional): default: all - Permissions to register.
        // permissions: {
        //   alert: true,
        //   badge: true,
        //   sound: true,
        // },
      
        // Should the initial notification be popped automatically
        // default: true
        popInitialNotification: true,
      
        /**
         * (optional) default: true
         * - Specified if permissions (ios) and token (android and ios) will requested or not,
         * - if not, you must call PushNotificationsHandler.requestPermissions() later
         * - if you are not using remote notification or do not have Firebase installed, use this:
         *     requestPermissions: Platform.OS === 'ios'
         */
        requestPermissions: true,
      });
    }



      render(){
        return(
          <>
            <Container>

            {this.state.pageListOrder ?
              <CustomNavbar title={'Daftar Order'}/> : 
             this.state.pageBuatInvoice ?
             <CustomNavbar buttonBack={this.backToListWithoutLoad} title={'Buat Invoice'}/> :
              <CustomNavbar title={'E-POD'}/>
            }

              {/* <CustomNavbar title={'EPOD'}/> */}

            {
            this.state.isConnected == false?
              <LostConnection restartConnection={this.restartConnection}/> 
            :
            this.state.pageListOrder ?       
                  <FlatListOrder 
                  {...this.state}
                  backToListPod={this.backToListPod}
                  onDokumenPODSelected={this.onDokumenPODSelected}
                  onUpdateDokumenPOD={this.onUpdateDokumenPOD}
                  onDokumenKwitansiSelected={this.onDokumenKwitansiSelected}
                  onUpdateKwitansi={this.onUpdateKwitansi}
                  />                  
                  :

                  this.state.pageBuatInvoice ?

                  //kalo gasalah ... itu utk ngambil semua state&props dari sebelumnya atau dari page ini
                  <InvoiceView {...this.state} {...this.props} gantiValueBiayaInap={this.gantiValueBiayaInap} gantiValueBiayaKuli={this.gantiValueBiayaKuli} 
                  backToMenu={this.backToMenu} pageInvoice={this.pageInvoice}
                  funcSweetAlert={this.funcSweetAlert}
                  /> :

                  this.state.pagePdfInvoice ? 

                  <InvoiceWebView urlWebView={this.state.urlWebView}
                  funcSweetAlert={this.funcSweetAlert}
                  />

                  :
              
                  <ListEpodPage
                  {...this.state}
                  {...this.props}
                  setStartEndDate={this.setStartEndDate}
                  onSearchButtonClicked={this.onSearchButtonClicked}
                  gantiValue={this.gantiValue} //contoh props2x bisa jadi 3x karena listEpodPage
                  onPodSelected={this.onPodSelected}
                  pageBuatInvoice={this.pageBuatInvoice}
                  refreshContent={this.refreshContent}
                  
                />
            }
              

              


            </Container>
            {this.state.isLoading ? <Loading/> : false}
          </>
        );
      }
}

ListEpodFunc= (props) => {
  const navigation = useNavigation();
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      BackHandler.addEventListener("hardwareBackPress", ()=>props.backAction('ListEpod', navigation));
    });

    return unsubscribe;
  }, [navigation]);

  return <ListEpod navigation={navigation}/>;
}

export default ListEpodFunc;