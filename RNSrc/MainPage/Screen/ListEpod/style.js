import { StyleSheet, Platform } from 'react-native';

const styles1 = StyleSheet.create({
  
    
  title:{
    fontSize: 18,
    fontWeight: 'bold', 
    textAlign: 'center', 
    marginTop: 15, 
    // marginBottom: 5, 
    color: '#7d8d8c'
  },

  viewTextInput:{
    flex: 1, 
    flexDirection: 'column'
  },

  label:{
    fontSize: 14, 
    marginLeft: 10, 
    marginTop: 10, 
    alignSelf: 'flex-start', 
    alignItems: 'flex-start'
  },

  textInputTransporter:{
    flex:1, 
    borderColor: 'gray', 
    borderWidth: 2, 
    borderRadius: 5, 
    marginTop: 10, 
    marginLeft: 20, 
    marginRight: 20, 
    height: 40, 
    backgroundColor: '#E9EDF2', 
    color: '#494A4A', 
    paddingLeft: 10, 
    paddingRight: 10, 
    fontSize: 14 
  },

  textInput:{
    flex:1, 
    borderColor: 'gray', 
    borderWidth: 2, 
    borderRadius: 5, 
    marginTop: 10, 
    marginLeft: 20, 
    marginRight: 20, 
    height: 40, 
    color: '#494A4A', 
    paddingLeft: 10, 
    paddingRight: 10, 
    fontSize: 14
  },

  viewButton:{
    // flex: 1, 
    flexDirection: 'column'
  },

  button:{
    width: '94%', 
    // borderColor: 'gray', 
    // borderWidth: 2, 
    borderRadius: 5, 
    marginTop: 10, 
    marginLeft: 10, 
    marginRight: 10, 
    height: 40, 
    // color: '#494A4A',
    backgroundColor: '#008c45', 
    padding: 10 
  },
  viewPager: {
    flex: 1,
  },
  bgColor: {
      backgroundColor: '#008c45'
  },
  color: {
      color: '#008c45',
  }, 
  titleMargin:{
      marginLeft: 20,
      marginRight: 20
  },
  textStyle: {
      color: '#008c45'
  }





});

export default styles1;