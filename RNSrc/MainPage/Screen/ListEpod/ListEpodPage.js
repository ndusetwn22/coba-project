import React,{Component} from 'react'
import {View, ScrollView, SafeAreaView, StyleSheet, Alert, Linking, Image, ImageBackground, Dimensions} from "react-native";
import styles1 from './style'
import {Text,Button, Content, Container, Label, Icon} from 'native-base'
// import ViewPager from '@react-native-community/viewpager';
import {PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator} from 'react-native-best-viewpager';
import ColorIcon from '../Report/Component/ColorIcon/ColorIcon';
import FlatListOnProcess from './Component/FlatListOnProcess/FlatListOnProcess';
import FlatListPod from './Component/FlatListPod/FlatListPod'
import FlatListInvoice from './Component/FlatListInvoice/FlatListInvoice'
import FlatListBayar from './Component/FlatListBayar/FlatListBayar'
import FlatListReject from './Component/FlatListReject/FlatListReject'
import ModalCalendar from './Component/DateRange/ModalCalendar'
import DateRange from './Component/DateRange/DateRange'
import DocumentPicker from 'react-native-document-picker';
import storage from '@react-native-firebase/storage';
import SweetAlert from 'react-native-sweet-alert';
import ModalSKB from './Component/ModalSKB/ModalSKB';
import ModalUploadFaktur from './Component/FlatListPod/ModalUploadFaktur/ModalUploadFaktur'
import {uploadSKB, cekDokumenSKB, lihatDokumenSKB} from './ListEpodAxios'

const width = Dimensions.get('window').width;


// import FlatListUser from './Component/FlatListUser/FlatListUser'
var logoPin = require('../../../../Assets/pin2.png')
var listKosong = require('../../../../Assets/list_kosong.png');
var listKosong2 = require('../../../../Assets/list_kosong2.png');
var listKosong3 = require('../../../../Assets/list_kosong3.png');


class ListEpodPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paramSearch:'',
            modalCalenderVisible:false,
            modalSkbVisible: false,
            documentFileName: 'Pilih Dokumen SKB',
            documentSource: null,
        };
    }

    componentDidMount= async()=>{
        // console.log('epodpage', this.props.profile.id_transporter, this.props.url, this.props.profile.id_user)

    }

    _renderTitleIndicator() {
        return <PagerTitleIndicator 
                titles={['On Process', 'POD', 'Invoice', 'Bayar', 'Reject']} 
                selectedBorderStyle={styles.bgColor}
                selectedItemTextStyle={styles.color}
                // itemStyle={styles.titleMargin} 
                // itemTextStyle={styles.textStyle}
                trackScroll={true}/>;
    }
 
    _renderDotIndicator() {
        return <PagerDotIndicator pageCount={3} />;
    }

    onSelectDate=async (start,end)=>{
        await this.props.setStartEndDate(start,end);
        await this.setState({modalCalenderVisible: false})
        await this.props.onSearchButtonClicked();
    }

    openModalCalendar=(typeTrip)=>{
        this.setState({modalCalenderVisible: true})
    }

    closeModal=()=>{
        this.setState({modalCalenderVisible: false})
    }

    toggleModalSkb = () => {
        this.setState({modalSkbVisible: !this.state.modalSkbVisible, documentFileName: 'Pilih Dokumen SKB', documentSource: null});
      };

    
    funcSweetAlert = async(title, subTitle, style) =>{
        SweetAlert.showAlertWithOptions({
            title: title,
            subTitle: subTitle,
            confirmButtonTitle: 'OK',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: style,
            cancellable: true
          },
            callback => console.log('callback'));
    }

    pilihDokumen = async() => {
          //Baru dijagain pdf aja
          // Pick a single file

        var _cekDokumenSKB = await cekDokumenSKB(this.props.url, this.props.profile.id_transporter, this.props.profile.id_user)
    
            try {
                const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.pdf],
                });
                await this.setState({documentFileName: res.name, documentSource: res.uri})
                console.log(
                res.uri,
                res.type, // mime type
                res.name,
                res.size
                );
            } catch (err) {
                if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
                } else {
                this.funcSweetAlert('status', 'Failed upload SKB','warning')
                throw err;
                }
            }

            if (_cekDokumenSKB.foto_skb != null || _cekDokumenSKB.skb_isvalid == 'Y') { 
                this.funcSweetAlert('status','SKB sudah terlampir, tekan hide untuk membatalkan', 'warning')
            }

    }

    funcCombinedUpload = async() => {
        
        var namaDokumen = this.state.documentFileName
        var uriDokumen = this.state.documentSource

        if (namaDokumen != 'Pilih Dokumen SKB' && uriDokumen != null) {

            //QUERY UPLOAD DULU
            //Upload ke Firebasenya bisa abis setState
            var _uploadSKB = await uploadSKB(this.props.url, this.props.profile.id_transporter, this.props.profile.id_user, namaDokumen, uriDokumen)



            this.funcSweetAlert('status', _uploadSKB[1] , _uploadSKB[2])
        }else{
            this.funcSweetAlert('status', 'Failed upload SKB','warning')
        }
        
        this.setState({modalSkbVisible: !this.state.modalSkbVisible, documentFileName: 'Pilih Dokumen SKB', documentSource: null});
    }

    lihatDokumen = async() =>{
        // console.log('lihat dokumen')
        var _lihatDokumenSKB = await lihatDokumenSKB(this.props.url, this.props.profile.id_transporter, this.props.profile.id_user)

        if (_lihatDokumenSKB.foto_skb == null || _lihatDokumenSKB.foto_skb == "") {
            this.funcSweetAlert('status', 'File SKB Kosong','warning')
        }else{
            Linking.openURL(_lihatDokumenSKB.foto_skb)
        }

        this.setState({modalSkbVisible: !this.state.modalSkbVisible})
    }


    render(){
        return(
          <>
          {/* <Content> */}
                <View style={styles1.viewButton}>   
                  {/* <Label style={styles1.label}>Foto Pengemudi</Label> */}
                    <Button
                      style={styles1.button}
                      onPress={this.toggleModalSkb}  
                    >
                      <Text uppercase={false} style={{fontSize: 14}} numberOfLines={1}>Upload Dokumen SKB</Text>
                      <Icon type='MaterialCommunityIcons' name='file-document' />
                    </Button>
                </View>

                
                {/* <View style={{marginBottom: 10}}>
                    <DateRange openModalCalendar={this.openModalCalendar} {...this.props}/>
                </View> */}
        
                <IndicatorViewPager
                    indicator={this._renderTitleIndicator()}
                    style={{flex: 1, flexDirection: 'column-reverse', backgroundColor:'white', marginTop: 10}}
                    
                >
                    <View>
                        {/* <FlatListOnProcess {...this.state} {...this.props}></FlatListOnProcess> */}
                        {this.props.listOnProcess.length > 0 ? 
                            <FlatListOnProcess {...this.state} {...this.props}></FlatListOnProcess>
                
                        : 
                            <View>
                                <Image resizeMode='cover' source={listKosong} style={{marginTop: 20, width: '50%', height: '50%', opacity: 0.5, alignSelf: 'center'}}></Image>
                                <Text note style={{marginBottom: width, textAlign: 'center', fontStyle:'italic'}}>List kosong</Text>
                            </View>
                            // <Text note style={{flex: 1, textAlign: 'center', fontStyle:'italic', marginTop: 50}}>List kosong</Text>
                        }
                    </View>
                    <View>
                        {this.props.listPod.length > 0 ? 
                            <FlatListPod {...this.state} {...this.props} funcSweetAlert={this.funcSweetAlert}></FlatListPod>
                        : 
                            <View>
                                <Image resizeMode='cover' source={listKosong} style={{marginTop: 20, width: '50%', height: '50%', opacity: 0.5, alignSelf: 'center'}}></Image>
                                <Text note style={{marginBottom: width, textAlign: 'center', fontStyle:'italic'}}>List kosong</Text>
                            </View>
                            // <Text note style={{flex: 1, textAlign: 'center', fontStyle:'italic', marginTop: 50}}>List kosong</Text>
                        }
                    </View>
                    <View>
                        {this.props.listInvoice.length > 0 ? 
                            <FlatListInvoice {...this.state} {...this.props}></FlatListInvoice>
                            
                        : 
                            <View>
                                <Image resizeMode='cover' source={listKosong} style={{marginTop: 20, width: '50%', height: '50%', opacity: 0.5, alignSelf: 'center'}}></Image>
                                <Text note style={{marginBottom: width, textAlign: 'center', fontStyle:'italic'}}>List kosong</Text>
                            </View>
                            // <Text note style={{flex: 1, textAlign: 'center', fontStyle:'italic', marginTop: 50}}>List kosong</Text>
                        }
                    </View>
                    <View>
                        
                            <View>
                                <DateRange openModalCalendar={this.openModalCalendar} {...this.props}/>
                            </View>
                            <FlatListBayar {...this.state} {...this.props}></FlatListBayar>

                        {this.props.listBayar.length == 0 ? 
                            <View>
                                <Image resizeMode='cover' source={listKosong} style={{ width: '50%', height: '50%', opacity: 0.5, alignSelf: 'center'}}></Image>
                                <Text note style={{marginBottom: width, textAlign: 'center', fontStyle:'italic'}}>List kosong</Text>
                            </View>
                        :null}                    
                    </View>
                    <View>
                        
                            <View>
                                <DateRange openModalCalendar={this.openModalCalendar} {...this.props}/>
                            </View>
                            <FlatListReject {...this.state} {...this.props}></FlatListReject>
                        {this.props.listReject.length == 0 ? 
                            // <Text note style={{flex: 1, textAlign: 'center', fontStyle:'italic'}}>List kosong</Text>
                            <View>
                                <Image resizeMode='cover' source={listKosong} style={{ width: '50%', height: '50%', opacity: 0.5, alignSelf: 'center'}}></Image>
                                <Text note style={{marginBottom: width, textAlign: 'center', fontStyle:'italic'}}>List kosong</Text>
                            </View>
                        : null }  
                    </View>
                </IndicatorViewPager>
                {/* </Content> */}

                <ModalCalendar {...this.state} {...this.props} onSelectDate={this.onSelectDate} closeModal={this.closeModal}/>
                <ModalSKB {...this.state} {...this.props} toggleModalSkb={this.toggleModalSkb} pilihDokumen={this.pilihDokumen} funcCombinedUpload={this.funcCombinedUpload} lihatDokumen={this.lihatDokumen}></ModalSKB>

            {/* Assets */}
            {/* <View>
                <Image resizeMode='cover' source={listKosong} style={{marginTop: 20, width: '50%', height: '50%', opacity: 0.5, alignSelf: 'center'}}></Image>
                <Text note style={{marginBottom: width, textAlign: 'center', fontStyle:'italic'}}>List kosong</Text>
            </View> */}
            
          </>
        );
    }
}

const styles = StyleSheet.create({
    viewPager: {
      flex: 1,
    },
    bgColor: {
        backgroundColor: '#008c45'
    },
    color: {
        color: '#008c45',
    }, 
    titleMargin:{
        marginLeft: 20,
        marginRight: 20
    },
    textStyle: {
        color: '#008c45'
    }
  });

export default ListEpodPage;