import Axios from 'axios';
import storage from '@react-native-firebase/storage';
import {encrypt, firebase} from '../../../../APIProp/ApiConstants';

  export const getListOnProcess = async (url,trans_id, uid, startDate, endDate) =>{

    startDate = startDate.format('YYYY-MM-DD')
    endDate = endDate.format('YYYY-MM-DD')

    // and mmt.tanggal_pengiriman >= '`+startDate+`'
    // and mmt.tanggal_pengiriman <= '`+endDate+`'

    let dataQry = `
        select mmt.id as id_tripnya ,mmt.id_trip, mmt.status, to_char(mmt.tanggal_pengiriman, 'dd Mon yyyy') as tanggal_pengiriman, to_char(mmt.jam_pengiriman, 'hh24:mi') as jam_pengiriman, asal.nama as asal, tujuan.nama as tujuan, 
        kendaraan.jenis_kendaraan as kendaraan,
            (select sum(harga_transporter) as harga_transporter from mt_shipper_order mso join mt_trip_order_transaction mtot
            on mtot.order_id = mso.id
            where mtot.trip_id = mmt.id
            group by mtot.trip_id) as harga,
            (select (coalesce(sum(biaya_kuli_muat),0) + coalesce(sum(biaya_kuli_bongkar),0)) as biaya_kuli from mt_shipper_order mso join mt_trip_order_transaction mtot
            on mtot.order_id = mso.id
            where mtot.trip_id = mmt.id
            group by mtot.trip_id) as biaya_kuli,
            (select (coalesce(sum(biaya_kuli_muat),0)) as biaya_kuli_muat from mt_shipper_order mso 
						join mt_trip_order_transaction mtot
						on mtot.order_id = mso.id
						where mtot.trip_id = mmt.id
						group by mtot.trip_id) as biaya_kuli_muat,
						(select (coalesce(sum(biaya_kuli_bongkar),0)) as biaya_kuli_bongkar from mt_shipper_order mso 
						join mt_trip_order_transaction mtot
						on mtot.order_id = mso.id
						where mtot.trip_id = mmt.id
						group by mtot.trip_id) as biaya_kuli_bongkar  
        from mt_master_trip mmt 
        left join mt_master_kota as asal 
        on mmt.asal_id = asal.id
        left join mt_master_kota as tujuan
        on mmt.tujuan_id = tujuan.id
        left join mt_master_jenis_kendaraan as kendaraan 
        on mmt.jenis_kendaraan_id = kendaraan.id
        where mmt.transporter_id = `+trans_id+`
        and (mmt.status = 'accepted' or mmt.status = 'confirm')
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('list onProcess', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
      // console.log('error', err)
    });
  }



  export const getListPod = async (url,trans_id, uid, startDate, endDate, startdate_epod) =>{

    console.log('startdate_epod', startdate_epod)
    startDate = startDate.format('YYYY-MM-DD')
    endDate = endDate.format('YYYY-MM-DD')

    // and tr.tanggal_pengiriman between '`+startDate+`' and ('`+endDate+`'::date)

    let dataQry = `
        select distinct tr.id,tr.id_trip, tr.status, concat( to_char(tr.tanggal_pengiriman, 'dd Mon yyyy'),' ', 
        to_char(tr.jam_pengiriman, 'HH24:MI')) jadwal, 
        (select count(*) as jumlah_order from mt_trip_order_transaction mtot where mtot.trip_id = tr.id) jumlah_order,		
        (select nama from mt_master_kota k where tr.asal_id = k.id) asal, 	
        (select nama from mt_master_kota k where tr.tujuan_id = k.id) tujuan, jk.jenis_kendaraan,		
        (select coalesce(sum(ord.harga_transporter),0) from mt_shipper_order ord where ord.order_reference = o.order_reference) harga,		
        (select coalesce(sum(ord.biaya_kuli_bongkar),0 ) kuli_bongkar from mt_shipper_order ord 
        where ord.order_reference = o.order_reference 	),		
        (select coalesce(sum(ord.biaya_kuli_muat),0) kuli_muat from mt_shipper_order ord where ord.order_reference = o.order_reference) from mt_master_trip tr			
        left join mt_trip_order_transaction tod on tod.trip_id = tr.id		
        left join mt_shipper_order o on o.id = tod.order_id 	
        left join mt_master_jenis_kendaraan jk on jk.id = tr.jenis_kendaraan_id 	
        where transporter_id = '`+trans_id+`' 
        and 	tr.status = 'close' 
        and  
        tr.id not in 	(select det.trip_id from 	
        mt_detail_epod det left join mt_master_epod epod on det.master_epod_id = epod.id  where epod.status < 5 ) 
        and tr.tanggal_pengiriman::date >= '`+startdate_epod+`'
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('list POD', x)

      for (let i = 0; i < x.length; i++) {
        const element = x[i];
        var biaya_kuli = {}
        x[i].biaya_kuli = parseInt(x[i].kuli_muat) + parseInt(x[i].kuli_bongkar)
      }

      if(x==""){
        return [];
      }else{
        for(let i=0;i<x.length;i++){
          x[i].isChecked = false;
          // x[i].amount_due_remaining = Math.ceil(0.98 * x[i].amount_due_remaining);
        }
        return x;
      }
    }).catch(err=>{
      return [];
      // console.log('error', err)
    });
  }


  export const getListInvoice = async (url,trans_id, uid, startDate, endDate) =>{
    //Invoice tanpa adjust date
    startDate = startDate.format('YYYY-MM-DD')
    endDate = endDate.format('YYYY-MM-DD')

    let dataQry = `
      select *, mme.id as epod_id, (to_char(mme.due_date, 'dd Mon yyyy') || ' 12:00') as jatuh_tempo, mmes.status as status_epod,
      to_char(mme.create_date, 'dd Mon yyyy HH24:MI') as create_date2,
      (select count(*) as jumlah_trip from mt_detail_epod mde where mde.master_epod_id = mme.id)
      from mt_master_epod mme
      left join mt_master_epod_status mmes
      on mme.status = mmes.id 
      where mme.transporter_id = `+trans_id+`
      and mme.status = 2
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('list Invoice', x)

      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
      // console.log('error', err)
    });
  }


  export const getListBayar = async (url,trans_id, uid, startDate, endDate) =>{

    startDate = startDate.format('YYYY-MM-DD')
    endDate = endDate.format('YYYY-MM-DD')

    let dataQry = `
      select *, mme.id as epod_id, (to_char(mme.due_date, 'dd Mon yyyy') || ' 12:00') as jatuh_tempo, mmes.status as status_epod,
      to_char(mme.create_date, 'dd Mon yyyy HH24:MI') as create_date2,
      (select count(*) as jumlah_trip from mt_detail_epod mde where mde.master_epod_id = mme.id)
      from mt_master_epod mme
      left join mt_master_epod_status mmes
      on mme.status = mmes.id 
      where mme.transporter_id = `+trans_id+`
      and mme.status = 4
      and mme.create_date::date >= '`+startDate+`'
      and mme.create_date::date <= '`+endDate+`'
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('list Bayar', x)

      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
      // console.log('error', err)
    });
  }


  export const getListReject = async (url,trans_id, uid, startDate, endDate) =>{

    startDate = startDate.format('YYYY-MM-DD')
    endDate = endDate.format('YYYY-MM-DD')

    let dataQry = `
      select *, mme.id as epod_id, mre.keterangan  as keterangan, (to_char(mme.due_date, 'dd Mon yyyy') || ' 12:00') as jatuh_tempo, mmes.status as status_epod,
      to_char(mme.create_date, 'dd Mon yyyy HH24:MI') as create_date2,
      (select count(*) as jumlah_trip from mt_detail_epod mde where mde.master_epod_id = mme.id)
      from mt_master_epod mme
      left join mt_master_epod_status mmes
      on mme.status = mmes.id
      left join  mt_reason_epod mre
			on mme.reason_id = mre.id  
      where mme.transporter_id = `+trans_id+`
      and mme.status = 5
      and mme.create_date::date >= '`+startDate+`'
      and mme.create_date::date <= '`+endDate+`' 
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('list Reject', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
      // console.log('error', err)
    });
  }


  export const getListOrder = async (url,trans_id, uid, id_trip) =>{


    let dataQry = `
      select mso.id , mso.id_order, msos.nama as status_order, to_char(mso.jadwal_penjemputan, 'dd Mon yyyy HH24:MI') as jadwal_penjemputan, mso.kwitansi_kuli_muat, mso.kwitansi_kuli_bongkar, mso.dokumen_pod, mso.dokumen_pod_2, mso.dokumen_pod_3, mso.dokumen_pod_4, mso.dokumen_pod_5
      from mt_shipper_order mso
      left join mt_shipper_order_status msos
      on mso.status = msos.id 
      where mso.id in (select mtot.order_id from mt_trip_order_transaction mtot where mtot.trip_id = `+id_trip+`)
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      // console.log('list Order dari id_trip', x)

      // x.dokumenGambar = []

      for (let i = 0; i < x.length; i++) {
        const element = x[i];
        x[i].dokumenGambar = []
        x[i].dokumenGambar.push(x[i].kwitansi_kuli_bongkar == null ? firebase.linkStorage+"DokumenKosong.png?alt=media" : x[i].kwitansi_kuli_bongkar)
        x[i].dokumenGambar.push(x[i].kwitansi_kuli_muat == null ? firebase.linkStorage+"DokumenKosong.png?alt=media" : x[i].kwitansi_kuli_muat)       
        
        //dokumen POD
        x[i].dokumenPOD = []
        if (x[i].dokumen_pod != null && x[i].dokumen_pod != "") {
            x[i].dokumenPOD.push(x[i].dokumen_pod)

            if (x[i].dokumen_pod_2 != null && x[i].dokumen_pod_2 != "") {
              x[i].dokumenPOD.push(x[i].dokumen_pod_2)

              if (x[i].dokumen_pod_3 != null && x[i].dokumen_pod_3 != "") {
                x[i].dokumenPOD.push(x[i].dokumen_pod_3)

                if (x[i].dokumen_pod_4 != null && x[i].dokumen_pod_4 != "") {
                  x[i].dokumenPOD.push(x[i].dokumen_pod_4)

                  if (x[i].dokumen_pod_5 != null && x[i].dokumen_pod_5 != "") {
                    x[i].dokumenPOD.push(x[i].dokumen_pod_5)

                  }

                }

              }

            }

          }else{x[i].dokumenPOD.push(firebase.linkStorage+"DokumenKosong.png?alt=media")}

        

        

        
        
        

         // x[i].dokumenPOD.push(x[i].dokumen_pod == null || x[i].dokumen_pod == "" ? firebase.linkStorage+"DokumenKosong.png?alt=media" : x[i].dokumen_pod)
        // x[i].dokumenPOD.push(x[i].dokumen_pod_2 == null || x[i].dokumen_pod_2 == "" ? firebase.linkStorage+"DokumenKosong.png?alt=media" : x[i].dokumen_pod_2)
        // x[i].dokumenPOD.push(x[i].dokumen_pod_3 == null || x[i].dokumen_pod_3 == "" ? firebase.linkStorage+"DokumenKosong.png?alt=media" : x[i].dokumen_pod_3)
        // x[i].dokumenPOD.push(x[i].dokumen_pod_4 == null || x[i].dokumen_pod_4 == "" ? firebase.linkStorage+"DokumenKosong.png?alt=media" : x[i].dokumen_pod_4)
        // x[i].dokumenPOD.push(x[i].dokumen_pod_5 == null || x[i].dokumen_pod_5 == "" ? firebase.linkStorage+"DokumenKosong.png?alt=media" : x[i].dokumen_pod_5)
      }
      console.log('list Order dari id_trip', x)

      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
      // console.log('error', err)
    });
  }


  export const getProfileTransporter = async (url,trans_id) =>{


    let dataQry = `
      select * from mt_master_transporter mmt 
      where mmt.id = `+trans_id+`
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data[0];
      console.log('profileTransporter : ', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
      // console.log('error', err)
    });
  }


  export const uploadSKB = async (url,trans_id, user_id, namaDokumen, sourceDokumen) =>{

    let tempDokumen = firebase.linkStorage+'uploadSKB%2F'+namaDokumen+'?alt=media'

    //   insertFoto
    const FireBaseStorage = storage();

    //Insert firebase
    const documentSource = sourceDokumen
    //pake ini untuk bikin folder ref
    const documentStorageRef = FireBaseStorage.ref(`DokumenSKB/${trans_id}/${namaDokumen}`)
   //  Promise.resolve(documentStorageRef.putFile(documentSource));
    return await documentStorageRef.putFile(documentSource)
     .then(async(response)=>{
       return await documentStorageRef.getDownloadURL()
         .then(async(urlDownload)=>{
              tempDokumen=urlDownload; 
              var c = tempDokumen.split("&token");
              tempDokumen = c[0]
              //  var res = ['status', 'Sukses Upload SKB','success']
              //  return res;
              let dataQry = `
                update mt_master_transporter 
                set 
                foto_skb = '`+tempDokumen+`', 
                skb_isvalid = 'N', 
                update_date = now(), 
                update_by = `+user_id+` 
                where id = `+trans_id+`
              `
              let x = await encrypt(dataQry);
              return await Axios.post(url.select,{
                query: x
              }).then(async(value)=>{
                let x = value.data.data;
                console.log(x)
                var res = ['status', 'Sukses Upload SKB','success']
                return res;
              }).catch(err=>{
                console.log('error', err)
                var res = ['status', 'Gagal Upload SKB','warning']
                return res;
              });

         ;}).catch((error)=>{
          var res = ['status', 'Gagal Upload SKB (Firebase)','warning']
          return res;
         })
     }).catch((error) => {   
         console.log('catch error', error)    
         var res = ['status', 'Gagal Upload SKB (Firebase)','warning']
         return res;
     })
  }


  export const cekDokumenSKB = async (url,trans_id, user_id) =>{


    let dataQry = `
      select * from mt_master_transporter mmt
      where id = `+trans_id+`
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data[0];
      console.log('cek dokumen SKB : ', x.foto_skb, x.skb_isvalid)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      console.log('error', err)
      return [];
    });
  }

  export const lihatDokumenSKB = async (url,trans_id, user_id) =>{

    let dataQry = `
      select * from mt_master_transporter mmt
      where id = `+trans_id+`
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data[0];
      console.log('Lihat Dokumen SKB : ', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      console.log('error', err)
      return [];
    });
  }

  export const cekKwitansiOrder = async (url,id_trip, trans_id, user_id) =>{


    let dataQry = `
      select mso.id, mso.kwitansi_kuli_muat, mso.kwitansi_kuli_bongkar 
      from mt_shipper_order mso
      where mso.id in (select mtot.order_id from mt_trip_order_transaction mtot where mtot.trip_id = `+id_trip+`)
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;

      let _temp = []
      //true = salah satunya ada yg null --> maka alert
      //false = semua kwitansi ada isinya --> lanjut proses

      //harusnya ini bener
      for (let i = 0; i < x.length; i++) {
        const element = x[i];
        if (x[i].kwitansi_kuli_muat == null || x[i].kwitansi_kuli_muat == "" || x[i].kwitansi_kuli_bongkar == null || x[i].kwitansi_kuli_bongkar == "") {
          _temp.push(true)        
        }else{
          _temp.push(false)
        }
      }

console.log(_temp)
console.log(_temp.length)


      let _temp2 = _temp.includes(true)
      console.log('temp2', _temp2)
      return _temp2

      // console.log('cek kwitansi muat : ', x.kwitansi_kuli_muat)
      // console.log('cek kwitansi bongkar : ', x.kwitansi_kuli_bongkar)
      // if (x.kwitansi_kuli_muat == null || x.kwitansi_kuli_muat == "" || x.kwitansi_kuli_bongkar == null || x.kwitansi_kuli_bongkar == "") {
      //     return false        
      // }else{
      //   return true
      // }
    }).catch(err=>{
      console.log('error', err)
      return false;
    });
  }


  export const getListDetailOrder = async (url,id_trip, trans_id, user_id) =>{


    let dataQry = `
      select mso.id, mso.id_order, mso.kwitansi_kuli_muat, mso.kwitansi_kuli_bongkar 
      from mt_shipper_order mso
      where mso.id in (select mtot.order_id from mt_trip_order_transaction mtot where mtot.trip_id = `+id_trip+`)
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('list order', x)

      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      console.log('error', err)
      return [];
    });
  }

  export const updateKwitansiMuat_ = async (url,id, user_id, trans_id, FileName, Source_) =>{

    let tempKwitansi = firebase.linkStorage+FileName+'?alt=media'

      const FireBaseStorage = storage();
  
  
      //Insert firebase foto
      const fotoSource = Source_.uri
      const fotoStorageRef = FireBaseStorage.ref(`FotoKwitansi/Muat/${FileName}`)
      // Promise.resolve(fotoStorageRef.putFile(fotoSource))
      return await fotoStorageRef.putFile(fotoSource)
        .then(async(response)=>{
          return await fotoStorageRef.getDownloadURL()
            .then(async(urlDownload)=>{
                  tempKwitansi=urlDownload; 
                  var c = tempKwitansi.split("&token");
                  tempKwitansi = c[0]
                  

                    let dataQry = `
                      update mt_shipper_order 
                      set 
                      kwitansi_kuli_muat = '`+tempKwitansi+`',
                      update_date = now(), 
                      update_by = `+user_id+` 
                      where id = `+id+`
                    `
                    let x = await encrypt(dataQry);
                    return await Axios.post(url.select,{
                      query: x
                    }).then(async(value)=>{
                      let x = value.data.data;
                      // console.log(x)

                      // console.log('sukses query', value)
                      //   insertFoto
                      var res = ['status', 'Sukses Upload Kwitansi','success']
                      return res;



                    }).catch(err=>{
                      console.log('error', err)
                      var res = ['status', 'Gagal Upload Kwitansi','warning']
                      return res;
                    });

            ;}).catch((error)=>{
              var res = ['status', 'Gagal Upload Kwitansi (Firebase)','warning']
              return res;
              })
        }).catch((error) => {   
            console.log('catch error', error)    
            var res = ['status', 'Gagal Upload Kwitansi (Firebase)','warning']
            return res;
        })
  }

  export const updateKwitansiBongkar_ = async (url,id, user_id, trans_id, FileName, Source_) =>{

    let tempKwitansi = firebase.linkStorage+FileName+'?alt=media'

    const FireBaseStorage = storage();
  
  
      //Insert firebase foto
      const fotoSource = Source_.uri
      const fotoStorageRef = FireBaseStorage.ref(`FotoKwitansi/Bongkar/${FileName}`)
      // Promise.resolve(fotoStorageRef.putFile(fotoSource))
      return await fotoStorageRef.putFile(fotoSource)
        .then(async(response)=>{
          return await fotoStorageRef.getDownloadURL()
            .then(async(urlDownload)=>{
                  tempKwitansi=urlDownload; 
                  var c = tempKwitansi.split("&token");
                  tempKwitansi = c[0]

                  let dataQry = `
                    update mt_shipper_order 
                    set 
                    kwitansi_kuli_bongkar = '`+tempKwitansi+`',
                    update_date = now(), 
                    update_by = `+user_id+` 
                    where id = `+id+`
                  `
                  let x = await encrypt(dataQry);
                  return await Axios.post(url.select,{
                    query: x
                  }).then(async(value)=>{
                    let x = value.data.data;
                    var res = ['status', 'Sukses Upload Kwitansi','success']
                    return res;
                  }).catch(err=>{
                    console.log('error', err)
                    var res = ['status', 'Gagal Upload Kwitansi','warning']
                    return res;
                  });
            ;}).catch((error)=>{
              var res = ['status', 'Gagal Upload Kwitansi (Firebase)','warning']
              return res;
              })
        }).catch((error) => {   
            console.log('catch error', error)    
            var res = ['status', 'Gagal Upload Kwitansi (Firebase)','warning']
            return res;
        })

  }

  export const updateDokumenPOD = async (url,id, user_id, FileName, Source_, indexDokumen) =>{

    // let tempDokumen = firebase.linkStorage+FileName+'?alt=media'

      const FireBaseStorage = storage();

      let index = indexDokumen
      
      console.log('filename', FileName)
      


      //Insert firebase foto
      const fotoSource = Source_.uri
      const fotoStorageRef = FireBaseStorage.ref(`FotoPOD/${FileName}`)
      // Promise.resolve(fotoStorageRef.putFile(fotoSource))
      return await fotoStorageRef.putFile(fotoSource)
        .then(async(response)=>{
          return await fotoStorageRef.getDownloadURL()
            .then(async(urlDownload)=>{
                  let tempDokumen=urlDownload; 
                  var c = tempDokumen.split("&token");
                  tempDokumen = c[0]
                  
                  let dataQry = ''
                  if (index == 1) {
                      dataQry = `
                      update mt_shipper_order 
                      set 
                      dokumen_pod = '`+tempDokumen+`',
                      update_date = now(), 
                      update_by = `+user_id+`
                      where id = `+id+`
                    `
                  }else if(index == 2){
                      dataQry = `
                      update mt_shipper_order 
                      set 
                      dokumen_pod_2 = '`+tempDokumen+`',
                      update_date = now(), 
                      update_by = `+user_id+`
                      where id = `+id+`
                    `
                  }else if(index == 3){
                      dataQry = `
                      update mt_shipper_order 
                      set 
                      dokumen_pod_3 = '`+tempDokumen+`',
                      update_date = now(), 
                      update_by = `+user_id+`
                      where id = `+id+`
                    `
                  }else if(index == 4){
                      dataQry = `
                      update mt_shipper_order 
                      set 
                      dokumen_pod_4 = '`+tempDokumen+`',
                      update_date = now(), 
                      update_by = `+user_id+`
                      where id = `+id+`
                    `
                  }else{
                      dataQry = `
                      update mt_shipper_order 
                      set 
                      dokumen_pod_5 = '`+tempDokumen+`',
                      update_date = now(), 
                      update_by = `+user_id+`
                      where id = `+id+`
                    `
                  }
                    




                    let x = await encrypt(dataQry);
                    return await Axios.post(url.select,{
                      query: x
                    }).then(async(value)=>{
                      let x = value.data.data;
                      // console.log(x)

                      // console.log('sukses query', value)
                      //   insertFoto
                      var res = ['status', 'Sukses Upload Dokumen','success']
                      return res;



                    }).catch(err=>{
                      console.log('error', err)
                      var res = ['status', 'Gagal Upload Dokumen','warning']
                      return res;
                    });

            ;}).catch((error)=>{
              console.log('catch error', error)    
              var res = ['status', 'Gagal Upload Dokumen (Firebase)','warning']
              return res;
              })
        }).catch((error) => {   
            console.log('catch error', error)    
            var res = ['status', 'Gagal Upload Dokumen (Firebase)','warning']
            return res;
        })


    
  }

  export const lihatDokumenPod = async (url,id) =>{

    let dataQry = `
      select  mso.id, 
              mso.id_order, 
              mso.dokumen_pod, 
              mso.dokumen_pod_2, 
              mso.dokumen_pod_3, 
              mso.dokumen_pod_4, 
              mso.dokumen_pod_5 
      from mt_shipper_order mso 
      where mso.id = '`+id+`'
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;

      for (let i = 0; i < x.length; i++) {
 
        //dokumen POD
        x[i].dokumenPOD = []
        if (x[i].dokumen_pod != null && x[i].dokumen_pod != "") {
            x[i].dokumenPOD.push(x[i].dokumen_pod)

            if (x[i].dokumen_pod_2 != null && x[i].dokumen_pod_2 != "") {
              x[i].dokumenPOD.push(x[i].dokumen_pod_2)

              if (x[i].dokumen_pod_3 != null && x[i].dokumen_pod_3 != "") {
                x[i].dokumenPOD.push(x[i].dokumen_pod_3)

                if (x[i].dokumen_pod_4 != null && x[i].dokumen_pod_4 != "") {
                  x[i].dokumenPOD.push(x[i].dokumen_pod_4)

                  if (x[i].dokumen_pod_5 != null && x[i].dokumen_pod_5 != "") {
                    x[i].dokumenPOD.push(x[i].dokumen_pod_5)

                  }

                }

              }

            }

          }else{x[i].dokumenPOD.push(firebase.linkStorage+"DokumenKosong.png?alt=media")}
        }

        console.log('Lihat Dokumen POD : ', x)

      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      console.log('error', err)
      return [];
    });
  }

  export const lihatDokumenKwitansi = async (url,id) =>{

    let dataQry = `
      select  mso.id, 
              mso.id_order, 
              mso.kwitansi_kuli_muat,
              mso.kwitansi_kuli_bongkar
      from mt_shipper_order mso 
      where mso.id = '`+id+`'
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;

      // for (let i = 0; i < x.length; i++) {
 
      //   x[i].dokumenKwitansi = []
      //   x[i].dokumenKwitansi.push(x[i].kwitansi_kuli_bongkar == null ? firebase.linkStorage+"DokumenKosong.png?alt=media" : x[i].kwitansi_kuli_bongkar)
      //   x[i].dokumenKwitansi.push(x[i].kwitansi_kuli_muat == null ? firebase.linkStorage+"DokumenKosong.png?alt=media" : x[i].kwitansi_kuli_muat)       
        
      // }

        console.log('Lihat Dokumen Kwitansi : ', x)

      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      console.log('error', err)
      return [];
    });
  }

  export const updateKwitansi = async (url, id, id_order, user_id, FileName, Source_) =>{

    let tempKwitansi = firebase.linkStorage+FileName+'?alt=media'

      const FireBaseStorage = storage();
  
  
      //Insert firebase foto
      const fotoSource = Source_.uri
      var fotoStorageRef = null

      if (FileName == 'Kwitansi Muat') {
        fotoStorageRef = FireBaseStorage.ref(`FotoKwitansi/${id_order}/Muat/${FileName}`)
      }else{
        fotoStorageRef = FireBaseStorage.ref(`FotoKwitansi/${id_order}/Bongkar/${FileName}`)
      }
      // const fotoStorageRef = FireBaseStorage.ref(`FotoKwitansi/Muat/${FileName}`)
      // Promise.resolve(fotoStorageRef.putFile(fotoSource))
      return await fotoStorageRef.putFile(fotoSource)
        .then(async(response)=>{
          return await fotoStorageRef.getDownloadURL()
            .then(async(urlDownload)=>{
                  tempKwitansi=urlDownload; 
                  var c = tempKwitansi.split("&token");
                  tempKwitansi = c[0]
                  
                    let dataQry = ''

                    if (FileName == 'Kwitansi Muat') {
                      dataQry = `
                        update mt_shipper_order 
                        set 
                        kwitansi_kuli_muat = '`+tempKwitansi+`',
                        update_date = now(), 
                        update_by = `+user_id+` 
                        where id = `+id+`
                      `
                    }else{
                      dataQry = `
                        update mt_shipper_order 
                        set 
                        kwitansi_kuli_bongkar = '`+tempKwitansi+`',
                        update_date = now(), 
                        update_by = `+user_id+` 
                        where id = `+id+`
                      `
                    }

                    

                    let x = await encrypt(dataQry);
                    return await Axios.post(url.select,{
                      query: x
                    }).then(async(value)=>{
                      let x = value.data.data;
                      // console.log(x)

                      // console.log('sukses query', value)
                      //   insertFoto
                      var res = ['status', 'Sukses Upload Kwitansi','success']
                      return res;



                    }).catch(err=>{
                      console.log('error', err)
                      var res = ['status', 'Gagal Upload Kwitansi','warning']
                      return res;
                    });

            ;}).catch((error)=>{
              var res = ['status', 'Gagal Upload Kwitansi (Firebase)','warning']
              return res;
              })
        }).catch((error) => {   
            console.log('catch error', error)    
            var res = ['status', 'Gagal Upload Kwitansi (Firebase)','warning']
            return res;
        })
  }