import Axios from 'axios';
import {encrypt, firebase} from '../../../../../APIProp/ApiConstants';
//firebase
import { utils } from '@react-native-firebase/app';
import storage from '@react-native-firebase/storage';


  export const insertDriver = async (url, namaPengemudi, role, noHp, email, password, status, trid, uid, fotoFileName, ktpFileName, simFileName, refId, noKtp, noSim, fotoSource_, ktpSource_, simSource_) =>{


    // let tempFotoDriver = firebase.linkStorage+fotoFileName+'?alt=media'
    // let tempFotoKtp = firebase.linkStorage+ktpFileName+'?alt=media'
    // let tempFotoSim = firebase.linkStorage+simFileName+'?alt=media'
    var tempFotoDriver = ''
    var tempFotoKtp = ''
    var tempFotoSim = ''

    //   insertFoto
    const FireBaseStorage = storage();

    // Insert DRIVER
    if (role == 'DRIVER') {

      //     let dataQry = `
      //     insert into mt_master_user
      //     (nama, hp, email, password, role, hak_akses, active, shipper_id , transporter_id , create_date , create_by , update_date, update_by)
      //     values ('`+namaPengemudi+`', '`+noHp+`', '`+email+`', epm_encrypt('`+password+`'), '`+role+`', 'USER', '`+status+`', 0, `+trid+`, now(), `+uid+`, now(), `+uid+`)
      //     returning *
      // `

            let dataQry = `
                select * from mobiletra_insertuserdriver('`+namaPengemudi+`', '`+noHp+`', '`+email+`', '`+password+`', '`+role+`', '`+status+`', `+trid+`, `+uid+`)
            `    
        console.log('query insert driver', dataQry)
          let x = await encrypt(dataQry);
          return await Axios.post(url.select,{
            query: x
          }).then(async(value)=>{
            let returnValue = value.data.data[0];
            
            console.log('sukses query user', returnValue)
          
                const fotoSource = fotoSource_.uri
                const fotoStorageRef = FireBaseStorage.ref(`FotoDriver/${returnValue.id_user}/${fotoFileName}`)
                // Promise.resolve(fotoStorageRef.putFile(fotoSource));
                return await fotoStorageRef.putFile(fotoSource)
                  .then(async(response)=>{
                    return await fotoStorageRef.getDownloadURL()      
                    .then(async(urlDownload)=>{
                          tempFotoDriver = urlDownload
                          var c = tempFotoDriver.split("&token");
                          tempFotoDriver = c[0]

                          const ktpSource = ktpSource_.uri
                          const ktpStorageRef = FireBaseStorage.ref(`FotoDriver/${returnValue.id_user}/${ktpFileName}`)
                          return await ktpStorageRef.putFile(ktpSource)
                            .then(async(response)=>{
                              return await ktpStorageRef.getDownloadURL()      
                                .then(async(urlDownload)=>{
                                      tempFotoKtp = urlDownload
                                      var d = tempFotoKtp.split("&token");
                                      tempFotoKtp = d[0]
                                
                                      const simSource = simSource_.uri
                                      const simStorageRef = FireBaseStorage.ref(`FotoDriver/${returnValue.id_user}/${simFileName}`)
                                      return await simStorageRef.putFile(simSource)
                                        .then(async(response)=>{
                                          return await simStorageRef.getDownloadURL()      
                                            .then(async(urlDownload)=>{
                                                  tempFotoSim = urlDownload
                                                  var e = tempFotoSim.split("&token");
                                                  tempFotoSim = e[0]

                                                  //query disini
                                                  console.log('sim', tempFotoSim)

                                                  // let dataQry = `
                                                  //   insert into mt_driver_info
                                                  //   (user_id, external_id, no_ktp, no_sim, foto_driver, foto_ktp, foto_sim, create_date, create_by, update_date, update_by)
                                                  //   values ('`+returnValue.id_user+`', '`+refId+`', '`+noKtp+`', '`+noSim+`', '`+tempFotoDriver+`', '`+tempFotoKtp+`', '`+tempFotoSim+`', now(), `+uid+`, now(), `+uid+`)
                                                  // `
                                                  let dataQry = `
                                                      select * from mobiletra_insertuserdriverinfo('`+returnValue.id_user+`', '`+refId+`', '`+noKtp+`', '`+noSim+`', '`+tempFotoDriver+`', '`+tempFotoKtp+`', '`+tempFotoSim+`', `+uid+`)
                                                  `  
                                                  console.log('dataquery driverinfo', dataQry)
                                                  let x = await encrypt(dataQry);
                                                  return await Axios.post(url.select,{
                                                    query: x
                                                  }).then(async(value)=>{
                                                    let y = value.data.data;
                                              
                                                    console.log('sukses query', value)
                                              
                                                    return ['Sukses tambah pengemudi!', 'success'];
                                              
                                                  }).catch(err=>{
                                                  //   return [];
                                                    console.log('error', err)
                                                    return ['Gagal tambah pengemudi!', 'warning'];
                                                  });

                                            }).catch((error)=>{
                                              return ['Gagal upload sim pengemudi!', 'warning'];      
                                            })
                                        }).catch((error) => {   
                                            console.log('catch error', error)    
                                            return ['Gagal upload sim pengemudi!', 'warning'];      
                                        })
                                }).catch((error)=>{
                                  return ['Gagal upload ktp pengemudi!', 'warning'];      
                                })

                            }).catch((error) => {   
                                console.log('catch error', error)    
                                return ['Gagal upload ktp pengemudi!', 'warning'];      
                              })
                    }).catch((error)=>{
                          return ['Gagal upload foto pengemudi!', 'warning'];      
                        })
                  }).catch((error) => {   
                      console.log('catch error', error)    
                      return ['Gagal upload foto pengemudi!', 'warning'];      
                  })
      
            // return ['Sukses tambah pengemudi!', 'success'];
      
          }).catch(err=>{
          //   return [];
            console.log('error', err)
            return ['Gagal tambah pengemudi!', 'warning'];
          });
    }

    
    // INSERT KERANI
    else {
  //     let dataQry = `
  //     insert into mt_master_user
  //     (nama, hp, email, password, role, hak_akses, active, shipper_id , transporter_id , create_date , create_by , update_date, update_by)
  //     values ('`+namaPengemudi+`', '`+noHp+`', '`+email+`', epm_encrypt('`+password+`'), '`+role+`', 'USER', '`+status+`', 0, `+trid+`, now(), `+uid+`, now(), `+uid+`)
  //     returning *
  // `

          let dataQry = `
                select * from mobiletra_insertuserdriver('`+namaPengemudi+`', '`+noHp+`', '`+email+`', '`+password+`', '`+role+`', '`+status+`', `+trid+`, `+uid+`)
            `    
        console.log('query insert kerani', dataQry)

      let x = await encrypt(dataQry);
      return await Axios.post(url.select,{
        query: x
      }).then(async(value)=>{
        let returnValue = value.data.data[0];
  
        console.log('sukses query user', returnValue)
      
            const fotoSource = fotoSource_.uri
            const fotoStorageRef = FireBaseStorage.ref(`FotoDriver/${returnValue.id_user}/${fotoFileName}`)
            // Promise.resolve(fotoStorageRef.putFile(fotoSource));
            return await fotoStorageRef.putFile(fotoSource)
              .then(async(response)=>{
                return await fotoStorageRef.getDownloadURL()      
                .then(async(urlDownload)=>{
                      tempFotoDriver = urlDownload
                      var c = tempFotoDriver.split("&token");
                      tempFotoDriver = c[0]

                      const ktpSource = ktpSource_.uri
                      const ktpStorageRef = FireBaseStorage.ref(`FotoDriver/${returnValue.id_user}/${ktpFileName}`)
                      return await ktpStorageRef.putFile(ktpSource)
                        .then(async(response)=>{
                          return await ktpStorageRef.getDownloadURL()      
                            .then(async(urlDownload)=>{
                                  tempFotoKtp = urlDownload
                                  var d = tempFotoKtp.split("&token");
                                  tempFotoKtp = d[0]

                                              //query disini
                                              // console.log('sim', tempFotoSim)

                                          //     let dataQry = `
                                          //     insert into mt_driver_info
                                          //     (user_id, external_id, no_ktp, no_sim, foto_driver, foto_ktp, foto_sim, create_date, create_by, update_date, update_by)
                                          //     values ('`+returnValue.id_user+`', '`+refId+`', '`+noKtp+`', '`+noSim+`', '`+tempFotoDriver+`', '`+tempFotoKtp+`', '`+tempFotoSim+`', now(), `+uid+`, now(), `+uid+`)
                                          // `
                                              let dataQry = `
                                              select * from mobiletra_insertuserdriverinfo('`+returnValue.id_user+`', '`+refId+`', '`+noKtp+`', '`+noSim+`', '`+tempFotoDriver+`', '`+tempFotoKtp+`', '`+tempFotoSim+`', `+uid+`)
                                          `  

                                              console.log('keraniinfo', dataQry)
                                              let x = await encrypt(dataQry);
                                              return await Axios.post(url.select,{
                                                query: x
                                              }).then(async(value)=>{
                                                let y = value.data.data;
                                          
                                                console.log('sukses query', value)
                                          
                                                return ['Sukses tambah kerani!', 'success'];
                                          
                                              }).catch(err=>{
                                              //   return [];
                                                console.log('error', err)
                                                return ['Gagal tambah kerani!', 'warning'];
                                              });

                            }).catch((error)=>{
                              return ['Gagal upload ktp kerani!', 'warning'];      
                            })

                        }).catch((error) => {   
                            console.log('catch error', error)    
                            return ['Gagal upload ktp kerani!', 'warning'];      
                          })
                }).catch((error)=>{
                      return ['Gagal upload foto kerani!', 'warning'];      
                    })
              }).catch((error) => {   
                  console.log('catch error', error)    
                  return ['Gagal upload foto kerani!', 'warning'];      
              })
  
        // return ['Sukses tambah pengemudi!', 'success'];
  
      }).catch(err=>{
      //   return [];
        console.log('error', err)
        return ['Gagal tambah kerani!', 'warning'];
      });
    }
  }

