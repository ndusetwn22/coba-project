import React,{Component} from 'react'
import {Text, View,Icon, Content, Button} from 'native-base'
import {
    TouchableOpacity,
    ScrollView,FlatList, Image, Dimensions, RefreshControl, ToastAndroid
} from "react-native";
import styles from './style'
import CardKerani from './CardKerani/CardKerani'
import SearchInput,{ createFilter } from 'react-native-search-filter';
import { getListDriver } from '../../ListKeraniAxios';
const KEYS_TO_FILTERS = ['nama', 'email','hp','active', 'role'];
var listKosong = require('../../../../../../Assets/list_kosong.png');
const width = Dimensions.get('window').width;

class FlatListKerani extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paramSearch:'',
            refreshing: false,
        };
    }

    componentDidMount= async()=>{

    } 

    onRefresh= async() => {
        this.setState({refreshing: true})
        var _tempLength = this.props.listDriver.length
        var _listDriver = await getListDriver(this.props.url, this.props.profile.id_transporter)
        if (_listDriver.length > _tempLength || _listDriver.length < _tempLength) {
            
            if (_listDriver.length > _tempLength) {
                console.log('refresh', this.props.listDriver.length)
                this.props.refreshContent(_listDriver)
                this.setState({refreshing: false})
    
                var selisih = parseInt(_listDriver.length) - parseInt(_tempLength)
                ToastAndroid.show('New data added '+selisih, ToastAndroid.SHORT);
            }else{
                console.log('refresh', this.props.listDriver.length)
                this.props.refreshContent(_listDriver)
                this.setState({refreshing: false})
    
                ToastAndroid.show('Refresh ...', ToastAndroid.SHORT);
            }


        }
        else{
          ToastAndroid.show('No more new data available', ToastAndroid.SHORT);
          this.setState({refreshing: false})
        }
      };

    render(){

        const filteredDriver = this.props.listDriver.filter(createFilter(this.state.paramSearch, KEYS_TO_FILTERS))
        const hak_akses = this.props.hak_akses

        return(
            <>
                
                    <View style={{flexDirection: 'row', backgroundColor: 'white', top: 1}}>
                    {hak_akses == 'ADMIN' ? 
                        <Button onPress={()=>this.props.onDriverAdd()} style={{marginTop: 10, marginLeft: 10, borderRadius: 10, padding: 0, height: 40, backgroundColor: '#008c45'}}>
                            <Icon type="MaterialIcons" name='person-add' />
                        </Button>
                    :
                        <Button disabled onPress={()=>this.props.onDriverAdd()} style={{marginTop: 10, marginLeft: 10, borderRadius: 10, padding: 0, height: 40, backgroundColor: '#008c45'}}>
                            <Icon type="MaterialIcons" name='person-add' />
                        </Button>
                    }
                        <SearchInput 
                            onChangeText={(term) => { this.setState({paramSearch:term}) }} 
                            style={{
                                paddingLeft:10,
                                paddingTop:5,
                                marginTop: 10,
                                marginLeft:10,
                                marginRight:10,
                                marginBottom:5,
                                paddingBottom:5,
                                borderColor: '#008c45',
                                borderWidth: 1,
                                backgroundColor:"white",
                                borderRadius: 10,
                                width: '232%',
                            }}
                            placeholder="Search Kerani"
                            inputFocus={true}
                            removeClippedSubviews={true} // Unmount components when outside of window 
                            initialNumToRender={2} // Reduce initial render amount
                            maxToRenderPerBatch={1} // Reduce number in each render batch
                            maxToRenderPerBatch={10} // Increase time between renders
                            windowSize={7} // Reduce the window size
                        />
                    </View>
                <Content
                    refreshControl={
                        <RefreshControl
                          refreshing={this.state.refreshing}
                          onRefresh={this.onRefresh.bind(this)}
                        />}
                >
                {this.props.listDriver.length == 0 ? 
                    <View>
                        <Image resizeMode='cover' source={listKosong} style={{marginTop: 50, width: '50%', height: '50%', opacity: 0.5, alignSelf: 'center'}}></Image>
                        <Text note style={{marginBottom: width, textAlign: 'center', fontStyle:'italic'}}>Kerani kosong</Text>
                    </View>
                :
                    <ScrollView>
                            <FlatList
                                data={
                                    filteredDriver
                                }
                                style={{marginBottom:10}}
                                renderItem={({ item: value, index }) => {
                                    return (
                                        <CardKerani {...this.props} index={index} value={value} />
                                    );
                                }}
                                keyExtractor={(item, index) => index.toString()}
                            /> 
                    </ScrollView>
                }
                </Content>
            </>
        );
    }
}

export default FlatListKerani;