import Axios from 'axios';
import {encrypt, firebase} from '../../../../../APIProp/ApiConstants';
//firebase
import { utils } from '@react-native-firebase/app';
import storage from '@react-native-firebase/storage';


    export const getDataUser = async (url, id) =>{
        // console.log('id', id, nama, hp, email, password, hak_akses, active, transporter_id, user_id, id)
        let dataQry = `
            select *, epm_decrypt(mmu.password) as pass from mt_master_user mmu 
            where mmu.id = `+id+`
        `
        let x = await encrypt(dataQry);
        return await Axios.post(url.select,{
            query: x
        }).then(value=>{
            let x = value.data.data[0];
            console.log('sukses data user', x)
          if(x==""){
            return [];
          }
          return x;
        }).catch(err=>{
            console.log('error narik data user', err)
            return [];
            // return 'Gagal narik data';
        });
    }

    export const getDataDriver = async (url, id) =>{
        // console.log('id', id, nama, hp, email, password, hak_akses, active, transporter_id, user_id, id)
        let dataQry = `
            select * from mt_driver_info mdi 
            where mdi.user_id = `+id+`
        `
        let x = await encrypt(dataQry);
        return await Axios.post(url.select,{
            query: x
        }).then(value=>{
            let x = value.data.data[0];

            //firebase pandu
            // var depan = firebase.linkStorage

            //firebase prod
            var depan = firebase.linkStorage
            // var test = x.foto_driver
            // var test1 = test.split('FotoDriver')
            // var depan = test1[0]
            // console.log('depanns', depan)
    
              //foto driver logic
              var _fotoFilename = x.foto_driver
              _fotoFilename = _fotoFilename.substr(depan.length);
              _fotoFilename = _fotoFilename.split('?alt=media')
              _fotoFilename = _fotoFilename[0]
    
              //foto ktp logic
              var _ktpFilename = x.foto_ktp
              _ktpFilename = _ktpFilename.substr(depan.length);
              _ktpFilename = _ktpFilename.split('?alt=media')
              _ktpFilename = _ktpFilename[0]
    
              //foto sim logic
              var _simFilename = x.foto_sim
              _simFilename = _simFilename.substr(depan.length);
              _simFilename = _simFilename.split('?alt=media')
              _simFilename = _simFilename[0]

              x.fotoFileName = _fotoFilename
              x.ktpFileName = _ktpFilename
              x.simFileName = _simFilename

            
            console.log('sukses data driver', x)
          if(x==""){
            return [];
          }
          return x;
        }).catch(err=>{
            console.log('error narik data driver', err)
            return [];
            // return 'Gagal narik driver';
        });
    }





  export const editPengemudi = async (url, id, trid, uid, namaPengemudi, role, noHp, email, status, password, refId, noKtp, noSim, fotoFileName, ktpFileName, simFileName, fotoSource_, ktpSource_, simSource_, tempFotoFileName, tempKtpFileName, tempSimFileName) =>{
    //   console.log('id', id, trid, uid, namaPengemudi, role, noHp, email, status, password, refId, noKtp, noSim, fotoFileName, ktpFileName, simFileName, fotoSource_, ktpSource_, simSource_, tempFotoFileName, tempKtpFileName, tempSimFileName)
    // let tempFotoDriver = firebase.linkStorage+fotoFileName+'?alt=media'
    // let tempFotoKtp = firebase.linkStorage+ktpFileName+'?alt=media'
    // let tempFotoSim = firebase.linkStorage+simFileName+'?alt=media'

    // let dataQry = `
    //     update mt_master_user SET
    //     nama = '`+namaPengemudi+`',
    //     role = '`+role+`',
    //     hp = '`+noHp+`',
    //     email = '`+email+`',
    //     active = '`+status+`',
    //     password = epm_encrypt('`+password+`'),
    //     update_date = now(),
    //     update_by = `+uid+`
    //     WHERE id = `+id+`
    //     and active != 'B'
    //     returning id as return_id;
    // `

    let dataQry = `
        select * from mobiletra_edituserdriver(`+id+`, '`+namaPengemudi+`', '`+role+`', '`+noHp+`', '`+email+`', '`+status+`', '`+password+`', '`+uid+`')
    `
    console.log('query user kerani : ', dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data[0];
      console.log('sukses user', value)

      if (x.return_id == id) {
        let _updateDriver =  editPengemudiDriver(url, id, trid, uid, namaPengemudi, role, noHp, email, status, password, refId, noKtp, noSim, fotoFileName, ktpFileName, simFileName, fotoSource_, ktpSource_, simSource_, tempFotoFileName, tempKtpFileName, tempSimFileName)
        return _updateDriver
      }
    else{
        return ['Gagal edit kerani!', 'warning'];
    }

    //   const FireBaseStorage = storage();

    //   //cek disini kalo sama == gausah upload
    //   if (tempFotoFileName != tempFotoDriver) {
    //     //Insert firebase foto
    //     const fotoSource = fotoSource_.uri
    //     const fotoStorageRef = FireBaseStorage.ref(fotoFileName)
    //     Promise.resolve(fotoStorageRef.putFile(fotoSource));  
    //   }

    //   if (tempKtpFileName != tempFotoKtp) {
    //     //ktp
    //     const ktpSource = ktpSource_.uri
    //     const ktpStorageRef = FireBaseStorage.ref(ktpFileName)
    //     Promise.resolve(ktpStorageRef.putFile(ktpSource));
    //   }

    //   if (tempSimFileName != tempFotoSim) {
    //     //sim
    //     const simSource = simSource_.uri
    //     const simStorageRef = FireBaseStorage.ref(simFileName)
    //     Promise.resolve(simStorageRef.putFile(simSource));
    //   }

    let _updateDriver =  editPengemudiDriver(url, id, trid, uid, namaPengemudi, role, noHp, email, status, password, refId, noKtp, noSim, fotoFileName, ktpFileName, simFileName, fotoSource_, ktpSource_, simSource_, tempFotoFileName, tempKtpFileName, tempSimFileName)
      return _updateDriver;

    }).catch(err=>{
    //   return [];
      console.log('error', err)
      return ['Gagal edit kerani!', 'warning'];
    });
  }


  export const editPengemudiDriver = async (url, id, trid, uid, namaPengemudi, role, noHp, email, status, password, refId, noKtp, noSim, fotoFileName, ktpFileName, simFileName, fotoSource_, ktpSource_, simSource_, tempFotoFileName, tempKtpFileName, tempSimFileName) =>{
    //   console.log('id', id, trid, uid, namaPengemudi, role, noHp, email, status, password, refId, noKtp, noSim, fotoFileName, ktpFileName, simFileName, fotoSource_, ktpSource_, simSource_, tempFotoFileName, tempKtpFileName, tempSimFileName)

    //firebase pandu
    // let tempFotoDriver = firebase.linkStorage+fotoFileName+'?alt=media'
    // let tempFotoKtp = firebase.linkStorage+ktpFileName+'?alt=media'
    // let tempFotoSim = firebase.linkStorage+simFileName+'?alt=media'

    //firebase prod
    let tempFotoDriver = firebase.linkStorage+fotoFileName+'?alt=media'
    let tempFotoKtp = firebase.linkStorage+ktpFileName+'?alt=media'
    let tempFotoSim = firebase.linkStorage+simFileName+'?alt=media'

    // console.log('wkkw', tempFotoDriver)

    const FireBaseStorage = storage();

      //cek disini kalo sama == gausah upload
      if (tempFotoFileName != tempFotoDriver) {
        //Insert firebase foto
        const fotoSource = fotoSource_.uri
        const fotoStorageRef = FireBaseStorage.ref(`FotoDriver/${id}/${fotoFileName}`)
        // Promise.resolve(fotoStorageRef.putFile(fotoSource)); 
        await fotoStorageRef.putFile(fotoSource) 
          .then(async(response)=>{
              console.log('sukses edit foto')
              await fotoStorageRef.getDownloadURL()
                .then(async(urlDownload)=>{
                  tempFotoDriver = urlDownload
                  var c = tempFotoDriver.split("&token");
                  tempFotoDriver = c[0]
                  // return ['Sukses edit pool!', 'success'];
                })
                .catch((error)=> {
                  return ['Gagal upload foto kerani!', 'warning'];      
                })
          }).catch((error) => {   
              console.log('catch error', error)    
              return ['Gagal upload foto kerani!', 'warning'];      
          })
      }

      if (tempKtpFileName != tempFotoKtp) {
        //ktp
        const ktpSource = ktpSource_.uri
        const ktpStorageRef = FireBaseStorage.ref(`FotoDriver/${id}/${ktpFileName}`)
        // Promise.resolve(ktpStorageRef.putFile(ktpSource));
        await ktpStorageRef.putFile(ktpSource)
          .then(async(response)=>{
              console.log('sukses edit ktp')
              await ktpStorageRef.getDownloadURL()
                .then(async(urlDownload)=>{
                  tempFotoKtp = urlDownload
                  var d = tempFotoKtp.split("&token");
                  tempFotoKtp = d[0]
                  // return ['Sukses edit pool!', 'success'];
                })
                .catch((error)=> {
                  return ['Gagal upload ktp kerani!', 'warning'];      
                })
          }).catch((error) => {   
              console.log('catch error', error)    
              return ['Gagal upload ktp kerani!', 'warning'];      
          })

      }

    if (role == 'DRIVER') {  
      if (tempSimFileName != tempFotoSim) {
        //sim
        const simSource = simSource_.uri
        const simStorageRef = FireBaseStorage.ref(`FotoDriver/${id}/${simFileName}`)
        // Promise.resolve(simStorageRef.putFile(simSource));
        await simStorageRef.putFile(simSource)
          .then(async(response)=>{
              console.log('sukses edit sim')
              await simStorageRef.getDownloadURL()
                .then(async(urlDownload)=>{
                  tempFotoSim = urlDownload
                  var d = tempFotoSim.split("&token");
                  tempFotoSim = d[0]
                  // return ['Sukses edit pool!', 'success'];
                })
                .catch((error)=> {
                  return ['Gagal upload sim kerani!', 'warning'];      
                })
          }).catch((error) => {   
              console.log('catch error', error)    
              return ['Gagal upload sim kerani!', 'warning'];      
          })
      }
    }
    // KERANI SIM
    else if (role == 'KERANI'){
      tempFotoSim = '';
    }


    // let dataQry = `
    //     update mt_driver_info SET
    //     external_id = '`+refId+`',
    //     no_ktp = '`+noKtp+`',
    //     no_sim = '`+noSim+`',
    //     foto_driver = '`+tempFotoDriver+`',
    //     foto_ktp = '`+tempFotoKtp+`',
    //     foto_sim = '`+tempFotoSim+`',
    //     update_date = now(),
    //     update_by = `+uid+`
    //     WHERE user_id = `+id+`;
    // `

    let dataQry = `
        select * from mobiletra_edituserdriverinfo(`+id+`, '`+refId+`', '`+noKtp+`', '`+noSim+`', '`+tempFotoDriver+`', '`+tempFotoKtp+`', '`+tempFotoSim+`', '`+uid+`')
    `
    console.log('data query user driver info : ', dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(async(value)=>{
      let x = value.data.data;
      console.log('sukses driver', value)
    
      return ['Sukses edit kerani!', 'success'];
    }).catch(err=>{
    //   return [];
      console.log('error', err)
      return ['Gagal edit info kerani!', 'warning'];
    });
  }