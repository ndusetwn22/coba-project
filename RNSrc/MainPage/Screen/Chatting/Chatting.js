import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { WebView } from 'react-native-webview';
import base64 from 'react-native-base64'
import {encryptChat,getProfile, decrypt} from '../../../../APIProp/ApiConstants'
import { NavigationContainer,useNavigation,DrawerActions  } from '@react-navigation/native';
import { Container, Button,Text } from 'native-base';
 
// ...
class Chatting extends Component {

    constructor(props) {
        super(props);
        this.state = {
            profileObj:null,
            urlChat:null
        }
    }
    componentDidMount=async()=>{
        var _profileObj = JSON.parse(await decrypt(await getProfile()))
        var strChat = 
        _profileObj.nama+"~"+
        _profileObj.hp+"~"+
        "TRANSPORTER~"+
        _profileObj.id_user+"~"+
        _profileObj.foto_logo+"~"+
        _profileObj.id_transporter+"~"+
        _profileObj.phone_transporter+"~"+
        _profileObj.nama_transporter+"~"+
        "mostrans12345678";
        var encParam = await encryptChat(strChat);
        encParam = base64.encode(encParam);
        await this.setState({profileObj:_profileObj, urlChat:"https://mostrans.co.id/ChatBox/?param="+encParam})
    }

  render() {
  return (
      <Container>
        <Button onPress={()=>{this.props.navigation.replace('MainRouter', { screen:'Dashboard'})}}><Text style={{textAlign:"center", width:"100%"}}>Back To Dashboard</Text></Button>
        <WebView source={{ uri: this.state.urlChat }} />
      </Container>
      
      );
  }
}

ChattingFunc= (props) => {
    const navigation = useNavigation();
    return <Chatting navigation={navigation}/>;
  }

export default ChattingFunc;