import { StyleSheet, PixelRatio } from 'react-native';

const styles = StyleSheet.create({
    modal:{
        backgroundColor: '#ffffff',
        padding: 10,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#008c45',
    },
    tombolClose2:{
        width: 100,
        height: 30,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#008c45',
        alignSelf: 'flex-end',
        justifyContent: 'center'
    },
    textclose:{
        color: '#008c45',
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 15
    },
    lineStyle:{
        borderWidth: 0.5,
        borderColor:'#008c45'
    },
    lineStyle:{
        borderWidth: 0.5,
        borderColor:'#008c45',
        marginTop:10,
        marginBottom:5
    },
});

export default styles;
