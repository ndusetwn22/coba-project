import React,{Component} from 'react'
import {View, ScrollView,TouchableOpacity, Image} from "react-native";
import styles from './style.js'
import {Text,Button, Card,CardItem,Body, Separator} from 'native-base'
import Timeline from 'react-native-timeline-flatlist'
import Modal from 'react-native-modal';

const arrMonth =['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus','September','Oktober','November', 'Desember'];

class ModalOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        };
    }

    componentDidMount= async()=>{
        console.log('modal order props', this.props)
    }

    convDateString =(param)=>{
        var today = new Date(param);
        // console.log('param', param)
        today = new Date(today.setHours(today.getHours()-1));
        return today.getDate() + " " + arrMonth[today.getMonth()] + " " + today.getFullYear();
    }
    
    
    render(){

        if (this.props.tripSelected.infoTrip.naikkapal_date != null && this.props.tripSelected.infoTrip.naikkapal_date != undefined && this.props.tripSelected.infoTrip.naikkapal_date != '') {

            var waktu_naikKapal = this.props.tripSelected.infoTrip.naikkapal_date.split('T')[1]
            var slice_waktuNaikKapal = waktu_naikKapal.split(':')
            var waktuNaikKapal_new = slice_waktuNaikKapal[0]+':'+slice_waktuNaikKapal[1]

        }

        if (this.props.tripSelected.infoTrip.turunkapal_date != null && this.props.tripSelected.infoTrip.turunkapal_date != undefined && this.props.tripSelected.infoTrip.turunkapal_date != '') {

            var waktu_turunKapal = this.props.tripSelected.infoTrip.turunkapal_date.split('T')[1]
            var slice_waktuTurunKapal = waktu_turunKapal.split(':')
            var waktuTurunKapal_new = slice_waktuTurunKapal[0]+':'+slice_waktuTurunKapal[1]

        }

        var asal = this.props.selectedOrder.asal_perusahaan
        var arr_asal = this.props.selectedOrder.asal_perusahaan.split(' ');

        if (arr_asal.length > 2){
            asal = arr_asal[0] + ' ' +arr_asal[1] + '\n';
            for (let index = 2; index < arr_asal.length; index++) {
                const element = arr_asal[index];
                asal = asal + element + ' ';
                
            }
        }

        // if (this.props.selectedOrder.asal_perusahaan.length > 20) {
        //     var _bagi = this.props.selectedOrder.asal_perusahaan.length/2
        //     _bagi = Math.round(_bagi)

        //     var asal = this.props.selectedOrder.asal_perusahaan.slice(0, _bagi)+'\n'+this.props.selectedOrder.asal_perusahaan.slice(_bagi, this.props.selectedOrder.asal_perusahaan.length)
        // }

        var tujuan = this.props.selectedOrder.tujuan_perusahaan
        var arr_tujuan = this.props.selectedOrder.tujuan_perusahaan.split(' ');

        if (arr_tujuan.length > 2){
            tujuan = arr_tujuan[0] + ' ' +arr_tujuan[1] + '\n';
            for (let index = 2; index < arr_tujuan.length; index++) {
                const element = arr_tujuan[index];
                tujuan = tujuan + element + ' ';
                
            }
        }
        // if (this.props.selectedOrder.tujuan_perusahaan.length > 20) {
        //     var _bagi2 = this.props.selectedOrder.tujuan_perusahaan.length/2
        //     _bagi2 = Math.round(_bagi)

        //     var tujuan = this.props.selectedOrder.tujuan_perusahaan.slice(0, _bagi2)+'\n'+this.props.selectedOrder.tujuan_perusahaan.slice(_bagi2, this.props.selectedOrder.tujuan_perusahaan.length)
        // }

        return(
            <>
            <Modal
                animationType="slide"
                style={{backgroundColor:"rgba(0,0,0,0.7)", width:"100%", height:"100%", margin:0}}
                visible={this.props.showModalOrder}
                onRequestClose={this.props.closeModalOrder}
            >

            {this.props.tripSelected.infoTrip.tipe_muatan == 'LAUT' ?  
                // LAUT
                <View style={{padding:20}}>
                    <View style={styles.modal}>
                        <View style={{flexDirection:"row"}}>
                            <Text style={{flex:2, marginLeft:10, fontSize:22, color:"#008c45", fontWeight:"bold"}}>
                                Detail Order Laut
                            </Text>
                            <TouchableOpacity style={[styles.tombolClose2,{flex:1, width:"50%"}]} onPress={this.props.closeModalOrder}>
                                <Text style={styles.textclose}>Tutup</Text>
                            </TouchableOpacity>
                        </View>
                        <View style = {[styles.lineStyle,{marginBottom:10,marginTop:10}]} />
                        <View style={{flexDirection:"row"}}>
                            <View style={{flex:1,marginLeft:10}}>
                                <Text style={{fontWeight:"bold", fontSize:15}}>
                                    Order
                                </Text>
                                <Text style={{fontSize:12}}>
                                    {this.props.selectedOrder.id_order}
                                </Text>
                            </View>
                            <View style={{flex:1}}>
                                <Text style={{fontWeight:"bold", textAlign:"center", fontSize:15}}>
                                    Status
                                </Text>
                                <Text style={{textAlign:"center", fontSize:12}}>
                                    {this.props.selectedOrder.status_detail}
                                </Text>
                            </View>
                            <View style={{flex:1,marginRight:10}}>
                                <Text style={{fontWeight:"bold", textAlign:"right", fontSize:15}}>
                                    Pickup Date
                                </Text>
                                <Text style={{textAlign:"right", fontSize:12}}>
                                    {this.props.selectedOrder.jadwal_penjemputan.split('T')[0]}
                                </Text>
                            </View>
                        </View>
                        <View style = {[styles.lineStyle,{marginBottom:15,marginTop:10}]} />

                        

                        <View style={{backgroundColor:"#008c45",width:"100%", borderRadius:10, borderWidth: 5, borderColor: '#008c45'}}>
                            <View style={{flexDirection:"row", margin:0, backgroundColor:"white",justifyContent: 'center', alignItems: 'center'}}>
                                <View style={{margin:5, marginBottom: 0}}>
                                    <View style={{flexDirection:'column'}}>
                                        <Text style={{fontWeight:"bold", fontSize:15}}>
                                            Trip
                                        </Text>
                                        <Text style={{fontSize:12, marginBottom:8}}>
                                            {this.props.tripSelected.infoTrip.id_trip}
                                        </Text>
                                    </View>
                                    <View style={{flexDirection:'column'}}>
                                        <Text style={{fontWeight:"bold", fontSize:15}}>
                                            Asal Pengiriman
                                        </Text>
                                        <Text style={{fontSize:12, marginBottom:8}}>
                                            {asal}
                                            {/* {this.props.selectedOrder.asal_perusahaan} */}
                                        </Text>
                                    </View>
                                    <View style={{flexDirection:'column'}}>
                                        <Text style={{fontWeight:"bold", fontSize:15}}>
                                            Target Penjemputan
                                        </Text>
                                        <Text style={{fontSize:12, marginBottom:8}}>
                                            {(this.props.selectedOrder.jadwal_penjemputan.replace('T',' ')).substring(0,16)}
                                        </Text>
                                    </View>
                                    <View style={{flexDirection:'column'}}>
                                        <Text style={{fontWeight:"bold", fontSize:15}}>
                                            Naik Kapal
                                        </Text>
                                        <Text style={{fontSize:12, marginBottom:8}}>
                                            {this.props.tripSelected.infoTrip.naikkapal_date == null || this.props.tripSelected.infoTrip.naikkapal_date == '' ? ' - ': this.convDateString(this.props.tripSelected.infoTrip.naikkapal_date.split('T')[0])+'\n'+waktuNaikKapal_new}
                                        </Text>
                                    </View>
                                    {/* <View style={{flexDirection:'column', width: '100%'}}>
                                        <Text style={{fontWeight:"bold", fontSize:15}}>
                                            Naik Kapal
                                        </Text>
                                        <Text style={{fontSize:12, marginBottom:8}}>
                                            {this.props.tripSelected.infoTrip.naikkapal_date == null || this.props.tripSelected.infoTrip.naikkapal_date == '' ? '-': this.convDateString(this.props.tripSelected.infoTrip.naikkapal_date.split('T')[0])+'\n'+waktuNaikKapal_new}
                                        </Text>
                                    </View> */}
                                </View>
                                <View style={{margin: 5, marginBottom: 0}}>
                                    <View style={{flexDirection:'column'}}>
                                        <Text style={{fontWeight:"bold", fontSize:15}}>
                                            Berat / Dimensi
                                        </Text>
                                        <Text style={{fontSize:12, marginBottom:8}}>
                                                {this.props.selectedOrder.weight} KG / {this.props.selectedOrder.dimension} CBM
                                        </Text>
                                    </View>
                                    <View style={{flexDirection:'column'}}>
                                        <Text style={{fontWeight:"bold", fontSize:15}}>
                                            Tujuan Pengiriman
                                        </Text>
                                        <Text style={{fontSize:12, marginBottom:8}}>
                                            {tujuan}
                                        </Text>
                                        {/* <Separator style={{height:undefined}}>
                                            <Text>{tujuan}</Text>
                                        </Separator> */}
                                    </View>
                                    <View style={{flexDirection:'column'}}>
                                        <Text style={{fontWeight:"bold", fontSize:15}}>
                                            Target Sampai
                                        </Text>
                                        <Text style={{fontSize:12, marginBottom:8}}>
                                            {(this.props.selectedOrder.jadwal_sampai.replace('T',' ')).substring(0,16)}
                                        </Text>
                                    </View>
                                    <View style={{flexDirection:'column'}}>
                                        <Text style={{fontWeight:"bold", fontSize:15}}>
                                            Turun Kapal
                                        </Text>
                                        <Text style={{fontSize:12, marginBottom:8}}>
                                            {this.props.tripSelected.infoTrip.turunkapal_date == null || this.props.tripSelected.infoTrip.turunkapal_date == '' ? ' - ': this.convDateString(this.props.tripSelected.infoTrip.turunkapal_date.split('T')[0])+'\n'+waktuTurunKapal_new}
                                        </Text>
                                    </View>
                                    <View style={{flexDirection:'column', flex: 1}}>
                                        {/* <Text style={{fontWeight:"bold", fontSize:15}}>
                                            Naik Kapal
                                        </Text>
                                        <Text style={{fontSize:12, marginBottom:8}}>
                                            {this.props.tripSelected.infoTrip.naikkapal_date == null || this.props.tripSelected.infoTrip.naikkapal_date == '' ? '-': this.convDateString(this.props.tripSelected.infoTrip.naikkapal_date.split('T')[0])+'\n'+waktuNaikKapal_new}
                                        </Text> */}
                                    </View>
                                </View>
                            </View>
                            <View style={{flexDirection:'column', width: '100%', backgroundColor: 'white', justifyContent: 'center', alignItems: 'center'}}>
                                <Text style={{fontWeight:"bold", fontSize:15}}>
                                    Instruksi
                                </Text>
                                <Text style={{fontSize:12, marginBottom:7}}>
                                    <Text style={{fontSize:12, marginBottom:7}}>
                                        {this.props.selectedOrder.instruksi == null || this.props.selectedOrder.instruksi == undefined || this.props.selectedOrder.instruksi == '' ? ' - ': this.props.selectedOrder.instruksi}
                                    </Text>
                                </Text>
                            </View>
                        </View>



                        <ScrollView style={{width:"100%", padding:10}}>
                            <Timeline
                            data={this.props.timelineData}
                            circleSize={20}
                            circleColor='rgb(0,140,69)'
                            lineColor='rgb(0,140,69)'
                            timeContainerStyle={{minWidth:52, marginTop: -5}}
                            timeStyle={{marginTop:5,textAlign: 'center', backgroundColor:'#ff6a61', color:'white', padding:5, borderRadius:13}}
                            descriptionStyle={{color:'gray'}}
                            options={{
                                style:{paddingTop:5}
                            }}
                            innerCircle={'dot'}
                            />
                        </ScrollView>
                    </View>
                </View>
                
            : 
                
                // DARAT
                <View style={{padding:20}}>
                    <View style={styles.modal}>
                        <View style={{flexDirection:"row"}}>
                            <Text style={{flex:2, marginLeft:10, fontSize:22, color:"#008c45", fontWeight:"bold"}}>
                                Detail Order
                            </Text>
                            <TouchableOpacity style={[styles.tombolClose2,{flex:1, width:"50%"}]} onPress={this.props.closeModalOrder}>
                                <Text style={styles.textclose}>Tutup</Text>
                            </TouchableOpacity>
                        </View>
                        <View style = {[styles.lineStyle,{marginBottom:10,marginTop:10}]} />
                        <View style={{flexDirection:"row"}}>
                            <View style={{flex:1,marginLeft:10}}>
                                <Text style={{fontWeight:"bold", fontSize:15}}>
                                    Order
                                </Text>
                                <Text style={{fontSize:12}}>
                                    {this.props.selectedOrder.id_order}
                                </Text>
                            </View>
                            <View style={{flex:1}}>
                                <Text style={{fontWeight:"bold", textAlign:"center", fontSize:15}}>
                                    Status
                                </Text>
                                <Text style={{textAlign:"center", fontSize:12}}>
                                    {this.props.selectedOrder.status_detail}
                                </Text>
                            </View>
                            <View style={{flex:1,marginRight:10}}>
                                <Text style={{fontWeight:"bold", textAlign:"right", fontSize:15}}>
                                    Pickup Date
                                </Text>
                                <Text style={{textAlign:"right", fontSize:12}}>
                                    {this.props.selectedOrder.jadwal_penjemputan.split('T')[0]}
                                </Text>
                            </View>
                        </View>
                        <View style = {[styles.lineStyle,{marginBottom:15,marginTop:10}]} />
                        <View style={{backgroundColor:"#008c45",width:"100%", borderRadius:10}}>
                            <View style={{flexDirection:"row", margin:7, backgroundColor:"white",justifyContent: 'center', alignItems: 'center'}}>
                                <Image source={{uri:this.props.profile.foto_logo}} style={{flex:1, marginTop:7,marginBottom:20, width:80, height:50, resizeMode:"contain"}}></Image>
                                <View style={{flex:3, padding:10}}>
                                <Text style={{fontWeight:"bold", fontSize:15}}>
                                    Transporter
                                </Text>
                                <Text style={{fontSize:12, marginBottom:8}}>
                                    {this.props.profile.nama_transporter}
                                </Text>

                                <Text style={{fontWeight:"bold", fontSize:15}}>
                                    Asal Pengiriman
                                </Text>
                                <Text style={{fontSize:12, marginBottom:8}}>
                                {this.props.selectedOrder.asal_perusahaan}
                                </Text>

                                <Text style={{fontWeight:"bold", fontSize:15}}>
                                    Tujuan Pengiriman
                                </Text>
                                <Text style={{fontSize:12, marginBottom:8}}>
                                {this.props.selectedOrder.tujuan_perusahaan}
                                </Text>

                                <Text style={{fontWeight:"bold", fontSize:15}}>
                                    Target Sampai
                                </Text>
                                <Text style={{fontSize:12, marginBottom:8}}>
                                {(this.props.selectedOrder.jadwal_sampai.replace('T',' ')).substring(0,16)}
                                </Text>

                                <Text style={{fontWeight:"bold", fontSize:15}}>
                                    Berat / Dimensi
                                </Text>
                                <Text style={{fontSize:12, marginBottom:7}}>
                                    <Text style={{fontSize:12, marginBottom:7}}>
                                        {this.props.selectedOrder.weight} KG / {this.props.selectedOrder.dimension} CBM
                                    </Text>
                                </Text>

                            {this.props.selectedOrder.suhu_id == null || this.props.selectedOrder.suhu_id == 0 ? null :
                                <Text style={{fontWeight:"bold", fontSize:15}}>
                                    Permintaan Suhu (C)
                                </Text>
                            }
                            {this.props.selectedOrder.suhu_id == null || this.props.selectedOrder.suhu_id == 0 ? null :
                                <Text style={{fontSize:12, marginBottom:7}}>
                                    <Text style={{fontSize:12, marginBottom:7}}>
                                        {this.props.selectedOrder.min_suhu} s.d {this.props.selectedOrder.maks_suhu}
                                    </Text>
                                </Text>
                            }

                            {/* {this.props.selectedOrder.instruksi == null || this.props.selectedOrder.instruksi == undefined ? null : */}
                                <Text style={{fontWeight:"bold", fontSize:15}}>
                                    Instruksi
                                </Text>
                            {/* } */}
                                <Text style={{fontSize:12, marginBottom:7}}>
                                    <Text style={{fontSize:12, marginBottom:7}}>
                                        {this.props.selectedOrder.instruksi == null || this.props.selectedOrder.instruksi == undefined || this.props.selectedOrder.instruksi == '' ? ' - ': this.props.selectedOrder.instruksi}
                                    </Text>
                                </Text>
                            


                                </View>
                            </View>
                        </View>
                        <ScrollView style={{width:"100%", padding:10}}>
                            <Timeline
                            data={this.props.timelineData}
                            circleSize={20}
                            circleColor='rgb(0,140,69)'
                            lineColor='rgb(0,140,69)'
                            timeContainerStyle={{minWidth:52, marginTop: -5}}
                            timeStyle={{marginTop:5,textAlign: 'center', backgroundColor:'#ff6a61', color:'white', padding:5, borderRadius:13}}
                            descriptionStyle={{color:'gray'}}
                            options={{
                                style:{paddingTop:5}
                            }}
                            innerCircle={'dot'}
                            />
                        </ScrollView>
                    </View>
                </View>
                
                }
          </Modal>
          </>
        );
    }
}

export default ModalOrder;