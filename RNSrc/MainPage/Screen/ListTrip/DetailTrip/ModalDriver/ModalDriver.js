import React,{Component} from 'react'
import {View, ScrollView,TouchableOpacity, Image} from "react-native";
import styles from './style.js'
import {Text,Button, Card,CardItem,Body, Item, Picker, Icon} from 'native-base'
import SearchableDropdown from 'react-native-searchable-dropdown';
import Modal from 'react-native-modal';

class ModalDriver extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedDriver:null
        };
    }

    componentDidMount= async()=>{

    }

    onValueChangeDriver = (value) => { //biasanya digunakan pada picker yg select value (bisa milih/combo box)
        this.setState({
            selectedDriver: value
        });
    };
    
    render(){
        return(
            <Modal
                animationType="slide"
                style={{backgroundColor:"rgba(0,0,0,0.7)", width:"100%", height:"100%", margin:0}}
                visible={this.props.showModalDriver}
                onRequestClose={this.props.closeModalOrder}
            >
                <View style={{padding:20}}>
                    <View style={styles.modal}>
                        <View style={{flexDirection:"row"}}>
                            <Text style={{flex:2, marginLeft:10, fontSize:22, color:"#008c45", fontWeight:"bold"}}>
                                Assign {this.props.driverOrKerani}
                                {/* Assign Driver/Kerani */}
                            </Text>
                            <Button
                                danger
                                onPress={this.props.closeModalOrder}
                                style={{ alignSelf: "flex-end",padding: 0,height:30, marginLeft:10 }}
                            >
                                <Text uppercase={false}>Tutup</Text>
                            </Button>
                            <Button
                                success
                                onPress={()=>{this.props.onSetDriver(this.state.selectedDriver); this.props.closeModalOrder()}}
                                style={{ alignSelf: "flex-end",padding: 0,height:30, marginLeft:10 }}
                            >
                                <Text uppercase={false}>Assign</Text>
                            </Button>
                        </View>
                        <View style = {styles.lineStyle} />
                        <Text style={{ marginTop: 10, fontSize: 14, marginLeft:10 }}>Pilih {this.props.driverOrKerani}</Text>
                        <View style={{marginBottom:10, maxHeight:'70%'}}>
                            <SearchableDropdown
                                onTextChange={text => console.log(text)}
                                onItemSelect={(item) => {this.onValueChangeDriver(item)}}
                                containerStyle={{ padding: 5 , width:"100%"}}
                                textInputStyle={{
                                    padding: 3,
                                    borderWidth: 1,
                                    borderColor: '#ccc',
                                    backgroundColor: '#FAF7F6',
                                    borderRadius:10,
                                    textAlign:"center"
                                }}
                                itemStyle={{
                                    padding: 10,
                                    marginTop: 2,
                                    backgroundColor: '#FAF9F8',
                                    borderColor: '#bbb',
                                    borderWidth: 1
                                }}
                                itemTextStyle={{
                                    color: '#222'
                                }}
                                itemsContainerStyle={{
                                    maxHeight: '90%',
                                }}
                                items={this.props.listDriver}
                                placeholder={"Pilih "+this.props.driverOrKerani}
                                resetValue={false}
                                underlineColorAndroid="transparent"
                            />
                        </View>
                    </View>
                </View>
          </Modal>
        );
    }
}

export default ModalDriver;