import React,{Component} from 'react'
import {View, ScrollView,TouchableOpacity, Image, TextInput} from "react-native";
import styles from './style.js'
import {Text,Button, Card,CardItem,Body, Item, Picker, Icon} from 'native-base'
import SearchableDropdown from 'react-native-searchable-dropdown';
import Modal from 'react-native-modal';

class ModalNoContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedKendaraan:null
        };
    }

    componentDidMount= async()=>{
        console.log('modal container', this.props)
    }

    onValueChangeKendaraan = (value) => { //biasanya digunakan pada picker yg select value (bisa milih/combo box)
        this.setState({
            selectedKendaraan: value
        });
    };
    
    render(){
        return(
            <Modal
                animationType="slide"
                style={{backgroundColor:"rgba(0,0,0,0.7)", width:"100%", height:"100%", margin:0}}
                visible={this.props.showModalNoContainer}
                onRequestClose={this.props.closeModalOrder}
            >
                <View style={{padding:20}}>
                    <View style={styles.modal}>
                        <View style={{flexDirection:"row"}}>
                            <Text style={{flex:2, marginLeft:10, fontSize:22, color:"#008c45", fontWeight:"bold"}}>
                                Assign No Container
                            </Text>
                            {/* <Button
                                danger
                                onPress={this.props.closeModalOrder}
                                style={{ alignSelf: "flex-end",padding: 0,height:30, marginLeft:10 }}
                            >
                                <Text uppercase={false}>Tutup</Text>
                            </Button> */}
                            <Button
                                success
                                onPress={()=>{this.props.closeModalOrder()}}
                                style={{ alignSelf: "flex-end",padding: 0,height:30, marginLeft:10 }}
                            >
                                <Text uppercase={false}>Assign</Text>
                            </Button>
                        </View>
                        <View style = {styles.lineStyle} />
                        <Text style={{ marginTop: 10, fontSize: 14, marginLeft:10, textAlign: 'center' }}>No Container</Text>
                        <View style={{marginBottom:10, maxHeight:'70%'}}>
                        <TextInput
                        autoCapitalize='characters'
                            style={styles.textInput}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder="No Container"
                            placeholderTextColor = "#CACCD7"
                            selectionColor="#008c45"
                            value={this.props.tripSelected.infoTrip.no_container != undefined && this.props.tripSelected.infoTrip.no_container != null ? this.props.tripSelected.infoTrip.no_container : ''}
                            onChangeText={input=>this.props.onChangeTextNoContainer(input)}
                            />
                        </View>
                    </View>
                </View>
          </Modal>
        );
    }
}

export default ModalNoContainer;