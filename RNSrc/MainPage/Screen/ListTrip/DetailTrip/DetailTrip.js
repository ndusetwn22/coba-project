import React,{Component} from 'react'
import {View, TouchableOpacity, ScrollView,FlatList,Dimensions} from "react-native";
import styles from './style'
import {Text,Button,Icon, DatePicker} from 'native-base'
import CustomMap from './Component/Map/Map'
import CardOrder from './Component/CardOrder/CardOrder'
import ModalOrder from './ModalOrder/ModalOrder'
import ModalDriver from './ModalDriver/ModalDriver'
import ModalKendaraan from './ModalKendaraan/ModalKendaraan'
import ModalReject from './ModalReject/ModalReject'
import moment from 'moment'
import DateTimePicker from '@react-native-community/datetimepicker';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import ModalNoContainer from './ModalNoContainer/ModalNoContainer';
import ModalKendaraanTujuan from './ModalKendaraanTujuan/ModalKendaraanTujuan';
import ModalDriverTujuan from './ModalDriverTujuan/ModalDriverTujuan';



var {width, height} = Dimensions.get('window')

const statusDetail = [
    "Planning",
    "Confirmed",
    "Accepted",
    "Arrival",
    "Start Loaded",
    "Finish Loaded",
    "On The Way",
    "Dropoff",
    "Start Unloaded",
    "Finish Unloaded",
    "POD"
]

const keteranganStatus = [
    "Shipper Order : ",
    "Order Diterima : ",
    "Driver Konfirmasi : ",
    "Tiba Lokasi Muat : ",
    "Mulai Muat : ",
    "Selesai Muat : ",
    "Mulai Perjalanan : ",
    "Tiba Lokasi Bongkar : ",
    "Mulai Bongkar : ",
    "Selesai Bongkar : ",
    "POD Dokumen : "
]

class DetailTrip extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModalDriver:false,
            showModalDriverTujuan: false,
            showModalKendaraan:false,
            showModalKendaraanTujuan: false,
            showModalOrder:false,
            showModalReject: false,
            showModalNoContainer: false,
            showModalKerani: false,
            selectedOrder:null,
            timelineData:null,
            chosenDateNaik: new Date(),
            chosenDateTurun: new Date(),
            mode: 'date',
            type: 'naikTgl',
            isDatePickerVisible: false,
            setDatePickerVisibility: false,
            dateNaik: 'Pilih Tanggal Naik',
            dateTurun: 'Pilih Tanggal Turun',
            timeNaik: 'Pilih Waktu Naik',
            timeTurun: 'Pilih Waktu Turun',
            asal_tujuan: '',
            driverOrKerani: ''

        };
    }

    componentDidMount= async()=>{
        console.log('Info Tripsss', this.props)
        // console.log('naikkapal ', this.props.tripSelected.infoTrip.naikkapal_date.split('T')[0])

        
        if (this.props.tripSelected.infoTrip.naikkapal_date != null && this.props.tripSelected.infoTrip.naikkapal_date != undefined && this.props.tripSelected.infoTrip.naikkapal_date != '') {
            // TGL NAIK
            var tgl_naikKapal = this.props.tripSelected.infoTrip.naikkapal_date.split('T')[0]
            var slice_naikKapal = tgl_naikKapal.split('-')
            var naikKapal_new = slice_naikKapal[2]+'-'+slice_naikKapal[1]+'-'+slice_naikKapal[0]
            // console.log('TGL TAMPILIN', naikkapal_new)

            // JAM NAIK
            var waktu_naikKapal = this.props.tripSelected.infoTrip.naikkapal_date.split('T')[1]
            var slice_waktuNaikKapal = waktu_naikKapal.split(':')
            var waktuNaikKapal_new = slice_waktuNaikKapal[0]+':'+slice_waktuNaikKapal[1]

            await this.setState({dateNaik: naikKapal_new+' '+waktuNaikKapal_new, timeNaik: waktuNaikKapal_new})
        }

        if (this.props.tripSelected.infoTrip.turunkapal_date != null && this.props.tripSelected.infoTrip.turunkapal_date != undefined && this.props.tripSelected.infoTrip.turunkapal_date != '') {
            // TGL TURUN
            var tgl_turunKapal = this.props.tripSelected.infoTrip.turunkapal_date.split('T')[0]
            var slice_turunKapal = tgl_turunKapal.split('-')
            var turunKapal_new = slice_turunKapal[2]+'-'+slice_turunKapal[1]+'-'+slice_turunKapal[0]
            // console.log('TGL TAMPILIN', turunkapal_new)

            // JAM TURUN
            var waktu_turunKapal = this.props.tripSelected.infoTrip.turunkapal_date.split('T')[1]
            var slice_waktuTurunKapal = waktu_turunKapal.split(':')
            var waktuTurunKapal_new = slice_waktuTurunKapal[0]+':'+slice_waktuTurunKapal[1]

            await this.setState({dateTurun: turunKapal_new+' '+waktuTurunKapal_new, timeTurun: waktuTurunKapal_new})
        }
    }

    openModalDriver=async()=>{
        await this.props.onShowModalDriver();
        this.setState({driverOrKerani: 'Driver'})
        await this.setState({showModalDriver:true})
    }

    openModalKerani=async()=>{
        await this.props.onShowModalKerani();
        this.setState({driverOrKerani: 'Kerani Asal'})
        await this.setState({showModalDriver:true})
    }

    openModalKeraniTujuan=async()=>{
        await this.props.onShowModalKeraniTujuan();
        await this.setState({showModalDriverTujuan:true})
    }

    openModalKendaraan=async()=>{
        await this.props.onShowModalKendaraan();
        await this.setState({showModalKendaraan:true})
    }
    
    openModalKendaraanTujuan=async()=>{
        await this.props.onShowModalKendaraanTujuan();
        await this.setState({showModalKendaraanTujuan:true})
    }

    openModalOrder=async(infoOrder)=>{
        await this.setState({
            timelineData:await this.openDetailOrderModal(infoOrder),
            selectedOrder:infoOrder,
            showModalOrder:true
        })
    }

    openModalNoContainer = async() => {
        // await this.props.onShowModalNoContainer();
        await this.setState({showModalNoContainer: true})
    }

    closeModalOrder=async()=>{
        await this.setState({showModalOrder:false,showModalDriver:false, showModalDriverTujuan: false ,showModalKendaraan:false, showModalNoContainer: false, showModalKendaraanTujuan: false})
    }

    toggleModalReject = async() => {
        await this.setState({showModalReject: !this.state.showModalReject, showModalOrder:false,showModalDriver:false,showModalKendaraan:false, showModalKendaraanTujuan: false})
    }

    functionCombinedReject = async(idAlasan) => {
        console.log('Alasan', idAlasan)
    }

    setDateNaik = (newDate) => {
        this.setState({ chosenDateNaik: newDate });
    
        console.log('chosen date: ', newDate)
        console.log('type of: ', typeof(newDate))
      }

    setDateTurun = (newDate) => {
    this.setState({ chosenDateTurun: newDate });

    console.log('chosen date: ', newDate)
    console.log('type of: ', typeof(newDate))
    }

    showDatePicker = (type, mode) => {
        // if (type == 'naikTgl') {
        //     this.setState({mode: mode, type: type})
        // }else if(type == 'turunTgl'){
        //     this.setState({mode: mode, type: type})
        // }

        this.setState({mode: mode, type: type})
        this.setState({setDatePickerVisibility: true});
      };

    hideDatePicker = () => {
        this.setState({setDatePickerVisibility: false});
    };

    handleConfirm = async(date) => {
        console.log("A date has been pickeds: ", date);
        var curr_date = date.getDate();
        var curr_month = date.getMonth() + 1; //Months are zero based
        var curr_year = date.getFullYear();
        var date_pick = curr_date + "-" + curr_month + "-" + curr_year

        var hours = date.getHours();
        var minutes = date.getMinutes();
        if (hours == 0) {
            hours += '0'
        } else if(hours < 10){
            hours = '0' + hours
        }

        if (minutes == 0){
            minutes += '0'
        } else if (minutes < 10){
            minutes = '0' + minutes
        }

        // var time_pick = date.getHours() + ":" + date.getMinutes();
        var time_pick = hours + ":" + minutes;
        // console.log('hours ', date.getHours())
        // console.log('minutes ', date.getMinutes())
        console.log('hours ', hours)
        console.log('minutes ', minutes)

        // var time_pick = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();

        if (this.state.type == 'naikTgl') {
            await this.setState({dateNaik: date_pick})
            await this.hideDatePicker();
            await this.showDatePicker('naikWaktu','time')

            await this.props.onSetDateNaikTurunKapal('naikJagain', this.state.dateNaik)
        }else if(this.state.type == 'turunTgl'){
            this.setState({dateTurun: date_pick})
            await this.hideDatePicker();
            await this.showDatePicker('turunWaktu','time')

            await this.props.onSetDateNaikTurunKapal('turunJagain', this.state.dateTurun)
        }else if(this.state.type == 'naikWaktu'){
            await this.setState({dateNaik: this.state.dateNaik+' '+time_pick})
            // await this.setState({timeNaik: time_pick})
            // var _tempNaik = this.state.dateNaik+' '+time_pick
            await this.props.onSetDateNaikTurunKapal('naik', this.state.dateNaik)
            await this.hideDatePicker();
        }else{
            this.setState({dateTurun: this.state.dateTurun+' '+time_pick})
            // await this.setState({timeTurun: time_pick})
            // var _tempTurun = this.state.dateTurun+' '+time_pick
            await this.props.onSetDateNaikTurunKapal('turun', this.state.dateTurun)
            await this.hideDatePicker();
        }

        // this.hideDatePicker();
    };

    openDetailOrderModal=async (value)=>{
        var tmpTimelineData =[];
        for (var i = 0;i< Number(value.status);i++){
          var tmpTanggal;
          var tmpJam;
          switch (i){
            case 0: if(value.booked_date != null) {tmpTanggal= value.booked_date.split('T')[0];tmpJam = value.booked_date.split('T')[1].substring(0,5)};break;
            case 1: if(value.confirmed_date != null){tmpTanggal= value.confirmed_date.split('T')[0];tmpJam =value.confirmed_date.split('T')[1].substring(0,5)};break;
            case 2: if(value.accepted_date != null){tmpTanggal= value.accepted_date.split('T')[0];tmpJam =value.accepted_date.split('T')[1].substring(0,5)};break;
            case 3: if(value.arrival_date != null){tmpTanggal= value.arrival_date.split('T')[0];tmpJam =value.arrival_date.split('T')[1].substring(0,5)};break;
            case 4: if(value.startloaded_date != null){tmpTanggal= value.startloaded_date.split('T')[0];tmpJam =value.startloaded_date.split('T')[1].substring(0,5)};break;
            case 5: if(value.finishloaded_date != null){tmpTanggal= value.finishloaded_date.split('T')[0];tmpJam =value.finishloaded_date.split('T')[1].substring(0,5)};break;
            case 6: if(value.ontheway_date != null){tmpTanggal= value.ontheway_date.split('T')[0];tmpJam =value.ontheway_date.split('T')[1].substring(0,5)};break;
            case 7: if(value.dropoff_date != null){tmpTanggal= value.dropoff_date.split('T')[0];tmpJam =value.dropoff_date.split('T')[1].substring(0,5)};break;
            case 8: if(value.startunloaded_date != null){tmpTanggal= value.startunloaded_date.split('T')[0];tmpJam =value.startunloaded_date.split('T')[1].substring(0,5)};break;
            case 9: if(value.finishunloaded_date != null){tmpTanggal= value.finishunloaded_date.split('T')[0];tmpJam =value.finishunloaded_date.split('T')[1].substring(0,5)};break;
            case 10: if(value.pod_date != null){tmpTanggal= value.pod_date.split('T')[0];tmpJam =value.pod_date.split('T')[1].substring(0,5)};break;
          }
          tmpTimelineData.push({
            time:tmpJam,
            title:statusDetail[i],
            description:keteranganStatus[i]+tmpTanggal
          })
        }

        return tmpTimelineData;
    }

    render(){
        return(
          <>
          {this.props.tripSelected.infoTrip.tipe_muatan == "LAUT" ? 
            // LAUT
                <View style={{flex: 1}}>
                    <View style={[{backgroundColor:"#008c45", marginBottom:0}]}>
                        <Text style={styles.classHeaderDetailNoFlex}>
                            {this.props.tripSelected.infoTrip.id_trip} {this.props.tripSelected.infoTrip.tipe_muatan == "LAUT" ? '( LAUT )': null}
                        </Text>
                    </View>
                    <CustomMap {...this.props}/>
                    <View style = {styles.lineStyle} />
                        <Text style={{ width:'100%', color:"#008c45", fontSize:13, fontWeight:'bold', textAlign:"center", fontStyle:"italic"}}>
                            {this.props.tripSelected.infoTrip.jenis_kendaraan}
                        </Text>
                    <View style = {styles.lineStyle} />
                    <ScrollView>
                        {this.props.tripSelected.location != "" ? 
                            this.props.tripSelected.location[0].suhu_latest != undefined && this.props.tripSelected.location[0].suhu_latest != null && this.props.tripSelected.location[0].suhu_latest != 0  ? 
                                <View style={[styles.viewDetailTrip,{marginBottom:10, marginTop: 5}]}>
                                                <View 
                                                                            style={[styles.classCardStyle,{
                                                                            shadowOffset: {
                                                                                width: 0,
                                                                                height: 2,
                                                                            },
                                                                            shadowOpacity: 0.1,
                                                                            shadowRadius: 3.84,                                                
                                                                            elevation:1
                                                                            }]}>
                                                    <Text style={[{textAlign: 'center', fontSize: 12, marginBottom: 5}]}>Suhu : {this.props.tripSelected.location[0].suhu_latest} C</Text>
                                            
                                                </View>
                                </View>
                            : null
                        : null}
                        <View style={[styles.viewDetailTrip,{marginBottom:10}]}>
                            <TouchableOpacity 
                                                disabled={this.props.tripSelected.infoTrip.flag_no_container == 'true' ? true:false} 
                                                // this.props.tripSelected.infoTrip.status == 'close' ? true : 
                                                //         this.props.tripSelected.infoTrip.status == 'cancel' ? true : 
                                                //         this.props.tripSelected.infoTrip.status == 'reject' ? true :
                                                //         false
                                                        style={
                                                            this.props.tripSelected.infoTrip.flag_no_container == 'true' ?
                                                        styles.classCardStyle:styles.classCardStyleNew
                                                        //     [styles.classCardStyle,{borderColor:"#008c45",
                                                        // shadowOffset: {
                                                        //     width: 0,
                                                        //     height: 2,
                                                        // },
                                                        // shadowOpacity: 0.1,
                                                        // shadowRadius: 3.84,                                                
                                                        // elevation:5
                                                        // }]
                                                    } 
                                                        onPress={()=>this.openModalNoContainer()
                                                        }>
                                <Text style={[styles.classCardHeader,{textAlign: 'center'}]}>No Container</Text>
                                <Text 
                                    style={
                                        this.props.tripSelected.infoTrip.no_container==null || this.props.tripSelected.infoTrip.no_container==''?
                                        [{fontSize:14,color:"red", padding:5, textAlign: 'center'}]:[{fontSize:13, padding:5, textAlign: 'center'}]
                                    }
                                >
                                    {
                                        this.props.tripSelected.infoTrip.no_container==null || this.props.tripSelected.infoTrip.no_container==''?
                                        "Belum Diproses":
                                        this.props.tripSelected.infoTrip.no_container
                                    } 
                                </Text>
                            </TouchableOpacity>
                        </View>


                        <View style={[styles.viewDetailTrip,{marginBottom:10}]}>
                            <TouchableOpacity disabled={this.props.tripSelected.infoTrip.flag_kendaraan_asal == 'true' ? true: false} 
                                                        style=
                                                        {
                                                        // this.props.tripSelected.infoTrip.naikkapal_date != null ? 
                                                        // this.props.tripSelected.infoTrip.status_order >= 7 ? 
                                                        this.props.tripSelected.infoTrip.flag_kendaraan_asal == 'true' ?
                                                        styles.classCardStyle:styles.classCardStyleNew}
                                                        onPress={()=>this.openModalKendaraan()}>
                                <Text style={styles.classCardHeader}>Kendaraan Asal</Text>
                                {
                                this.props.selectedKendaraan==null?
                                <Text 
                                    style={
                                        this.props.tripSelected.infoTrip.kendaraan_id==null || this.props.tripSelected.infoTrip.kendaraan_id=='0'?
                                        [{fontSize:14,color:"red", padding:5}]:[{fontSize:13, padding:5}]
                                    }
                                >
                                    {
                                        this.props.tripSelected.infoTrip.kendaraan_id==null || this.props.tripSelected.infoTrip.kendaraan_id=='0'?
                                        "Belum Diproses":
                                        this.props.tripSelected.infoTrip.plat_nomor_asal
                                    } 
                                </Text>
                                :
                                <Text 
                                style={{fontSize:14,color:"green", padding:5}}
                                >
                                    {
                                        this.props.selectedKendaraan.name
                                    } 
                                </Text>
                                }
                            </TouchableOpacity>
                            <TouchableOpacity disabled={this.props.tripSelected.infoTrip.flag_kendaraan_tujuan == 'true' ? true:false} 
                            // this.props.tripSelected.infoTrip.status == 'close' ? true : 
                            // this.props.tripSelected.infoTrip.status == 'cancel' ? true : 
                            // this.props.tripSelected.infoTrip.status == 'reject' ? true :
                            // false
                                                        style={
                                                        this.props.tripSelected.infoTrip.flag_kendaraan_tujuan == 'true' ?
                                                        styles.classCardStyle:styles.classCardStyleNew}

                                                        onPress={()=>this.openModalKendaraanTujuan()}>
                                <Text style={styles.classCardHeader}>Kendaraan Tujuan</Text>
                                {
                                this.props.selectedKendaraanTujuan==null?
                                <Text 
                                    style={
                                        this.props.tripSelected.infoTrip.kendaraan_id_tuj==null || this.props.tripSelected.infoTrip.kendaraan_id_tuj=='0'?
                                        [{fontSize:14,color:"red", padding:5}]:[{fontSize:13, padding:5}]
                                    }
                                >
                                    {
                                        this.props.tripSelected.infoTrip.kendaraan_id_tuj==null || this.props.tripSelected.infoTrip.kendaraan_id_tuj=='0'?
                                        "Belum Diproses":
                                        this.props.tripSelected.infoTrip.plat_nomor_tujuan
                                    } 
                                </Text>
                                :
                                <Text 
                                style={{fontSize:14,color:"green", padding:5}}
                                >
                                    {
                                        this.props.selectedKendaraanTujuan.name
                                    } 
                                </Text>
                                }
                            </TouchableOpacity>
                        </View>

                        <View style={[styles.viewDetailTrip,{marginBottom:10}]}>
                            <TouchableOpacity disabled={this.props.tripSelected.infoTrip.flag_kerani_asal == 'true' ? true : false} 
                            // this.props.tripSelected.infoTrip.status == 'close' ? true : 
                            // this.props.tripSelected.infoTrip.status == 'cancel' ? true : 
                            // this.props.tripSelected.infoTrip.status == 'reject' ? true :
                            // this.props.tripSelected.infoTrip.status_order >= 7 ? true :
                            // false
                                                        style=
                                                        {
                                                        // this.props.tripSelected.infoTrip.naikkapal_date != null ? 
                                                        this.props.tripSelected.infoTrip.flag_kerani_asal == 'true' ? 
                                                        // this.props.tripSelected.infoTrip.status_order >= 7 ? 
                                                        styles.classCardStyle:styles.classCardStyleNew}
                                                        onPress={()=>this.openModalKerani()}
                                                        >
                                <Text style={styles.classCardHeader}>Kerani Asal</Text>
                                {
                                this.props.selectedDriver==null?
                                <Text 
                                    style={
                                        this.props.tripSelected.infoTrip.kerani_id_asal==null || this.props.tripSelected.infoTrip.kerani_id_asal== '0'?
                                        [{fontSize:14,color:"red", padding:5}]:[{fontSize:13, padding:5}]
                                    }
                                >
                                    {
                                        this.props.tripSelected.infoTrip.kerani_id_asal==null || this.props.tripSelected.infoTrip.kerani_id_asal== '0'?
                                        "Belum Diproses":
                                        this.props.tripSelected.infoTrip.nama_kerani_asal
                                    } 
                                </Text>
                                :
                                <Text 
                                style={{fontSize:14,color:"green", padding:5}}
                                >
                                    {
                                        this.props.selectedDriver.name
                                    } 
                                </Text>
                                }
                            </TouchableOpacity>
                            {/* {this.props.tripSelected.infoTrip.status_order >= 7 ?  */}

                            <TouchableOpacity disabled={this.props.tripSelected.infoTrip.flag_kerani_tujuan == 'true' ? true : false} 
                                                        style=
                                                        {
                                                        // this.props.tripSelected.infoTrip.naikkapal_date != null ? 
                                                        this.props.tripSelected.infoTrip.flag_kerani_tujuan == 'true' ? 
                                                        styles.classCardStyle:styles.classCardStyleNew} 
                                                        onPress={()=>this.openModalKeraniTujuan()}>
                                <Text style={styles.classCardHeader}>Kerani Tujuan</Text>
                                {
                                this.props.selectedDriverTujuan==null?
                                <Text 
                                    style={
                                        this.props.tripSelected.infoTrip.kerani_id_tujuan==null || this.props.tripSelected.infoTrip.kerani_id_tujuan== '0'?
                                        [{fontSize:14,color:"red", padding:5}]:[{fontSize:13, padding:5}]
                                    }
                                >
                                    {
                                        this.props.tripSelected.infoTrip.kerani_id_tujuan==null || this.props.tripSelected.infoTrip.kerani_id_tujuan== '0'?
                                        "Belum Diproses":
                                        this.props.tripSelected.infoTrip.nama_kerani_tujuan
                                    } 
                                </Text>
                                :
                                <Text 
                                style={{fontSize:14,color:"green", padding:5}}
                                >
                                    {
                                        this.props.selectedDriverTujuan.name
                                    } 
                                </Text>
                                }
                            </TouchableOpacity>
                        </View>

                    {/* {this.props.tripSelected.infoTrip.status_order >= 7 ?  */}
                        <View style={[styles.viewDetailTrip,{marginBottom:10}]}>
                            <TouchableOpacity disabled={this.props.tripSelected.infoTrip.flag_naik == 'true' ? true:false} 
                                                        
                                                        // this.props.tripSelected.infoTrip.status == 'close' ? true : 
                                                        // this.props.tripSelected.infoTrip.status == 'cancel' ? true : 
                                                        // this.props.tripSelected.infoTrip.status == 'reject' ? true :
                                                        // this.props.tripSelected.infoTrip.status_order >= 7 ? false :
                                                        // false}

                                                        style=
                                                        {
                                                        this.props.tripSelected.infoTrip.flag_naik == 'true' ? 
                                                        // this.props.tripSelected.infoTrip.status_order >= 7 ? 
                                                        styles.classCardStyle:styles.classCardStyleNew}
                                                        onPress={()=>this.showDatePicker('naikTgl', 'date')}
                                                        >
                                <Text style={styles.classCardHeader}>Tanggal Naik Kapal</Text>
                                <Text style={
                                    // this.props.tripSelected.infoTrip.naikkapal_date == null || this.props.tripSelected.infoTrip.naikkapal_date == undefined || this.props.tripSelected.infoTrip.naikkapal_date == '' ?
                                    this.props.tripSelected.infoTrip.flag_naik == 'false' ?
                                    [{fontSize:13, padding:5, color: 'red'}] : [{fontSize:13, padding:5}]
                                    }>
                                    {/* {this.state.dateNaik.toString()+' '+this.state.timeNaik.toString()} */}
                                    
                                                    {this.state.dateNaik.toString()}{' '}
                                                    {/* {this.state.timeNaik.toString() == 'Pilih Waktu Naik' ? null: this.state.timeNaik.toString()} */}
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity disabled={this.props.tripSelected.infoTrip.flag_turun == 'true' ? true:false}  
                                                        // this.props.tripSelected.infoTrip.status == 'close' ? true : 
                                                        // this.props.tripSelected.infoTrip.status == 'cancel' ? true : 
                                                        // this.props.tripSelected.infoTrip.status == 'reject' ? true :
                                                        // this.props.tripSelected.infoTrip.status_order >= 7 ? false :
                                                        // false
                                                        style=
                                                        {
                                                        // this.props.tripSelected.infoTrip.naikkapal_date != null ? 
                                                        this.props.tripSelected.infoTrip.flag_turun == 'true' ? 
                                                        styles.classCardStyle:styles.classCardStyleNew}
                                                        onPress={()=>this.showDatePicker('turunTgl', 'date')}
                                                        >
                                <Text style={styles.classCardHeader}>Tanggal Turun Kapal</Text>
                                <Text style={
                                    // this.props.tripSelected.infoTrip.turunkapal_date == null || this.props.tripSelected.infoTrip.turunkapal_date == undefined || this.props.tripSelected.infoTrip.turunkapal_date == '' ?
                                    this.props.tripSelected.infoTrip.flag_turun == 'false' ?
                                    [{fontSize:13, padding:5, color: 'red'}] : [{fontSize:13, padding:5}]
                                    }>
                                                {this.state.dateTurun.toString()}{' '}
                                    {/* {this.state.dateTurun.toString()} {this.state.timeTurun.toString() == 'Pilih Waktu Turun' ? null: this.state.timeTurun.toString()} */}
                                </Text>
                            </TouchableOpacity>
                        </View>
                    {/* :null} */}
                        {/* WAKTU */}

                        {/* <View style={[styles.viewDetailTrip,{marginBottom:10}]}>
                            <TouchableOpacity disabled={this.props.tripSelected.infoTrip.status == 'close' ? true : 
                                                        this.props.tripSelected.infoTrip.status == 'cancel' ? true : 
                                                        this.props.tripSelected.infoTrip.status == 'reject' ? true :
                                                        false} style={[styles.classCardStyle,{borderColor:"#008c45",
                                                        shadowOffset: {
                                                            width: 0,
                                                            height: 2,
                                                        },
                                                        shadowOpacity: 0.1,
                                                        shadowRadius: 3.84,                                                
                                                        elevation:5
                                                        }]} 
                                                        onPress={()=>this.showDatePicker('naikWaktu', 'time')}
                                                        >
                                <Text style={styles.classCardHeader}>Waktu Naik Kapal</Text>
                                <Text style={[{fontSize:13, padding:5}]}>
                                    {this.state.timeNaik.toString()}
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity disabled={this.props.tripSelected.infoTrip.status == 'close' ? true : 
                                                        this.props.tripSelected.infoTrip.status == 'cancel' ? true : 
                                                        this.props.tripSelected.infoTrip.status == 'reject' ? true :
                                                        false} style={[styles.classCardStyle,{borderColor:"#008c45",
                                                        shadowOffset: {
                                                            width: 0,
                                                            height: 2,
                                                        },
                                                        shadowOpacity: 0.1,
                                                        shadowRadius: 3.84,                                                
                                                        elevation:5
                                                        }]} 
                                                        onPress={()=>this.showDatePicker('turunWaktu', 'time')}
                                                        >
                                <Text style={styles.classCardHeader}>Waktu Turun Kapal</Text>
                                <Text style={[{fontSize:13, padding:5}]}>
                                    {this.state.timeTurun.toString()}
                                </Text>
                            </TouchableOpacity>
                        </View> */}


                        <View style={[styles.viewDetailTrip,{marginBottom:10}]}>
                            <View style={[styles.classCardStyle]}>
                                <Text style={styles.classCardHeader}>Kota Asal</Text>
                                <Text 
                                    style={[{fontSize:13, padding:5}]}
                                >
                                    {this.props.tripSelected.infoTrip.kota_asal}
                                </Text>
                            </View>
                            <View style={[styles.classCardStyle]}>
                                <Text style={styles.classCardHeader}>Kota Tujuan</Text>
                                <Text 
                                    style={[{fontSize:13, padding:5}]}
                                >
                                    {this.props.tripSelected.infoTrip.kota_tujuan}
                                </Text>
                            </View>
                        </View>

                        <View style={[styles.viewDetailTrip,{marginBottom:10}]}>
                            <View style={[styles.classCardStyle]}>
                                <Text style={styles.classCardHeader}>Waktu Pengiriman</Text>
                                <Text 
                                    style={[{fontSize:13, padding:5}]}
                                >
                                    {this.props.tripSelected.infoTrip.tanggal_pengiriman.split('T')[0]}{" "}{this.props.tripSelected.infoTrip.jam_pengiriman.substring(0,5)}
                                </Text>
                            </View>
                            <View style={[styles.classCardStyle]}>
                                <Text style={styles.classCardHeader}>Waktu Sampai</Text>
                                <Text 
                                    style={[{fontSize:13, padding:5}]}
                                >
                                    {this.props.tripSelected.infoTrip.tanggal_sampai.split('T')[0]}{" "}{this.props.tripSelected.infoTrip.jam_sampai.substring(0,5)}
                                </Text>
                            </View>
                        </View>

                        <View style={[styles.viewDetailTrip,{marginBottom:10}]}>
                            <View style={[styles.classCardStyle]}>
                                <Text style={styles.classCardHeader}>Estimasi berangkat (ETD)</Text>
                                <Text 
                                    style={[{fontSize:13, padding:5}]}
                                >
                                    {/* 2020-11-20 */}
                                    -
                                    {/* {this.props.tripSelected.infoTrip.tanggal_pengiriman.split('T')[0]}{" "}{this.props.tripSelected.infoTrip.jam_pengiriman.substring(0,5)} */}
                                </Text>
                            </View>
                            <View style={[styles.classCardStyle]}>
                                <Text style={styles.classCardHeader}>Estimasi tiba (ETA)</Text>
                                <Text 
                                    style={[{fontSize:13, padding:5}]}
                                >
                                    {/* 2020-12-01 */}
                                    -
                                    {/* {this.props.tripSelected.infoTrip.tanggal_sampai.split('T')[0]}{" "}{this.props.tripSelected.infoTrip.jam_sampai.substring(0,5)} */}
                                </Text>
                            </View>
                        </View>

                        <Text style={{fontSize:15,color:"black",width:"50%",marginLeft:20}}>Daftar Order</Text>

                        <FlatList
                            horizontal
                            data={this.props.tripSelected.infoOrder}
                            style={{marginLeft:20,marginBottom:10}}
                            renderItem={({ item: value }) => {
                            return (
                                <CardOrder value={value} openModalOrder={this.openModalOrder}/>
                            );
                            }}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </ScrollView>
                    {this.state.showModalOrder?
                        <ModalOrder closeModalOrder={this.closeModalOrder}  {...this.state} {...this.props}/>
                    :null}


                {this.props.tripSelected.infoTrip.status == 'close' || this.props.tripSelected.infoTrip.status == 'cancel' || this.props.tripSelected.infoTrip.status == 'reject' ? 
                    <View style={{flexDirection: 'row', justifyContent: "center", alignItems: "center", margin: 5, padding: 5}} >
                        <Button iconLeft warning block style={{ marginLeft:5,marginRight:5, borderRadius: 5, flex: 1}} onPress={()=>this.props.backToList()}>
                            <Icon small type="MaterialCommunityIcons" name='close-circle-outline' />
                            <Text uppercase={false}>Back</Text>
                        </Button>
                    </View>
                :null}

                {this.props.tripSelected.infoTrip.status == 'confirm' || this.props.tripSelected.infoTrip.status == 'accepted' ? 
                    <View style={{flexDirection: 'row', justifyContent: "center", alignItems: "center", margin: 5, padding: 5}} >
                        <Button iconLeft warning block style={{ marginLeft:5,marginRight:5, borderRadius: 5, flex: 1}} onPress={()=>this.props.backToList()}>
                            <Icon small type="MaterialCommunityIcons" name='close-circle-outline' />
                            <Text uppercase={false}>Back</Text>
                        </Button>
                        <Button iconLeft success block style={{marginLeft:5,marginRight:5, borderRadius: 5, flex: 1 }} onPress={()=>this.props.onSaveAssignLaut()}>
                            <Icon small type="MaterialCommunityIcons" name='check-circle-outline' />
                            <Text uppercase={false}>Save</Text>
                        </Button>
                    </View>
                :null}

                {this.props.tripSelected.infoTrip.status == 'planning' ? 
                    <View style={{flexDirection: 'row', justifyContent: "center", alignItems: "center", margin: 5, padding: 5}} >
                        <Button iconLeft warning block style={{ marginLeft:5,marginRight:5, borderRadius: 5, flex: 1}} onPress={()=>this.props.backToList()}>
                            <Icon small type="MaterialCommunityIcons" name='close-circle-outline' />
                            <Text uppercase={false}>Back</Text>
                        </Button>
                        <Button iconLeft danger block style={{marginLeft:5,marginRight:5, borderRadius: 5, flex: 1}} onPress={()=>this.toggleModalReject()} >
                            <Icon small type="MaterialCommunityIcons" name='close-circle-outline' />
                            <Text uppercase={false}>Reject</Text>
                        </Button>
                        <Button iconLeft success block style={{marginLeft:5,marginRight:5, borderRadius: 5, flex: 1 }} onPress={()=>this.props.onConfirmTripLaut()}>
                            <Icon small type="MaterialCommunityIcons" name='check-circle-outline' />
                            <Text uppercase={false}>Confirm</Text>
                        </Button>
                    </View>
                :null}
                    
                </View>            
            :
            // DARAT
            <View style={{flex: 1}}>
                <View style={[{backgroundColor:"#008c45", marginBottom:0}]}>
                    <Text style={styles.classHeaderDetailNoFlex}>
                        {this.props.tripSelected.infoTrip.id_trip}
                    </Text>
                </View>
                <CustomMap {...this.props}/>
                <View style = {styles.lineStyle} />
                    <Text style={{ width:'100%', color:"#008c45", fontSize:13, fontWeight:'bold', textAlign:"center", fontStyle:"italic"}}>
                        {this.props.tripSelected.infoTrip.jenis_kendaraan}
                    </Text>
                <View style = {styles.lineStyle} />
                <ScrollView>
                    {this.props.tripSelected.location != "" ? 
                        this.props.tripSelected.location[0].suhu_latest != undefined && this.props.tripSelected.location[0].suhu_latest != null && this.props.tripSelected.location[0].suhu_latest != 0 ? 
                            <View style={[styles.viewDetailTrip,{marginBottom:10, marginTop: 5}]}>
                                            <View 
                                                                        style={[styles.classCardStyle,{
                                                                        shadowOffset: {
                                                                            width: 0,
                                                                            height: 2,
                                                                        },
                                                                        shadowOpacity: 0.1,
                                                                        shadowRadius: 3.84,                                                
                                                                        elevation:1
                                                                        }]}>
                                                <Text style={[{textAlign: 'center', fontSize: 12, marginBottom: 5}]}>Suhu : {this.props.tripSelected.location[0].suhu_latest} C</Text>
                                        
                                            </View>
                            </View>
                        : null
                    : null}
                    <View style={[styles.viewDetailTrip,{marginBottom:10}]}>
                        <TouchableOpacity disabled={this.props.tripSelected.infoTrip.status == 'close' ? true :
                                                    this.props.tripSelected.infoTrip.status == 'cancel' ? true : 
                                                    this.props.tripSelected.infoTrip.status == 'reject' ? true :
                                                    false} style={[styles.classCardStyle,{borderColor:"#008c45",
                                                    shadowOffset: {
                                                        width: 0,
                                                        height: 2,
                                                    },
                                                    shadowOpacity: 0.1,
                                                    shadowRadius: 3.84,                                                
                                                    elevation:1
                                                    }]} onPress={()=>this.openModalDriver()}>
                            <Text style={styles.classCardHeader}>Nama Driver</Text>
                            {
                            this.props.selectedDriver==null?
                            <Text 
                                style={
                                    this.props.tripSelected.infoTrip.nama_driver==null?
                                    [{fontSize:14,color:"red", padding:5}]:[{fontSize:13, padding:5}]
                                }
                            >
                                {
                                    this.props.tripSelected.infoTrip.nama_driver==null?
                                    "Belum Diproses":
                                    this.props.tripSelected.infoTrip.nama_driver
                                } 
                            </Text>
                            :
                            <Text 
                                style={{fontSize:14,color:"green", padding:5}}
                            >
                                {
                                    this.props.selectedDriver.name
                                } 
                            </Text>
                            }
                        </TouchableOpacity>
                        <TouchableOpacity disabled={this.props.tripSelected.infoTrip.status == 'close' ? true : 
                                                    this.props.tripSelected.infoTrip.status == 'cancel' ? true : 
                                                    this.props.tripSelected.infoTrip.status == 'reject' ? true :
                                                    false} style={[styles.classCardStyle,{borderColor:"#008c45",
                                                    shadowOffset: {
                                                        width: 0,
                                                        height: 2,
                                                    },
                                                    shadowOpacity: 0.1,
                                                    shadowRadius: 3.84,                                                
                                                    elevation:1
                                                    }]} onPress={()=>this.openModalKendaraan()}>
                            <Text style={styles.classCardHeader}>Plat Nomor</Text>
                            {
                            this.props.selectedKendaraan==null?
                            <Text 
                                style={
                                    this.props.tripSelected.infoTrip.plat_nomor==null?
                                    [{fontSize:14,color:"red", padding:5}]:[{fontSize:13, padding:5}]
                                }
                            >
                                {
                                    this.props.tripSelected.infoTrip.plat_nomor==null?
                                    "Belum Diproses":
                                    this.props.tripSelected.infoTrip.plat_nomor
                                } 
                            </Text>
                            :
                            <Text 
                            style={{fontSize:14,color:"green", padding:5}}
                            >
                                {
                                    this.props.selectedKendaraan.name
                                } 
                            </Text>
                            }
                        </TouchableOpacity>
                    </View>
                    
                    <View style={[styles.viewDetailTrip,{marginBottom:10}]}>
                        <View style={[styles.classCardStyle]}>
                            <Text style={styles.classCardHeader}>Kota Asal</Text>
                            <Text 
                                style={[{fontSize:13, padding:5}]}
                            >
                                {this.props.tripSelected.infoTrip.kota_asal}
                            </Text>
                        </View>
                        <View style={[styles.classCardStyle]}>
                            <Text style={styles.classCardHeader}>Kota Tujuan</Text>
                            <Text 
                                style={[{fontSize:13, padding:5}]}
                            >
                                {this.props.tripSelected.infoTrip.kota_tujuan}
                            </Text>
                        </View>
                    </View>

                    <View style={[styles.viewDetailTrip,{marginBottom:10}]}>
                        <View style={[styles.classCardStyle]}>
                            <Text style={styles.classCardHeader}>Waktu Pengiriman</Text>
                            <Text 
                                style={[{fontSize:13, padding:5}]}
                            >
                                {this.props.tripSelected.infoTrip.tanggal_pengiriman.split('T')[0]}{" "}{this.props.tripSelected.infoTrip.jam_pengiriman.substring(0,5)}
                            </Text>
                        </View>
                        <View style={[styles.classCardStyle]}>
                            <Text style={styles.classCardHeader}>Waktu Sampai</Text>
                            <Text 
                                style={[{fontSize:13, padding:5}]}
                            >
                                {this.props.tripSelected.infoTrip.tanggal_sampai.split('T')[0]}{" "}{this.props.tripSelected.infoTrip.jam_sampai.substring(0,5)}
                            </Text>
                        </View>
                    </View>


                    <Text style={{fontSize:15,color:"black",width:"50%",marginLeft:20}}>Daftar Order</Text>
                    <FlatList
                        horizontal
                        data={this.props.tripSelected.infoOrder}
                        style={{marginLeft:20,marginBottom:10}}
                        renderItem={({ item: value }) => {
                        return (
                            <CardOrder value={value} openModalOrder={this.openModalOrder}/>
                        );
                        }}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </ScrollView>
                {this.state.showModalOrder?
                    <ModalOrder closeModalOrder={this.closeModalOrder}  {...this.state} {...this.props}/>
                :null}

                {/* <View style={{position:'absolute', bottom:0,width: width, height: 65}}> */}


                {this.props.tripSelected.infoTrip.status == 'close' || this.props.tripSelected.infoTrip.status == 'cancel' || this.props.tripSelected.infoTrip.status == 'reject' ? 
                    <View style={{flexDirection: 'row', justifyContent: "center", alignItems: "center", margin: 5, padding: 5}} >
                        <Button iconLeft warning block style={{ marginLeft:5,marginRight:5, borderRadius: 5, flex: 1}} onPress={()=>this.props.backToList()}>
                            <Icon small type="MaterialCommunityIcons" name='close-circle-outline' />
                            <Text uppercase={false}>Back</Text>
                        </Button>
                    </View>
                :null}

                {this.props.tripSelected.infoTrip.status == 'confirm' || this.props.tripSelected.infoTrip.status == 'accepted' ? 
                    <View style={{flexDirection: 'row', justifyContent: "center", alignItems: "center", margin: 5, padding: 5}} >
                        <Button iconLeft warning block style={{ marginLeft:5,marginRight:5, borderRadius: 5, flex: 1}} onPress={()=>this.props.backToList()}>
                            <Icon small type="MaterialCommunityIcons" name='close-circle-outline' />
                            <Text uppercase={false}>Back</Text>
                        </Button>
                        <Button iconLeft success block style={{marginLeft:5,marginRight:5, borderRadius: 5, flex: 1 }} onPress={()=>this.props.onSaveAssign()}>
                            <Icon small type="MaterialCommunityIcons" name='check-circle-outline' />
                            <Text uppercase={false}>Save</Text>
                        </Button>
                    </View>
                :null}

                {this.props.tripSelected.infoTrip.status == 'planning' ? 
                    <View style={{flexDirection: 'row', justifyContent: "center", alignItems: "center", margin: 5, padding: 5}} >
                        <Button iconLeft warning block style={{ marginLeft:5,marginRight:5, borderRadius: 5, flex: 1}} onPress={()=>this.props.backToList()}>
                            <Icon small type="MaterialCommunityIcons" name='close-circle-outline' />
                            <Text uppercase={false}>Back</Text>
                        </Button>
                        <Button iconLeft danger block style={{marginLeft:5,marginRight:5, borderRadius: 5, flex: 1}} onPress={()=>this.toggleModalReject()} >
                            <Icon small type="MaterialCommunityIcons" name='close-circle-outline' />
                            <Text uppercase={false}>Reject</Text>
                        </Button>
                        <Button iconLeft success block style={{marginLeft:5,marginRight:5, borderRadius: 5, flex: 1 }} onPress={()=>this.props.onConfirmTrip()}>
                            <Icon small type="MaterialCommunityIcons" name='check-circle-outline' />
                            <Text uppercase={false}>Confirm</Text>
                        </Button>
                    </View>
                :null}
            </View>
            }

                {/* <View style={{flexDirection: 'row', justifyContent: "center", alignItems: "center", margin: 5, padding: 5}} >


                    <Button iconLeft warning block style={{ marginLeft:5,marginRight:5, borderRadius: 5, flex: 1}} onPress={()=>this.props.backToList()}>
                        <Icon small type="MaterialCommunityIcons" name='close-circle-outline' />
                        <Text uppercase={false}>Back</Text>
                    </Button>


                    <Button iconLeft warning block style={{ marginLeft:5,marginRight:5, borderRadius: 5, flex: 1}} onPress={()=>this.props.backToList()}>
                        <Icon small type="MaterialCommunityIcons" name='close-circle-outline' />
                        <Text uppercase={false}>Back</Text>
                    </Button>
                

                        <Button iconLeft danger block style={{marginLeft:5,marginRight:5, borderRadius: 5, flex: 1}} onPress={()=>this.toggleModalReject()} >
                            <Icon small type="MaterialCommunityIcons" name='close-circle-outline' />
                            <Text uppercase={false}>Reject</Text>
                        </Button>
                        

                    
                        <Button iconLeft success block style={{marginLeft:5,marginRight:5, borderRadius: 5, flex: 1 }} onPress={()=>this.props.onSaveAssign()}>
                            <Icon small type="MaterialCommunityIcons" name='check-circle-outline' />
                            <Text uppercase={false}>Save</Text>
                        </Button>
                        <Button iconLeft success block style={{marginLeft:5,marginRight:5, borderRadius: 5, flex: 1 }} onPress={()=>this.props.onConfirmTrip()}>
                            <Icon small type="MaterialCommunityIcons" name='check-circle-outline' />
                            <Text uppercase={false}>Confirm</Text>
                        </Button>
                </View> */}
            {/* </View> */}

            <DateTimePickerModal
                isVisible={this.state.setDatePickerVisibility}
                mode={this.state.mode}
                onConfirm={(date)=> this.handleConfirm(date)}
                onCancel={()=>this.hideDatePicker()}
            />

            <ModalReject showModalReject={this.state.showModalReject} toggleModalReject={this.toggleModalReject} backToMenu={this.props.backToMenu} {...this.props} {...this.state}/>    
            <ModalDriver closeModalOrder={this.closeModalOrder} driverOrKerani={this.state.driverOrKerani} {...this.props} {...this.state}/>
            <ModalDriverTujuan closeModalOrder={this.closeModalOrder} {...this.props} {...this.state}></ModalDriverTujuan>
            <ModalKendaraan closeModalOrder={this.closeModalOrder} {...this.props} {...this.state}/>
            <ModalKendaraanTujuan closeModalOrder={this.closeModalOrder} {...this.props} {...this.state}></ModalKendaraanTujuan>
            <ModalNoContainer closeModalOrder={this.closeModalOrder} {...this.props} {...this.state}></ModalNoContainer>
          </>
        );
    }
}

export default DetailTrip;