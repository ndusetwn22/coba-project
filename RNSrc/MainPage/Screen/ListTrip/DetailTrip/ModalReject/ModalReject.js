import React,{Component} from 'react'
import {View, ScrollView,TouchableOpacity, Image, TextInput} from "react-native";
import styles from './style.js'
import {Text,Button, Card,CardItem,Body, Label, Picker} from 'native-base'
import Timeline from 'react-native-timeline-flatlist'
import Modal from 'react-native-modal';
import SweetAlert from 'react-native-sweet-alert';
import { allRejectTrip, rejectTrip } from '../../ListTripAxios.js';

class ModalReject extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allReject: [],
            reject: ''
        };
    }

    componentDidMount= async()=>{
        console.log('id trip', this.props.tripSelected.infoTrip)
        console.log('id user', this.props.profile)
        var _allReject = await allRejectTrip(this.props.url)
        this.setState({allReject: _allReject, reject: _allReject[0].id})
    }

    funcSweetAlert = async(title, subTitle, style) =>{
        SweetAlert.showAlertWithOptions({
            title: title,
            subTitle: subTitle,
            confirmButtonTitle: 'OK',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: style,
            cancellable: true
          },
            callback => console.log('callback'));
    }

    functionCombinedReject = async(idAlasan) =>{
        // console.log('alasannn', idAlasan)
        if(this.state.reject === '' ) {this.funcSweetAlert('status', 'Silahkan pilih alasan reject!', 'warning')}
        else{
            var _updateReject = await rejectTrip(this.props.url, this.props.profile.id_transporter, this.props.profile.id_user, this.props.tripSelected.infoTrip.id, this.state.reject)
            this.funcSweetAlert('status', _updateReject[0], _updateReject[1])
            this.props.toggleModalReject()
            this.props.backToMenu()
        }
    }
    
    
    render(){
        return(
            <Modal
                // style={{backgroundColor:"rgba(0,0,0,0.7)", width:"100%", height:"100%", margin:0}}
                isVisible={this.props.showModalReject}
                animationType='slide' 
                onBackdropPress={this.props.toggleModalReject}
            >
                <View style={styles.circleViewStyle}>
                {/* <Text style={{textAlign: 'center', marginTop: 20}}>Kwitansi</Text> */}
                <View style={{flexDirection: 'column', margin: 5, marginTop: 10, justifyContent: 'center'}}>
                    <Text style={{textAlign: 'center', marginTop: 10}}>Alasan Reject</Text>
                        <View 
                            style={{flex: 1,  
                            alignItems: 'center',  
                            justifyContent: 'center',
                            borderColor: 'gray', 
                            borderWidth: 2, 
                            borderRadius: 5, 
                            marginTop: 10, 
                            marginLeft: 20, 
                            marginRight: 20, height: 40, color: '#494A4A', padding: 10 }}>

                                
                                        <Picker
                                            style={{ 
                                            width: "100%",
                                            color: '#344953',  
                                            justifyContent: 'center',
                                            }}
                                            selectedValue={this.state.reject}
                                            onValueChange={(itemValue, itemIndex) => 
                                            this.setState({reject: itemValue})
                                            }
                                        >
                                            
                                            {this.state.allReject.map((value, index) => {
                                                return (
                                                    <Picker.Item
                                                    label={value.keterangan}
                                                    value={value.id}
                                                    key={index}
                                                    />
                                                );
                                                })}


                                        </Picker>

                        </View>                    
                </View>
                
                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                    <Button warning style={{height: 30, marginLeft: 10, marginRight: 10, marginTop: 10}} onPress={this.props.toggleModalReject}>
                        <Text style={{fontSize: 8}}>  Hide  </Text>
                    </Button>
                    <Button style={{backgroundColor: '#CE3C3E', height: 30, marginLeft: 10, marginRight: 10, marginTop: 10}} onPress={()=>this.functionCombinedReject(this.state.reject)}>
                        <Text style={{fontSize: 8}}> Reject </Text>
                    </Button>
                </View>
            </View>
                
          </Modal>
        );
    }
}

export default ModalReject;