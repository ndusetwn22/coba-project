import React,{Component} from 'react'
import {View, ScrollView, RefreshControl, ToastAndroid} from "react-native";
import styles from './style'
import {Text,Button, Content} from 'native-base'
import DateRange from './Component/DateRange/DateRange'
import ModalCalendar from './Component/DateRange/ModalCalendar'
import CardHeader from './Component/CardHeader/CardHeader'
import FlatListTrip from './Component/FlatListTrip/FlatListTrip'
import { getActiveTrip, getActiveTripPlanning } from './ListTripAxios';
import Toast from 'react-native-toast-message'


class ListTripPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            statusTripModal:'confirm',
            modalCalenderVisible:false,
            paramSearch:'',
            statusTripModal:'ALL'
        };
    }

    componentDidMount= async()=>{

    }

    onSelectDate=async (start,end)=>{
        await this.props.setStartEndDate(start,end);
        await this.setState({modalCalenderVisible: false})
        await this.props.onSearchButtonClicked();
    }

    openModalCalendar=(typeTrip)=>{
        this.setState({modalCalenderVisible: true})
    }

    closeModal=()=>{
        this.setState({modalCalenderVisible: false})
    }

    setStatusParam = (status)=>{
        this.setState({statusTripModal:status})
    }

    onRefresh= async() => {
        this.setState({refreshing: true})
        
            var _listActiveTrip = await getActiveTrip(this.props.url, this.props.profile.id_transporter, this.props.startDate, this.props.endDate)
            var _listActiveTripNew = await getActiveTripPlanning(this.props.url, this.props.profile.id_transporter, this.props.startDate, this.props.endDate)

            for ( var index=0; index<_listActiveTripNew.length; index++ ) {
                _listActiveTrip.push(_listActiveTripNew[index]);
        }

            await this.props.refreshContent(_listActiveTrip)
            
            Toast.show({
                type: 'info',
                position: 'bottom',
                text1: 'Refreshing',
                text2: 'Refresh data ...',
                visibilityTime: 3000,
                autoHide: true,
                topOffset: 30,
                bottomOffset: 40,
                onShow: () => {},
                onHide: () => {}
              })
  
            // ToastAndroid.show('Refreshing', ToastAndroid.SHORT);
            this.setState({refreshing: false})
  
      };

    render(){
        return(
          <>
            <Content 
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh.bind(this)}
                    />}
            >
                <View style={styles.flexGridNoPadding}>
                    <View style={{flex:1}}>
                        <DateRange openModalCalendar={this.openModalCalendar} {...this.props}/>
                    </View>
                </View>
                
                <View style={styles.flexGrid}>
                    <CardHeader setStatusParam={this.setStatusParam} {...this.props}/>
                </View>
                
                <FlatListTrip {...this.state} {...this.props}/>
            </Content>
            <ModalCalendar {...this.state} {...this.props} onSelectDate={this.onSelectDate} closeModal={this.closeModal}/>
            <Toast ref={(ref) => Toast.setRef(ref)} />
          </>
        );
    }
}

export default ListTripPage;