import React,{Component} from 'react'
import {BackHandler,TouchableOpacity, Text,View} from "react-native";
import { useNavigation, useRoute  } from '@react-navigation/native';
import Loading,{getProfile, setUrl, getUrl, decrypt} from '../../../../APIProp/ApiConstants'
import CustomNavbar from '../../Header/Navbar/Navbar'
import ListTripPage from './ListTripPage'
import moment from 'moment';
import {
  getActiveTrip,
  getActiveTripPlanning,
  getDetailOrder,
  getLatestLocation,
  getListDriver,
  getListKendaraan,
  funcConfirmTrip, 
  funcConfirmOrder,
  funcAssignKendaraan,
  funcAssignDriver,
  getDetailTripFromNotif,
  getListKerani,
  funcAssignKeraniAsal,
  funcAssignKeraniTujuan,
  funcAssignKendaraanTujuan,
  funcAssignNoContainer,
  funcAssignNaikKapal,
  funcAssignTurunKapal
} from './ListTripAxios'
import DetailTrip from './DetailTrip/DetailTrip'
import SweetAlert from 'react-native-sweet-alert';
import { Container } from 'native-base';
import messaging from '@react-native-firebase/messaging';
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import PushNotification from 'react-native-push-notification'
import NetInfo from "@react-native-community/netinfo";
import LostConnection from '../../Component/LostConnection';
import Axios from 'axios';



class ListTrip extends Component {
  constructor(props) {
    super(props);
    this.state = {
      paramLogin:{
        username:'',
        password:'',
      },
      listActiveTrip:[],
      startDate: moment(),
      endDate: moment(),
      displayedDate: moment(),
      isLoading:false,
      showDetailTrip:false,
      listDriver:[],
      listKerani: [],
      selectedDriver:null,
      selectedDriverTujuan: null,
      selectedKendaraan:null,
      selectedKendaraanTujuan: null,
      dateNaikKapal: null,
      dateTurunKapal: null,
      tempNoContainer: '',
      listKendaraan:[],
      url:null,
      profile:null,
      isConnected: null,
      isConnectedChange: null,
    };
  }
    
  componentDidMount= async()=>{
    await this.setState({isLoading:true});
    await setUrl();
    await getUrl().then(async(result)=>{
      this.setState({url:result})
    });
    await this.funcGetProfile();
    await this.pushNotif();

    await this.cekConnection();

    // console.log('trip route', this.props.route)
    


    // var _listActiveTrip= await getActiveTrip(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate);
    // await this.setState({isLoading:false,listActiveTrip:_listActiveTrip});
    // console.log('active trip', _listActiveTrip)

    if (this.props.route.params != undefined && this.props.route.params.notif_type == 'trip') {
      this.onTripSelectedFromNotif(this.props.route.params.id, this.props.route.params.id_trip)
    }else{
      var _listActiveTrip= await getActiveTrip(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate);
      var _listActiveTripNew= await getActiveTripPlanning(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate);

      for ( var index=0; index<_listActiveTripNew.length; index++ ) {
        _listActiveTrip.push(_listActiveTripNew[index]);
      }

      this.setState({isLoading:false,listActiveTrip:_listActiveTrip});
      console.log('active trip', _listActiveTrip)
    }
  }

  cekConnection = async() =>{
    // Cek Connection
    NetInfo.addEventListener(state => {
      if (this.state.isConnected == null) {
        this.setState({isConnected: state.isConnected})
      }
      if (state.isConnected == false) {
        this.setState({isConnected: state.isConnected, isConnectedChange: state.isConnected})
      }
      if (state.isConnected == true) {
        this.setState({isConnectedChange: state.isConnected})
      }      
      console.log("Is connected?", state.isConnected);
    });
  }

  funcGetProfile=async()=>{
    await getProfile().then(result=>{
      if(result!=null){
        this.setState({profileStr:result})
      }
    })
    if(this.state.profileStr==null){
      this.props.navigation.replace('Login')
    }
    var tmpStr = await decrypt(this.state.profileStr);
    this.setState({profile:JSON.parse(tmpStr)})
  }

  setStartEndDate=async(s,e)=>{
    await this.setState({
      startDate:moment(s, 'YYYY-MM-DD'),
      endDate:moment(e, 'YYYY-MM-DD')
    })
  }

  onSearchButtonClicked=async()=>{
    await this.setState({isLoading:true});
    
    var startDate =this.state.startDate;
    var endDate;

    if(this.state.endDate==null){
      endDate = this.state.startDate;
    }else{
      endDate = this.state.endDate;
    }

    var _listActiveTrip= await getActiveTrip(this.state.url, this.state.profile.id_transporter, startDate, endDate);
    var _listActiveTripNew= await getActiveTripPlanning(this.state.url, this.state.profile.id_transporter, startDate, endDate);
    // _listActiveTrip.push(_listActiveTripNew)
    // console.log('active trip', _listActiveTrip)
    // console.log('active trip', _listActiveTripNew.length)

    for ( var index=0; index<_listActiveTripNew.length; index++ ) {
            _listActiveTrip.push(_listActiveTripNew[index]);
    }
    // console.log('push data', _listActiveTrip)
    // data = tempData;

    await this.setState({
      isLoading:false,
      listActiveTrip:_listActiveTrip
    });
  }

  onTripSelected=async(infoTrip)=>{
    await this.setState({isLoading:true,tripSelected:null, selectedDriver:null, selectedKendaraan:null, selectedDriverTujuan: null, selectedKendaraanTujuan: null, dateNaikKapal: null, dateTurunKapal: null});
    // console.log('tripselected', infoTrip)
    var _tripSelected = {};
    _tripSelected.infoTrip = infoTrip;
    _tripSelected.infoOrder = await getDetailOrder(this.state.url,infoTrip.id);
    _tripSelected.location = await getLatestLocation(this.state.url,infoTrip.id);

    console.log('tripselected : ', _tripSelected)
    // await this.setState({selectedDriver: _tripSelected.infoTrip.kerani_id_asal, selectedDriverTujuan: _tripSelected.infoTrip.kerani_id_tujuan, selectedKendaraan: _tripSelected.infoTrip.kendaraan_id, selectedKendaraanTujuan: _tripSelected.infoTrip.kendaraan_id_tuj, dateNaikKapal: _tripSelected.infoTrip.naikkapal_date, dateTurunKapal: _tripSelected.infoTrip.turunkapal_date})

    await this.setState({isLoading:false, tripSelected:_tripSelected, showDetailTrip:true});
  }

  onTripSelectedFromNotif=async(id, id_trip)=>{
    // console.log('masuk trip detail',this.props.route.params.id)
    await this.setState({isLoading: true, showDetailTrip: true});
    var _tripSelected = {};
    _tripSelected.infoTrip = await getDetailTripFromNotif(this.state.url, id);
    _tripSelected.infoOrder = await getDetailOrder(this.state.url, id);
    _tripSelected.location = await getLatestLocation(this.state.url, id);
    console.log('detail trip from notif', _tripSelected)
    await this.setState({isLoading: false, tripSelected:_tripSelected, showDetailTrip: true});
  }

  onShowModalKerani=async()=>{
    await this.setState({isLoading:true});
    var _listKerani = await getListKerani(this.state.url,this.state.profile.id_transporter);
    var listKerani =[];
    _listKerani.map((value,index)=>{
      listKerani.push({id:value.id_driver, name:value.nama_driver});
    });
    await this.setState({isLoading:false, listDriver:listKerani});
  }

  onShowModalKeraniTujuan=async()=>{
    await this.setState({isLoading:true});
    var _listKerani = await getListKerani(this.state.url,this.state.profile.id_transporter);
    var listKerani =[];
    _listKerani.map((value,index)=>{
      listKerani.push({id:value.id_driver, name:value.nama_driver});
    });
    await this.setState({isLoading:false, listDriver:listKerani});
  }

  onShowModalDriver=async()=>{
    await this.setState({isLoading:true});
    var _listDriver = await getListDriver(this.state.url,this.state.profile.id_transporter);
    var listDriver =[];
    _listDriver.map((value,index)=>{
      listDriver.push({id:value.id_driver, name:value.nama_driver});
    });
    await this.setState({isLoading:false, listDriver:listDriver});
  }

  onShowModalKendaraan=async()=>{
    await this.setState({isLoading:true});
    var _listKendaraan = await getListKendaraan(this.state.url,this.state.profile.id_transporter, this.state.tripSelected.infoTrip.jenis_kendaraan_id);
    var listKendaraan =[];
    _listKendaraan.map((value,index)=>{
      listKendaraan.push(
        {
          id:value.id, 
          name:value.external_id+' - '+value.kendaraan, 
          // name:value.kendaraan, 
          jatuh_tempo_kir:value.jatuh_tempo_kir,
          tahun_kendaraan:value.tahun_kendaraan,
          status_integrasi: value.status_integrasi,
          external_id: value.external_id,
          flag_validasi : value.flag_validasi,
          flag_tahun_kendaraan : value.flag_tahun_kendaraan
        });
    });

    await this.setState({isLoading:false, listKendaraan:listKendaraan});
  }

  onShowModalKendaraanTujuan=async()=>{
    await this.setState({isLoading:true});
    var _listKendaraan = await getListKendaraan(this.state.url,this.state.profile.id_transporter, this.state.tripSelected.infoTrip.jenis_kendaraan_id);
    var listKendaraan =[];
    _listKendaraan.map((value,index)=>{
      listKendaraan.push(
        {
          id:value.id, 
          name:value.external_id+' - '+value.kendaraan, 
          jatuh_tempo_kir:value.jatuh_tempo_kir,
          tahun_kendaraan:value.tahun_kendaraan,
          status_integrasi: value.status_integrasi,
          flag_validasi : value.flag_validasi,
          flag_tahun_kendaraan : value.flag_tahun_kendaraan 
        });
    });

    await this.setState({isLoading:false, listKendaraan:listKendaraan});
  }

  onShowModalNoContainer = async() => {
    await this.setState({isLoading: true})

  }

  onChangeTextNoContainer = async(input) => {
      var temp = this.state.tripSelected

      temp.infoTrip.no_container = input
      this.setState({
          tripSelected: temp,
          tempNoContainer: input
      })
  }

  onSetDriver=async (driver)=>{
    await this.setState({selectedDriver:driver})
  }

  onSetDriverTujuan=async (driver)=>{
    await this.setState({selectedDriverTujuan:driver})
  }

  onSetKendaraan=async (kendaraan)=>{
    console.log('KENDARAAN', kendaraan)

    // var _temp = this.state.tripSelected
    // _temp.infoTrip.kendaraan_id = 
    
    var year = moment().year()
    // kendaraan.tahun_kendaraan <= (year-10)
    if (kendaraan.flag_tahun_kendaraan && kendaraan.flag_validasi) {
      this.funcSweetAlert('warning','Failed',"Tahun Kendaraan > 10 Tahun");
    }else{
      if (kendaraan.status_integrasi == 'belum integrasi' && kendaraan.flag_validasi) {
        this.funcSweetAlert('warning','Failed',"Kendaraan belum terintegrasi dengan GPS");
      }else{
        await this.setState({selectedKendaraan:kendaraan})
        console.log('my selected kendaraan', this.state.selectedKendaraan)
      }
    }
  }

  onSetKendaraanTujuan=async (kendaraan)=>{
    console.log('KENDARAAN TUJUAN', kendaraan)
    var year = moment().year()
    if (kendaraan.flag_tahun_kendaraan && kendaraan.flag_validasi) {
      this.funcSweetAlert('warning','Failed',"Tahun Kendaraan > 10 Tahun");
    }else{
      if (kendaraan.status_integrasi == 'belum integrasi' && kendaraan.flag_validasi) {
        this.funcSweetAlert('warning','Failed',"Kendaraan belum terintegrasi dengan GPS");
      }else{
        await this.setState({selectedKendaraanTujuan:kendaraan})
      }
    }
  }

  onSetDateNaikTurunKapal=async (type, tanggal)=>{
    // console.log('Naik Turun Kapal : ', type+' '+tanggal+':00')
    var _temp = tanggal.split(' ')[0]
    var _slice = _temp.split('-')
    var _tempWaktu = _slice[2]+'-'+_slice[1]+'-'+_slice[0]
    _temp = _tempWaktu+' '+tanggal.split(' ')[1]

    console.log('hasil olahan', _temp)

    if (type == 'naikJagain') {
      this.setState({dateNaikKapal: tanggal})
    }
    if (type == 'turunJagain') {
      this.setState({dateTurunKapal: tanggal})
    }


    if (type == 'naik') {
      this.setState({dateNaikKapal: _temp+':00'})
      // console.log('NAIK', (_temp+':00').length)
    }
    if (type == 'turun') {
      this.setState({dateTurunKapal: _temp+':00'})
      // console.log('NAIK', (_temp+':00').length)
    }

    
    // if (type == 'naik') {
    //   this.setState({dateNaikKapal: _temp+':00'})
    //   // console.log('NAIK', (_temp+':00').length)
    // }else{
    //   this.setState({dateTurunKapal: _temp+':00'})
    //   // console.log("TURUN", (_temp+':00').length)
    // }
  }

  onConfirmTrip=async()=>{
    await this.setState({isLoading:true});
    var dataConfirm = await funcConfirmTrip(this.state.url, this.state.tripSelected.infoTrip.id, this.state.profile.id_user);
    console.log("dataConfirm")
    console.log(dataConfirm)
    if(dataConfirm !=null || dataConfirm !=undefined){
      if(dataConfirm.length>0){
        if(dataConfirm[0].id_trip == this.state.tripSelected.infoTrip.id){
          console.log("dataConfirmed")
          if(this.state.selectedDriver!=null){
            await funcAssignDriver(this.state.url,this.state.tripSelected.infoTrip.id, this.state.profile.id_user,this.state.selectedDriver.id)
          }
          console.log("dataDriver")
          if(this.state.selectedKendaraan!=null){
            var _idKendaraan = await funcAssignKendaraan(this.state.url,this.state.tripSelected.infoTrip.id, this.state.profile.id_user,this.state.selectedKendaraan.id)
              console.log("dataKendaraan", _idKendaraan)
              if (_idKendaraan[0] != undefined || _idKendaraan[0] != null) {
                if (_idKendaraan[0].id_trip != this.state.selectedKendaraan.id) {
                  this.funcSweetAlert('warning','Failed',"Gagal Assign Kendaraan");
                }else{
                  // sukses assign kendaraan
                  // this.funcSweetAlert('success','Sukses',"Simpan Perubahan!");
                }
              }else{
                this.funcSweetAlert('warning','Failed',"Gagal Assign Kendaraan");
              }
          }
          console.log("dataKendaraan")
          var dataOrder = this.state.tripSelected.infoOrder;
          console.log(dataOrder)
          for(var i=0;i<dataOrder.length;i++){
            await funcConfirmOrder(this.state.url,dataOrder[i].idoforder, this.state.profile.id_user)
          } 
          console.log('dataOrder')


          var id_trip = {
            "id_trip" : this.state.tripSelected.infoTrip.id
          }
          

          var hit_api = 
              await Axios.post("https://mostrans.co.id/GPSVendor/transporterconf",id_trip)
              .then(async(result)=>{
                console.log('response hit', result)
                var response = result.data.desc
                return response;
                // return this.funcSweetAlert('success','Success',"Confirmed Trip, Thankyou! "+response.desc);
              })
              .catch(error=>{
                console.log('error', error)
                return 'error hit api';
              });


          // return await Axios.post("https://mostrans.co.id/GPSVendor/transporterconf",id_trip)
          //   .then(async(result)=>{
          //     console.log('response hit', result)
          //     var response = result.data
          //     return this.funcSweetAlert('success','Success',"Confirmed Trip, Thankyou! "+response.desc);
          //   })
          //   .catch(error=>{
          //     console.log('error', error)
          //   });

          await this.funcSweetAlert('success','Success',"Confirmed Trip, Thankyou! ("+hit_api+")");
          await this.setState({isLoading: false});
          return ;
        }
      }
    }
    this.funcSweetAlert('warning','Failed',"Gagal Konfirmasi Trip");
    await this.setState({isLoading: false});
  }

  onConfirmTripLaut=async()=>{
    await this.setState({isLoading:true});
    var dataConfirm = await funcConfirmTrip(this.state.url, this.state.tripSelected.infoTrip.id, this.state.profile.id_user);
    console.log("dataConfirm")
    console.log(dataConfirm)
    if(dataConfirm !=null || dataConfirm !=undefined){
      if(dataConfirm.length>0){
        if(dataConfirm[0].id_trip == this.state.tripSelected.infoTrip.id){
          console.log("dataConfirmed")

          // Assign Container
          if(this.state.tripSelected.infoTrip.no_container!='' || this.state.tripSelected.infoTrip.no_container!=null){
            await funcAssignNoContainer(this.state.url,this.state.tripSelected.infoTrip.id, this.state.profile.id_user, this.state.tripSelected.infoTrip.no_container)
            console.log("Container")
          }

          // Kerani Asal
          if(this.state.selectedDriver!=null){
            await funcAssignKeraniAsal(this.state.url,this.state.tripSelected.infoTrip.id, this.state.profile.id_user,this.state.selectedDriver.id)
          }
          console.log("dataKeraniAsal")

          // Kerani Tujuan
          if(this.state.selectedDriverTujuan!=null){
            await funcAssignKeraniTujuan(this.state.url,this.state.tripSelected.infoTrip.id, this.state.profile.id_user,this.state.selectedDriverTujuan.id)
          }
          console.log("dataKeraniTujuan")


          // Kendaraan Asal
          if(this.state.selectedKendaraan!=null){
            var _idKendaraan = await funcAssignKendaraan(this.state.url,this.state.tripSelected.infoTrip.id, this.state.profile.id_user,this.state.selectedKendaraan.id)
              console.log("dataKendaraan", _idKendaraan)
              if (_idKendaraan[0] != undefined || _idKendaraan[0] != null) {
                if (_idKendaraan[0].id_trip != this.state.selectedKendaraan.id) {
                  this.funcSweetAlertWithoutCallback('warning','Failed',"Gagal Assign Kendaraan Asal");
                }else{
                  // this.funcSweetAlert('success','Sukses',"Simpan Perubahan!");
                }
              }else{
                this.funcSweetAlertWithoutCallback('warning','Failed',"Gagal Assign Kendaraan Asal");
              }
          }
          console.log("dataKendaraan")


          // Kendaraan Tujuan
          if(this.state.selectedKendaraanTujuan!=null){
            var _idKendaraanTujuan = await funcAssignKendaraanTujuan(this.state.url,this.state.tripSelected.infoTrip.id, this.state.profile.id_user,this.state.selectedKendaraanTujuan.id)
              console.log("dataKendaraan", _idKendaraan)
              if (_idKendaraanTujuan[0] != undefined || _idKendaraanTujuan[0] != null) {
                if (_idKendaraanTujuan[0].id_trip != this.state.selectedKendaraanTujuan.id) {
                  this.funcSweetAlertWithoutCallback('warning','Failed',"Gagal Assign Kendaraan Tujuan");
                }else{
                  // this.funcSweetAlert('success','Sukses',"Simpan Perubahan!");
                }
              }else{
                this.funcSweetAlertWithoutCallback('warning','Failed',"Gagal Assign Kendaraan Tujuan");
              }
          }
          console.log("dataKendaraanTujuan")


          var dataOrder = this.state.tripSelected.infoOrder;
          // console.log(dataOrder)
          for(var i=0;i<dataOrder.length;i++){
            await funcConfirmOrder(this.state.url,dataOrder[i].idoforder, this.state.profile.id_user)
          } 
          console.log('dataOrder')


          // // Assign Naik Kapal
          // var dataOrderJumlah = this.state.tripSelected.infoOrder;
          // // console.log(dataOrderJumlah)
          // if(this.state.dateNaikKapal!=null){
          //   for(var i=0;i<dataOrderJumlah.length;i++){
          //     await funcAssignNaikKapal(this.state.url,dataOrderJumlah[i].idoforder, this.state.profile.id_user,this.state.dateNaikKapal)
          //   } 
          //   console.log("assignNaikKapal")
          // }

          // // Assign Turun Kapal
          // if(this.state.dateTurunKapal!=null){
          //   for(var i=0;i<dataOrderJumlah.length;i++){
          //     await funcAssignTurunKapal(this.state.url,dataOrderJumlah[i].idoforder, this.state.profile.id_user,this.state.dateTurunKapal)
          //   }
          //   console.log("assignTurunKapal") 
          // }

          var id_trip = {
            "id_trip" : this.state.tripSelected.infoTrip.id
          }
          

          var hit_api = 
              await Axios.post("https://mostrans.co.id/GPSVendor/transporterconf",id_trip)
              .then(async(result)=>{
                console.log('response hit', result)
                var response = result.data.desc
                return response;
                // return this.funcSweetAlert('success','Success',"Confirmed Trip, Thankyou! "+response.desc);
              })
              .catch(error=>{
                console.log('error', error)
                return 'error hit api';
              });


          await this.funcSweetAlert('success','Success',"Confirmed Trip Laut, Thankyou! ("+hit_api+")");
          await this.setState({isLoading: false});
          return ;
          
        }
      }
    }
    this.funcSweetAlert('warning','Failed',"Gagal Konfirmasi Trip Laut");
    await this.setState({isLoading: false});
  }

  onSaveAssign=async()=>{
    await this.setState({isLoading: true});

    var boolDriver = true;
    var boolKendaraan = true;
    if(this.state.selectedDriver!=null){
      var _assignDriver = await funcAssignDriver(this.state.url,this.state.tripSelected.infoTrip.id, this.state.profile.id_user,this.state.selectedDriver.id)
      console.log("dataDriver", _assignDriver)
      if (_assignDriver[0] != undefined || _assignDriver[0] != null) {
        if (_assignDriver[0].id_trip != this.state.selectedDriver.id) {
          boolDriver = false;
        }else{
          boolDriver = true;
        }
      }else{
        boolDriver = false;
      }
    }
    if(this.state.selectedKendaraan!=null){
      var _idKendaraan = await funcAssignKendaraan(this.state.url,this.state.tripSelected.infoTrip.id, this.state.profile.id_user,this.state.selectedKendaraan.id)
      console.log("dataKendaraan", _idKendaraan)
      if (_idKendaraan[0] != undefined || _idKendaraan[0] != null) {
        if (_idKendaraan[0].id_trip != this.state.selectedKendaraan.id) {
          boolKendaraan = false;
        }else{
          boolKendaraan = true;
        }
      }else{
        boolKendaraan = false;
      }
    }

    var id_trip = {
      "id_trip" : this.state.tripSelected.infoTrip.id
    }

    if (boolDriver || boolKendaraan) {
      // Call API, tambah concat 
        var hit_api = 
                await Axios.post("https://mostrans.co.id/GPSVendor/transporterconf",id_trip)
                .then(async(result)=>{
                  console.log('response hit', result)
                  var response = result.data.desc
                  return response;
                  // return this.funcSweetAlert('success','Success',"Confirmed Trip, Thankyou! "+response.desc);
                })
                .catch(error=>{
                  console.log('error', error)
                  return 'error hit api';
                });
    }

    if (boolDriver && boolKendaraan) {
        this.funcSweetAlert('success','Sukses',"Simpan Perubahan! ("+hit_api+")");
    }else if(boolDriver == false && boolKendaraan == false){
        this.funcSweetAlert('warning','Failed',"Gagal Simpan Kendaraan & Driver! ("+hit_api+")");
      }else if(boolDriver == false && boolKendaraan == true){
        this.funcSweetAlert('warning','Failed',"Gagal Simpan Driver! ("+hit_api+")");
      }else {
        this.funcSweetAlert('warning','Failed',"Gagal Simpan Kendaraan! ("+hit_api+")");
      }

      await this.setState({isLoading: false});
  }

  onSaveAssignLaut=async()=>{
    await this.setState({isLoading: true});
    var boolDriver = true;
    var boolKendaraan = true;
    var boolDriverTujuan = true;
    var boolKendaraanTujuan = true;
    var boolContainerPenjagaan1 = true; // true boleh query
    var boolContainerPenjagaan2 = true; // true boleh query

    // console.log('date naik kapal: ', this.state.dateNaikKapal.length)

    // NGECEK KALO STATUS ORDERNYA LEBIH DARI SAMADENGAN 7 --> untuk assign naik turun tgl kapal
    // if (this.state.tripSelected.infoTrip.status_order >= 7) {

      if (this.state.dateNaikKapal != null) {
        if (this.state.dateNaikKapal.length < 19 ) {
          return this.funcSweetAlertWithoutCallback('warning', 'Failed', 'Tanggal naik harus ada jam!')
        }else{
          
            // Assign Naik Kapal
            var dataOrderJumlah = this.state.tripSelected.infoOrder;
            // console.log(dataOrderJumlah)
            for(var i=0;i<dataOrderJumlah.length;i++){
              await funcAssignNaikKapal(this.state.url,dataOrderJumlah[i].idoforder, this.state.profile.id_user,this.state.dateNaikKapal)
            } 
            console.log("assignNaikKapal")
        }
      }

      console.log('state turun kapal', this.state.dateTurunKapal)
      if (this.state.dateTurunKapal != null) {
        if (this.state.dateTurunKapal.length < 19 ) {
          return this.funcSweetAlertWithoutCallback('warning', 'Failed', 'Tanggal turun harus ada jam!')
        }else{

            // Assign Turun Kapal
            var dataOrderJumlah = this.state.tripSelected.infoOrder;
              for(var i=0;i<dataOrderJumlah.length;i++){
                await funcAssignTurunKapal(this.state.url,dataOrderJumlah[i].idoforder, this.state.profile.id_user,this.state.dateTurunKapal)
              }
              console.log("assignTurunKapal") 

          // this.funcSweetAlertWithoutCallback('success', 'Sukses', 'Query tanggal turun kapal!')
        }
      }

    // }

    if(this.state.tripSelected.infoTrip.kerani_id_asal == 0 && this.state.selectedDriver == null){boolContainerPenjagaan1=true;}
    if(this.state.tripSelected.infoTrip.kerani_id_asal == 0 && this.state.selectedDriver != null){boolContainerPenjagaan1=false;} // false ada penjagaan dibawah
    if(this.state.tripSelected.infoTrip.kerani_id_asal == null && this.state.selectedDriver == null){boolContainerPenjagaan1=true;}
    if(this.state.tripSelected.infoTrip.kerani_id_asal == null && this.state.selectedDriver != null){boolContainerPenjagaan1=false;} // false ada penjagaan dibawah
    if(this.state.tripSelected.infoTrip.kerani_id_asal != 0 && this.state.tripSelected.infoTrip.kerani_id_asal != null && this.state.selectedDriver == null){boolContainerPenjagaan1=false;}
    if(this.state.tripSelected.infoTrip.kerani_id_asal != 0 && this.state.tripSelected.infoTrip.kerani_id_asal != null && this.state.selectedDriver != null){boolContainerPenjagaan1=false;} // false ada penjagaan dibawah

    if(this.state.tripSelected.infoTrip.kerani_id_tujuan == 0 && this.state.selectedDriverTujuan == null){boolContainerPenjagaan2=true;}
    if(this.state.tripSelected.infoTrip.kerani_id_tujuan == 0 && this.state.selectedDriverTujuan != null){boolContainerPenjagaan2=false;}
    if(this.state.tripSelected.infoTrip.kerani_id_tujuan == null && this.state.selectedDriverTujuan == null){boolContainerPenjagaan2=true;}
    if(this.state.tripSelected.infoTrip.kerani_id_tujuan == null && this.state.selectedDriverTujuan != null){boolContainerPenjagaan2=false;}
    if(this.state.tripSelected.infoTrip.kerani_id_tujuan != 0 && this.state.tripSelected.infoTrip.kerani_id_tujuan != null && this.state.selectedDriverTujuan == null){boolContainerPenjagaan2=false;}
    if(this.state.tripSelected.infoTrip.kerani_id_tujuan != 0 && this.state.tripSelected.infoTrip.kerani_id_tujuan != null && this.state.selectedDriverTujuan != null){boolContainerPenjagaan2=false;}

    // this.funcSweetAlertWithoutCallback('success', 'Sukses', 'DRIVER ASAL : '+this.state.selectedDriver+' Driver TUjuan :'+this.state.selectedDriverTujuan)
    // this.funcSweetAlertWithoutCallback('success', 'Sukses', 'Cek Boolean : !'+boolContainerPenjagaan1+boolContainerPenjagaan2)



    if (boolContainerPenjagaan1 && boolContainerPenjagaan2) {
        await funcAssignNoContainer(this.state.url,this.state.tripSelected.infoTrip.id, this.state.profile.id_user, this.state.tripSelected.infoTrip.no_container.toUpperCase())
        // this.funcSweetAlertWithoutCallback('success', 'Sukses', 'Lakukan query assign1 container!')
    }else if(boolContainerPenjagaan1 == true && boolContainerPenjagaan2 == false){
      if (boolContainerPenjagaan1 != true) {
        if (this.state.tripSelected.infoTrip.no_container=='' || this.state.tripSelected.infoTrip.no_container==null) {
          return this.funcSweetAlertWithoutCallback('warning', 'Failed', 'No Container Harus Diisi!')
          // return this.funcSweetAlertWithoutCallback('warning', 'Failed', 'No Container Tidak Boleh Kosong1!'+boolContainerPenjagaan1+boolContainerPenjagaan2)
        }
      }
      else{
        await funcAssignNoContainer(this.state.url,this.state.tripSelected.infoTrip.id, this.state.profile.id_user, this.state.tripSelected.infoTrip.no_container.toUpperCase())
        // this.funcSweetAlertWithoutCallback('success', 'Sukses', 'Lakukan query assign2 container2!') 
      }
    }else if(boolContainerPenjagaan1 == false && boolContainerPenjagaan2 == true){
      if (boolContainerPenjagaan2 != true) {
        if (this.state.tripSelected.infoTrip.no_container=='' || this.state.tripSelected.infoTrip.no_container==null) {
          return this.funcSweetAlertWithoutCallback('warning', 'Failed', 'No Container Harus Diisi!')
          // return this.funcSweetAlertWithoutCallback('warning', 'Failed', 'No Container Tidak Boleh Kosong2!'+boolContainerPenjagaan1+boolContainerPenjagaan2)
        }
      }else{
        await funcAssignNoContainer(this.state.url,this.state.tripSelected.infoTrip.id, this.state.profile.id_user, this.state.tripSelected.infoTrip.no_container.toUpperCase())
        // this.funcSweetAlertWithoutCallback('success', 'Sukses', 'Lakukan query assign3 container3!') 
      }
    }else{
      if (this.state.tripSelected.infoTrip.no_container=='' || this.state.tripSelected.infoTrip.no_container==null) {
        return this.funcSweetAlertWithoutCallback('warning', 'Failed', 'No Container Harus Diisi!')
        // return this.funcSweetAlertWithoutCallback('warning', 'Failed', 'No Container Tidak Boleh Kosong3!'+boolContainerPenjagaan1+boolContainerPenjagaan2)
      }else{
        await funcAssignNoContainer(this.state.url,this.state.tripSelected.infoTrip.id, this.state.profile.id_user, this.state.tripSelected.infoTrip.no_container.toUpperCase())
        // this.funcSweetAlertWithoutCallback('success', 'Sukses', 'Lakukan query assign4 container4!') 
      }
    }
    
    // else{
    //   // kalo salah satu false
    //   if (this.state.tripSelected.infoTrip.kerani_id_tujuan == 0 || this.state.tripSelected.infoTrip.kerani_id_tujuan == null ) {
        
    //   }


    //   // if (this.state.tripSelected.infoTrip.no_container=='' || this.state.tripSelected.infoTrip.no_container==null) {
    //   //     return this.funcSweetAlertWithoutCallback('warning', 'Failed', 'No Container Tidak Boleh Kosong!'+boolContainerPenjagaan1+boolContainerPenjagaan2)
    //   // }else{
    //   //   this.funcSweetAlertWithoutCallback('success', 'Sukses', 'Assign Container Null!')
    //   // }
    // }

    // KERANI ASAL
    if(this.state.selectedDriver!=null){
      var _assignDriver = await funcAssignKeraniAsal(this.state.url,this.state.tripSelected.infoTrip.id, this.state.profile.id_user,this.state.selectedDriver.id)
      console.log("dataKerani : ", _assignDriver)
      if (_assignDriver[0] != undefined || _assignDriver[0] != null) {
        if (_assignDriver[0].id_trip != this.state.selectedDriver.id) {
          boolDriver = false;
          this.funcSweetAlert('warning','Failed',"Gagal Simpan Kerani Asal!");
        }else{
          boolDriver = true;
        }
      }else{
        boolDriver = false;
        this.funcSweetAlert('warning','Failed',"Gagal Simpan Kerani Asal!");
      }
    }

    // KERANI TUJUAN
    if(this.state.selectedDriverTujuan!=null){
      var _assignDriverTujuan = await funcAssignKeraniTujuan(this.state.url,this.state.tripSelected.infoTrip.id, this.state.profile.id_user,this.state.selectedDriverTujuan.id)
      console.log("dataKeraniTujuan : ", _assignDriverTujuan)
      if (_assignDriverTujuan[0] != undefined || _assignDriverTujuan[0] != null) {
        if (_assignDriverTujuan[0].id_trip != this.state.selectedDriverTujuan.id) {
          boolDriverTujuan = false;
          this.funcSweetAlert('warning','Failed',"Gagal Simpan Kerani Tujuan!");
        }else{
          boolDriverTujuan = true;
        }
      }else{
        boolDriverTujuan = false;
        this.funcSweetAlert('warning','Failed',"Gagal Simpan Kerani Tujuan!");
      }
    }

    // KENDARAAN ASAL
    if(this.state.selectedKendaraan!=null){
      var _idKendaraan = await funcAssignKendaraan(this.state.url,this.state.tripSelected.infoTrip.id, this.state.profile.id_user,this.state.selectedKendaraan.id)
      console.log("dataKendaraan final : ", _idKendaraan)
      if (_idKendaraan[0] != undefined || _idKendaraan[0] != null) {
        if (_idKendaraan[0].id_trip != this.state.selectedKendaraan.id) {
          boolKendaraan = false;
          this.funcSweetAlert('warning','Failed',"Gagal Simpan Kendaraan Asal!");
        }else{
          boolKendaraan = true;
        }
      }else{
        boolKendaraan = false;
        this.funcSweetAlert('warning','Failed',"Gagal Simpan Kendaraan Asal!");
      }
    }

    // KENDARAAN Tujuan
    if(this.state.selectedKendaraanTujuan!=null){
      var _idKendaraanTujuan = await funcAssignKendaraanTujuan(this.state.url,this.state.tripSelected.infoTrip.id, this.state.profile.id_user,this.state.selectedKendaraanTujuan.id)
      console.log("dataKendaraanTujuan : ", _idKendaraanTujuan)
      if (_idKendaraanTujuan[0] != undefined || _idKendaraanTujuan[0] != null) {
        if (_idKendaraanTujuan[0].id_trip != this.state.selectedKendaraanTujuan.id) {
          boolKendaraanTujuan = false;
          this.funcSweetAlert('warning','Failed',"*Gagal Simpan Kendaraan Tujuan!");
        }else{
          boolKendaraanTujuan = true;
        }
      }else{
        boolKendaraanTujuan = false;
        this.funcSweetAlert('warning','Failed',"Gagal Simpan Kendaraan Tujuan!");
      }
    }

    // // Assign Naik Kapal
    // var dataOrderJumlah = this.state.tripSelected.infoOrder;
    // // console.log(dataOrderJumlah)
    // if(this.state.dateNaikKapal!=null){
    //   for(var i=0;i<dataOrderJumlah.length;i++){
    //     await funcAssignNaikKapal(this.state.url,dataOrderJumlah[i].idoforder, this.state.profile.id_user,this.state.dateNaikKapal)
    //   } 
    //   console.log("assignNaikKapal")
    // }
    

    // // Assign Turun Kapal
    // if(this.state.dateTurunKapal!=null){
    //   for(var i=0;i<dataOrderJumlah.length;i++){
    //     await funcAssignTurunKapal(this.state.url,dataOrderJumlah[i].idoforder, this.state.profile.id_user,this.state.dateTurunKapal)
    //   }
    //   console.log("assignTurunKapal") 
    // }

    // if (boolDriver && boolKendaraan && boolDriverTujuan && boolKendaraanTujuan) {
    //     this.funcSweetAlert('success','Sukses',"Simpan Perubahan!");
    // }else if(boolDriver == false && boolKendaraan == false && boolDriverTujuan == false && boolKendaraanTujuan == false){
    //     this.funcSweetAlert('warning','Failed',"Gagal Simpan Kendaraan & Driver!");
    //   }else if(boolDriver == false && boolKendaraan == true && boolDriverTujuan == false && boolKendaraanTujuan == true){
    //     this.funcSweetAlert('warning','Failed',"Gagal Simpan Driver!");
    //   }else if(boolDriver == true && boolKendaraan == false && boolDriverTujuan == true && boolKendaraanTujuan == false){
    //     this.funcSweetAlert('warning','Failed',"Gagal Simpan Kendaraan!");
    //   }else {
    //     this.funcSweetAlert('warning','Failed',"Gagal Simpan Kendaraan!");
    //   }

    var id_trip = {
      "id_trip" : this.state.tripSelected.infoTrip.id
    }

    var hit_api = 'OK'
    if (boolDriver || boolKendaraan || boolDriverTujuan || boolKendaraanTujuan) {
      // Call API, tambah concat 
        hit_api = 
                await Axios.post("https://mostrans.co.id/GPSVendor/transporterconf",id_trip)
                .then(async(result)=>{
                  console.log('response hit', result)
                  var response = result.data.desc
                  return response;
                  // return this.funcSweetAlert('success','Success',"Confirmed Trip, Thankyou! "+response.desc);
                })
                .catch(error=>{
                  console.log('error', error)
                  return 'error hit api';
                });
    }

    // setTimeout(() => {}, 500) 

    if (boolDriver && boolKendaraan && boolDriverTujuan && boolKendaraanTujuan) {
      var response = "Simpan Perubahan! ("+hit_api+".) ";
      return await this.funcSweetAlert('success','Sukses',response);
    }
    // else{
    //   this.funcSweetAlert('warning','Failed',"Gagal Simpan Kendaraan & Driver!");
    // }
    await this.setState({isLoading: false});
  }

  funcSweetAlert=(style,title,subTitle)=>{
    var context =this;
    SweetAlert.showAlertWithOptions({
      title: title,
      subTitle: subTitle,
      confirmButtonTitle: 'OK',
      confirmButtonColor: '#000',
      otherButtonTitle: 'Cancel',
      otherButtonColor: '#dedede',
      style: style,
      cancellable: false
    },
    callback => context.backToList(context));
  }

  funcSweetAlertWithoutCallback=(style,title,subTitle)=>{
    var context =this;
    SweetAlert.showAlertWithOptions({
      title: title,
      subTitle: subTitle,
      confirmButtonTitle: 'OK',
      confirmButtonColor: '#000',
      otherButtonTitle: 'Cancel',
      otherButtonColor: '#dedede',
      style: style,
      cancellable: false
    },
    callback => console.log('callback'));
  }

  refreshContent = async(_listActiveTrip) => {
    await this.setState({isLoading: true})
    await this.setState({isLoading:false, listActiveTrip:_listActiveTrip});
  }

  pushNotif = async () => {
    // Must be outside of any component LifeCycle (such as `componentDidMount`).
    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: function (token) {
        console.log("TOKEN:", token);
      },
    
      // (required) Called when a remote is received or opened, or local notification is opened
      onNotification: async (notification) => {
        console.log("NOTIFICATION:", notification);
        //Utk dpt Notif
        // process the notification
        //mau apa disini
        //disini setelah ada userInteraction/di klik
        if (notification.userInteraction) {
          console.log('DO SOMETHING NOTIFICATION IN PERJALANAN') 

          // console.log(notification)
          // await this.onTripSelectedFromNotif(notification.data.id, notification.data.id_trip)
          
          this.props.navigation.navigate('Dashboard', {screen: 'Dashboard'})

        }
    
        // (required) Called when a remote is received or opened, or local notification is opened
        notification.finish(PushNotificationIOS.FetchResult.NoData);
      },

      
      // // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
      // onAction: function (notification) {
      //   console.log("ACTION:", notification.action);
      //   console.log("NOTIFICATION ACTION:", notification);
    
      //   // process the action

      //   console.log('waiwawa')
      // },
    
      // // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
      // onRegistrationError: function(err) {
      //   console.error(err.message, err);
      // },
    
      // // IOS ONLY (optional): default: all - Permissions to register.
      // permissions: {
      //   alert: true,
      //   badge: true,
      //   sound: true,
      // },
    
      // Should the initial notification be popped automatically
      // default: true
      popInitialNotification: true,
    
      /**
       * (optional) default: true
       * - Specified if permissions (ios) and token (android and ios) will requested or not,
       * - if not, you must call PushNotificationsHandler.requestPermissions() later
       * - if you are not using remote notification or do not have Firebase installed, use this:
       *     requestPermissions: Platform.OS === 'ios'
       */
      requestPermissions: true,
    });
  }

  backToList=async(context)=>{
    var _listActiveTrip= await getActiveTrip(context.state.url, context.state.profile.id_transporter, context.state.startDate, context.state.endDate);
    var _listActiveTripNew= await getActiveTripPlanning(context.state.url, context.state.profile.id_transporter, context.state.startDate, context.state.endDate);

    for ( var index=0; index<_listActiveTripNew.length; index++ ) {
      _listActiveTrip.push(_listActiveTripNew[index]);
    }

    await context.setState({isLoading:false,showDetailTrip:false, tripSelected:null,listActiveTrip:_listActiveTrip})
  }

  // backToList2=async()=>{
  //   var _listActiveTrip= await getActiveTrip(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate);
  //   await this.setState({isLoading:false,showDetailTrip:false, tripSelected:null,listActiveTrip:_listActiveTrip})
  // }

  backToMenu=async()=>{
    await this.setState({isLoading: true})
    var _listActiveTrip= await getActiveTrip(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate);
    var _listActiveTripNew= await getActiveTripPlanning(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate);

    for ( var index=0; index<_listActiveTripNew.length; index++ ) {
            _listActiveTrip.push(_listActiveTripNew[index]);
    }
    await this.setState({isLoading:false,showDetailTrip:false, tripSelected:null,listActiveTrip:_listActiveTrip})
  }
  pergi = async() =>{
    console.log('pergi')
    this.props.navigation.replace('MainRouter', { screen:'ListUser'})
  }

  restartConnection = async() =>{
    console.log('Restart Connection', this.state.isConnected)
    if (this.state.isConnected == false && this.state.isConnectedChange == true) {
      console.log('HARUSNYA INI OFF', this.state.isConnected)
      this.props.navigation.replace('MainRouter', {screen: 'ListTrip'})
    }
  }

  render(){
    return(
      <>
      {/* <Container> */}
        <CustomNavbar buttonBack={this.state.showDetailTrip && this.state.tripSelected!=null?this.backToMenu:null} title={'Perjalanan'}/>
        {
        this.state.isConnected == false?
          <LostConnection restartConnection={this.restartConnection}/> 
        :
        this.state.showDetailTrip && this.state.tripSelected!=null?
          <DetailTrip 
            onSetDriver={this.onSetDriver}
            onSetDriverTujuan={this.onSetDriverTujuan}
            onSetKendaraan={this.onSetKendaraan}
            onSetKendaraanTujuan={this.onSetKendaraanTujuan}
            onShowModalDriver={this.onShowModalDriver}
            onShowModalKerani={this.onShowModalKerani}
            onShowModalKeraniTujuan={this.onShowModalKeraniTujuan}
            onShowModalKendaraan={this.onShowModalKendaraan}
            onShowModalKendaraanTujuan={this.onShowModalKendaraanTujuan}
            onShowModalNoContainer={this.onShowModalNoContainer}
            onChangeTextNoContainer={this.onChangeTextNoContainer}
            onConfirmTrip={this.onConfirmTrip}
            onConfirmTripLaut={this.onConfirmTripLaut}
            onSetDateNaikTurunKapal={this.onSetDateNaikTurunKapal}
            onSaveAssign={this.onSaveAssign}
            onSaveAssignLaut={this.onSaveAssignLaut}
            // backToList={this.backToList2}
            backToList={this.backToMenu}
            backToMenu={this.backToMenu}
            {...this.state}
          />
          :
          <ListTripPage 
            setStartEndDate={this.setStartEndDate}
            onSearchButtonClicked={this.onSearchButtonClicked}
            onTripSelected={this.onTripSelected}
            refreshContent={this.refreshContent}
            {...this.state}
          />
        }
        {/* </Container> */}
        {this.state.isLoading ? <Loading/> : false}
      </>
    );
  }
}

ListTripFunc= (props) => {
  const navigation = useNavigation();
  const route = useRoute()
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      BackHandler.addEventListener("hardwareBackPress", ()=>props.backAction('LisTrip', navigation));
    });

    return unsubscribe;
  }, [navigation]);

  return <ListTrip navigation={navigation} route={route}/>;
}

export default ListTripFunc;