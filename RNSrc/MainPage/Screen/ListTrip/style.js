import { StyleSheet, PixelRatio } from 'react-native';


// sebelumnya elevation: 1
const styles = StyleSheet.create({
    flexGrid:{
        flexDirection: 'row',
        padding:10
    },
    flexGridNoPadding:{
        flexDirection: 'row',
    }
})

export default styles;