import React,{Component} from 'react'
import {View, ScrollView,TouchableOpacity,FlatList} from "react-native";
import Modal from 'react-native-modal';
import { StyleSheet } from 'react-native';
import DateRangePicker from './DateRangePicker'

class ModalCalendar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paramSearch:""
        };
    }

    componentDidMount= async()=>{

    }

    render(){
        return(
        <Modal
            animationType="slide"
            style={{backgroundColor:"rgba(0,0,0,0.7)", width:"100%", height:"100%", margin:0}}
            visible={this.props.modalCalenderVisible}
            onRequestClose={this.props.closeModal}
            onBackdropPress={this.props.closeModal}
        >
            <View style={{padding:20}}>
                <View style={tmpstyles.container}>
                    <DateRangePicker
                        initialRange={[this.props.startDate.format('YYYY-MM-DD'), this.props.endDate.format('YYYY-MM-DD')]}
                        onSuccess={(s, e) => this.props.onSelectDate(s,e)}
                        theme={{ markColor: '#008c45', markTextColor: 'white' }}
                    />
                </View>
            </View>
          </Modal>
        );
    }
}

export default ModalCalendar;

const tmpstyles = StyleSheet.create({
    container: {
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#008c45'
    }
  });