import React,{Component} from 'react'
import {Text, View,Icon} from 'native-base'
import {
    TouchableOpacity
} from "react-native";
import styles from './style'

class CardHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount= async()=>{

    } 

    render(){
        return(
            <>
                <View style={styles.cardStyling}>
                    <Text style={styles.styleHeaderPlaceholder}>Total Trip</Text>
                    <TouchableOpacity onPress={()=>this.props.setStatusParam('ALL')}>
                        <Text style={styles.styleCountAllTrip}>{this.props.listActiveTrip.length}</Text>
                    </TouchableOpacity>
                    <View style = {styles.lineStyle} />
                    <View style={styles.styleFlex}>
                        <TouchableOpacity style={{flex:1}} onPress={()=>this.props.setStatusParam('planning')}>
                            <Text style={styles.styleNewTrip}>
                                {(this.props.listActiveTrip.filter((element)=>{
                                    return element.status=='planning'
                                })).length}
                            </Text>
                            <Text style={styles.styleHeaderPlaceholder}>Baru</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex:1}} onPress={()=>this.props.setStatusParam('confirm')}>
                            <Text style={styles.styleSubTrip}>
                                {(this.props.listActiveTrip.filter((element)=>{
                                    return element.status=='confirm'
                                })).length}
                            </Text>
                            <Text style={styles.styleHeaderPlaceholder}>Konfirmasi</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex:1}} onPress={()=>this.props.setStatusParam('accepted')}>
                            <Text style={styles.styleSubTrip}>
                                {(this.props.listActiveTrip.filter((element)=>{
                                    return element.status=='accepted'
                                })).length}
                            </Text>
                            <Text style={styles.styleHeaderPlaceholder}>Aktif</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.styleFlex}>
                        <TouchableOpacity style={{flex:1}} onPress={()=>this.props.setStatusParam('close')}>
                            <Text style={[styles.styleSubTrip,{color:"#008c45"}]}>
                                {(this.props.listActiveTrip.filter((element)=>{
                                    return element.status=='close'
                                })).length}
                            </Text>
                            <Text style={styles.styleHeaderPlaceholder}>Close</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex:1}} onPress={()=>this.props.setStatusParam('cancel')}>
                            <Text style={styles.styleSubTrip}>
                                {(this.props.listActiveTrip.filter((element)=>{
                                    return element.status=='cancel'
                                })).length}
                            </Text>
                            <Text style={styles.styleHeaderPlaceholder}>Cancel</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex:1}} onPress={()=>this.props.setStatusParam('reject')}>
                            <Text style={styles.styleSubTrip}>
                                {(this.props.listActiveTrip.filter((element)=>{
                                    return element.status=='reject'
                                })).length}
                            </Text>
                            <Text style={styles.styleHeaderPlaceholder}>Reject</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </>
        );
    }
}

export default CardHeader;