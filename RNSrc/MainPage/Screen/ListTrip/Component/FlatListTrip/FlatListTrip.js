import React,{Component} from 'react'
import {Text, View,Icon} from 'native-base'
import {
    TouchableOpacity,
    ScrollView,FlatList
} from "react-native";
import styles from './style'
import CardTrip from './CardTrip/CardTrip'
import SearchInput from 'react-native-search-filter';

class FlatListTrip extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paramSearch:''
        };
    }

    componentDidMount= async()=>{
        // console.log('flatlist trip', this.props.listActiveTrip)
    } 

    render(){
        return(
            <>
                <SearchInput 
                    onChangeText={(term) => { this.setState({paramSearch:term}) }} 
                    style={{
                        paddingLeft:10,
                        paddingTop:5,
                        marginLeft:10,
                        marginRight:10,
                        marginBottom:5,
                        paddingBottom:5,
                        borderColor: '#008c45',
                        borderWidth: 1,
                        backgroundColor:"white",
                        borderRadius: 10,
                        width: '95%',
                        fontSize: 12
                    }}
                    placeholder="Search Trip / Nopol / Driver / Jenis Kendaraan / Tipe Trip"
                    inputFocus={true}
                    removeClippedSubviews={true} // Unmount components when outside of window 
                    initialNumToRender={2} // Reduce initial render amount
                    maxToRenderPerBatch={1} // Reduce number in each render batch
                    maxToRenderPerBatch={10} // Increase time between renders
                    windowSize={7} // Reduce the window size
                />
                <ScrollView>
                    <FlatList
                        data={
                            this.props.listActiveTrip.filter((element)=>{
                                var boolFilter = false;

                                var _jenisMuatan = element.tipe_muatan;
                                if (_jenisMuatan == null) {
                                    _jenisMuatan = 'DARAT'.toUpperCase();
                                }
                                if(_jenisMuatan!=null && _jenisMuatan!=''){
                                    _jenisMuatan = _jenisMuatan.toUpperCase();
                                }

                                var _jenisKendaraan = element.jenis_kendaraan;
                                if(_jenisKendaraan!=null && _jenisKendaraan!=''){
                                    _jenisKendaraan = _jenisKendaraan.toUpperCase();
                                }
                                var _namaDriver = element.nama_driver;
                                if(_namaDriver!=null && _namaDriver!=''){
                                    _namaDriver = _namaDriver.toUpperCase();
                                }
                                var _platNomor = element.plat_nomor;
                                if(_platNomor!=null && _platNomor!=''){
                                    _platNomor = _platNomor.toUpperCase();
                                }
                                var _paramSearch = this.state.paramSearch;
                                if(_paramSearch!=null || _paramSearch!=''){
                                    _paramSearch = _paramSearch.toUpperCase();
                                }

                                if(
                                    element.status == this.props.statusTripModal || this.props.statusTripModal=='ALL'
                                ){
                                    if(element.id_trip.indexOf(_paramSearch) >-1){
                                        boolFilter=true;
                                    }
                                    if(_jenisMuatan!=null || _jenisMuatan==null){
                                        if(
                                            _jenisMuatan.indexOf(_paramSearch) >-1
                                        ){
                                            boolFilter = true;
                                        }
                                    }
                                    if(_jenisKendaraan!=null){
                                        if(
                                            _jenisKendaraan.indexOf(_paramSearch) >-1
                                        ){
                                            boolFilter = true;
                                        }
                                    }
                                    if(_namaDriver!=null){
                                        if(
                                            _namaDriver.indexOf(_paramSearch) >-1
                                        ){
                                            boolFilter = true;
                                        }
                                    }
                                    if(_platNomor!=null){
                                        if(
                                            _platNomor.indexOf(_paramSearch) >-1 
                                        ){
                                            boolFilter = true;
                                        }
                                    }
                                }
                                return boolFilter;
                            })
                        }
                        style={{marginBottom:10}}
                        renderItem={({ item: value, index }) => {
                            return (
                                <CardTrip {...this.props} index={index} value={value} />
                            );
                        }}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </ScrollView>
            </>
        );
    }
}

export default FlatListTrip;