import React,{Component} from 'react'
import {
    View
} from "react-native";
import styles from './style'
import {
    Text,  
    Card,
    CardItem,
    Body,
    Left,
    Right,
    Button,
    Icon,
    Thumbnail
} from 'native-base'
import { TouchableOpacity } from 'react-native-gesture-handler';

const arrMonth =['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus','September','Oktober','November', 'Desember'];

class CardHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount= async()=>{
        // console.log('props card trip', this.props.value)
    } 

    convDateString =(param)=>{
        var today = new Date(param);
        // console.log('param', param)
        today = new Date(today.setHours(today.getHours()-1));
        return today.getDate() + " " + arrMonth[today.getMonth()] + " " + today.getFullYear();
    }

    render(){

        if (this.props.value.naikkapal_date != null && this.props.value.naikkapal_date != undefined && this.props.value.naikkapal_date != '') {

            var waktu_naikKapal = this.props.value.naikkapal_date.split('T')[1]
            var slice_waktuNaikKapal = waktu_naikKapal.split(':')
            var waktuNaikKapal_new = slice_waktuNaikKapal[0]+':'+slice_waktuNaikKapal[1]

        }

        if (this.props.value.turunkapal_date != null && this.props.value.turunkapal_date != undefined && this.props.value.turunkapal_date != '') {

            var waktu_turunKapal = this.props.value.turunkapal_date.split('T')[1]
            var slice_waktuTurunKapal = waktu_turunKapal.split(':')
            var waktuTurunKapal_new = slice_waktuTurunKapal[0]+':'+slice_waktuTurunKapal[1]

        }

        return(
            <>
        {this.props.value.tipe_muatan == 'LAUT' ? 
        // LAUT
            <TouchableOpacity style={{paddingLeft:10,paddingRight:10,paddingTop:5}} onPress={()=>this.props.onTripSelected(this.props.value)}>
                <Card key={this.props.index}>
                    <CardItem
                        header
                        bordered
                        style={[styles.cardPaddingHeader,{
                            // backgroundColor: "rgba(0, 140, 69, 0.1)",
                            backgroundColor: "rgba(227, 242, 233, 1)",
                            justifyContent: "center"
                        }]}
                    >
                        <View style={{flexDirection: 'column'}}>
                        <Text style={{ color: "#000000",padding:0,margin:0, textAlign: 'center'}}>{this.props.value.id_trip}</Text>
                            <Text style={{ color: "gray",padding:0,margin:0, textAlign: 'center', fontSize: 12}}>
                                <Text style={this.props.value.no_container == null || this.props.value.no_container == ''? { fontSize: 12 , color:"red"}:{fontSize: 12}}>{this.props.value.no_container == null || this.props.value.no_container == ''? 'No Container Kosong ':this.props.value.no_container+' '}</Text>
                                ( LAUT )
                            </Text>
                        </View>
                    </CardItem>
                    <CardItem bordered  style={styles.cardPadding}>
                        <Left>
                            <Text style={{fontSize: 10}}>Asal : {'\n'}
                                <Text note style={this.props.value.plat_nomor_asal==null?{ color:"red",fontSize: 10}:{fontSize: 10, color: 'gray'}}>{this.props.value.plat_nomor_asal==null?"Plat Asal":this.props.value.plat_nomor_asal}</Text>{"\n"}
                                <Text note style={this.props.value.nama_kerani_asal==null?{ color:"red",fontSize: 10}:{fontSize: 10, color: 'gray'}}>{this.props.value.nama_kerani_asal==null?"Kerani Asal":this.props.value.nama_kerani_asal}</Text>{"\n"}
                                <Text note style={{fontSize: 10, color: 'gray'}}>{this.props.value.hp_kerani_asal==null?"":this.props.value.hp_kerani_asal}</Text>
                            </Text>
                        </Left>
                        <Left style={{margin: 5}}>
                            <Text style={{fontSize: 10}}>Tujuan : {'\n'}
                                <Text note style={this.props.value.plat_nomor_tujuan==null?{ color:"red",fontSize: 10}:{fontSize: 10, color: 'gray'}}>{this.props.value.plat_nomor_tujuan==null?"Plat Tujuan":this.props.value.plat_nomor_tujuan}</Text>{"\n"}
                                <Text note style={this.props.value.nama_kerani_tujuan==null?{ color:"red",fontSize: 10}:{fontSize: 10, color: 'gray'}}>{this.props.value.nama_kerani_tujuan==null?"Kerani Tujuan":this.props.value.nama_kerani_tujuan}</Text>{"\n"}
                                <Text note style={{fontSize: 10, color: 'gray'}}>{this.props.value.hp_kerani_tujuan==null?"":this.props.value.hp_kerani_tujuan}</Text>
                            </Text>
                        </Left>
                        <Body style={{flex: 1, margin: 5}}>
                            <Text style={{ fontSize: 10 }} >{this.props.value.kota_asal} </Text>
                            <Text note style={{fontSize: 10, color: 'gray'}}>{this.convDateString(this.props.value.tanggal_pengiriman)}</Text>
                            <Text note style={{fontStyle:"italic", fontSize: 10, color: 'gray'}}>{(this.props.value.jam_pengiriman.toString()).substring(0,5)}</Text>
                        </Body>
                        <Right>
                            <View style={{flex: 1, margin: 5}}>
                                <Text style={{ fontSize: 10 }} >{this.props.value.kota_tujuan} </Text>
                                <Text note style={{fontSize: 10, color: 'gray'}}>{this.convDateString(this.props.value.tanggal_sampai)}</Text>
                                <Text note style={{fontStyle:"italic", fontSize: 10, color: 'gray'}}>{(this.props.value.jam_sampai.toString()).substring(0,5)}</Text>
                            </View>
                        </Right>
                    </CardItem>
                    <CardItem bordered style={styles.cardPadding}>
                        <View style={{flexDirection: 'row'}}>
                            <View style={{flex: 1, margin: 5}}>
                                <Text style={{ fontSize: 10, textAlign: 'center'}}>
                                    Naik Kapal : 
                                        <Text style={this.props.value.naikkapal_date == null || this.props.value.naikkapal_date == '' ? {fontSize: 10, color: 'red', textAlign: 'center'} : { fontSize: 10, textAlign: 'center', color: 'gray'}}>
                                            {'\n'}{this.props.value.naikkapal_date == null || this.props.value.naikkapal_date == '' ? 'Naik Kapal  ': this.convDateString(this.props.value.naikkapal_date.split('T')[0])+'\n'+waktuNaikKapal_new}
                                        </Text>
                                </Text>
                            </View>
                            <View style={{flex: 1, margin: 5}}>
                                <Text style={{ fontSize: 10, textAlign: 'center'}}>
                                    Turun Kapal : 
                                    <Text style={this.props.value.turunkapal_date == null || this.props.value.turunkapal_date == '' ? {fontSize: 10, color: 'red', textAlign: 'center'} : { fontSize: 10, textAlign: 'center', color: 'gray'}}>
                                        {'\n'}{this.props.value.turunkapal_date == null || this.props.value.turunkapal_date == '' ? 'Turun Kapal  ': this.convDateString(this.props.value.turunkapal_date.split('T')[0])+'\n'+waktuTurunKapal_new}
                                    </Text>
                                </Text>
                            </View>
                        </View>
                        
                    </CardItem>
                    <CardItem bordered style={styles.cardPadding}>
                        <View style={{flexDirection: 'row' ,width:"100%"}}>
                            <View style={{width:"18%"}}>
                                <Thumbnail  source={require('../../../../../../../Assets/logo.png')} />
                            </View>
                            <View>
                                <Text style={{ fontSize: 11, flex:8, fontStyle:"italic" , marginTop:10, marginLeft:10}}>
                                    {this.props.value.jenis_kendaraan}
                                </Text>
                                <Text style={{ fontSize: 10, flex:8 , marginTop:-10, marginLeft:10, textAlign:"left", fontWeight:"bold", color:"#008c45"}}>
                                    {this.props.value.status} 
                                    <Text style={{ fontSize: 10, flex:8 , marginTop:-10, marginLeft:10, textAlign:"left", fontWeight:"bold", color:'red'}}>
                                    {this.props.value.flag_naik == 'false' ? ' (Mohon isi tanggal naik kapal)' : this.props.value.flag_turun == 'false' ? ' (Mohon isi tanggal turun kapal)' : null}
                                    </Text>
                                </Text>
                            </View>
                        </View>
                        
                    </CardItem>
                </Card>
            </TouchableOpacity>

    : 

    // DARAT
            <TouchableOpacity style={{paddingLeft:10,paddingRight:10,paddingTop:5}} onPress={()=>this.props.onTripSelected(this.props.value)}>
                <Card key={this.props.index}>
                    <CardItem
                        header
                        bordered
                        style={[styles.cardPaddingHeader,{
                            backgroundColor: "rgba(0, 140, 69, 0.1)",
                            justifyContent: "center"
                        }]}
                    >
                        <Text style={{ color: "#000000",padding:0,margin:0}}>{this.props.value.id_trip}</Text>
                    </CardItem>
                    <CardItem bordered  style={styles.cardPadding}>
                        {/* <Left>
                            <Text style={this.props.value.plat_nomor==null?{ fontSize: 10 , color:"red"}:{ fontSize: 10, color: 'gray' }} >{this.props.value.plat_nomor==null?"No Data Plat":this.props.value.plat_nomor} {"\n"}
                                <Text note style={this.props.value.nama_driver==null?{ color:"red",fontSize: 10}:{fontSize: 10, color: 'gray'}}>{this.props.value.nama_driver==null?"No Data Driver":this.props.value.nama_driver}</Text>{"\n"}
                                <Text note style={{fontSize: 10}}>{this.props.value.hp_driver==null?"":this.props.value.hp_driver}</Text>
                            </Text>
                        </Left> */}
                        <Left style={{flex: 1, margin: 5}}>
                            <Text style={{fontSize: 10}}>Pengemudi : {'\n'}
                                <Text style={this.props.value.plat_nomor==null?{ fontSize: 10 , color:"red"}:{ fontSize: 10, color: 'gray' }} >{this.props.value.plat_nomor==null?"Plat":this.props.value.plat_nomor}</Text> {"\n"}
                                <Text note style={this.props.value.nama_driver==null?{ color:"red",fontSize: 10}:{fontSize: 10, color: 'gray'}}>{this.props.value.nama_driver==null?"Driver":this.props.value.nama_driver}</Text>{"\n"}
                                <Text note style={{fontSize: 10}}>{this.props.value.hp_driver==null?"":this.props.value.hp_driver}</Text>
                            </Text>
                        </Left>
                        <Body style={{flex: 1, margin: 5}}>
                            <Text style={{ fontSize: 10 }} >{this.props.value.kota_asal} </Text>
                            <Text note style={{fontSize: 10}}>{this.convDateString(this.props.value.tanggal_pengiriman)}</Text>
                            <Text note style={{fontStyle:"italic", fontSize: 10}}>{(this.props.value.jam_pengiriman.toString()).substring(0,5)}</Text>
                        </Body>
                        <Right>
                            <View style={{flex: 1, margin: 5}}>
                                <Text style={{ fontSize: 10 }} >{this.props.value.kota_tujuan} </Text>
                                <Text note style={{fontSize: 10}}>{this.convDateString(this.props.value.tanggal_sampai)}</Text>
                                <Text note style={{fontStyle:"italic", fontSize: 10}}>{(this.props.value.jam_sampai.toString()).substring(0,5)}</Text>
                            </View>
                        </Right>
                    </CardItem>
                    <CardItem bordered style={styles.cardPadding}>
                        <View style={{flexDirection: 'row' ,width:"100%"}}>
                            <View style={{width:"18%"}}>
                                <Thumbnail  source={require('../../../../../../../Assets/logo.png')} />
                            </View>
                            <View>
                                <Text style={{ fontSize: 11, flex:8, fontStyle:"italic" , marginTop:10, marginLeft:10}}>
                                    {this.props.value.jenis_kendaraan}
                                </Text>
                                <Text style={{ fontSize: 10, flex:8 , marginTop:-10, marginLeft:10, textAlign:"left", fontWeight:"bold", color:"#008c45"}}>
                                    {this.props.value.status}
                                </Text>
                            </View>
                        </View>
                        
                    </CardItem>
                </Card>
            </TouchableOpacity>

            }
            </>
        );
    }
}

export default CardHeader;