import Axios from 'axios';
import {encrypt} from '../../../../APIProp/ApiConstants';

export const getActiveTrip = async (url,transporter_id, _startDate,_endDate) =>{
    var startDate = _startDate
    var endDate = _endDate
    startDate = startDate.format('YYYY-MM-DD')
    endDate = endDate.format('YYYY-MM-DD')
  
    let dataQry = `
      select * from mobiletra_getlisttripv3('`+transporter_id+`','`+startDate+`','`+endDate+`')  
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
    });
  }

  export const getActiveTripPlanning = async (url,transporter_id, _startDate,_endDate) =>{
    var startDate = _startDate
    var endDate = _endDate
    startDate = startDate.format('YYYY-MM-DD')
    endDate = endDate.format('YYYY-MM-DD')
  
    let dataQry = `
      select * from mobiletra_getlisttripplanningv3('`+transporter_id+`')  
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
    });
  }

  export const getDetailOrder = async (url,trip_id) =>{
    let dataQry = `
    select * from mobiletra_getdetailorder('`+trip_id+`')
`
    // let dataQry = `
    //   select 
    //   mso.id, mso.id_order, mso.order_reference, mso.status, mso.customer_reference, mso.remarks, mso.instruksi, 
    //   mso.shipment_number, mso.weight, mso.dimension, mso.tipe_customer_id, mso.shipper_id, 
    //   mso.tipe_kendaraan, mso.booked_date, mso.booked_by, mso.confirmed_date, 
    //   mso.confirmed_by, mso.accepted_date, mso.accepted_by, mso.accepted_latitude,
    //   mso.suhu_id ,
    //   (select mms.minimal_suhu from mt_master_suhukendaraan mms where id = mso.suhu_id ) as min_suhu,
    //   (select mms.maksimal_suhu from mt_master_suhukendaraan mms where id = mso.suhu_id ) as maks_suhu,
    //   mso.accepted_longitude, mso.arrival_date, mso.arrival_latitude, 
    //   mso.arrival_longitude, mso.startloaded_date, mso.startloaded_latitude, 
    //   mso.startloaded_longitude, mso.finishloaded_date, mso.finishloaded_latitude, 
    //   mso.finishloaded_longitude, mso.ontheway_date, mso.ontheway_latitude, mso.ontheway_longitude, 
    //   mso.dropoff_date, mso.dropoff_latitude, mso.dropoff_longitude, mso.startunloaded_date, mso.startunloaded_latitude,
    //   mso.startunloaded_longitude, mso.finishunloaded_date, mso.finishunloaded_latitude, mso.finishunloaded_longitude, 
    //   mso.pod_date, mso.cancel_date, mso.length, mso.width, mso.height, mso.fee, mso.asal_kota,
    //   mso.tujuan_kota, mso.asal_id, mso.tujuan_id, mso.jenis_kendaraan_id, mso.tipe_muatan, mso.tipe_armada, 
    //   mso.dokumen_pod, mso.deskripsi_dokumen, (mso.jadwal_penjemputan - interval '1' hour) as jadwal_penjemputan,
    //   mso.jadwal_sampai, mso.star, mso.create_date, mso.create_by, mso.update_date, mso.update_by, mso.dokumen_pod_2, 
    //   mso.deskripsi_dokumen_2, mso.dokumen_pod_3, mso.deskripsi_dokumen_3, mso.dokumen_pod_4, mso.deskripsi_dokumen_4,
    //   mso.dokumen_pod_5, mso.deskripsi_dokumen_5, mso.keterangan, 
    //   sos.nama as status_detail,
    //   msc.nama_perusahaan as asal_perusahaan, msc.lat as lat_asal, msc.lang as lang_asal,
    //   msc2.nama_perusahaan as tujuan_perusahaan, msc2.lat as lat_tujuan, msc.lang as lang_tujuan
    // from
    //   mt_trip_order_transaction tot,
    //   mt_shipper_order mso,
    //   mt_shipper_order_status sos,
    //   mt_shipper_contact msc,
    //   mt_shipper_contact msc2
    // where
    //   tot.order_id = mso.id and
    //   mso.status = sos.id and
    //   mso.asal_id = msc.id and
    //   mso.tujuan_id= msc2.id and
    //     tot.trip_id = '`+trip_id+`'
    // `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
    });
  }

  export const getDetailTripFromNotif = async (url,trip_id) =>{
    let dataQry = `
        select 
        mmt.id, 
        mmt.id_trip, 
        mmt.transporter_id, 
        mmt.driver_id, 
        mmt.kendaraan_id, 
        mmt.jenis_kendaraan_id, 
        mmt.asal_id, 
        mmt.tujuan_id, 
        mmt.tanggal_pengiriman, 
        (mmt.jam_pengiriman - interval '1' hour) as jam_pengiriman, 
        mmt.tanggal_sampai, 
        mmt.jam_sampai, 
        mmt.status, 
        mmt.create_date, 
        mmt.create_by, 
        mmt.update_date, 
        mmt.update_by,
        mtka.nama as kota_asal,
        mtkt.nama as kota_tujuan,
        mjk.jenis_kendaraan,
        mmk.plat_nomor,
        mmu.nama as nama_driver,
        mmu.hp as hp_driver
      from 
        mt_master_trip mmt
        left join mt_master_kota mtka
        on mmt.asal_id = mtka.id
        left join mt_master_kota mtkt
        on mmt.tujuan_id = mtkt.id
        left join mt_master_jenis_kendaraan mjk
        on mmt.jenis_kendaraan_id = mjk.id
        left join mt_master_kendaraan mmk
        on mmt.kendaraan_id = mmk.id
        left join
        mt_master_user mmu
        on mmt.driver_id = mmu.id
      where mmt.id = '`+trip_id+`'
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data[0];
      console.log('infoTrip notif', x)
      return x;
    }).catch(err=>{
      console.log('error', err)
      return [];
    });
  }

  export const getLatestLocation = async (url,trip_id) =>{
    let dataQry = `
      select * from mobiletra_getlatestloc('`+trip_id+`')
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
    });
  }

  export const getListDriver = async (url,trans_id) =>{
    let dataQry = `
      select * from mobiletra_getlistdriver('`+trans_id+`')
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
    });
  }

  export const getListKerani = async (url,trans_id) =>{
    let dataQry = `
      select * from mobiletra_getlistkerani('`+trans_id+`')
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
    });
  }

  export const getListKendaraan = async (url,trans_id,jenis_kendaraan_id) =>{
    // let dataQry = `
    //   select * from mobiletra_getlistkendaraan('`+trans_id+`','`+jenis_kendaraan_id+`')
    // `

    let dataQry = `
      select * from mobiletra_getlistkendaraanv2('`+trans_id+`','`+jenis_kendaraan_id+`')
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
    });
  }

  export const funcConfirmTrip = async (url,trip_id, user_id) =>{
    let dataQry = `
      select * from mobiletra_confirmtrip('`+trip_id+`','`+user_id+`')
    `
    console.log(dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
    });
  }

  export const funcConfirmOrder = async (url,order_id, user_id) =>{
    let dataQry = `
      select * from mobiletra_confirmorder('`+order_id+`','`+user_id+`')
    `
    console.log(dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
    });
  }

  export const funcAssignNoContainer = async (url,trip_id, user_id, no_container) =>{
    let dataQry = `
      select * from mobiletra_assignnocontainer('`+no_container+`','`+user_id+`','`+trip_id+`')
    `
    console.log('assign query no container', dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
    });
  }

  export const funcAssignNaikKapal = async (url,order_id, user_id, naikkapal) =>{
    let dataQry = `
      select * from mobiletra_assignnaikkapal('`+order_id+`','`+user_id+`','`+naikkapal+`')
    `
    console.log('assign naik kapal : ',dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
    });
  }

  export const funcAssignTurunKapal = async (url,order_id, user_id, turunkapal) =>{
    let dataQry = `
      select * from mobiletra_assignturunkapal('`+order_id+`','`+user_id+`','`+turunkapal+`')
    `
    console.log('assign turun kapal : ',dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
    });
  }

  export const funcAssignKendaraan = async (url,trip_id, user_id, kendaraan_id) =>{
    let dataQry = `
      select * from mobiletra_assignkendaraanv2('`+kendaraan_id+`','`+user_id+`','`+trip_id+`')
    `
    console.log('assign query kendaraan', dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
    });
  }

  export const funcAssignKendaraanTujuan = async (url,trip_id, user_id, kendaraan_id) =>{
    let dataQry = `
      select * from mobiletra_assignkendaraantujuanv2('`+kendaraan_id+`','`+user_id+`','`+trip_id+`')
    `
    console.log('assign query kendaraan', dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
    });
  }

  export const funcAssignDriver = async (url,trip_id, user_id, driver_id) =>{
    let dataQry = `
      select * from mobiletra_assigndriver('`+driver_id+`','`+user_id+`','`+trip_id+`')
    `
    console.log('query assign driver', dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
    });
  }

  export const funcAssignKeraniAsal = async (url,trip_id, user_id, kerani_id) =>{
    let dataQry = `
      select * from mobiletra_assignkeraniasal('`+kerani_id+`','`+user_id+`','`+trip_id+`')
    `
    console.log('query assign driver', dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
    });
  }

  export const funcAssignKeraniTujuan = async (url,trip_id, user_id, kerani_id) =>{
    let dataQry = `
      select * from mobiletra_assignkeranitujuan('`+kerani_id+`','`+user_id+`','`+trip_id+`')
    `
    console.log('query assign driver', dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
    });
  }

  export const allRejectTrip = async (url) =>{
    // let dataQry = `
    //   SELECT id, keterangan from mt_master_reason where mt_master_reason.role = 'T' and mt_master_reason.id != 7
    // `
    let dataQry = `
        select * from mobiletra_getlistreject()
    `
    console.log('getlist reject : ', dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('all reject', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
    });
  }

  export const rejectTrip = async (url, trans_id, user_id, id_trip, idAlasan) =>{
    // let dataQry = `
    //   UPDATE mt_master_trip 
    //   SET 
    //   update_by = `+user_id+`, 
    //   update_date=now(), 
    //   status = 'reject', 
    //   reason_cancel_id = `+idAlasan+`
    //   where id = `+id_trip+`
    //   and status = 'planning'
    //   returning id as id_return
    // `

    let dataQry = `
        select * from mobiletra_rejecttrip(`+id_trip+`, `+user_id+`, `+idAlasan+`)
    `
    console.log('query reject trip : ', dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data[0];
      console.log('hasil return', x.id_return)
      if (x.id_return == id_trip) {
        console.log('sukses reject')
        return ['Sukses reject trip!', 'success']
      }else{
        console.log('gagal reject')
        return ['Gagal reject trip!', 'warning']
      }
      // console.log('all reject', x)
      // if(x==""){
      //   return [];
      // }
      // return x;
      // console.log('success reject trip')
      // return ['Sukses reject trip!', 'success']
    }).catch(err=>{
      return ['Gagal reject trip!', 'warning']
    });
  }

  // status skrng harus planning, returning id --> 