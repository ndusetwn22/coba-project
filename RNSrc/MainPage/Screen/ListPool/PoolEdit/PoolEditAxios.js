import Axios from 'axios';
import {encrypt, firebase} from '../../../../../APIProp/ApiConstants';
import {Alert} from 'react-native'

//firebase
import { utils } from '@react-native-firebase/app';
import storage from '@react-native-firebase/storage';

  export const getKota = async (url) =>{
    let dataQry = `
        select * from mt_master_kota
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('get Kota : ', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
      console.log('error', err)
    });
  }


  export const getDataPool = async (url, id) =>{
    let dataQry = `
      select * from mt_master_pool mmp
      where mmp.id = `+id+`
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data[0];

      // firebase pandu
      // var depan = firebase.linkStorage

      // firebase prod
      var depan = firebase.linkStorage
    
      //foto driver logic
      var _fotoFilename = x.foto_pool
      _fotoFilename = _fotoFilename.substr(depan.length);
      _fotoFilename = _fotoFilename.split('?alt=media')
      _fotoFilename = _fotoFilename[0]

      x.fotoFileName = _fotoFilename
    

      console.log('sukses data pool : ', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
      console.log('error', err)
    });
  }

  export const editPool = async (url, id, trid, uid, namaPool, refId, namaLokasi, detailAlamat, nomorKontak, kota, kodePos, lat, lang, fotoFileName, fotoSource_, tempFotoFileName) =>{

    // firebase pandu
    // let tempFotoPool = firebase.linkStorage+fotoFileName+'?alt=media'

    // firebase prod
    let tempFotoPool = firebase.linkStorage+fotoFileName+'?alt=media'


    const FireBaseStorage = storage();

      if (tempFotoFileName != tempFotoPool) {
        //Insert firebase foto
        const fotoSource = fotoSource_.uri
        const fotoStorageRef = FireBaseStorage.ref(`FotoPool/${fotoFileName}`)
        await fotoStorageRef.putFile(fotoSource)
          .then(async(response)=>{
            await fotoStorageRef.getDownloadURL()
              .then(async(urlDownload)=>{
                tempFotoPool = urlDownload
                var c = tempFotoPool.split("&token");
                tempFotoPool = c[0]
                // return ['Sukses edit pool!', 'success'];
              })
              .catch((error)=> {
                return ['Gagal edit foto pool!', 'warning'];      
              })
            // return ['Sukses edit, foto pool!', 'success'];
          }).catch((error) => {   
              console.log('catch error', error)    
              return ['Gagal edit foto pool!', 'warning'];      
          })
      }

      console.log('foto baru', tempFotoPool)

    // let dataQry = `
    //     update mt_master_pool SET
    //     nama = '`+namaPool+`',
    //     external_id = '`+refId+`',
    //     alamat1 = '`+namaLokasi+`',
    //     alamat2 = '`+detailAlamat+`',
    //     phone = '`+nomorKontak+`',
    //     kota_id = '`+kota+`',
    //     kode_pos = '`+kodePos+`',
    //     lat = '`+lat+`',
    //     lang = '`+lang+`',
    //     foto_pool = '`+tempFotoPool+`',
    //     update_date = now(),
    //     update_by = '`+uid+`'
    //     WHERE id = `+id+`
    // `

    let dataQry = `
        select * from mobiletra_editpool(`+id+`, '`+namaPool+`', '`+refId+`', '`+namaLokasi+`', '`+detailAlamat+`', '`+nomorKontak+`', `+kota+`, '`+kodePos+`', '`+lat+`', '`+lang+`', '`+tempFotoPool+`', '`+uid+`')
    `

    console.log('edit pool : ', dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(async(value)=>{
      let x = value.data.data;
      console.log('sukses pool', value)

      
    
      return ['Sukses edit pool!', 'success'];
    }).catch(err=>{
    //   return [];
      console.log('error', err)
      return ['Gagal edit pool!', 'warning'];
    });
  }