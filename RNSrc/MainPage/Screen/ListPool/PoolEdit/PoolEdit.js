import React,{Component} from 'react'
import { AppState,Platform,Dimensions, Image, FlatList , TouchableOpacity,ScrollView, ImageBackground, BackHandler, Alert, PixelRatio, StyleSheet, TextInput, Picker } from "react-native";
import { useNavigation  } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
import CustomNavbar from '../../../Header/Navbar/Navbar'
import Loading,{getProfile, encrypt, setUrl, getUrl, decrypt, firebase} from '../../../../../APIProp/ApiConstants'
import Axios from 'axios';
import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Left,
    Right,
    Body,
    Text,
    Icon,
    Card,
    CardItem,
    Grid,
    Row,
    View,
    Col, 
    List,
    ListItem,
    Thumbnail,
    Form,
    Label,
    Input,
    Item,
    DatePicker
} from "native-base";
import styles from "./style";
import moment from 'moment'

//image picker
import ImagePicker from 'react-native-image-picker';

//Google Places
import RNGooglePlaces from 'react-native-google-places';

//Axios
import { getKota, getDataPool, editPool } from './PoolEditAxios';

//firebase
import { utils } from '@react-native-firebase/app';
import storage from '@react-native-firebase/storage';

var { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE = -6.117664;
const LONGITUDE = 106.906349;
const LATITUDE_DELTA = 1;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const id = 1;



class PoolEdit extends Component {
    constructor(props) {
            super(props);
            this.state = {
            paramLogin:{
                username:'',
                password:'',
            },
            isLoading:false,
            url:null,
            profile: null,
            refId: '',
            namaPool: '',
            nomorKontak: '',
            tempFotoFileName: '',
            fotoFileName: 'Pilih foto pool',
            fotoSource: null,
            changeTextFoto: '',
            allKota: [],
            kota: '',
            alamat: '',
            detailAlamat: '',
            kodePos: '',
            namaLokasi: 'Pilih lokasi',
            lat: 0,
            long: 0,
            region: {
            latitude: LATITUDE,
            longitude: LONGITUDE,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
            },
            markers: {
            coordinate: {
                latitude: 4,
                longitude: 4,
                },
            color: 'red',
            }
            
        };
      }
    
      componentDidMount= async()=>{
        await this.setState({isLoading:true});
        await setUrl();
        await getUrl().then(async(result)=>{
          this.setState({url:result})
        });

        await this.funcGetProfile();
        var _getKota = await getKota(this.state.url)
        var _getDataPool = await getDataPool(this.state.url, this.props.poolSelected.value.id)
        await this.setState({
          allKota: _getKota,
          refId: _getDataPool.external_id,
          namaPool: _getDataPool.nama,
          namaLokasi: _getDataPool.alamat1,
          detailAlamat: _getDataPool.alamat2,
          nomorKontak: _getDataPool.phone,
          kota: _getDataPool.kota_id,
          kodePos: _getDataPool.kode_pos,
          lat: _getDataPool.lat,
          long: _getDataPool.lang,
          fotoFileName: _getDataPool.fotoFileName,
          tempFotoFileName: _getDataPool.foto_pool,

      })
        setTimeout(() => {this.setState({isLoading: false})}, 2000)
      }

      funcGetProfile=async()=>{
        await getProfile().then(result=>{
          if(result!=null){
            this.setState({profileStr:result})
          }
        })
        if(this.state.profileStr==null){
          this.props.navigation.replace('Login')
        }
        var tmpStr = await decrypt(this.state.profileStr);
        this.setState({profile:JSON.parse(tmpStr)})
        console.log('profile', JSON.parse(tmpStr))
      }

      selectFotoTapped = () => {
        const options = {
          quality: 1.0,
          maxWidth: 500,
          maxHeight: 500,
          storageOptions: {
            skipBackup: true,
          },
        };
    
        ImagePicker.showImagePicker(options, response => {
          console.log('Response = ', response);
    
          if (response.didCancel) {
            console.log('User cancelled photo picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            let source = {uri: response.uri};
    
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
            var changeText = "(*foto sudah dirubah)"
            console.log('options : ', options)
            console.log('uri :', response.uri)
            this.setState({
              // avatarSource: response.uri,
              fotoSource: source,
              fotoFileName: response.fileName,
              changeTextFoto: changeText
            });
    
          }
        });
      }

      openSearchModal = () => {
        RNGooglePlaces.openAutocompleteModal()
        .then((place) => {
        console.log(place);
        console.log('nama alamat :', place.address)
        console.log('long lat : ', place.location)
        console.log('long:', place.location.longitude)
        console.log('long:', typeof(place.location.longitude.toString()))
    
        this.setState({
          namaLokasi: place.address,
          lat: place.location.latitude,
          long: place.location.longitude
        })
            // place represents user's selection from the
            // suggestions and it is a simplified Google Place object.
        })
        .catch(error => console.log(error.message));  // error is a Javascript Error object
      }
    
      editPool=async ()=>{
        // this.setState({buttondisabled: true});
        let title = 'Invalid Input';
        if(this.state.refId === '' ) Alert.alert(title, 'Ref. ID Pool tidak boleh kosong!');
        else if(this.state.namaPool === '') Alert.alert(title, 'Nama Pool tidak boleh kosong!');
        else if(this.state.namaLokasi === 'Pilih lokasi') Alert.alert(title, 'Lokasi pool tidak boleh kosong!');
        else if(this.state.nomorKontak === '') Alert.alert(title, 'Nomor Kontak tidak boleh kosong!');
        else if(this.state.tempFotoFileName === null) Alert.alert(title, 'Foto pool tidak boleh kosong!');
        else if(this.state.kota === '') Alert.alert(title, 'Kota tidak boleh kosong!');
        // else if(this.state.alamat === '') Alert.alert(title, 'Alamat tidak boleh kosong!');
        else if(this.state.detailAlamat === '') Alert.alert(title, 'Detail alamat tidak boleh kosong!');
        else if(this.state.kodePos === '') Alert.alert(title, 'Kode pos tidak boleh kosong!');
        else{
          
          this.setState({isLoading:true});
    
          let lat = this.state.lat.toString()
          let lang = this.state.long.toString()
          let trid = this.state.profile.id_transporter
          let uid = this.state.profile.id_user

          let _editPool = await editPool(this.state.url, this.props.poolSelected.value.id, this.state.profile.id_transporter, this.state.profile.id_user, this.state.namaPool, this.state.refId, this.state.namaLokasi, this.state.detailAlamat, this.state.nomorKontak, this.state.kota, this.state.kodePos, lat, lang, this.state.fotoFileName, this.state.fotoSource, this.state.tempFotoFileName)
          this.setState({isLoading:false}); 
          
            // Alert.alert("Status", _editPool ,[
            //   {
            //     text: 'Ok',
            //     onPress: ()=>this.props.backToList()
            //   }
            // ]);
            this.props.funcSweetAlert('status',_editPool[0], _editPool[1])

        }
      }

      getData = async () => {
        let query = `
            select * from mt_master_pool mmp
            where mmp.id = `+this.props.poolSelected.value.id+`
      `
          await encrypt(query).then(result => {
            query = result;
          });
          Axios.post(url.select, {
            query: query 
          })
            .then(result => {
    
              let res = result.data.data[0]
            //   console.log('getData pool :', res)
    
              var depan = firebase.linkStorage
    
              //foto driver logic
              var _fotoFilename = res.foto_pool
              _fotoFilename = _fotoFilename.substr(depan.length);
              _fotoFilename = _fotoFilename.split('?alt=media')
              _fotoFilename = _fotoFilename[0]
    
    
              //foto pool belom
              this.setState({
                  refId: res.external_id,
                  namaPool: res.nama,
                  namaLokasi: res.alamat1,
                  detailAlamat: res.alamat2,
                  nomorKontak: res.phone,
                  kota: res.kota_id,
                  kodePos: res.kode_pos,
                  lat: res.lat,
                  long: res.lang,
                  fotoFileName: _fotoFilename,
                  tempFotoFileName: res.foto_pool,
    
              })
            })
            .catch(error => {
              console.log("Error Log: " + error);
            });
      }



      render(){
        this.selectFotoTapped = this.selectFotoTapped.bind(this);
        return(
          <>
            <Content>

            {/* <Text style={styles.title}>EDIT POOL</Text> */}


                    <View style={styles.viewTextInput}>
                        <Label style={styles.label}>Ref. ID Pool</Label>
                        <TextInput
                            style={styles.textInputDisabled}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder="Masukan ref.ID Pool"
                            placeholderTextColor = "#CACCD7"
                            selectionColor="#008c45"
                            onChangeText={input=>this.setState({refId: input})}
                            value = {this.state.refId}
                            caretHidden={true}
                            editable ={false}
                            selectTextOnFocus = {false}
                            
                            />
                    </View>

                    <View style={styles.viewTextInput}>
                        <Label style={styles.label}>Nama Pool</Label>
                        <TextInput
                            style={styles.textInputDisabled}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder="Nama Pool"
                            placeholderTextColor = "#CACCD7"
                            selectionColor="#008c45"
                            onChangeText={input=>this.setState({namaPool: input})}
                            value = {this.state.namaPool}
                            caretHidden={true}
                            editable ={false}
                            selectTextOnFocus = {false}
                            
                            />
                    </View>

                    <View style={styles.viewButton}>   
                        <Label style={styles.label}>Pilih lokasi pool</Label>
                        <Button
                            style={styles.button}
                            onPress={() => this.openSearchModal()}
                
                        >
                            <Text uppercase={false} style={{fontSize: 14}} numberOfLines={1}>{this.state.namaLokasi}</Text>
                            <Icon type='MaterialCommunityIcons' name='google-maps' />
                        </Button>
                    </View>

                    <View style={styles.viewTextInput}>   
                        <Label style={styles.label}>Nomor Kontak</Label>
                        <TextInput
                            style={styles.textInput}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder="Nomor Kontak"
                            placeholderTextColor = "#CACCD7"
                            selectionColor="#008c45"
                            keyboardType="phone-pad"
                            onChangeText={input=>this.setState({nomorKontak: input})}
                            value = {this.state.nomorKontak}
                            
                        />
                    </View>


                    <View style={styles.viewButton}>   
                        <Label style={styles.label}>Foto Pool {this.state.changeTextFoto}</Label>
                        <Button
                            style={styles.button}
                            onPress={this.selectFotoTapped.bind(this)}
                
                        >
                            <Text uppercase={false} style={{fontSize: 14}} numberOfLines={1}>{this.state.fotoFileName}</Text>
                            <Icon type="MaterialIcons" name='photo' />
                        </Button>
                    </View>


                <View style={{flex: 1, flexDirection: 'column'}}>   
                    <Label style={{fontSize: 14, marginLeft: 10, marginTop: 10, alignSelf: 'flex-start', alignItems: 'flex-start'}}>Kota</Label>
                        <View 
                            style={{flex: 1,  
                            alignItems: 'center',  
                            justifyContent: 'center',
                            borderColor: 'gray', 
                            borderWidth: 2, 
                            borderRadius: 5, 
                            marginTop: 10, 
                            marginLeft: 20, 
                            marginRight: 20, height: 40, color: '#494A4A', padding: 10 }}>

                                
                                        <Picker
                                            style={{ 
                                            width: "100%",
                                            color: '#344953',  
                                            justifyContent: 'center',
                                            }}
                                            selectedValue={this.state.kota}
                                            onValueChange={input=>this.setState({kota: input})}
                                        >
                                            {this.state.allKota.map((value, index) => {
                                                return (
                                                    <Picker.Item
                                                    label={value.nama}
                                                    value={value.id}
                                                    key={index}
                                                    />
                                                );
                                                })}
                                        </Picker>

                        </View>
                </View>


                
                <View style={styles.viewTextInput}>
                    <Label style={styles.label}>Alamat</Label>
                    <TextInput
                        style={styles.textInputDisabled}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder='Alamat'
                        placeholderTextColor = "#CACCD7"
                        selectionColor="#008c45"
                        caretHidden={true}
                        editable ={false}
                        maxLength={50}
                        // onChangeText={input=>this.setState({namaLokasi: input})}
                        value={this.state.namaLokasi == 'Pilih lokasi'? 'Pilih lokasi dengan maps': this.state.namaLokasi}
                        
                        />
                    </View>


                    <View style={styles.viewTextInput}>
                    <Label style={styles.label}>Detail Alamat</Label>
                    <TextInput
                        style={styles.textInput}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder="Detail Alamat"
                        placeholderTextColor = "#CACCD7"
                        selectionColor="#008c45"
                        onChangeText={input=>this.setState({detailAlamat: input})}
                        value={this.state.detailAlamat}
                        
                        />
                    </View>

                <View style={styles.viewTextInput}>   
                    <Label style={styles.label}>Kode Pos</Label>
                        <TextInput
                        style={styles.textInput}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder="Kode Pos"
                        placeholderTextColor = "#CACCD7"
                        selectionColor="#008c45"
                        keyboardType="phone-pad"
                        onChangeText={input=>this.setState({kodePos: input})}
                        value={this.state.kodePos}
                        
                        />
                </View>


                

                <View style={{flex: 1, flexDirection: 'column', marginBottom: 15, marginTop: 10}}>   
                    {/* <Label style={{fontSize: 14, 
                                    // marginLeft: 10, 
                                    marginTop: 10, alignSelf: 'center', alignItems: 'center'}}>Edit Pool</Label> */}
                        <Button
                        style={[styles.button, {justifyContent: 'center', alignItems: 'center'}]}
                            onPress={this.editPool}
                
                        >
                        <Text uppercase={false} style={{fontSize: 14, textAlign: "center"}}>Edit Pool</Text>
                        </Button>
                </View>  
    

            </Content>

            {this.state.isLoading ? <Loading/> : false}
          </>
        );
      }
}

export default PoolEdit;