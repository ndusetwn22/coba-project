import React,{Component} from 'react'
import {BackHandler,TouchableOpacity, Text,View} from "react-native";
import { useNavigation  } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
import CustomNavbar from '../../Header/Navbar/Navbar'
import {Container} from 'native-base'
import Loading,{getProfile, setUrl, getUrl, decrypt} from '../../../../APIProp/ApiConstants'
import SweetAlert from 'react-native-sweet-alert';
import {
  getListPool,
} from './ListPoolAxios'
import ListPoolPage from './ListPoolPage'
import PoolAdd from './PoolAdd/PoolAdd'
import PoolEdit from './PoolEdit/PoolEdit';
import messaging from '@react-native-firebase/messaging';
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import PushNotification from 'react-native-push-notification'
import NetInfo from "@react-native-community/netinfo";
import LostConnection from '../../Component/LostConnection';

class ListPool extends Component {
    constructor(props) {
        super(props);
        this.state = {
          paramLogin:{
            username:'',
            password:'',
          },
          isLoading:false,
          url:null,
          profile: null,
          isConnected: null,
          isConnectedChange: null,
          listPool: [],
          poolAdd: false,
          poolEdit: false,
          poolSelected: null,
        };
      }
    
      componentDidMount= async()=>{
        // this.props.changeHeaderTitle("List Trip")
        await this.setState({isLoading:true});
        await setUrl();
        await getUrl().then(async(result)=>{
          this.setState({url:result})
        });
        await this.funcGetProfile();
        var _listPool = await getListPool(this.state.url, this.state.profile.id_transporter)
        await this.setState({isLoading:false, listPool: _listPool});

        await this.pushNotif();
        await this.cekConnection();
      }

      cekConnection = async()=>{
        // Cek Connection
        NetInfo.addEventListener(state => {
          if (this.state.isConnected == null) {
            this.setState({isConnected: state.isConnected})
          }
          if (state.isConnected == false) {
            this.setState({isConnected: state.isConnected, isConnectedChange: state.isConnected})
          }
          if (state.isConnected == true) {
            this.setState({isConnectedChange: state.isConnected})
          }      
          console.log("Is connected?", state.isConnected);
        });
      }

      restartConnection = async() =>{
        console.log('Restart Connection', this.state.isConnected)
        if (this.state.isConnected == false && this.state.isConnectedChange == true) {
          console.log('HARUSNYA INI OFF', this.state.isConnected)
          this.props.navigation.replace('MainRouter', {screen: 'ListPool'})
        }
      }

      funcGetProfile=async()=>{
        await getProfile().then(result=>{
          if(result!=null){
            this.setState({profileStr:result})
          }
        })
        if(this.state.profileStr==null){
          this.props.navigation.replace('Login')
        }
        var tmpStr = await decrypt(this.state.profileStr);
        await this.setState({profile:JSON.parse(tmpStr)})
        await this.setState({hak_akses: JSON.parse(tmpStr).hak_akses}) // tambah ini utk hak_akses dan state
        console.log('profile', JSON.parse(tmpStr))
      }

      funcSweetAlert = async(title, subTitle, style) =>{
        SweetAlert.showAlertWithOptions({
            title: title,
            subTitle: subTitle,
            confirmButtonTitle: 'OK',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: style,
            cancellable: true
          },
            callback => {
              console.log('callback') 
              this.backToList()
            }
          );
    }

      onPoolAdd=async()=>{
        await this.setState({isLoading:true,poolAdd:false});
        // var _tripSelected = {};
        // _tripSelected.infoTrip = infoTrip;
        // _tripSelected.infoOrder = await getDetailOrder(this.state.url,infoTrip.id);
        // _tripSelected.location = await getLatestLocation(this.state.url,infoTrip.id);
        await this.setState({isLoading:false, poolAdd: true});
      }

      onPoolEdit=async(value)=>{
        await this.setState({isLoading:true, poolEdit: false});
        var _poolSelected = {};
        _poolSelected.value = value
        await this.setState({isLoading:false, poolEdit: true, poolSelected: _poolSelected});
        // console.log('Hasil tarik data : ', _kendaraanSelected)
      }

      backToList=async()=>{
        await this.setState({isLoading:true});
        var _listPool = await getListPool(this.state.url, this.state.profile.id_transporter)
        await this.setState({isLoading:false, listPool: _listPool, poolAdd: false, poolEdit: false});
      }

      backToListWithoutLoad=async()=>{
        await this.setState({poolAdd: false, poolEdit: false});
      }

      refreshContent = async(_listPool) => {
        await this.setState({isLoading: true})
        await this.setState({isLoading:false, listPool: _listPool});
      }

      pushNotif = async () => {
        // Must be outside of any component LifeCycle (such as `componentDidMount`).
        PushNotification.configure({
          // (optional) Called when Token is generated (iOS and Android)
          onRegister: function (token) {
            console.log("TOKEN:", token);
          },
        
          // (required) Called when a remote is received or opened, or local notification is opened
          onNotification: async (notification) => {
            console.log("NOTIFICATION:", notification);
            //Utk dpt Notif
            // process the notification
            //mau apa disini
            //disini setelah ada userInteraction/di klik
            if (notification.userInteraction) {
              // console.log('DO SOMETHING NOTIFICATION KENDARAAN') 
              // this.props.navigation.navigate('ListTrip',{
              //   screen: 'ListTrip',
              //   id_trip: notification.data.id_trip,
              //   id: notification.data.id
              // })
              this.props.navigation.navigate('Dashboard', {screen: 'Dashboard'})
            }
        
            // (required) Called when a remote is received or opened, or local notification is opened
            notification.finish(PushNotificationIOS.FetchResult.NoData);
          },
  
          
          // // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
          // onAction: function (notification) {
          //   console.log("ACTION:", notification.action);
          //   console.log("NOTIFICATION ACTION:", notification);
        
          //   // process the action
  
          //   console.log('waiwawa')
          // },
        
          // // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
          // onRegistrationError: function(err) {
          //   console.error(err.message, err);
          // },
        
          // // IOS ONLY (optional): default: all - Permissions to register.
          // permissions: {
          //   alert: true,
          //   badge: true,
          //   sound: true,
          // },
        
          // Should the initial notification be popped automatically
          // default: true
          popInitialNotification: true,
        
          /**
           * (optional) default: true
           * - Specified if permissions (ios) and token (android and ios) will requested or not,
           * - if not, you must call PushNotificationsHandler.requestPermissions() later
           * - if you are not using remote notification or do not have Firebase installed, use this:
           *     requestPermissions: Platform.OS === 'ios'
           */
          requestPermissions: true,
        });
      }

      render(){
        return(
          <>
          <Container>

            {this.state.poolAdd == true ?  
              <CustomNavbar buttonBack={this.backToListWithoutLoad} title={'Tambah Pool'}/> : 
              this.state.poolEdit == true ?
              <CustomNavbar buttonBack={this.backToListWithoutLoad} title={'Edit Pool'}/> :
              <CustomNavbar title={'Daftar Pool'}/>
            }


        {
          this.state.isConnected == false?
            <LostConnection restartConnection={this.restartConnection}/> 
          : 
          this.state.poolAdd ?     
              <PoolAdd
              {...this.state}
              backToList={this.backToList}
              funcSweetAlert={this.funcSweetAlert}
              // {...this.props}
              />
              
              :

              this.state.poolEdit ?

              <PoolEdit
              {...this.state}
              backToList={this.backToList}
              funcSweetAlert={this.funcSweetAlert}
              // {...this.props}
              /> 

              :


              <ListPoolPage 
                  {...this.state}
                  {...this.props}
                  onPoolAdd = {this.onPoolAdd}
                  onPoolEdit = {this.onPoolEdit}
                  refreshContent={this.refreshContent}
                />

              
            }

            {/* <ListPoolPage 
                  {...this.state}
                  {...this.props}
                /> */}


          </Container>
            {this.state.isLoading ? <Loading/> : false}
          </>
        );
      }
}

ListPoolFunc= (props) => {
  const navigation = useNavigation();
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      BackHandler.addEventListener("hardwareBackPress", ()=>props.backAction('ListPool', navigation));
    });

    return unsubscribe;
  }, [navigation]);

  return <ListPool navigation={navigation}/>;
}

export default ListPoolFunc;