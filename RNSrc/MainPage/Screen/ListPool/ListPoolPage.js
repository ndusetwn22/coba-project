import React,{Component} from 'react'
import {View, ScrollView} from "react-native";
import styles from './style'
import {Text,Button, Content, Container, Icon} from 'native-base'

import FlatListPool from './Component/FlatListPool/FlatListPool';

import SearchInput,{ createFilter } from 'react-native-search-filter';
const KEYS_TO_FILTERS = ['nama', 'phone', 'nama_kota', 'kode_pos', 'alamat1'];


class ListPoolPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paramSearch:'',
        };
    }

    componentDidMount= async()=>{

    }

    render(){
        const filteredPool = this.props.listPool.filter(createFilter(this.state.paramSearch, KEYS_TO_FILTERS))
        return(
          <>
            {/* <View style={styles.flexGridNoPadding}>
               
            </View> */}
            
            {/* <Container> */}
                {/* <Content> */}
                    {/* <Content>
                    <View style={{flexDirection: 'row'}}>
                        <Button onPress={()=>this.props.onPoolAdd()} style={{marginTop: 10, marginLeft: 10, borderRadius: 10, padding: 0, height: 40}}>
                            <Icon type="MaterialCommunityIcons" name="flag" />
                        </Button>
                        <SearchInput 
                            onChangeText={(term) => { this.setState({paramSearch:term}) }} 
                            style={{
                                paddingLeft:10,
                                paddingTop:5,
                                marginTop: 10,
                                marginLeft:10,
                                marginRight:10,
                                marginBottom:5,
                                paddingBottom:5,
                                borderColor: '#008c45',
                                borderWidth: 1,
                                backgroundColor:"white",
                                borderRadius: 10,
                                width: '248%',
                            }}
                            placeholder="Search Pool"
                            inputFocus={true}
                            removeClippedSubviews={true} // Unmount components when outside of window 
                            initialNumToRender={2} // Reduce initial render amount
                            maxToRenderPerBatch={1} // Reduce number in each render batch
                            maxToRenderPerBatch={10} // Increase time between renders
                            windowSize={7} // Reduce the window size
                        />
                    </View>
                    </Content> */}
                    <FlatListPool {...this.state} {...this.props}/>
                {/* </Content> */}
            {/* </Container> */}
          </>
        );
    }
}

export default ListPoolPage;