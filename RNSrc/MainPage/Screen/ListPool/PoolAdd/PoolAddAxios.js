import Axios from 'axios';
import {encrypt, firebase} from '../../../../../APIProp/ApiConstants';
import {Alert} from 'react-native'
//firebase
import { utils } from '@react-native-firebase/app';
import storage from '@react-native-firebase/storage';

  export const getKota = async (url) =>{
    let dataQry = `
        select * from mt_master_kota
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('get Kota : ', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
      console.log('error', err)
    });
  }


  export const insertPool = async (url, trid, uid, namaPool, refId, namaLokasi, detailAlamat, nomorKontak, kota, kodePos, lat, lang, fotoFileName, fotoSource_) =>{

    //firebase pandu
    // let tempFotoPool = firebase.linkStorage+fotoFileName+'?alt=media'

    //firebase prod
    let tempFotoPool = firebase.linkStorage+fotoFileName+'?alt=media'

    //   insertFoto
    const FireBaseStorage = storage();

    //ini yg bener
    //Insert firebase foto
    const fotoSource = fotoSource_.uri
    const fotoStorageRef = FireBaseStorage.ref(`FotoPool/${fotoFileName}`)
    return await fotoStorageRef.putFile(fotoSource)
    .then(async(response)=>{
      return await fotoStorageRef.getDownloadURL()
      .then(async(urlDownload)=>{
          tempFotoPool=urlDownload; 
          var c = tempFotoPool.split("&token");
          tempFotoPool = c[0]
          console.log('link foto', tempFotoPool); 

          // let dataQry = `
          //   insert into mt_master_pool
          //   (nama, external_id, alamat1, alamat2, phone, kota_id, kode_pos, lat, lang, transporter_id, foto_pool, create_date, create_by, update_date, update_by)
          //   values ('`+namaPool+`', '`+refId+`', '`+namaLokasi+`', '`+detailAlamat+`', '`+nomorKontak+`', '`+kota+`', '`+kodePos+`', '`+lat+`', '`+lang+`', '`+trid+`', '`+tempFotoPool+`', now(), '`+uid+`', now(), '`+uid+`')
          // `

          let dataQry = `
              select * from mobiletra_insertpool('`+namaPool+`', '`+refId+`', '`+namaLokasi+`', '`+detailAlamat+`', '`+nomorKontak+`', `+kota+`, '`+kodePos+`', '`+lat+`', '`+lang+`', '`+tempFotoPool+`', '`+trid+`', '`+uid+`')
          `
          console.log('query insert pool : ', dataQry)
          let x = await encrypt(dataQry);
          return await Axios.post(url.insert,{
            query: x
          }).then(async(value)=>{
            let x = value.data.data;
            console.log('sukses query', value)
            return ['Sukses tambah pool!', 'success']; 
          }).catch(err=>{
            console.log('error', err)
            return ['Gagal tambah pool!', 'warning'];
          });

        ;})
      .catch((error)=>{
        return ['Gagal upload foto pool!', 'warning'];      
      })
    }).catch((error) => {   
        console.log('catch error', error)    
        return ['Gagal upload foto pool!', 'warning'];      
    })



    // let dataQry = `
    //     insert into mt_master_pool
    //     (nama, external_id, alamat1, alamat2, phone, kota_id, kode_pos, lat, lang, transporter_id, foto_pool, create_date, create_by, update_date, update_by)
    //     values ('`+namaPool+`', '`+refId+`', '`+namaLokasi+`', '`+detailAlamat+`', '`+nomorKontak+`', '`+kota+`', '`+kodePos+`', '`+lat+`', '`+lang+`', '`+trid+`', '`+tempFotoPool+`', now(), '`+uid+`', now(), '`+uid+`')
    // `
    // let x = await encrypt(dataQry);
    // return await Axios.post(url.insert,{
    //   query: x
    // }).then(async(value)=>{
    //   let x = value.data.data;

    //   console.log('sukses query', value)

    // }).catch(err=>{
    // //   return [];
    //   console.log('error', err)
    //   return ['Gagal tambah pool!', 'warning'];
    // });
  }

