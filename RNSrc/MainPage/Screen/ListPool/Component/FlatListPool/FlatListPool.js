import React,{Component} from 'react'
import {Text, View,Icon, Content, Button, Container} from 'native-base'
import {
    TouchableOpacity,
    ScrollView,FlatList, Image, Dimensions, RefreshControl, ToastAndroid
} from "react-native";
import styles from './style'
import CardPool from './CardPool/CardPool'
import SearchInput,{ createFilter } from 'react-native-search-filter';
import { getListPool } from '../../ListPoolAxios';
const KEYS_TO_FILTERS = ['nama', 'phone', 'nama_kota', 'kode_pos', 'alamat1'];
var listKosong = require('../../../../../../Assets/list_kosong.png');
const width = Dimensions.get('window').width;


class FlatListPool extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paramSearch:'',
            refreshing: false,
        };
    }

    componentDidMount= async()=>{
        // setTimeout(() => {this.onRefresh}, 3000)
        // this.onRefresh()
    } 

    onRefresh= async() => {
        this.setState({refreshing: true})
        var _tempLength = this.props.listPool.length
        var _listPool = await getListPool(this.props.url, this.props.profile.id_transporter)
        if (_listPool.length > _tempLength || _listPool.length < _tempLength) {
            
            if (_listPool.length > _tempLength) {
                console.log('refresh', this.props.listPool.length)
                this.props.refreshContent(_listPool)
                this.setState({refreshing: false})
    
                var selisih = parseInt(_listPool.length) - parseInt(_tempLength)
                ToastAndroid.show('New data added '+selisih, ToastAndroid.SHORT);
            }else{
                console.log('refresh', this.props.listPool.length)
                this.props.refreshContent(_listPool)
                this.setState({refreshing: false})
    
                ToastAndroid.show('Refresh ...', ToastAndroid.SHORT);
            }


        }
        else{
          ToastAndroid.show('No more new data available', ToastAndroid.SHORT);
          this.setState({refreshing: false})
        }
      };
    

    render(){

        const filteredPool = this.props.listPool.filter(createFilter(this.state.paramSearch, KEYS_TO_FILTERS))
        const hak_akses = this.props.hak_akses

        return(
            <>
                {/* adjust content dan scrollview */}
                    <View style={{flexDirection: 'row', backgroundColor: 'white', top: 1}}>
                    {hak_akses == 'ADMIN' ? 
                        <Button onPress={()=>this.props.onPoolAdd()} style={{marginTop: 10, marginLeft: 10, borderRadius: 10, padding: 0, height: 40, backgroundColor: '#008c45'}}>
                            <Icon type="MaterialCommunityIcons" name="flag-plus" />
                        </Button>
                    : 
                        <Button disabled onPress={()=>this.props.onPoolAdd()} style={{marginTop: 10, marginLeft: 10, borderRadius: 10, padding: 0, height: 40, backgroundColor: '#008c45'}}>
                            <Icon type="MaterialCommunityIcons" name="flag-plus" />
                        </Button>
                    }
                        <SearchInput 
                            onChangeText={(term) => { this.setState({paramSearch:term}) }} 
                            style={{
                                paddingLeft:10,
                                paddingTop:5,
                                marginTop: 10,
                                marginLeft:10,
                                marginRight:10,
                                marginBottom:5,
                                paddingBottom:5,
                                borderColor: '#008c45',
                                borderWidth: 1,
                                backgroundColor:"white",
                                borderRadius: 10,
                                width: '248%',
                            }}
                            placeholder="Search Pool"
                            inputFocus={true}
                            removeClippedSubviews={true} // Unmount components when outside of window 
                            initialNumToRender={2} // Reduce initial render amount
                            maxToRenderPerBatch={1} // Reduce number in each render batch
                            maxToRenderPerBatch={10} // Increase time between renders
                            windowSize={7} // Reduce the window size
                        />
                    </View>
                <Content
                    refreshControl={
                        <RefreshControl
                          refreshing={this.state.refreshing}
                          onRefresh={this.onRefresh.bind(this)}
                        />}
                >
                {this.props.listPool.length == 0 ? 
                    <View>
                        <Image resizeMode='cover' source={listKosong} style={{marginTop: 50, width: '50%', height: '50%', opacity: 0.5, alignSelf: 'center'}}></Image>
                        <Text note style={{marginBottom: width, textAlign: 'center', fontStyle:'italic'}}>Pool kosong</Text>
                    </View>
                :            
                    <ScrollView>
                            <FlatList
                                data={
                                    filteredPool
                                }
                                style={{marginBottom:10}}
                                renderItem={({ item: value, index }) => {
                                    return (
                                        <CardPool {...this.props} index={index} value={value} />
                                    );
                                }}
                                keyExtractor={(item, index) => index.toString()}
                            /> 
                    </ScrollView>
                }
                </Content>
            </>
        );
    }
}

export default FlatListPool;