import React,{Component} from 'react'
import {
    View
} from "react-native";
import styles from './style'
import {
    Text,  
    Card,
    CardItem,
    Body,
    Left,
    Right,
    Button,
    Icon,
    Thumbnail
} from 'native-base'
import { TouchableOpacity } from 'react-native-gesture-handler';

class CardPool extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount= async()=>{

    } 

    render(){
        const hak_akses = this.props.hak_akses

        return(
            // <Card style={{marginLeft: 10, marginRight: 10}}>
            //     <CardItem>
            //         <Body>
            //                 <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', alignContent:'center', alignSelf: 'center'}}>
            //                     <Text style={{fontSize: 16, fontWeight: 'bold', textAlign: 'center'}}>
            //                         {this.props.value.nama}
            //                     </Text>
            //                     <Text note style={{fontSize: 8, textAlign: 'center', marginBottom: -2}}>
            //                         {this.props.value.nama_kota}
            //                     </Text>
            //                 </View>

            //             <View style={{flex: 1, flexDirection: 'row'}}>
            //                 <View style={{flex:1, marginLeft: 10}}>
            //                     {/* <Text style={{width: "100%", fontSize: 14, fontWeight: 'bold'}}>
            //                         {this.props.value.nama}
            //                     </Text> */}
            //                     <Text note style={{width: "100%", fontSize: 12}}>
            //                         Nomor hp :{'\n'}{this.props.value.phone}
            //                     </Text>
            //                     {/* <Text note style={{width: "100%", fontSize: 12}}>
            //                         Kota :{'\n'}{this.props.value.nama_kota}
            //                     </Text> */}
            //                     <Text note style={{width: "100%", fontSize: 12}}>
            //                         Kode pos :{'\n'}{this.props.value.kode_pos}
            //                     </Text>
            //                     <Text note style={{width: "100%", fontSize: 12}}>
            //                         Ref ID :{'\n'}{this.props.value.external_id}
            //                     </Text>
            //                 </View>
            //                 <View style={{flex:1, justifyContent: 'center'}}>
            //                     <Text note style={{width: "100%", fontSize: 12, 
            //                     // fontStyle: 'italic', textAlign: 'center'
            //                     }}>
            //                     Alamat :{'\n'}{this.props.value.alamat1}
            //                     </Text>
            //                 </View>
            //             {hak_akses == 'ADMIN' ?
            //                 <View style={{justifyContent: 'center'}}>
            //                     <Button icon primary rounded small onPress={()=>this.props.onPoolEdit(this.props.value)} style={{margin: 5, backgroundColor: '#008c45'}}>
            //                         <Icon type="MaterialCommunityIcons" name='pencil-outline'/>
            //                     </Button>
            //                 </View>
            //             : null }
            //             </View>
            //         </Body>
            //     </CardItem>
            // </Card>

            <Card style={{marginLeft: 10, marginRight: 10, borderLeftWidth: 8, borderColor: 'green'}}>
                    <CardItem>
                    <Body>
                            
                        <View style={{flex: 1, flexDirection: 'row'}}>
                            <Left>
                                <View style={{flexDirection: 'column'}}>
                                    <Text style={{fontSize: 16, fontWeight: 'bold', textAlign: 'left'}}>
                                        {this.props.value.nama}
                                    </Text>
                                    <Text note style={{fontSize: 12, textAlign: 'left'}}>
                                        {this.props.value.nama_kota}
                                    </Text>
                                </View>
                            </Left>
                            <Right>
                                <View style={{flexDirection: 'column'}}>
                                        {hak_akses == 'ADMIN' ?
                                            <View style={{justifyContent: 'center'}}>
                                                <Button color="#008c45" icon rounded small onPress={()=>this.props.onPoolEdit(this.props.value)} style={{margin: 5, backgroundColor: '#008c45'}}>
                                                    <Icon type="MaterialCommunityIcons" name='pencil-outline'/>
                                                </Button>
                                            </View>
                                        : null }
                                </View>
                            </Right>
                        </View>


                            <View style = {{borderWidth: 0.5, marginTop: 5, marginBottom: 5, borderColor: '#008c45', width: '100%'}} />

                        <View style={{flex: 1, flexDirection: 'row'}}>
                            <View style={{flex:1}}>
                                <Text note style={{width: "100%", fontSize: 12}}>
                                    <Text style={{fontWeight:'bold', fontSize: 12}}>Telepon :</Text> {this.props.value.phone}
                                </Text>
                                {/* <Text note style={{width: "100%", fontSize: 12}}>
                                    <Text style={{fontWeight:'bold', fontSize: 12}}>Kode Pos :</Text> {this.props.value.kode_pos}
                                </Text> */}
                            </View>

                            <View style={{flex:1}}>
                                {/* <Text note style={{width: "100%", fontSize: 12}}>
                                    <Text style={{fontWeight:'bold', fontSize: 12}}>Alamat :</Text> {'\n'}{this.props.value.alamat1}
                                </Text> */}
                                <Text note style={{width: "100%", fontSize: 12}}>
                                    <Text style={{fontWeight:'bold', fontSize: 12}}>Kode Pos :</Text> {this.props.value.kode_pos}
                                </Text>
                            </View>
                       {/* {hak_akses == 'ADMIN' ?
                            <View style={{justifyContent: 'center'}}>
                                <Button icon primary rounded small onPress={()=>this.props.onPoolEdit(this.props.value)} style={{margin: 5, backgroundColor: '#008c45'}}>
                                    <Icon type="MaterialCommunityIcons" name='pencil-outline'/>
                                </Button>
                            </View>
                        : null } */}
                        </View>
                        <View style = {{borderWidth: 0.5, marginTop: 5, marginBottom: 5, borderColor: '#008c45', width: '100%'}} />

                            <Text note style={{width: "100%", fontSize: 12}}>
                                <Text style={{fontWeight:'bold', fontSize: 12}}>Alamat :</Text> {'\n'}{this.props.value.alamat1}
                            </Text>

                    </Body>
                </CardItem>
             </Card>
        );
    }
}

export default CardPool;