import React,{Component} from 'react'
import {Text, View,Icon} from 'native-base'
import ProgressBar from 'react-native-progress/Bar';
import styles from './style'

class TruckIdle extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount= async()=>{

    } 

    render(){
        return(
            <>
                <View style={styles.mtdViewStyleKiri}>
                  <View style={{backgroundColor: '#D1FCE8', height: 50, width: 50, borderRadius: 30, alignItems:'center', alignSelf: 'center', justifyContent: 'center'}}>
                    <Icon type="MaterialCommunityIcons" name='truck' style={{color: '#4D6680'}}/>
                  </View>
                  <View>
                    <Text style={{fontSize: 12, textAlign: 'center', marginTop: 5, color: '#7d8d8c'}}>Kendaraan Idle</Text>
                    <Text style={{fontSize: 18, fontWeight: 'bold', textAlign: 'center', marginTop: 10, color: '#7d8d8c'}}> {this.props.kendaraanIdle} </Text>
                  </View>
                </View>
            </>
        );
    }
}

export default TruckIdle;