import { StyleSheet } from 'react-native';


// sebelumnya elevation: 1
const styles = StyleSheet.create({
    mtdViewStyleKiri:{
        // flex: 1, //Kalo dikasih flex, jadi nya ke grid dibagi 2, gabisa pake width %
        backgroundColor: '#fafafa', 
        margin:10, 
        // borderRadius:10, 
        height: 150,
        width: '30%', 
        shadowColor: "#000",
        alignItems:'center', 
        alignSelf: 'center', 
        justifyContent: 'center',
    
        shadowOffset: {
          width: 0,
          height: 9,
        },
        shadowOpacity: 0.48,
        shadowRadius: 11.95,
        
        elevation: 1, 
        zIndex: 0
    },
});

export default styles;
