import React,{Component} from 'react'
import {Text, View} from 'native-base'
import ProgressCircle from 'react-native-progress/Circle';
import styles from './style'

class ProgressCircleIcon extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount= async()=>{

    }

    render(){
        return(
            <View style={styles.circleViewStyle}>
                <ProgressCircle 
                    style={{marginTop: 20}} 
                    color={this.props.colorChart} 
                    size={60} 
                    thickness={3} 
                    borderWidth={0} 
                    showsText={true}
                    unfilledColor={'#F2F4F4'} 
                    indeterminate={false} 
                    progress={
                        Number(this.props.percentage)
                    } 
                    textStyle={{fontSize: 14}} 
                    formatText={() => {
                        return (this.props.data)
                    }} 
                />

                <View style={{}}>
                  <Text style={styles.textStyleCircle}>{this.props.titleChart}</Text>
                </View>
            </View>
        );
    }
}

export default ProgressCircleIcon;