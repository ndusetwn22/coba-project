import { StyleSheet, PixelRatio, Platform } from 'react-native';


// sebelumnya elevation: 1
const styles = StyleSheet.create({
    circleViewStyle:{
        flex: 1,
        backgroundColor: '#fafafa', 
        margin: 10, 
        // borderRadius:10, //akan menghilangkan zIndex nya,
        // borderRadius: undefined,
        height: 110,
        width: '50%', 
        shadowColor: "#000",
        alignItems:'center', 
        alignSelf: 'center', 
        justifyContent: 'center',
    
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.48,
        shadowRadius: 11.95,
        // elevation: 10, 
        ...Platform.select({
            android:{
                elevation: 1,
            }
        }),
        zIndex: 0
    },
    textStyleCircle:{
        textAlign: 'center', 
        marginBottom: 20, 
        marginTop: 5, 
        fontSize: 12, 
        color: '#7d8d8c'
    },
});

export default styles;
