import React,{Component} from 'react'
import {Text, View} from 'native-base'
import ProgressBar from 'react-native-progress/Bar';
import styles from './style'

class ProgressBarIcon extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount= async()=>{

    }

    render(){
        return(
            <>
                <View style={styles.mtdViewStyleKananPosition}>
                    <View style={{flex: 4}}>
                        <Text style={styles.mtdViewStyleKananText}>{this.props.titleBar}</Text>
                    </View>
                    <View style={{flex: 2, marginRight: 10}}>
                        <Text style={styles.mtdViewStyleKananNumber}> {this.props.count}</Text>
                    </View>       
                </View>
                <ProgressBar 
                    progress={
                        Number(this.props.percentage)
                    } 
                    borderWidth={0} 
                    width={null} 
                    style={styles.mtdViewStyleKananProgressBar} 
                    color={this.props.barColor} 
                    unfilledColor={'#F2F4F4'}
                />
            </>
        );
    }
}

export default ProgressBarIcon;