import { StyleSheet } from 'react-native';


// sebelumnya elevation: 1
const styles = StyleSheet.create({
    mtdViewStyleKananPosition:{
        flex: 2, 
        flexDirection: 'row', 
        marginLeft: 10, 
        marginBottom:0,
        marginTop: 5
    },
    mtdViewStyleKananText:{
        fontSize: 12, 
        color: '#7d8d8c'
    },
    mtdViewStyleKananNumber:{
        fontSize: 12, 
        textAlign: 'right', 
        color: '#7d8d8c'
    },
    mtdViewStyleKananProgressBar:{
        margin: 10, 
        marginBottom: 5, 
        marginTop: -10
    },
});

export default styles;
