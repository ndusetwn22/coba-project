import React,{Component} from 'react'
import {Text, View,Icon} from 'native-base'
import {
    TouchableOpacity
} from "react-native";
import styles from './style'

class CardHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            total: 0
        };
    }

    componentDidMount= async()=>{
        // var total = 0

        // for (let index = 0; index < this.props.listActiveTrip.length; index++) {
        //     // const element = this.props.listActiveTrip[index];
        //     if (this.props.listActiveTrip[index].status != 'close' && this.props.listActiveTrip[index].status != 'cancel' && this.props.listActiveTrip[index].status != 'reject') {
        //         total = total + 1;
        //     }
        // }

        // this.setState({total: total})
    } 

    render(){
        var total = 0

        for (let index = 0; index < this.props.listActiveTrip.length; index++) {
            // const element = this.props.listActiveTrip[index];
            if (this.props.listActiveTrip[index].status != 'close' && this.props.listActiveTrip[index].status != 'cancel' && this.props.listActiveTrip[index].status != 'reject') {
                total = total + 1;
            }
        }

        return(
            <>
                <View style={styles.cardStyling}>
                    <Text style={styles.styleHeaderPlaceholder}>Total Trip</Text>
                    {/* <Text style={styles.styleCountAllTrip}>{this.props.listActiveTrip.length}</Text> */}
                    <Text style={styles.styleCountAllTrip}>{total}</Text>
                    <View style = {styles.lineStyle} />
                    <View style={styles.styleFlex}>
                        <TouchableOpacity style={{flex:1}} onPress={()=>this.props.goToTrip()}>
                            <Text style={styles.styleNewTrip}>
                                {(this.props.listActiveTrip.filter((element)=>{
                                    return element.status=='planning'
                                })).length}
                            </Text>
                            <Text style={styles.styleHeaderPlaceholder}>Baru</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex:1}} onPress={()=>this.props.openModal('confirm')}>
                            <Text style={styles.styleSubTrip}>
                                {(this.props.listActiveTrip.filter((element)=>{
                                    return element.status=='confirm'
                                })).length}
                            </Text>
                            <Text style={styles.styleHeaderPlaceholder}>Konfirmasi</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex:1}} onPress={()=>this.props.openModal('accepted')}>
                            <Text style={styles.styleSubTrip}>
                                {(this.props.listActiveTrip.filter((element)=>{
                                    return element.status=='accepted'
                                })).length}
                            </Text>
                            <Text style={styles.styleHeaderPlaceholder}>Aktif</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </>
        );
    }
}

export default CardHeader;