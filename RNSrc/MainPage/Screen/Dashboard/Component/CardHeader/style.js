import { StyleSheet } from 'react-native';


// sebelumnya elevation: 1
const styles = StyleSheet.create({
    cardStyling:{
        width:"100%",
        backgroundColor: '#fafafa', 
        marginLeft:0,
        // borderRadius:10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.3,
        shadowRadius: 11,
        elevation: 1, 
        zIndex: 0
    },
    styleCountAllTrip:{
        textAlign:"center",
        marginTop:0,
        fontSize:35,
        fontWeight:"bold",
        color:"#008c45"
    },
    styleHeaderPlaceholder:{
        textAlign:"center",
        marginTop:10,
        fontSize:12,
        color: '#7d8d8c'
    },
    styleSubTrip:{
        textAlign:"center",
        marginTop:0,
        fontSize:20,
        fontWeight:"bold",
        marginBottom:-10,
        fontStyle:"italic"
    },
    styleNewTrip:{
        textAlign:"center",
        marginTop:0,
        fontSize:20,
        fontWeight:"bold",
        marginBottom:-10,
        color:"#c98f3a",
        fontStyle:"italic"
    },
    lineStyle:{
        marginTop:10,
        marginBottom:2,
        marginLeft:15,
        marginRight:15,
        borderWidth: 0.5,
        borderColor:'#ddd'
    },
    styleFlex:{
        flexDirection: 'row',
        padding:10
    }
});

export default styles;
