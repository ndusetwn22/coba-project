import { StyleSheet, PixelRatio } from 'react-native';


// sebelumnya elevation: 1
const styles = StyleSheet.create({
    dateSetelected:{
        backgroundColor: '#008c45'
    },
    
    dateBackdrop:{
        elevation: 1,
        left: 0,
        right: 0,
        top: 0
    },

    dateContainer:{
        zIndex: 0
    },

    headerViewStyleGradientDate:{
        // borderRadius: 10,
        height: 35,
        marginLeft:10,
        marginRight:10,
        marginTop:10,
        backgroundColor: '#fafafa',
        borderWidth: 1,
        elevation:1,
        borderColor: '#F5AC41',
        justifyContent: 'center'
    },

    textStyle:{
        color:"#7d8d8c", 
        fontSize:15, 
        fontWeight:'bold', 
        textAlign: 'center'
    }
});

export default styles;
