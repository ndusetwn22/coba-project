import Axios from 'axios';
import {encrypt} from '../../../../APIProp/ApiConstants';

export const getActiveTrip = async (url,transporter_id, _startDate,_endDate) =>{
  var startDate = _startDate
  var endDate = _endDate
  startDate = startDate.format('YYYY-MM-DD')
  endDate = endDate.format('YYYY-MM-DD')

  // let dataQry = `
  //   select * from mobiletra_getactivetrip('`+transporter_id+`','`+startDate+`','`+endDate+`')  
  // `

  let dataQry = `
    select * from mobiletra_getlisttripv3('`+transporter_id+`','`+startDate+`','`+endDate+`')  
  `

  let x = await encrypt(dataQry);
  return await Axios.post(url.select,{
    query: x
  }).then(value=>{
    let x = value.data.data;
    if(x==""){
      return [];
    }
    return x;
  }).catch(err=>{
    return [];
  });
}

export const getActiveTripPlanning = async (url,transporter_id, _startDate,_endDate) =>{
  var startDate = _startDate
  var endDate = _endDate
  startDate = startDate.format('YYYY-MM-DD')
  endDate = endDate.format('YYYY-MM-DD')

  let dataQry = `
    select * from mobiletra_getlisttripplanningv3('`+transporter_id+`')  
  `
  let x = await encrypt(dataQry);
  return await Axios.post(url.select,{
    query: x
  }).then(value=>{
    let x = value.data.data;
    if(x==""){
      return [];
    }
    return x;
  }).catch(err=>{
    return [];
  });
}

export const getDataDashboard = async (url,user_id, _startDate,_endDate) =>{
  var startDate = _startDate
  var endDate = _endDate
  startDate = startDate.format('YYYY-MM-DD')
  endDate = endDate.format('YYYY-MM-DD')
  startDate += ' 00:00:00';
  endDate += ' 24:00:00';
  let dataQry = `
    select * from mobiletra_getcountdashboard('`+user_id+`','`+startDate+`','`+endDate+`')  
  `
  let x = await encrypt(dataQry);
  return await Axios.post(url.select,{
    query: x
  }).then(value=>{
    let x = value.data.data;
    if(x==""){
      return [];
    }
    return x;
  }).catch(err=>{
    return [];
  });
}

export const getOccupancy = async (url,user_id, _startDate,_endDate) =>{
  var startDate = _startDate
  var endDate = _endDate
  startDate = startDate.format('YYYY-MM-DD')
  endDate = endDate.format('YYYY-MM-DD')
  startDate += ' 00:00:00';
  endDate += ' 24:00:00';
  let dataQry = `
    select * from mobiletra_getoccupancy('`+user_id+`','`+startDate+`','`+endDate+`')  
  `
  let x = await encrypt(dataQry);
  return await Axios.post(url.select,{
    query: x
  }).then(value=>{
    let x = value.data.data;
    if(x==""){
      return [];
    }
    return x;
  }).catch(err=>{
    return [];
  });
}

export const getIdle = async (url,user_id, _startDate,_endDate) =>{
  var startDate = _startDate
  var endDate = _endDate
  startDate = startDate.format('YYYY-MM-DD')
  endDate = endDate.format('YYYY-MM-DD')
  startDate += ' 00:00:00';
  endDate += ' 24:00:00';
  let dataQry = `
    select * from mobiletra_getidle('`+user_id+`','`+startDate+`','`+endDate+`')  
  `
  let x = await encrypt(dataQry);
  return await Axios.post(url.select,{
    query: x
  }).then(value=>{
    let x = value.data.data;
    if(x==""){
      return [];
    }
    return x[0].kendaraan;
  }).catch(err=>{
    return [];
  });
}

export const getMTD = async (url,user_id) =>{
  let dataQry = `
    select * from mobiletra_getmtd('`+user_id+`')  
  `
  let x = await encrypt(dataQry);
  return await Axios.post(url.select,{
    query: x
  }).then(value=>{
    let x = value.data.data;
    console.log('mtd', x)
    if(x==""){
      return [];
    } else {

      if(x[0].batal == null){
        x[0].batal = 0
      }
      if(x[0].pesan == null){
        x[0].pesan = 0
      }
      if(x[0].telat == null){
        x[0].telat = 0
      }
      if(x[0].tepat == null){
        x[0].tepat = 0
      }
      if(x[0].total == null){
        x[0].total = 0
      }

    }
    return x[0];
  }).catch(err=>{
    return [];
  });
}

export const getDetailOrder = async (url,trip_id) =>{
  let dataQry = `
    select * from mobiletra_getdetailorder('`+trip_id+`')
  `

  // let dataQry = `
  //   select 
  //     mso.id, mso.id_order, mso.order_reference, mso.status, mso.customer_reference, mso.remarks, mso.instruksi, 
  //     mso.shipment_number, mso.weight, mso.dimension, mso.tipe_customer_id, mso.shipper_id, 
  //     mso.tipe_kendaraan, mso.booked_date, mso.booked_by, mso.confirmed_date, 
  //     mso.confirmed_by, mso.accepted_date, mso.accepted_by, mso.accepted_latitude,
  //     mso.suhu_id ,
  //     (select mms.minimal_suhu from mt_master_suhukendaraan mms where id = mso.suhu_id ) as min_suhu,
  //     (select mms.maksimal_suhu from mt_master_suhukendaraan mms where id = mso.suhu_id ) as maks_suhu, 
  //     mso.accepted_longitude, mso.arrival_date, mso.arrival_latitude, 
  //     mso.arrival_longitude, mso.startloaded_date, mso.startloaded_latitude, 
  //     mso.startloaded_longitude, mso.finishloaded_date, mso.finishloaded_latitude, 
  //     mso.finishloaded_longitude, mso.ontheway_date, mso.ontheway_latitude, mso.ontheway_longitude, 
  //     mso.dropoff_date, mso.dropoff_latitude, mso.dropoff_longitude, mso.startunloaded_date, mso.startunloaded_latitude,
  //     mso.startunloaded_longitude, mso.finishunloaded_date, mso.finishunloaded_latitude, mso.finishunloaded_longitude, 
  //     mso.pod_date, mso.cancel_date, mso.length, mso.width, mso.height, mso.fee, mso.asal_kota,
  //     mso.tujuan_kota, mso.asal_id, mso.tujuan_id, mso.jenis_kendaraan_id, mso.tipe_muatan, mso.tipe_armada, 
  //     mso.dokumen_pod, mso.deskripsi_dokumen, (mso.jadwal_penjemputan - interval '1' hour) as jadwal_penjemputan,
  //     mso.jadwal_sampai, mso.star, mso.create_date, mso.create_by, mso.update_date, mso.update_by, mso.dokumen_pod_2, 
  //     mso.deskripsi_dokumen_2, mso.dokumen_pod_3, mso.deskripsi_dokumen_3, mso.dokumen_pod_4, mso.deskripsi_dokumen_4,
  //     mso.dokumen_pod_5, mso.deskripsi_dokumen_5, mso.keterangan, 
  //     sos.nama as status_detail,
  //     msc.nama_perusahaan as asal_perusahaan, msc.lat as lat_asal, msc.lang as lang_asal,
  //     msc2.nama_perusahaan as tujuan_perusahaan, msc2.lat as lat_tujuan, msc.lang as lang_tujuan
  //   from
  //     mt_trip_order_transaction tot,
  //     mt_shipper_order mso,
  //     mt_shipper_order_status sos,
  //     mt_shipper_contact msc,
  //     mt_shipper_contact msc2
  //   where
  //     tot.order_id = mso.id and
  //     mso.status = sos.id and
  //     mso.asal_id = msc.id and
  //     mso.tujuan_id= msc2.id and
  //     tot.trip_id = '`+trip_id+`'
  // `
  let x = await encrypt(dataQry);
  return await Axios.post(url.select,{
    query: x
  }).then(value=>{
    let x = value.data.data;
    if(x==""){
      return [];
    }
    return x;
  }).catch(err=>{
    return [];
  });
}

export const getLatestLocation = async (url,trip_id) =>{
  let dataQry = `
    select * from mobiletra_getlatestloc('`+trip_id+`')
  `
  let x = await encrypt(dataQry);
  return await Axios.post(url.select,{
    query: x
  }).then(value=>{
    let x = value.data.data;
    if(x==""){
      return [];
    }
    return x;
  }).catch(err=>{
    return [];
  });
}



export const cekTokenFcm = async (url,user_id, token) =>{

  // let dataQry = `
  //   select * from mt_fcm_token mft
  //   where user_id = '`+user_id+`'
  //   and token_fcm = '`+token+`'
  // `

  let dataQry = `
        select * from mobiletra_cektokenfcm(`+user_id+`, '`+token+`')
  `
  console.log('cektoken FCM : ', dataQry)
  let x = await encrypt(dataQry);
  return await Axios.post(url.select,{
    query: x
  }).then(async(value)=>{
    let x = value.data.data;
    // console.log('value sukses query', value)
    if(x==""){
      // var strSql = `
      //   insert into mt_fcm_token (user_id, token_fcm, create_date, create_by, update_date, update_by)
      //   values (`+user_id+`, '`+token+`', now(), `+user_id+`, now(), `+user_id+`)
      //   `

      var strSql = `
        select * from mobiletra_inserttokenfcm(`+user_id+`, '`+token+`')
      `
        console.log('query insert fcm kalo kosong : ', strSql)
        let x = await encrypt(strSql);
        var result = await Axios.post(url.select,{
            query: x
        }).then(value=>{
            console.log('sukses token')
            return value;
        }).catch(err=>{
            console.log('gagal token', err)
            return null;
        });
      // return [];
    }

    return x;
  }).catch(err=>{
    return [];
  });
}