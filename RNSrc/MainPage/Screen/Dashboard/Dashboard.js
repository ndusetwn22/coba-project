import React,{Component, useEffect, useState} from 'react'
import {BackHandler, Alert, View, Button, Text, AppState} from "react-native";
import { useNavigation, useRoute  } from '@react-navigation/native';
import CustomNavbar from '../../Header/Navbar/Navbar'
import Loading,{getProfile, setUrl, getUrl, decrypt} from '../../../../APIProp/ApiConstants'
import DashboardPage from './DashboardPage'
import {getActiveTrip, getActiveTripPlanning, getDataDashboard, getOccupancy, getIdle,getMTD,getDetailOrder,getLatestLocation, cekTokenFcm} from './DashboardAxios'
import moment from 'moment';
import DetailTrip from './DetailTrip/DetailTrip'
import messaging from '@react-native-firebase/messaging';
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import PushNotification from 'react-native-push-notification'
import NetInfo from "@react-native-community/netinfo";
import LostConnection from '../../Component/LostConnection';
import AsyncStorage from '@react-native-community/async-storage';
// var PushNotification = require("react-native-push-notification");


class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
          appState: AppState.currentState,
          listActiveTrip:[],
          //ini sesuai urutan dari kolom db axios
          countDashboard:{
            q_pesanan :0, 
            q_sudahkonfirm :0, 
            q_belumkonfirm :0,
            q_batal :0,
            q_sudahdtg :0,
            q_belumdtg :0,
            q_sudahbrgkt :0,
            q_belumbrgkt :0,
            pengiriman :0,
            kedatangan :0, 
            tepat :0,
            pod :0
          },
          occupancy:{
            dimensi:0,
            maks_dimensi:0
          },
          mtd:{
            total:0,
            pesan:0,
            batal:0,
            tepat:0,
            telat:0
          },
          tripSelected:null,
          kendaraanIdle:0,
          profileStr:null,
          profile:null,
          startDate: moment(),
          endDate: moment(),
          displayedDate: moment(),
          showDetailTrip:false,
          isLoading:false,
          url:null,
          isConnected: null,
          isConnectedChange: null
        };
    }
    

    componentDidMount= async()=>{

      

      this.setState({isLoading:true});
      await setUrl();
      await getUrl().then(async(result)=>{
        this.setState({url:result})
      });      
      await this.funcGetProfile();
      await this.setupCloudMessage();
      await this.pushNotif();
      
      await messaging()
      .getToken()
      .then(async(token) => {
        var cekToken = await cekTokenFcm(this.state.url, this.state.profile.id_user, token)
        console.log('token messaging',token);
        return console.log('returning cekToken axios',cekToken);
      });

      await this.cekConnection();


      AppState.addEventListener('change', this._handleAppStateChange);



      messaging().onNotificationOpenedApp(remoteMessage => {
        console.log(
          'Notification caused app to open from background state:',
          remoteMessage.notification,
        );
        // navigation.navigate(remoteMessage.data.type);
      });


      // Register background handler
        messaging().setBackgroundMessageHandler(async remoteMessage => {
          console.log('Message handled in the background!', remoteMessage);
        });

        messaging()
        .getInitialNotification()
        .then(remoteMessage => {
          if (remoteMessage) {
            console.log(
              'Notification caused app to open from quit state:',
              remoteMessage,
            );
            // this.props.navigation.navigate('ListTrip',{
            //   screen: 'ListTrip',
            //   id_trip: remoteMessage.data.id_trip,
            //   id: remoteMessage.data.id
            // })

            this.props.navigation.navigate('Dashboard', {screen: 'Dashboard'})

            // setInitialRoute(remoteMessage.data.type); // e.g. "Settings"
          }
          // setLoading(false);
        });

      var _listActiveTrip= await getActiveTrip(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate);
      var _listActiveTripNew= await getActiveTripPlanning(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate);

      for ( var index=0; index<_listActiveTripNew.length; index++ ) {
        _listActiveTrip.push(_listActiveTripNew[index]);
      }

      var _countDashboard = await getDataDashboard(this.state.url, this.state.profile.id_user, this.state.startDate, this.state.endDate)
      var _occupancy = await getOccupancy(this.state.url, this.state.profile.id_user, this.state.startDate, this.state.endDate)
      var _kendaraanIdle = await getIdle(this.state.url, this.state.profile.id_user, this.state.startDate, this.state.endDate)
      var _mtd = await getMTD(this.state.url, this.state.profile.id_user)

      await this.setState({
        isLoading:false,
        listActiveTrip:_listActiveTrip,
        countDashboard:_countDashboard[0], 
        occupancy:_occupancy[0],
        kendaraanIdle:_kendaraanIdle,
        mtd:_mtd
      });
    }

    componentWillUnmount() {
      AppState.removeEventListener('change', this._handleAppStateChange);

      // ga ngefek dong wkwk
      // NetInfo.addEventListener(state => {
      //   this.setState({isConnected: state.isConnected})
      //   console.log("Connection type will Unmount:", state.type);
      //   // console.log("Is connected?", state.isConnected);
      // });
      
    }

    cekConnection= async() => {
      // Cek Connection
      NetInfo.addEventListener(state => {
        // salah, harusnya cek state nya
        if (this.state.isConnected == null) {
          this.setState({isConnected: state.isConnected})
        }

        if (state.isConnected == false) {
          this.setState({isConnected: state.isConnected, isConnectedChange: state.isConnected})
        }

        if (state.isConnected == true) {
          // tambah state restartConnection buat nahan
          this.setState({isConnectedChange: state.isConnected})
          // console.log("Is connected New?", state.isConnected);

        }
        // console.log("Connection type", state.type);
        console.log("Is connected?", state.isConnected);
      });
    }

    funcGetProfile=async()=>{
      await getProfile().then(result=>{
        console.log('result profile', result)
        if(result!=null){
          this.setState({profileStr:result})
        }
      })
      if(this.state.profileStr==null){
        this.props.navigation.replace('Login')
      }
      var tmpStr = await decrypt(this.state.profileStr);
      // this.setState({profile:JSON.parse(tmpStr)})
      console.log('profile', JSON.parse(tmpStr))
      console.log('profile dapetin is laut', JSON.parse(tmpStr).is_laut)


      if (JSON.parse(tmpStr).is_laut == undefined) {
        await AsyncStorage.clear();
        this.props.navigation.replace('Login')
      }else{
        this.setState({profile:JSON.parse(tmpStr)})
      }

      // try {
      //   console.log('masuk try catch')
      //   if (JSON.parse(tmpStr).is_laut == undefined) {
      //     this.setState({profile:JSON.parse(tmpStr)})
      //   }
      // } catch (error) {
      //     this.props.navigation.replace('Login')
      // }
    }

    setStartEndDate=async(s,e)=>{
      await this.setState({
        startDate:moment(s, 'YYYY-MM-DD'),
        endDate:moment(e, 'YYYY-MM-DD')
      })
    }

    onSearchButtonClicked=async()=>{
      await this.setState({isLoading:true});
      
      var startDate =this.state.startDate;
      var endDate;

      if(this.state.endDate==null){
        endDate = this.state.startDate;
      }else{
        endDate = this.state.endDate;
      }

      var _listActiveTrip= await getActiveTrip(this.state.url, this.state.profile.id_transporter, startDate, endDate);
      var _listActiveTripNew= await getActiveTripPlanning(this.state.url, this.state.profile.id_transporter, startDate, endDate);

      for ( var index=0; index<_listActiveTripNew.length; index++ ) {
        _listActiveTrip.push(_listActiveTripNew[index]);
      }

      var _countDashboard = await getDataDashboard(this.state.url, this.state.profile.id_user, startDate, endDate)
      var _occupancy = await getOccupancy(this.state.url, this.state.profile.id_user, startDate, endDate)
      var _kendaraanIdle = await getIdle (this.state.url, this.state.profile.id_user, startDate, endDate)

      await this.setState({
        isLoading:false,
        listActiveTrip:_listActiveTrip, 
        countDashboard:_countDashboard[0],
        occupancy:_occupancy[0],
        kendaraanIdle:_kendaraanIdle
      });
    }

    onTripSelected=async(infoTrip)=>{
      await this.setState({isLoading:true,tripSelected:null});
      var _tripSelected = {};
      _tripSelected.infoTrip = infoTrip;
      _tripSelected.infoOrder = await getDetailOrder(this.state.url,infoTrip.id);
      _tripSelected.location = await getLatestLocation(this.state.url,infoTrip.id);
      await this.setState({isLoading:false, tripSelected:_tripSelected, showDetailTrip:true});
    }

    hideDetailTrip=async()=>{
      await this.setState({showDetailTrip:false,tripSelected:null});
    }

    
  
    _handleAppStateChange = (nextAppState) => {
      if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
        console.log('App has come to the foreground!')
      }else{
        console.log('run background')
      }
      this.setState({appState: nextAppState});
    }

    setupCloudMessage = async() => {
      const authStatus = await messaging().requestPermission();
      const enabled =
        authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
        authStatus === messaging.AuthorizationStatus.PROVISIONAL;

      if (enabled) {
        console.log('Authorization status:', authStatus);
      }
    }

    refreshContent = async(_listActiveTrip, _countDashboard, _occupancy, _kendaraanIdle, _mtd) => {
      await this.setState({isLoading: true})
      await this.setState({
        isLoading:false,
        listActiveTrip:_listActiveTrip,
        countDashboard:_countDashboard, 
        occupancy:_occupancy,
        kendaraanIdle:_kendaraanIdle,
        mtd:_mtd
      });
    }

    
    pushNotif = async () => {
      // Must be outside of any component LifeCycle (such as `componentDidMount`).
      PushNotification.configure({
        // (optional) Called when Token is generated (iOS and Android)
        onRegister: function (token) {
          console.log("TOKEN:", token);
        },
      
        // (required) Called when a remote is received or opened, or local notification is opened
        onNotification: async (notification) => {
          console.log("NOTIFICATION:", notification);
          //Utk dpt Notif
          // process the notification
          //mau apa disini
          //disini setelah ada userInteraction/di klik
          if (notification.userInteraction) {
            console.log('DO SOMETHING NOTIFICATION IN DASHBOARD') 
            // this.props.navigation.navigate('ListTrip',{
            //   screen: 'ListTrip',
            //   id_trip: notification.data.id_trip,
            //   id: notification.data.id
            // })
            this.props.navigation.navigate('Dashboard')
          }
      
          // (required) Called when a remote is received or opened, or local notification is opened
          notification.finish(PushNotificationIOS.FetchResult.NoData);
        },

        
        // // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
        // onAction: function (notification) {
        //   console.log("ACTION:", notification.action);
        //   console.log("NOTIFICATION ACTION:", notification);
      
        //   // process the action

        //   console.log('waiwawa')
        // },
      
        // // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
        // onRegistrationError: function(err) {
        //   console.error(err.message, err);
        // },
      
        // // IOS ONLY (optional): default: all - Permissions to register.
        // permissions: {
        //   alert: true,
        //   badge: true,
        //   sound: true,
        // },
      
        // Should the initial notification be popped automatically
        // default: true
        popInitialNotification: true,
      
        /**
         * (optional) default: true
         * - Specified if permissions (ios) and token (android and ios) will requested or not,
         * - if not, you must call PushNotificationsHandler.requestPermissions() later
         * - if you are not using remote notification or do not have Firebase installed, use this:
         *     requestPermissions: Platform.OS === 'ios'
         */
        requestPermissions: true,
      });
    }

    sendNotif = async() =>{
      const FIREBASE_API_KEY = "AAAAT0n9D10:APA91bGKyMEys8nP8Ax1NiJy_TazJeaisVMID7p8EGHYcq-UZKYRR0Dw9gkElgcS3IX6EgB_qmNXfdVHUmgCqESTW0ZIpYmpbhyYQVcRFM8MujWg_NllhBO9YTP81QBVOiZgp3XQmaO4"
      var message = {
        registration_ids: [
          "dPYBjmJ9Riy17v-lPnyQoN:APA91bGJHumDgs1aISuCs-eqLDu4p8RgYTo4IgCQik6pV7CzD8zPRiZkjT8k01w4BifmnJPJSA9uaCTFWI01rSuB5Sq2LLGNDQ4DDOhko5c4wjs_CnCnoRkv7amv7WXMINGxxFYqvoiF",
        ],
        notification: {
          title: "Mostrans Transporter",
          body: "Test send notif",
          vibrate: 1,
          sound: 1,
          show_in_foreground: true,
          priority: "high",
          content_available: true,
          // imageUrl: 'https://firebasestorage.googleapis.com/v0/b/mostrans-dev.appspot.com/o/mostrans_logo.png?alt=media',
        },
        data: {
          title: "ID TRIP",
          body: "APA AJA",
          score: 120,
          wicket: 1,
          infoTrip: {
            id_trip: 100
          }
        },
      }

      let headers = new Headers({
        "Content-Type": "application/json",
        Authorization: "key=" + FIREBASE_API_KEY,
      })

      let response = await fetch("https://fcm.googleapis.com/fcm/send", {
        method: "POST",
        headers,
        body: JSON.stringify(message),
      }).then(async(result)=> {
        console.log('result send notif', result)
        var res = await result.json()
        console.log(res)
      }).catch((error)=>{
        console.log('error', error)
      })
      // response = await response.json()
      // console.log(response)
    }

    goToTrip = async() =>{
      this.props.navigation.navigate('MainRouter', {screen: 'ListTrip'})
      // replace('MainRouter', { screen:'ListUser'}
    }

    restartConnection = async() =>{
      console.log('Restart Connection', this.state.isConnected)
      if (this.state.isConnected == false && this.state.isConnectedChange == true) {
        console.log('HARUSNYA INI OFF', this.state.isConnected)
        this.props.navigation.replace('MainRouter', {screen: 'Dashboard'})
      }
    }
    
    render(){
        return(
          <>
            <CustomNavbar title={'Dashboard'}/>
            {
              this.state.isConnected == false?
                <LostConnection restartConnection={this.restartConnection}/> 
              :
              this.state.showDetailTrip?
              <DetailTrip
                hideDetailTrip={this.hideDetailTrip}
                {...this.state}
              />
              :
              <DashboardPage 
                {...this.state} 
                setStartEndDate={this.setStartEndDate}
                onTripSelected={this.onTripSelected} //ini props untuk detailnya
                onSearchButtonClicked={this.onSearchButtonClicked}
                sendNotif={this.sendNotif}
                refreshContent={this.refreshContent}
                goToTrip={this.goToTrip}
              />

            }
            {this.state.isLoading ? <Loading/> : false}
          </>
        );
    }
}

DashboardFunc= (props) => {
  const navigation = useNavigation();
  const route = useRoute();
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      BackHandler.addEventListener("hardwareBackPress", ()=>props.backAction('Dashboard', navigation));
    });

    return unsubscribe;
  }, [navigation]);

  return <Dashboard navigation={navigation} route={route}/>;
}

export default DashboardFunc;