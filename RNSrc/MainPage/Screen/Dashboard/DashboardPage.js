import React,{Component} from 'react'
import {View, ScrollView, Linking, TouchableOpacity, RefreshControl, ToastAndroid, Platform} from "react-native";
import styles from './style'
import {Text,Button, Content} from 'native-base'
import DateRange from './Component/DateRange/DateRange'
import ProgressCircleIcon from './Component/ProgressCircleIcon/ProgressCircleIcon'
import TruckIdle from './Component/TruckIdleCard/TruckIdle'
import ProgressBarIcon from './Component/ProgressBarIcon/ProgressBarIcon'
import CardHeader from './Component/CardHeader/CardHeader'
import ModalListTrip from './ModalListTrip/ModalListTrip'
import ModalCalendar from './Component/DateRange/ModalCalendar'
import {getActiveTrip, getActiveTripPlanning, getDataDashboard, getOccupancy, getIdle,getMTD,getDetailOrder,getLatestLocation} from './DashboardAxios'
import Toast from 'react-native-toast-message'
import NetInfo from "@react-native-community/netinfo";


class DashboardPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            modalVisible: false,
            modalCalenderVisible:false,
            modalDetailOrderVisible:false,
            statusTripModal:'confirm'
        };
    }

    componentDidMount= async()=>{
        // console.log('netInfo', NetInfo.fetch().then(state=>{
        //     console.log(state)
        // }))
    }



    openModal=(typeTrip)=>{
        this.setState({modalVisible: true, statusTripModal:typeTrip})
    }

    onSelectDate=async (start,end)=>{
        await this.props.setStartEndDate(start,end);
        await this.setState({modalCalenderVisible: false})
        await this.props.onSearchButtonClicked();
    }

    openModalCalendar=(typeTrip)=>{
        this.setState({modalCalenderVisible: true})
    }

    closeModal=()=>{
        this.setState({modalVisible: false, modalDetailOrderVisible:false,modalCalenderVisible: false})
    }

    retQtyChart = (status)=>{
        var res = this.props.countDashboard;
        var result=0;
        // console.log('res', res)
        
        switch(status){
            case 2: 
                result = res.q_sudahkonfirm ;
                break;
            case 4: 
                result = res.kedatangan ;
                break;
            case 7:
                result = res.pengiriman ;
                break;
            case 0:
                result = res.tepat ;
                break;
            default:
                if(this.props.occupancy.dimensi != 0 || this.props.occupancy.maks_dimensi != 0){
                    result = (this.props.occupancy.dimensi / this.props.occupancy.maks_dimensi) *100;
                    result= parseFloat(result).toFixed(0);
                    result = result+'%';
                }
                break;
        }
        return result;
    }

    countPercentage=(status)=>{
        //props dashboard bisa diambil karena {...this.state} dari Dashboard, lalu panggil disini degn props
        var res = this.props.countDashboard;
        var result=0;
        // console.log('res', res)
        
        switch(status){
            case 2: 
                if(res.q_sudahkonfirm!= 0 || (res.q_batal + res.q_sudahkonfirm) !=0){
                    result= parseFloat((res.q_sudahkonfirm / (res.q_batal + res.q_sudahkonfirm)).toFixed(2));
                };
                break;
            case 4: 
                if(res.kedatangan!= 0 || res.q_sudahkonfirm!=0){
                    result = (res.kedatangan/res.q_sudahkonfirm).toFixed(2)
                };
                break;
            case 7:
                if(res.pengiriman!= 0 || res.q_sudahkonfirm!=0){
                    result = (res.pengiriman/res.q_sudahkonfirm).toFixed(2)
                }; 
                break;
            case 0:
                if(res.tepat!= 0 || res.kedatangan!=0){
                    result = (res.tepat/res.kedatangan).toFixed(2)
                };   
                break;
            default:
                if(this.props.occupancy.dimensi != 0 || this.props.occupancy.maks_dimensi != 0){
                    result = (this.props.occupancy.dimensi / this.props.occupancy.maks_dimensi);
                }
                break;
        }
        return parseFloat(result).toFixed(2);
    }

    countPercentageMTD=(status)=>{
        var res = this.props.mtd;
        var result=0;
        
        if(res.total==0){
            return 0;
        }

        switch(status){
            case 1: 
                if(res.pesan!= 0){
                    result= (res.pesan/res.total).toFixed(2);
                };
                break;
            case 2: 
                if(res.tepat!= 0 ){
                    result = (res.tepat/res.total).toFixed(2)
                };
                break;
            case 3:
                if(res.telat!= 0 ){
                    result = (res.telat/res.total).toFixed(2)
                }; 
                break;
            case 4:
                if(res.batal!= 0 ){
                    result = (res.batal/res.total).toFixed(2)
                };   
                break;
        }
        return parseFloat(result).toFixed(2);
    }


    onRefresh= async() => {
        this.setState({refreshing: true})
  
            var _listActiveTrip= await getActiveTrip(this.props.url, this.props.profile.id_transporter, this.props.startDate, this.props.endDate);
            var _listActiveTripNew= await getActiveTripPlanning(this.props.url, this.props.profile.id_transporter, this.props.startDate, this.props.endDate);

            for ( var index=0; index<_listActiveTripNew.length; index++ ) {
                _listActiveTrip.push(_listActiveTripNew[index]);
            }

            var _countDashboard = await getDataDashboard(this.props.url, this.props.profile.id_user, this.props.startDate, this.props.endDate)
            var _occupancy = await getOccupancy(this.props.url, this.props.profile.id_user, this.props.startDate, this.props.endDate)
            var _kendaraanIdle = await getIdle(this.props.url, this.props.profile.id_user, this.props.startDate, this.props.endDate)
            var _mtd = await getMTD(this.props.url, this.props.profile.id_user)

            await this.props.refreshContent(_listActiveTrip, _countDashboard[0], _occupancy[0], _kendaraanIdle, _mtd)
  
            Toast.show({
                type: 'info',
                position: 'bottom',
                text1: 'Refreshing',
                text2: 'Refresh data ...',
                visibilityTime: 3000,
                autoHide: true,
                topOffset: 30,
                bottomOffset: 40,
                onShow: () => {},
                onHide: () => {}
              })

            // ToastAndroid.show('Refreshing', ToastAndroid.SHORT);
            this.setState({refreshing: false})
  
      };

    render(){
        return(
          <>
          <Content 
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh.bind(this)}
                    />}
          >
                <View style={styles.flexGridNoPadding}>
                    <View style={{flex:1}}>
                        <DateRange openModalCalendar={this.openModalCalendar} {...this.props}/>
                    </View>
                </View>
                
                <View style={styles.flexGrid}>
                    <CardHeader openModal={this.openModal} {...this.props}/>
                </View>
                <View style={{backgroundColor:"#fafafa"}}>
                    <View style = {styles.lineStyle} />
                    <Text onPress={this.props.sendNotif} style={{margin:10, color:"#008c45", fontSize:16, fontWeight:'bold', textAlign:"center", fontStyle:"italic"}}>Detail Process Order</Text>
                    <View style = {styles.lineStyle} />
                </View>
                <ScrollView>
                    <View style={styles.flexGridPLPR}>
                        <View style={{flex:1, flexDirection: 'row'}}>
                            <ProgressCircleIcon titleChart={'Konfirmasi'} colorChart='#64da8a' percentage={this.countPercentage(2)} data={this.retQtyChart(2)}/>
                        </View>
                        <View style={{flex:1, flexDirection: 'row'}}>
                            <ProgressCircleIcon titleChart={'Kedatangan'} colorChart='#2989E8' percentage={this.countPercentage(4)} data={this.retQtyChart(4)}/>
                        </View>
                        <View style={{flex:1, flexDirection: 'row'}}>
                            <ProgressCircleIcon titleChart={'Pengiriman'} colorChart='#4ecdc4' percentage={this.countPercentage(7)} data={this.retQtyChart(7)}/>
                        </View>
                    </View>
                    <View style={styles.flexGridPLPR}>
                        <View style={{flex:1, flexDirection: 'row'}}>
                            <ProgressCircleIcon titleChart={'On Time Kedatangan'} colorChart='#F5AC41' percentage={this.countPercentage(0)} data={this.retQtyChart(0)}/>
                        </View>
                        <View style={{flex:1, flexDirection: 'row'}}>
                            <ProgressCircleIcon titleChart={'Occupancy'} colorChart='#5c6b73' percentage={this.countPercentage(-1)} data={this.retQtyChart(-1)}/>
                        </View>
                    </View>
                    <View style={styles.flexGridPLPR}>
                        <TruckIdle {...this.props}/>
                        <View style={styles.mtdViewStyleKanan}>
                            <View style={{flex: 1}}>
                                <ProgressBarIcon titleBar='MTD Pesan' barColor='#64da8a' count={this.props.mtd.pesan} percentage={this.countPercentageMTD(1)}/>
                                <ProgressBarIcon titleBar='MTD Tepat Waktu' barColor='#F5AC41' count={this.props.mtd.tepat} percentage={this.countPercentageMTD(2)}/>
                                <ProgressBarIcon titleBar='MTD Terlambat' barColor='#5A6A7C' count={this.props.mtd.telat} percentage={this.countPercentageMTD(3)}/>
                                <ProgressBarIcon titleBar='MTD Batal' barColor='#EE585A' count={this.props.mtd.batal} percentage={this.countPercentageMTD(4)}/>
                            </View>
                        </View>
                    </View>
                    {/* <Button title="Click me" onPress={ ()=>{ Linking.openURL('https://google.com')}} /> */}
                </ScrollView>
            </Content>
            <ModalListTrip closeModal={this.closeModal} {...this.state} {...this.props}/> 
            <ModalCalendar {...this.state} {...this.props} onSelectDate={this.onSelectDate} closeModal={this.closeModal}/>
            <Toast ref={(ref) => Toast.setRef(ref)} />
          </>
        );
    }
}

export default DashboardPage;