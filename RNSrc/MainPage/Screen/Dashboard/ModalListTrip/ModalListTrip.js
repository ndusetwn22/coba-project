import React,{Component} from 'react'
import {View, ScrollView,TouchableOpacity,FlatList} from "react-native";
import styles from './style'
import {Text} from 'native-base'
import CardTrip from './Component/CardTrip/CardTrip'
import SearchInput from 'react-native-search-filter';
import Modal from 'react-native-modal';

class ModalListTrip extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paramSearch:""
        };
    }

    componentDidMount= async()=>{

    }

    render(){
        return(
        <Modal
            animationType="slide"
            style={{backgroundColor:"rgba(0,0,0,0.7)", width:"100%", height:"100%", margin:0}}
            visible={this.props.modalVisible}
            onRequestClose={this.props.closeModal}
        >
            <View style={{padding:20}}>
            <View style={styles.modal}>
                <View style={{flexDirection:"row"}}>
                <Text style={{width:"100%", textAlign:"left",paddingLeft:10, fontSize:20, flex:1, fontWeight:"bold"}}>
                    List TRIP
                </Text>
                <TouchableOpacity style={styles.tombolClose2} onPress={this.props.closeModal}>
                    <Text style={styles.textclose}>Tutup</Text>
                </TouchableOpacity>
                </View>
                <View style = {[styles.lineStyle,{margin:5}]} />
                <View style={{justifyContent: 'center', margin: 5}}> 
                    <SearchInput 
                        onChangeText={(term) => { this.setState({paramSearch:term}) }} 
                        style={{
                            paddingLeft:10,
                            paddingTop:5,
                            paddingBottom:5,
                            borderColor: '#CCC',
                            borderWidth: 2,
                            borderRadius: 10,
                            width: '100%',
                            fontSize: 10
                        }}
                        placeholder="Search Trip / Nopol / Driver / Jenis Kendaraan / Tipe Trip"
                        inputFocus={true}
                    />
                </View>
                    <View style = {[styles.lineStyle,{margin:8}]} />
                    <ScrollView>
                        <FlatList
                            data={
                                this.props.listActiveTrip.filter((element)=>{
                                    var boolFilter = false;

                                    var _jenisMuatan = element.tipe_muatan;
                                    if (_jenisMuatan == null) {
                                        _jenisMuatan = 'DARAT'.toUpperCase();
                                    }
                                    if(_jenisMuatan!=null && _jenisMuatan!=''){
                                        _jenisMuatan = _jenisMuatan.toUpperCase();
                                    }

                                    var _jenisKendaraan = element.jenis_kendaraan;
                                    if(_jenisKendaraan!=null && _jenisKendaraan!=''){
                                        _jenisKendaraan = _jenisKendaraan.toUpperCase();
                                    }
                                    var _namaDriver = element.nama_driver;
                                    if(_namaDriver!=null && _namaDriver!=''){
                                        _namaDriver = _namaDriver.toUpperCase();
                                    }
                                    var _platNomor = element.plat_nomor;
                                    if(_platNomor!=null && _platNomor!=''){
                                        _platNomor = _platNomor.toUpperCase();
                                    }
                                    var _paramSearch = this.state.paramSearch;
                                    if(_paramSearch!=null || _paramSearch!=''){
                                        _paramSearch = _paramSearch.toUpperCase();
                                    }

                                    if(
                                        element.status == this.props.statusTripModal
                                    ){
                                        if(element.id_trip.indexOf(_paramSearch) >-1){
                                            boolFilter=true;
                                        }
                                        if(_jenisMuatan!=null || _jenisMuatan==null){
                                            if(
                                                _jenisMuatan.indexOf(_paramSearch) >-1
                                            ){
                                                boolFilter = true;
                                            }
                                        }
                                        if(_jenisKendaraan!=null){
                                            if(
                                                _jenisKendaraan.indexOf(_paramSearch) >-1
                                            ){
                                                boolFilter = true;
                                            }
                                        }
                                        if(_namaDriver!=null){
                                            if(
                                                _namaDriver.indexOf(_paramSearch) >-1
                                            ){
                                                boolFilter = true;
                                            }
                                        }
                                        if(_platNomor!=null){
                                            if(
                                                _platNomor.indexOf(_paramSearch) >-1 
                                            ){
                                                boolFilter = true;
                                            }
                                        }
                                    }
                                    return boolFilter;
                                })
                            }
                            style={{marginBottom:10}}
                            renderItem={({ item: value }) => {
                                return (
                                    <CardTrip {...this.props} value={value} />
                                );
                            }}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </ScrollView>
                </View>
            </View>
          </Modal>
        );
    }
}

export default ModalListTrip;