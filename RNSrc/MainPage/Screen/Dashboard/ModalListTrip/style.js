import { StyleSheet, PixelRatio } from 'react-native';


// sebelumnya elevation: 1
const styles = StyleSheet.create({
    modal:{
        backgroundColor: '#ffffff',
        padding: 10,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#008c45',
        height:"100%"
    },
    tombolClose2:{
        width: 100,
        height: 30,
        borderRadius: 5,
        borderWidth: 1,
        flex:1,
        borderColor: '#008c45',
        alignSelf: 'flex-end',
        justifyContent: 'center'
    },
    textclose:{
        color: '#008c45',
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 15
    },
    lineStyle:{
        borderWidth: 0.5,
        borderColor:'#7d8d8c'
    },
});

export default styles;
