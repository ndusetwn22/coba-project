import { StyleSheet, PixelRatio } from 'react-native';


// sebelumnya elevation: 1
const styles = StyleSheet.create({
    listTripSize:{
        fontSize:15
    },
    flexGrid:{
        flex: 1,
        flexDirection: 'row'
    },
    mediumFontSize:{
        fontSize:14,
        padding:10
    },
    superMediumFontSize:{
        fontSize:13,
        width:"50%",
        fontStyle:"italic"
    },
    assignedStyle:{
        fontSize:13,
        width:"50%",
        fontWeight:"bold",
        fontStyle:"italic",
        color:"#595959"
    },
    unAssignedStyle:{
        fontSize:13,
        width:"50%",
        color:"red",
        fontStyle:"italic"
    },
    detailTripFontSize:{
        fontSize:11,
        width:"50%"
    },
    lineStyle:{
        borderWidth: 0.5,
        width:"100%",
        borderColor:'#008c45'
    },
    noContainerStyle: {
        width:'100%', 
        // color:"#595959",
        color: '#008c45',
        fontSize:12, 
        textAlign:"center", 
        fontStyle:"italic",
        fontWeight: 'bold'
    },
    noContainerStyleUnAssigned:{
        width:'100%', 
        color:"red", 
        fontSize:12, 
        textAlign:"center", 
        fontStyle:"italic",
        fontWeight: 'bold'
    }
});

export default styles;
