import React,{Component} from 'react'
import {
    View, 
    TouchableOpacity
} from "react-native";
import styles from './style'
import {
    Text,  
    Card,
    CardItem,
    Left,
    Right, 
    Thumbnail,
    Body
} from 'native-base'


const arrMonth =['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus','September','Oktober','November', 'Desember'];

class CardTrip extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount= async()=>{
        // console.log('PROPS CARD TRIP', this.props)
    }

    convDateString =(param)=>{
        var today = new Date(param);
        // console.log('param', param)
        today = new Date(today.setHours(today.getHours()-1));
        return today.getDate() + " " + arrMonth[today.getMonth()] + " " + today.getFullYear();
    }

    render(){

        if (this.props.value.naikkapal_date != null && this.props.value.naikkapal_date != undefined && this.props.value.naikkapal_date != '') {

            var waktu_naikKapal = this.props.value.naikkapal_date.split('T')[1]
            var slice_waktuNaikKapal = waktu_naikKapal.split(':')
            var waktuNaikKapal_new = slice_waktuNaikKapal[0]+':'+slice_waktuNaikKapal[1]

        }

        if (this.props.value.turunkapal_date != null && this.props.value.turunkapal_date != undefined && this.props.value.turunkapal_date != '') {

            var waktu_turunKapal = this.props.value.turunkapal_date.split('T')[1]
            var slice_waktuTurunKapal = waktu_turunKapal.split(':')
            var waktuTurunKapal_new = slice_waktuTurunKapal[0]+':'+slice_waktuTurunKapal[1]

        }
        return(
            <>
                {this.props.value.tipe_muatan == 'LAUT' ? 
                    // LAUT
                    <Card>
                        <TouchableOpacity 
                            onPress={()=>{this.props.closeModal();this.props.onTripSelected(this.props.value)}}
                        >
                            <CardItem >
                                <Body >
                                    <Text style={[styles.listTripSize,{width:"100%", backgroundColor:"#008c45", color:"white", textAlign:"center",borderRadius:4, marginBottom:10}]}>
                                        {this.props.value.id_trip} ( LAUT )
                                    </Text>

                                    {/* <View style={{width: '100%', marginTop: -5, marginBottom: 5}}>
                                        <Text style={
                                            this.props.value.no_container==null|| 
                                            this.props.value.no_container==""?
                                            [styles.unAssignedStyle, {textAlign: 'center', width: '100%'}]:[styles.assignedStyle, {textAlign: 'center', width: '100%'}]
                                        }>
                                            {this.props.value.no_container==null||this.props.value.no_container==""?
                                            "No Container Kosong"
                                            :
                                            this.props.value.no_container}
                                        </Text>
                                    </View> */}

                                    <View style = {styles.lineStyle} />
                                    <Text style={
                                            this.props.value.no_container==null|| 
                                            this.props.value.no_container==""?
                                            styles.noContainerStyleUnAssigned:styles.noContainerStyle
                                        }>
                                            {this.props.value.no_container==null||this.props.value.no_container==""?
                                            "No Container Kosong"
                                            :
                                            this.props.value.no_container}
                                    </Text>
                                    <View style = {styles.lineStyle} />

                                    <View style={[styles.flexGrid,{marginTop: 5}]}>
                                        <Text style={styles.assignedStyle}>
                                            Asal :
                                        </Text>

                                        <Text style={styles.assignedStyle}>
                                            Tujuan :
                                        </Text>

                                    </View>

                                    {/* PLAT NOMOR */}
                                    <View style={[styles.flexGrid,{marginBottom:5, marginTop: 5}]}>
                                        <Text style={
                                            this.props.value.plat_nomor_asal==null|| 
                                            this.props.value.plat_nomor_asal==""?
                                            styles.unAssignedStyle:styles.assignedStyle
                                        }>
                                            {this.props.value.plat_nomor_asal==null||this.props.value.plat_nomor_asal==""?
                                            "Plat Asal"
                                            :
                                            this.props.value.plat_nomor_asal}
                                        </Text>

                                        <Text style={
                                            this.props.value.plat_nomor_tujuan==null|| 
                                            this.props.value.plat_nomor_tujuan==""?
                                            styles.unAssignedStyle:styles.assignedStyle
                                        }>
                                            {this.props.value.plat_nomor_tujuan==null||this.props.value.plat_nomor_tujuan==""?
                                            "Plat Tujuan"
                                            :
                                            this.props.value.plat_nomor_tujuan}
                                        </Text>

                                    </View>
                                        
                                    {/* KERANI */}
                                    <View style={[styles.flexGrid,{marginBottom:5}]}>

                                        <Text style={
                                            this.props.value.nama_kerani_asal==null|| 
                                            this.props.value.nama_kerani_asal==""?
                                            styles.unAssignedStyle:styles.assignedStyle
                                        }>
                                            {
                                                this.props.value.nama_kerani_asal==null|| 
                                                this.props.value.nama_kerani_asal==""?
                                                "Kerani Asal"
                                                :
                                                this.props.value.nama_kerani_asal.substring(0,25)
                                            }
                                        </Text>

                                        <Text style={
                                            this.props.value.nama_kerani_tujuan==null|| 
                                            this.props.value.nama_kerani_tujuan==""?
                                            styles.unAssignedStyle:styles.assignedStyle
                                        }>
                                            {
                                                this.props.value.nama_kerani_tujuan==null|| 
                                                this.props.value.nama_kerani_tujuan==""?
                                                "Kerani Tujuan"
                                                :
                                                this.props.value.nama_kerani_tujuan.substring(0,25)
                                            }
                                        </Text>
                                    </View>

                                    <View style = {styles.lineStyle} />
                                    <Text style={{width:'100%', color:"#008c45", fontSize:12, fontWeight:'bold', textAlign:"center", fontStyle:"italic"}}>
                                            Naik dan Turun Kapal
                                    </Text>
                                    <View style = {styles.lineStyle} />
                                    
                                    <View style={{marginTop: 5}}></View>

                                    {/* NAIK DAN TURUN KAPAL */}
                                    <View style={[styles.flexGrid,{marginBottom:5}]}>

                                        <Text style={
                                            this.props.value.naikkapal_date==null|| 
                                            this.props.value.naikkapal_date==""?
                                            styles.unAssignedStyle:styles.assignedStyle
                                        }>
                                            {
                                                this.props.value.naikkapal_date==null|| 
                                                this.props.value.naikkapal_date==""?
                                                "Naik Kapal"
                                                :
                                                this.convDateString(this.props.value.naikkapal_date.split('T')[0])+'\n'+waktuNaikKapal_new
                                            }
                                        </Text>

                                        <Text style={
                                            this.props.value.turunkapal_date==null|| 
                                            this.props.value.turunkapal_date==""?
                                            styles.unAssignedStyle:styles.assignedStyle
                                        }>
                                            {
                                                this.props.value.turunkapal_date==null|| 
                                                this.props.value.turunkapal_date==""?
                                                "Turun Kapal"
                                                :
                                                this.convDateString(this.props.value.turunkapal_date.split('T')[0])+'\n'+waktuTurunKapal_new
                                            }
                                        </Text>
                                    </View>


                                    <View style = {styles.lineStyle} />
                                    <Text style={{ width:'100%', color:"#008c45", fontSize:12, fontWeight:'bold', textAlign:"center", fontStyle:"italic"}}>
                                        {this.props.value.jenis_kendaraan}
                                    </Text>
                                    <View style = {styles.lineStyle} />
                                    <View style={[styles.flexGrid,{marginBottom:5, marginTop:5}]}>
                                        <Text style={styles.superMediumFontSize}>
                                            {this.props.value.kota_asal}
                                        </Text>
                                        <Text style={styles.superMediumFontSize}>
                                            {this.props.value.kota_tujuan}
                                        </Text>
                                    </View>
                                    <View style={styles.flexGrid}>
                                        <Text style={styles.detailTripFontSize}>
                                            {this.props.value.tanggal_pengiriman.split('T')[0] +" "+ this.props.value.jam_pengiriman}
                                        </Text>
                                        <Text style={styles.detailTripFontSize}>
                                            {this.props.value.tanggal_sampai.split('T')[0]+" "+ this.props.value.jam_sampai}
                                        </Text>
                                    </View>
                                </Body>
                            </CardItem>
                        </TouchableOpacity>
                    </Card>

    : 

                // DARAT
                    <Card>
                        <TouchableOpacity 
                            onPress={()=>{this.props.closeModal();this.props.onTripSelected(this.props.value)}}
                        >
                            <CardItem >
                                <Body >
                                    <Text style={[styles.listTripSize,{width:"100%", backgroundColor:"#008c45", color:"white", textAlign:"center",borderRadius:4, marginBottom:10}]}>
                                        {this.props.value.id_trip}
                                    </Text>
                                    <View style={[styles.flexGrid,{marginBottom:5}]}>
                                        <Text style={
                                            this.props.value.plat_nomor==null|| 
                                            this.props.value.plat_nomor==""?
                                            styles.unAssignedStyle:styles.assignedStyle
                                        }>
                                            {
                                                this.props.value.plat_nomor==null|| 
                                                this.props.value.plat_nomor==""?
                                                "Kendaraan"
                                                :
                                                this.props.value.plat_nomor
                                            }
                                        </Text>
                                        <Text style={
                                            this.props.value.nama_driver==null|| 
                                            this.props.value.nama_driver==""?
                                            styles.unAssignedStyle:styles.assignedStyle
                                        }>
                                            {
                                                this.props.value.nama_driver==null|| 
                                                this.props.value.nama_driver==""?
                                                "Pengemudi"
                                                :
                                                this.props.value.nama_driver.substring(0,25)
                                            }
                                        </Text>
                                    </View>
                                    <View style = {styles.lineStyle} />
                                    <Text style={{ width:'100%', color:"#008c45", fontSize:12, fontWeight:'bold', textAlign:"center", fontStyle:"italic"}}>
                                        {this.props.value.jenis_kendaraan}
                                    </Text>
                                    <View style = {styles.lineStyle} />
                                    <View style={[styles.flexGrid,{marginBottom:5, marginTop:5}]}>
                                        <Text style={styles.superMediumFontSize}>
                                            {this.props.value.kota_asal}
                                        </Text>
                                        <Text style={styles.superMediumFontSize}>
                                            {this.props.value.kota_tujuan}
                                        </Text>
                                    </View>
                                    <View style={styles.flexGrid}>
                                        <Text style={styles.detailTripFontSize}>
                                            {this.props.value.tanggal_pengiriman.split('T')[0] +" "+ this.props.value.jam_pengiriman}
                                        </Text>
                                        <Text style={styles.detailTripFontSize}>
                                            {this.props.value.tanggal_sampai.split('T')[0]+" "+ this.props.value.jam_sampai}
                                        </Text>
                                    </View>
                                </Body>
                            </CardItem>
                        </TouchableOpacity>
                    </Card>

            }
            </>
        );
    }
}

export default CardTrip;