import { StyleSheet, PixelRatio } from 'react-native';

const styles = StyleSheet.create({
    flexGridNoPadding:{
        flexDirection: 'row',
    },
    lineStyle:{
        borderWidth: 0.5,
        borderColor:'#008c45',
        marginTop:5,
        marginBottom:5
    },
    textStyle:{
        flex:4,
        textAlign:"center",
        fontSize:20,
        fontWeight:"bold",
        marginTop:10
    },
    tombolClose3:{
        height: 25,
        borderRadius: 5,
        borderWidth: 1,
        backgroundColor:"white",
        borderColor: '#008c45',
        justifyContent: 'center',
        alignItems: 'stretch',
        margin: 10,
        flex:1
    },
    textclose:{
        color: '#008c45',
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 15
    },
    classHeaderDetail:{
        fontSize:18,
        color:"white",
        flex:4,
        fontWeight:"bold",
        marginTop:12,
        marginLeft:20,
        textAlign:"left"
    },
    classHeaderDetailNoFlex:{
        fontSize:16,
        width:"100%",
        color:"white",
        textAlign:"center",
        padding:5
    },
    viewDetailTrip:{
        flexDirection:"row",
        marginLeft:10,
        marginRight:10
    },
    classBodyData:{
        fontSize:14,
        width:"48%",
        paddingBottom:7,
        paddingLeft:10,
        marginRight:10,
        backgroundColor:"#fafafa",
        borderBottomWidth:1,
        borderLeftWidth:1,
        borderRightWidth:1,
        borderColor:"#ddd"
    },
    viewMarginBottom:{
        marginBottom:15
    },
    classBodyInfo:{
        fontSize:12,
        color:"#878787",
        width:"48%",
        fontStyle:"italic",
        backgroundColor:"#fafafa",
        paddingTop:7,
        paddingLeft:10,
        marginRight:10,
        borderTopWidth:1,
        borderLeftWidth:1,
        borderRightWidth:1,
        borderColor:"#ddd"
      },
      classCardStyle:{
        backgroundColor:"#fafafa",
        paddingTop:7,
        paddingLeft:10,
        marginRight:5,
        flex:1
      },
});

export default styles;
