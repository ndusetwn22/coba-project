import React,{Component} from 'react'
import {View, TouchableOpacity, ScrollView,FlatList} from "react-native";
import styles from './style'
import {Text,Button} from 'native-base'
import CustomMap from './Component/Map/Map'
import CardOrder from './Component/CardOrder/CardOrder'
import ModalOrder from './ModalOrder/ModalOrder'

const statusDetail = [
    "Planning",
    "Confirmed",
    "Accepted",
    "Arrival",
    "Start Loaded",
    "Finish Loaded",
    "On The Way",
    "Dropoff",
    "Start Unloaded",
    "Finish Unloaded",
    "POD"
]

const keteranganStatus = [
    "Shipper Order : ",
    "Order Diterima : ",
    "Driver Konfirmasi : ",
    "Tiba Lokasi Muat : ",
    "Mulai Muat : ",
    "Selesai Muat : ",
    "Mulai Perjalanan : ",
    "Tiba Lokasi Bongkar : ",
    "Mulai Bongkar : ",
    "Selesai Bongkar : ",
    "POD Dokumen : "
]

class DetailTrip extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModalOrder:false,
            selectedOrder:null,
            timelineData:null,
            dateNaik: 'Pilih Tanggal Naik',
            dateTurun: 'Pilih Tanggal Turun',
            timeNaik: 'Pilih Waktu Naik',
            timeTurun: 'Pilih Waktu Turun',
        };
    }

    componentDidMount= async()=>{
        console.log('props detail trip dashboard', this.props)

        if (this.props.tripSelected.infoTrip.naikkapal_date != null && this.props.tripSelected.infoTrip.naikkapal_date != undefined && this.props.tripSelected.infoTrip.naikkapal_date != '') {
            // TGL NAIK
            var tgl_naikKapal = this.props.tripSelected.infoTrip.naikkapal_date.split('T')[0]
            var slice_naikKapal = tgl_naikKapal.split('-')
            var naikKapal_new = slice_naikKapal[2]+'-'+slice_naikKapal[1]+'-'+slice_naikKapal[0]
            // console.log('TGL TAMPILIN', naikkapal_new)

            // JAM NAIK
            var waktu_naikKapal = this.props.tripSelected.infoTrip.naikkapal_date.split('T')[1]
            var slice_waktuNaikKapal = waktu_naikKapal.split(':')
            var waktuNaikKapal_new = slice_waktuNaikKapal[0]+':'+slice_waktuNaikKapal[1]

            await this.setState({dateNaik: naikKapal_new+' '+waktuNaikKapal_new, timeNaik: waktuNaikKapal_new})
        }

        if (this.props.tripSelected.infoTrip.turunkapal_date != null && this.props.tripSelected.infoTrip.turunkapal_date != undefined && this.props.tripSelected.infoTrip.turunkapal_date != '') {
            // TGL TURUN
            var tgl_turunKapal = this.props.tripSelected.infoTrip.turunkapal_date.split('T')[0]
            var slice_turunKapal = tgl_turunKapal.split('-')
            var turunKapal_new = slice_turunKapal[2]+'-'+slice_turunKapal[1]+'-'+slice_turunKapal[0]
            // console.log('TGL TAMPILIN', turunkapal_new)

            // JAM TURUN
            var waktu_turunKapal = this.props.tripSelected.infoTrip.turunkapal_date.split('T')[1]
            var slice_waktuTurunKapal = waktu_turunKapal.split(':')
            var waktuTurunKapal_new = slice_waktuTurunKapal[0]+':'+slice_waktuTurunKapal[1]

            await this.setState({dateTurun: turunKapal_new+' '+waktuTurunKapal_new, timeTurun: waktuTurunKapal_new})
        }
    }
    
    openModalOrder=async(infoOrder)=>{
        await this.setState({
            timelineData:await this.openDetailOrderModal(infoOrder),
            selectedOrder:infoOrder,
            showModalOrder:true
        })
    }

    closeModalOrder=async()=>{
        await this.setState({showModalOrder:false})
    }

    openDetailOrderModal=async (value)=>{
        var tmpTimelineData =[];
        for (var i = 0;i< Number(value.status);i++){
          var tmpTanggal;
          var tmpJam;
          switch (i){
            case 0: if(value.booked_date != null) {tmpTanggal= value.booked_date.split('T')[0];tmpJam = value.booked_date.split('T')[1].substring(0,5)};break;
            case 1: if(value.confirmed_date != null){tmpTanggal= value.confirmed_date.split('T')[0];tmpJam =value.confirmed_date.split('T')[1].substring(0,5)};break;
            case 2: if(value.accepted_date != null){tmpTanggal= value.accepted_date.split('T')[0];tmpJam =value.accepted_date.split('T')[1].substring(0,5)};break;
            case 3: if(value.arrival_date != null){tmpTanggal= value.arrival_date.split('T')[0];tmpJam =value.arrival_date.split('T')[1].substring(0,5)};break;
            case 4: if(value.startloaded_date != null){tmpTanggal= value.startloaded_date.split('T')[0];tmpJam =value.startloaded_date.split('T')[1].substring(0,5)};break;
            case 5: if(value.finishloaded_date != null){tmpTanggal= value.finishloaded_date.split('T')[0];tmpJam =value.finishloaded_date.split('T')[1].substring(0,5)};break;
            case 6: if(value.ontheway_date != null){tmpTanggal= value.ontheway_date.split('T')[0];tmpJam =value.ontheway_date.split('T')[1].substring(0,5)};break;
            case 7: if(value.dropoff_date != null){tmpTanggal= value.dropoff_date.split('T')[0];tmpJam =value.dropoff_date.split('T')[1].substring(0,5)};break;
            case 8: if(value.startunloaded_date != null){tmpTanggal= value.startunloaded_date.split('T')[0];tmpJam =value.startunloaded_date.split('T')[1].substring(0,5)};break;
            case 9: if(value.finishunloaded_date != null){tmpTanggal= value.finishunloaded_date.split('T')[0];tmpJam =value.finishunloaded_date.split('T')[1].substring(0,5)};break;
            case 10: if(value.pod_date != null){tmpTanggal= value.pod_date.split('T')[0];tmpJam =value.pod_date.split('T')[1].substring(0,5)};break;
          }
          tmpTimelineData.push({
            time:tmpJam,
            title:statusDetail[i],
            description:keteranganStatus[i]+tmpTanggal
          })
        }

        return tmpTimelineData;
    }

    render(){
        return(
          <>
            {this.props.tripSelected.infoTrip.tipe_muatan == "LAUT" ?

                // LAUT
                <View style={{flex: 1}}>
                    <View style={[styles.flexGridNoPadding,{backgroundColor:"#008c45"}]}>
                        <Text style={styles.classHeaderDetail}>
                        Detail Trip
                        </Text>
                        <TouchableOpacity style={styles.tombolClose3} onPress={this.props.hideDetailTrip}>
                            <Text style={styles.textclose}>Tutup</Text>
                        </TouchableOpacity>
                    </View>
                    <CustomMap {...this.props}/>
                    <View style={[{backgroundColor:"#008c45", marginBottom:10}]}>
                        <Text style={styles.classHeaderDetailNoFlex}>
                            {this.props.tripSelected.infoTrip.id_trip} ( LAUT )
                        </Text>
                    </View>
                    <ScrollView>
                        {this.props.tripSelected.location != "" ? 
                            this.props.tripSelected.location[0].suhu_latest != undefined && this.props.tripSelected.location[0].suhu_latest != null && this.props.tripSelected.location[0].suhu_latest != 0 ? 
                                <View style={[styles.viewDetailTrip,{marginBottom:10, marginTop: 5}]}>
                                                <View 
                                                                            style={[styles.classCardStyle,{
                                                                            shadowOffset: {
                                                                                width: 0,
                                                                                height: 2,
                                                                            },
                                                                            shadowOpacity: 0.1,
                                                                            shadowRadius: 3.84,                                                
                                                                            elevation:1
                                                                            }]}>
                                                    <Text style={[{textAlign: 'center', fontSize: 12, marginBottom: 5}]}>Suhu : {this.props.tripSelected.location[0].suhu_latest} C</Text>
                                            
                                                </View>
                                </View>
                            : null
                        : null}

                        {/* No Container */}
                        <View style={styles.viewDetailTrip}>
                            <Text style={[styles.classBodyInfo, {width: '100%', textAlign: 'center'}]}>No Container</Text>
                        </View>
                        <View style={[styles.viewDetailTrip, styles.viewMarginBottom]}>
                            <Text 
                                style={
                                    this.props.tripSelected.infoTrip.no_container==null || this.props.tripSelected.infoTrip.no_container==''?
                                    [styles.classBodyData,{color:"red", width: '100%', textAlign: 'center'}]:[styles.classBodyData, {width: '100%', textAlign: 'center'}]
                                }
                            >
                                {
                                        this.props.tripSelected.infoTrip.no_container==null || this.props.tripSelected.infoTrip.no_container==''?
                                        "Belum Diproses":
                                        this.props.tripSelected.infoTrip.no_container
                                } 
                            </Text>
                        </View>

                        {/* KENDARAAN */}
                        <View style={styles.viewDetailTrip}>
                            <Text style={styles.classBodyInfo}>Kendaraan Asal</Text>
                            <Text style={styles.classBodyInfo}>Kendaraan Tujuan</Text>
                        </View>
                        <View style={[styles.viewDetailTrip, styles.viewMarginBottom]}>
                            <Text 
                                style={
                                    this.props.tripSelected.infoTrip.kendaraan_id==null || this.props.tripSelected.infoTrip.kendaraan_id=='0'?
                                    [styles.classBodyData,{color:"red"}]:styles.classBodyData
                                }
                            >
                                {
                                    this.props.tripSelected.infoTrip.kendaraan_id==null || this.props.tripSelected.infoTrip.kendaraan_id=='0'?
                                    "Belum Diproses":
                                    this.props.tripSelected.infoTrip.plat_nomor_asal
                                } 
                            </Text>
                            <Text 
                                style={
                                    this.props.tripSelected.infoTrip.kendaraan_id_tuj==null || this.props.tripSelected.infoTrip.kendaraan_id_tuj=='0'?
                                    [styles.classBodyData,{color:"red"}]:styles.classBodyData
                                }
                            >
                                {
                                        this.props.tripSelected.infoTrip.kendaraan_id_tuj==null || this.props.tripSelected.infoTrip.kendaraan_id_tuj=='0'?
                                        "Belum Diproses":
                                        this.props.tripSelected.infoTrip.plat_nomor_tujuan
                                } 
                            </Text>
                        </View>

                        
                        {/* KERANI */}
                        <View style={styles.viewDetailTrip}>
                            <Text style={styles.classBodyInfo}>Kerani Asal</Text>
                            <Text style={styles.classBodyInfo}>Kerani Tujuan</Text>
                        </View>
                        <View style={[styles.viewDetailTrip, styles.viewMarginBottom]}>
                            <Text 
                                style={
                                    this.props.tripSelected.infoTrip.kerani_id_asal==null || this.props.tripSelected.infoTrip.kerani_id_asal== '0'?
                                    [styles.classBodyData,{color:"red"}]:styles.classBodyData
                                }
                            >
                                {
                                        this.props.tripSelected.infoTrip.kerani_id_asal==null || this.props.tripSelected.infoTrip.kerani_id_asal== '0'?
                                        "Belum Diproses":
                                        this.props.tripSelected.infoTrip.nama_kerani_asal
                                } 
                                {'\n'} 
                                {this.props.tripSelected.infoTrip.hp_kerani_asal==null?"":this.props.tripSelected.infoTrip.hp_kerani_asal}
                            </Text>
                            <Text 
                                style={
                                    this.props.tripSelected.infoTrip.kerani_id_tujuan==null || this.props.tripSelected.infoTrip.kerani_id_tujuan== '0'?
                                    [styles.classBodyData,{color:"red"}]:styles.classBodyData
                                }
                            >
                                {
                                        this.props.tripSelected.infoTrip.kerani_id_tujuan==null || this.props.tripSelected.infoTrip.kerani_id_tujuan== '0'?
                                        "Belum Diproses":
                                        this.props.tripSelected.infoTrip.nama_kerani_tujuan
                                }
                                {'\n'} 
                                {this.props.tripSelected.infoTrip.hp_kerani_tujuan==null?"":this.props.tripSelected.infoTrip.hp_kerani_tujuan}
                            </Text>
                        </View>
                        
                        <View style={styles.viewDetailTrip}>
                            <Text style={styles.classBodyInfo}>Naik Kapal</Text>
                            <Text style={styles.classBodyInfo}>Turun Kapal</Text>
                        </View>
                        <View style={[styles.viewDetailTrip, styles.viewMarginBottom]}>
                            <Text 
                                style={
                                    this.props.tripSelected.infoTrip.naikkapal_date != null && this.props.tripSelected.infoTrip.naikkapal_date != undefined && this.props.tripSelected.infoTrip.naikkapal_date != '' ?
                                    [styles.classBodyData]:[styles.classBodyData,{color:"red"}]
                                }
                            >
                                {
                                    this.props.tripSelected.infoTrip.naikkapal_date != null && this.props.tripSelected.infoTrip.naikkapal_date != undefined && this.props.tripSelected.infoTrip.naikkapal_date != '' ?
                                    this.state.dateNaik.toString():
                                    "Belum Diproses"
                                } 
                            </Text>
                            <Text 
                                style={
                                    this.props.tripSelected.infoTrip.turunkapal_date != null && this.props.tripSelected.infoTrip.turunkapal_date != undefined && this.props.tripSelected.infoTrip.turunkapal_date != '' ?
                                    [styles.classBodyData]:[styles.classBodyData,{color:"red"}]
                                }
                            >
                                {
                                    this.props.tripSelected.infoTrip.turunkapal_date != null && this.props.tripSelected.infoTrip.turunkapal_date != undefined && this.props.tripSelected.infoTrip.turunkapal_date != '' ?
                                    this.state.dateTurun.toString() :
                                    "Belum Diproses"
                                } 
                            </Text>
                        </View>
                        <View style={styles.viewDetailTrip}>
                            <Text style={styles.classBodyInfo}>Kota Asal</Text>
                            <Text style={styles.classBodyInfo}>Kota Tujuan</Text>
                        </View>
                        <View style={[styles.viewDetailTrip, styles.viewMarginBottom]}>
                            <Text style={styles.classBodyData}>{this.props.tripSelected.infoTrip.kota_asal}</Text>
                            <Text style={styles.classBodyData}>{this.props.tripSelected.infoTrip.kota_tujuan}</Text>
                        </View>
                        <View style={styles.viewDetailTrip}>
                            <Text style={styles.classBodyInfo}>Waktu Pengiriman</Text>
                            <Text style={styles.classBodyInfo}>Waktu Sampai</Text>
                        </View>
                        <View style={[styles.viewDetailTrip]}>
                            <Text style={[styles.classBodyData,{fontStyle:"italic"}]}>{this.props.tripSelected.infoTrip.tanggal_pengiriman.split('T')[0]}{" "}{this.props.tripSelected.infoTrip.jam_pengiriman}</Text>
                            <Text style={[styles.classBodyData,{fontStyle:"italic"}]}>{this.props.tripSelected.infoTrip.tanggal_sampai.split('T')[0]}{" "}{this.props.tripSelected.infoTrip.jam_sampai}</Text>
                        </View>
                        <View style = {styles.lineStyle} />
                            <Text style={{ width:'100%', color:"#008c45", fontSize:13, fontWeight:'bold', textAlign:"center", fontStyle:"italic"}}>
                                {this.props.tripSelected.infoTrip.jenis_kendaraan}
                            </Text>
                        <View style = {styles.lineStyle} />
                        <Text style={{fontSize:15,color:"black",width:"50%",marginLeft:20}}>Daftar Order</Text>
                        <FlatList
                            horizontal
                            data={this.props.tripSelected.infoOrder}
                            style={{marginLeft:20,marginBottom:10}}
                            renderItem={({ item: value }) => {
                            return (
                                <CardOrder value={value} openModalOrder={this.openModalOrder}/>
                            );
                            }}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </ScrollView>
                    {this.state.showModalOrder?
                        <ModalOrder closeModalOrder={this.closeModalOrder}  {...this.state} {...this.props}/>
                    :null}
                </View>


            :
                // DARAT
                <View style={{flex: 1}}>
                    <View style={[styles.flexGridNoPadding,{backgroundColor:"#008c45"}]}>
                        <Text style={styles.classHeaderDetail}>
                        Detail Trip
                        </Text>
                        <TouchableOpacity style={styles.tombolClose3} onPress={this.props.hideDetailTrip}>
                            <Text style={styles.textclose}>Tutup</Text>
                        </TouchableOpacity>
                    </View>
                    <CustomMap {...this.props}/>
                    <View style={[{backgroundColor:"#008c45", marginBottom:10}]}>
                        <Text style={styles.classHeaderDetailNoFlex}>
                            {this.props.tripSelected.infoTrip.id_trip}
                        </Text>
                    </View>
                    <ScrollView>
                        {this.props.tripSelected.location != "" ? 
                            this.props.tripSelected.location[0].suhu_latest != undefined && this.props.tripSelected.location[0].suhu_latest != null && this.props.tripSelected.location[0].suhu_latest != 0 ? 
                                <View style={[styles.viewDetailTrip,{marginBottom:10, marginTop: 5}]}>
                                                <View 
                                                                            style={[styles.classCardStyle,{
                                                                            shadowOffset: {
                                                                                width: 0,
                                                                                height: 2,
                                                                            },
                                                                            shadowOpacity: 0.1,
                                                                            shadowRadius: 3.84,                                                
                                                                            elevation:1
                                                                            }]}>
                                                    <Text style={[{textAlign: 'center', fontSize: 12, marginBottom: 5}]}>Suhu : {this.props.tripSelected.location[0].suhu_latest} C</Text>
                                            
                                                </View>
                                </View>
                            : null
                        : null}
                        <View style={styles.viewDetailTrip}>
                            <Text style={styles.classBodyInfo}>Nama Driver</Text>
                            <Text style={styles.classBodyInfo}>HP Driver</Text>
                        </View>
                        <View style={[styles.viewDetailTrip, styles.viewMarginBottom]}>
                            <Text 
                                style={
                                    this.props.tripSelected.infoTrip.nama_driver==null?
                                    [styles.classBodyData,{color:"red"}]:styles.classBodyData
                                }
                            >
                                {
                                    this.props.tripSelected.infoTrip.nama_driver==null?
                                    "Belum Diproses":
                                    this.props.tripSelected.infoTrip.nama_driver
                                } 
                            </Text>
                            <Text 
                                style={
                                    this.props.tripSelected.infoTrip.hp_driver==null?
                                    [styles.classBodyData,{color:"red"}]:styles.classBodyData
                                }
                            >
                                {
                                    this.props.tripSelected.infoTrip.hp_driver==null?
                                    "Belum Diproses":
                                    this.props.tripSelected.infoTrip.hp_driver
                                } 
                            </Text>
                        </View>
                        <View style={styles.viewDetailTrip}>
                            <Text style={styles.classBodyInfo}>Kota Asal</Text>
                            <Text style={styles.classBodyInfo}>Kota Tujuan</Text>
                        </View>
                        <View style={[styles.viewDetailTrip, styles.viewMarginBottom]}>
                            <Text style={styles.classBodyData}>{this.props.tripSelected.infoTrip.kota_asal}</Text>
                            <Text style={styles.classBodyData}>{this.props.tripSelected.infoTrip.kota_tujuan}</Text>
                        </View>
                        <View style={styles.viewDetailTrip}>
                            <Text style={styles.classBodyInfo}>Waktu Pengiriman</Text>
                            <Text style={styles.classBodyInfo}>Waktu Sampai</Text>
                        </View>
                        <View style={[styles.viewDetailTrip]}>
                            <Text style={[styles.classBodyData,{fontStyle:"italic"}]}>{this.props.tripSelected.infoTrip.tanggal_pengiriman.split('T')[0]}{" "}{this.props.tripSelected.infoTrip.jam_pengiriman}</Text>
                            <Text style={[styles.classBodyData,{fontStyle:"italic"}]}>{this.props.tripSelected.infoTrip.tanggal_sampai.split('T')[0]}{" "}{this.props.tripSelected.infoTrip.jam_sampai}</Text>
                        </View>
                        <View style = {styles.lineStyle} />
                            <Text style={{ width:'100%', color:"#008c45", fontSize:13, fontWeight:'bold', textAlign:"center", fontStyle:"italic"}}>
                                {this.props.tripSelected.infoTrip.jenis_kendaraan}
                            </Text>
                        <View style = {styles.lineStyle} />
                        <Text style={{fontSize:15,color:"black",width:"50%",marginLeft:20}}>Daftar Order</Text>
                        <FlatList
                            horizontal
                            data={this.props.tripSelected.infoOrder}
                            style={{marginLeft:20,marginBottom:10}}
                            renderItem={({ item: value }) => {
                            return (
                                <CardOrder value={value} openModalOrder={this.openModalOrder}/>
                            );
                            }}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </ScrollView>
                    {this.state.showModalOrder?
                        <ModalOrder closeModalOrder={this.closeModalOrder}  {...this.state} {...this.props}/>
                    :null}
                    </View>
                }
          </>
        );
    }
}

export default DetailTrip;