import React,{Component} from 'react'
import {View, ScrollView,TouchableOpacity} from "react-native";
import styles from './style'
import {Text,Button, Card,CardItem,Body} from 'native-base'


class CardOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount= async()=>{

    }

    
    render(){
        return(
            <Card>
                <TouchableOpacity onPress={()=>this.props.openModalOrder(this.props.value)}>
                    <CardItem >
                        <Body >
                            <Text style={{fontSize:12,width:"100%",textAlign:"center",borderRadius:4}}>
                                {this.props.value.id_order}
                            </Text>
                            <Text style={{fontSize:10,width:"100%",textAlign:"center",borderRadius:4}}>
                                {this.props.value.status_detail}
                            </Text>
                        </Body>
                    </CardItem>
                </TouchableOpacity>
            </Card>
        );
    }
}

export default CardOrder;