import React,{Component} from 'react'
import {View, ScrollView,Dimensions, Image} from "react-native";
import styles from './style'
import {Text,Button} from 'native-base'
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';

var logoTruck = require('../../../../../../../Assets/truck.png')
var logoCruise = require('../../../../../../../Assets/cruise-ship.png')
var logoPin = require('../../../../../../../Assets/pin2.png')

var { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE = -6.117664;
const LONGITUDE = 106.906349;
const LATITUDE_DELTA = 1;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

// const GOOGLE_MAPS_APIKEY = "AIzaSyBjwcsAzrWZnO_YKtgmQCxtr_Zy8HbCRUE";
//bawah baru
// const GOOGLE_MAPS_APIKEY = "AIzaSyCYYgxprwYTOyATj_d8M0kCN4s6venTAJQ";
const GOOGLE_MAPS_APIKEY = "AIzaSyAUlDuk4rcP2yCkypwtQxWux3IKwKXyfas";




class CustomMap extends Component {
    constructor(props) {
        super(props);
        this.state = {
            region: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
            },
        };
    }

    componentDidMount= async()=>{

    }

    
    render(){

        var myIcon = logoTruck
        if (this.props.tripSelected.infoTrip.tipe_muatan == 'LAUT' && this.props.tripSelected.infoTrip.icon_trip == 'true') {
            myIcon = logoTruck
        }

        if (this.props.tripSelected.infoTrip.tipe_muatan == 'LAUT' && this.props.tripSelected.infoTrip.icon_trip == 'false') {
            myIcon = logoCruise
        }

        return(
            <MapView
                provider={PROVIDER_GOOGLE}
                style={styles.mapStyle}
                showsUserLocation={true}
                showsMyLocationButton={true}
                region={this.state.region}
            >
                {
                    this.props.tripSelected.location!=undefined && this.props.tripSelected.location!=null?
                    this.props.tripSelected.location.length>0?
                        <MapView.Marker coordinate={{latitude:Number(this.props.tripSelected.location[0].lat_driver),longitude:Number(this.props.tripSelected.location[0].lang_driver)}}>
                            {/* <Text style={{borderColor:"#008c45",backgroundColor:"white", textAlign:"center",borderRadius:4,borderWidth: 1, fontSize:10, padding:4}}>DRIVER</Text> */}
                            {this.props.tripSelected.location[0].suhu_latest != undefined && this.props.tripSelected.location[0].suhu_latest != null && this.props.tripSelected.location[0].suhu_latest != 0? 
                            <Text style={{borderColor:"#008c45",backgroundColor:"white", textAlign:"center",borderRadius:10,borderWidth: 1, fontSize:10, padding:2}}>
                                Driver & Suhu{"\n"}
                                <Text style={{fontSize:8,fontStyle:"italic"}}>
                                    {this.props.tripSelected.location[0].suhu_latest} C
                                </Text>
                            </Text>
                            : null
                            }
                            {/* <Image source={logoTruck} style={{height: 30, width:30,resizeMode:'contain'}} /> */}
                            <Image source={myIcon} style={{height: 30, width:30,resizeMode:'contain'}} />
                        </MapView.Marker>
                    :
                    null
                    :
                    null
                }
                {this.props.tripSelected.infoOrder.map((value,index)=>{
                    return(
                        <View>
                            <MapView.Marker title="Asal"
                                coordinate={
                                    {
                                        latitude:Number(value.lat_asal),
                                        longitude:Number(value.lang_asal)
                                    }
                                }
                            >
                                <Text style={{borderColor:"#008c45",backgroundColor:"white", textAlign:"center",borderRadius:10,borderWidth: 1, fontSize:10, padding:2}}>
                                    ASAL{"\n"}
                                    <Text style={{fontSize:8,fontStyle:"italic"}}>
                                        {value.id_order}
                                    </Text>
                                </Text>
                                <Image source={logoPin} style={{height: 40, width:80,resizeMode: 'contain' }} />
                            </MapView.Marker>
                            <MapView.Marker title="Tujuan" 
                                coordinate={
                                    {
                                        latitude:Number(value.lat_tujuan),
                                        longitude:Number(value.lang_tujuan)
                                    }
                                }
                            >
                                <Text style={{borderColor:"#008c45",backgroundColor:"white", textAlign:"center",borderRadius:10,borderWidth: 1, fontSize:10, padding:2}}>
                                    TUJUAN{"\n"}
                                    <Text style={{fontSize:8,fontStyle:"italic"}}>
                                        {value.id_order}
                                    </Text>
                                </Text>
                                <Image source={logoPin} style={{height: 40, width:80,resizeMode: 'contain' }} />
                            </MapView.Marker>
                            <MapViewDirections
                                origin={{
                                    latitude:Number(value.lat_asal),
                                    longitude:Number(value.lang_asal)
                                }}
                                destination={{
                                    latitude:Number(value.lat_tujuan),
                                    longitude:Number(value.lang_tujuan)
                                }}
                                apikey={GOOGLE_MAPS_APIKEY}
                                mode="DRIVING"
                                strokeWidth={3}
                                strokeColor="hotpink"
                            />
                        </View>
                        
                    );
                })}
          </MapView>
        );
    }
}

export default CustomMap;