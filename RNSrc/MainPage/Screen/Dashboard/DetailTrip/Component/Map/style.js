import { StyleSheet, PixelRatio } from 'react-native';

const styles = StyleSheet.create({
    mapStyle: {
        width: '100%',
        height: 250,
        padding:10,
    },
});

export default styles;
