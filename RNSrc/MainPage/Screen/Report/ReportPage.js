import React,{Component} from 'react'
import {View, ScrollView, Dimensions, RefreshControl, ToastAndroid} from "react-native";
import styles from './style'
import {Text,Button, Content} from 'native-base'

import moment from 'moment'
import DateRange from './Component/DateRange/DateRange'
import ModalCalendar from './Component/DateRange/ModalCalendar'
import ProgressCircleIcon from './Component/ProgressCircleIcon/ProgressCircleIcon'
import TrendLabel from './Component/TrendLabel/TrendLabel'
import LineChartIcon from './Component/LineChartIcon/LineChartIcon';
import ColorIcon from './Component/ColorIcon/ColorIcon'
import {
    LineChart,
  } from "react-native-chart-kit";
import { getOF, getOTA, getOTL, getOTD, getTrend } from './ReportAxios';
import Toast from 'react-native-toast-message'
  

class ReportPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalCalenderVisible:false,
            refreshing: false,
            data : {
                labels: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agus", "Sep", "Okt", "Nov", "Des"],
                datasets: [
                  {
                    data: [0,0,0,0,0,100,0,0,0,0,0,0],
                    color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})`, // optional
                    strokeWidth: 2 // optional
                  },
                  {
                    data: [40, 25, 98, 10, 69, 50,0,0,0,0,0,0],
                    color: (opacity = 1) => `rgba(65, 134, 244, ${opacity})`, // optional
                    strokeWidth: 2 // optional
                  },
                  {
                    data: [10, 35, 18, 20, 79, 30,0,0,0,0,0,0],
                    color: (opacity = 1) => `rgba(20, 134, 244, ${opacity})`, // optional
                    strokeWidth: 2 // optional
                  },
                  {
                    data: [20, 15, 58, 80, 34, 20,0,0,0,0,0,0],
                    color: (opacity = 1) => `rgba(65, 40, 244, ${opacity})`, // optional
                    strokeWidth: 2 // optional
                  },
                  {
                    data: [24, 32, 40, 25, 96, 10,0,0,0,0,0,0],
                    color: (opacity = 1) => `rgba(65, 134, 50, ${opacity})`, // optional
                    strokeWidth: 2 // optional
                  },
                ],
                // legend: ["Rainy Days", "Sunny Days", "Snowy Days"] // optional
              }
        };
    }

    componentDidMount= async()=>{
      // console.log('props', this.props)
    }

    componentWillReceiveProps=async(nextProps)=>{
      // ini akan terulang terus jika ada props baru
      // console.log('wkkwkw', nextProps.allTrend)

        if (nextProps.allTrend.length > 0) {
            this.countGrafikChart(nextProps.allTrend)
        }
    }

    countPercentage=(status)=>{
        //props dashboard bisa diambil karena {...this.state} dari Dashboard, lalu panggil disini degn props
        var resOF = this.props.OF;
        var resOTA = this.props.OTA;
        var resOTL = this.props.OTL;
        var resOTD = this.props.OTD;
        var result=0;
        
        switch(status){
            case 1:
                //OF 
                if(resOF.of_hit!= 0 || (resOF.of_hit + resOF.of_no_hit) !=0 || resOF.of_no_hit != 0){
                    result= parseFloat((resOF.of_hit / (parseInt(resOF.of_hit) + parseInt(resOF.of_no_hit))).toFixed(2));
                };
                break;

            case 2: 
                //OTA
                if(resOTA.ota_hit!= 0 || (resOTA.ota_hit + resOTA.ota_no_hit) !=0 || resOTA.ota_no_hit != 0){
                    result= parseFloat((resOTA.ota_hit / (parseInt(resOTA.ota_hit) + parseInt(resOTA.ota_no_hit))).toFixed(2));
                };
                break;

            case 3:
                //OTL
                if(resOTL.otl_hit!= 0 || (resOTL.otl_hit + resOTL.otl_no_hit) !=0 || resOTL.otl_no_hit != 0){
                    result= parseFloat((resOTL.otl_hit / (parseInt(resOTL.otl_hit) + parseInt(resOTL.otl_no_hit))).toFixed(2));
                };
                break;

            case 4:
                //OTD
                if(resOTD.otd_hit!= 0 || (resOTD.otd_hit + resOTD.otd_no_hit) !=0 || resOTD.otd_no_hit != 0){
                    result= parseFloat((resOTD.otd_hit / (parseInt(resOTD.otd_hit) + parseInt(resOTD.otd_no_hit))).toFixed(2));
                };
                break;
            default:
                result=0
            break;
    }
        return parseFloat(result).toFixed(2);
    }

    countHitAndTotal=(status)=>{
        //props dashboard bisa diambil karena {...this.state} dari Dashboard, lalu panggil disini degn props
        var resOF = this.props.OF;
        var resOTA = this.props.OTA;
        var resOTL = this.props.OTL;
        var resOTD = this.props.OTD;
        var result=[];
        
        switch(status){
            case 1:
                //OF 
                result.push(parseInt(resOF.of_hit))
                result.push(parseInt(resOF.of_hit) + parseInt(resOF.of_no_hit))
                break;

            case 2: 
                //OTA
                result.push(parseInt(resOTA.ota_hit))
                result.push(parseInt(resOTA.ota_hit) + parseInt(resOTA.ota_no_hit))
                break;

            case 3:
                //OTL
                result.push(parseInt(resOTL.otl_hit))
                result.push(parseInt(resOTL.otl_hit) + parseInt(resOTL.otl_no_hit))
                break;

            case 4:
                //OTD
                result.push(parseInt(resOTD.otd_hit))
                result.push(parseInt(resOTD.otd_hit) + parseInt(resOTD.otd_no_hit))
                break;
            default:
                result=[0,0]
            break;
        }
        return result
    }

    countTrendTerbanyak=(status)=>{
        //props dashboard bisa diambil karena {...this.state} dari Dashboard, lalu panggil disini degn props
        var res = this.props.allTrend;
        // console.log('Trend terbanyak', res)
        var result=[];

        var tmpOtd = []
        var tmpOtl = []
        var tmpOta = []
        var tmpOrder = []
        var tmpTrip = []
        var tmpBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"]
        
        for (let i = 0; i < res.length; i++) {
            const element = res[i];
            // console.log('jml_otd', element.jml_otd)
            tmpOtd.push(parseInt(element.jml_otd))
            tmpOtl.push(parseInt(element.jml_otl))
            tmpOta.push(parseInt(element.jml_ota))
            tmpOrder.push(parseInt(element.jum_order))
            tmpTrip.push(parseInt(element.jum_trip))
        }

        switch(status){
            case 1:
                //OTD
                result.push(Math.max(...tmpOtd))
                result.push(tmpBulan[tmpOtd.indexOf(Math.max(...tmpOtd))])
                return result
                break;

            case 2: 
                //OTA
                result.push(Math.max(...tmpOta))
                result.push(tmpBulan[tmpOta.indexOf(Math.max(...tmpOta))])
                return result
                break;

            case 3:
                //OTL
                result.push(Math.max(...tmpOtl))
                result.push(tmpBulan[tmpOtl.indexOf(Math.max(...tmpOtl))])
                return result
                break;

            case 4:
                var data = { 
                    labels: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agus", "Sep", "Okt", "Nov", "Des"],
                    datasets: [
                          {
                            data:tmpOtd,
                            color: (opacity = 1) => `rgba(127, 255, 89, ${opacity})`, // optional
                            strokeWidth: 2 // optional
                          },
                          {
                            data:tmpOtl,
                            color: (opacity = 1) => `rgba(44, 101, 247, ${opacity})`, // optional
                            strokeWidth: 2 // optional
                          },
                          {
                            data:tmpOta,
                            color: (opacity = 1) => `rgba(233, 44, 247, ${opacity})`, // optional
                            strokeWidth: 2 // optional
                          },
                          {
                            data:tmpTrip,
                            color: (opacity = 1) => `rgba(255, 255, 99, ${opacity})`, // optional
                            strokeWidth: 2 // optional
                          },
                          {
                            data:tmpOrder,
                            color: (opacity = 1) => `rgba(255, 173, 91, ${opacity})`, // optional
                            strokeWidth: 2 // optional
                          },
                    ],
                }
                return data 

                break;

            default:
                result=[0,0]
            break;
        }
    }
    

    
    countGrafikChart=(nextProps)=>{
        //props dashboard bisa diambil karena {...this.state} dari Dashboard, lalu panggil disini degn props
        // var res = this.props.allTrend;
        var res = nextProps;
        console.log('ressss', res)
        var result=[];

        var tmpOtd = []
        var tmpOtl = []
        var tmpOta = []
        var tmpOrder = []
        var tmpTrip = []
        var tmpBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"]
        
        for (let i = 0; i < res.length; i++) {
            const element = res[i];
            // console.log('jml_otd', element.jml_otd)
            tmpOtd.push(parseInt(element.jml_otd))
            tmpOtl.push(parseInt(element.jml_otl))
            tmpOta.push(parseInt(element.jml_ota))
            tmpOrder.push(parseInt(element.jum_order))
            tmpTrip.push(parseInt(element.jum_trip))
        }

        this.setState({ 
            data:{ 
              labels: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agus", "Sep", "Okt", "Nov", "Des"],
              datasets: [
                    {
                      data:tmpOtd,
                      color: (opacity = 1) => `rgba(127, 255, 89, ${opacity})`, // optional
                      strokeWidth: 2 // optional
                    },
                    {
                      data:tmpOtl,
                      color: (opacity = 1) => `rgba(44, 101, 247, ${opacity})`, // optional
                      strokeWidth: 2 // optional
                    },
                    {
                      data:tmpOta,
                      color: (opacity = 1) => `rgba(233, 44, 247, ${opacity})`, // optional
                      strokeWidth: 2 // optional
                    },
                    {
                      data:tmpTrip,
                      color: (opacity = 1) => `rgba(255, 255, 99, ${opacity})`, // optional
                      strokeWidth: 2 // optional
                    },
                    {
                      data:tmpOrder,
                      color: (opacity = 1) => `rgba(255, 173, 91, ${opacity})`, // optional
                      strokeWidth: 2 // optional
                    },
              ],
            } 
          })
            // console.log('data', data)
            // return data
    }

    countGrafikChart2=()=>{
        //props dashboard bisa diambil karena {...this.state} dari Dashboard, lalu panggil disini degn props
        var res = this.props.allTrend;
        // var res = nextProps;
        console.log('res2', res)
        var result=[];

        var tmpOtd = []
        var tmpOtl = []
        var tmpOta = []
        var tmpOrder = []
        var tmpTrip = []
        var tmpBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"]
        
        for (let i = 0; i < res.length; i++) {
            const element = res[i];
            // console.log('jml_otd', element.jml_otd)
            tmpOtd.push(parseInt(element.jml_otd))
            tmpOtl.push(parseInt(element.jml_otl))
            tmpOta.push(parseInt(element.jml_ota))
            tmpOrder.push(parseInt(element.jum_order))
            tmpTrip.push(parseInt(element.jum_trip))
        }

            var data={ 
              labels: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agus", "Sep", "Okt", "Nov", "Des"],
              datasets: [
                    {
                      data:tmpOtd,
                      color: (opacity = 1) => `rgba(127, 255, 89, ${opacity})`, // optional
                      strokeWidth: 2 // optional
                    },
                    {
                      data:tmpOtl,
                      color: (opacity = 1) => `rgba(44, 101, 247, ${opacity})`, // optional
                      strokeWidth: 2 // optional
                    },
                    {
                      data:tmpOta,
                      color: (opacity = 1) => `rgba(233, 44, 247, ${opacity})`, // optional
                      strokeWidth: 2 // optional
                    },
                    {
                      data:tmpTrip,
                      color: (opacity = 1) => `rgba(255, 255, 99, ${opacity})`, // optional
                      strokeWidth: 2 // optional
                    },
                    {
                      data:tmpOrder,
                      color: (opacity = 1) => `rgba(255, 173, 91, ${opacity})`, // optional
                      strokeWidth: 2 // optional
                    },
              ],
            } 
            // console.log('data', data)
            return data
    }

    onSelectDate=async (start,end)=>{
        await this.props.setStartEndDate(start,end);
        await this.setState({modalCalenderVisible: false})
        await this.props.onSearchButtonClicked();
    }

    openModalCalendar=(typeTrip)=>{
        this.setState({modalCalenderVisible: true})
    }

    closeModal=()=>{
        this.setState({modalCalenderVisible: false})
    }

    onRefresh= async() => {
      this.setState({refreshing: true})

      var _getOF = await getOF(this.props.url, this.props.profile.id_transporter, this.props.startDate, this.props.endDate);
      var _getOTA = await getOTA(this.props.url, this.props.profile.id_transporter, this.props.startDate, this.props.endDate);
      var _getOTL = await getOTL(this.props.url, this.props.profile.id_transporter, this.props.startDate, this.props.endDate);
      var _getOTD = await getOTD(this.props.url, this.props.profile.id_transporter, this.props.startDate, this.props.endDate);
      var _getTrend = await getTrend(this.props.url, this.props.profile.id_user, moment().year())


      await this.props.refreshContent(_getOF[0], _getOTA[0], _getOTL[0], _getOTD[0], _getTrend)

      Toast.show({
        type: 'info',
        position: 'bottom',
        text1: 'Refreshing',
        text2: 'Refresh data ...',
        visibilityTime: 3000,
        autoHide: true,
        topOffset: 30,
        bottomOffset: 40,
        onShow: () => {},
        onHide: () => {}
      })

        // ToastAndroid.show('Refreshing', ToastAndroid.SHORT);
        this.setState({refreshing: false})

    };

    render(){
        return(
          <>
             {/* adjust content dan scrollview */}
            <View style={styles.flexGridNoPadding}>
                <View style={{flex:1}}>
                    <DateRange openModalCalendar={this.openModalCalendar} {...this.props}/>
                </View>
            </View>
          <Content
              refreshControl={
                <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this.onRefresh.bind(this)}
                />}
          >
            <View style={styles.flexGridPLPR}>
                <View style={{flex:1, flexDirection: 'row'}}>
                    <ProgressCircleIcon titleChart={'Trip Fulfillment (OF)'} colorChart='#64da8a' percentage={this.countPercentage(1)} hit={this.countHitAndTotal(1)[0]} total={this.countHitAndTotal(1)[1]}/>
                </View>
                <View style={{flex:1, flexDirection: 'row'}}>
                    <ProgressCircleIcon titleChart={'On Time Arrival (OTA)'} colorChart='#2989E8' percentage={this.countPercentage(2)} hit={this.countHitAndTotal(2)[0]} total={this.countHitAndTotal(2)[1]}/>
                </View>
            </View>
            <View style={styles.flexGridPLPR}>
                <View style={{flex:1, flexDirection: 'row'}}>
                    <ProgressCircleIcon titleChart={'On Time Loading (OTL)'} colorChart='#4ecdc4' percentage={this.countPercentage(3)} hit={this.countHitAndTotal(3)[0]} total={this.countHitAndTotal(3)[1]}/>
                </View>
                <View style={{flex:1, flexDirection: 'row'}}>
                    <ProgressCircleIcon titleChart={'On Time Delivery (OTD)'} colorChart='#5c6b73' percentage={this.countPercentage(4)} hit={this.countHitAndTotal(4)[0]} total={this.countHitAndTotal(4)[1]}/>
                </View>
            </View>

            <View style={{backgroundColor: 'white', 
                          // borderRadius: 10,
                          elevation: 1, 
                          marginLeft: 10, 
                          marginRight: 10}}>
              <Text style={{marginTop:5, color:"#7d8d8c", fontSize:14, fontWeight:'bold', textAlign: 'center'}}>Trend Pengiriman Tahun {moment().year()}</Text>
              <View style = {{borderWidth: 0.5,
                              borderColor:'#ddd',
                              marginTop:5,
                              marginBottom:5}} />
              <View style={{flex:1, flexDirection: 'row', margin: 10, marginTop: 5, marginBottom: 5}}>
                  <TrendLabel titleTrend={'OTD Terbanyak :'} maxTrend={this.countTrendTerbanyak(1)[0]} bulanMaxTrend={this.countTrendTerbanyak(1)[1]}></TrendLabel>
                  <TrendLabel titleTrend={'OTA Terbanyak :'} maxTrend={this.countTrendTerbanyak(2)[0]} bulanMaxTrend={this.countTrendTerbanyak(2)[1]}></TrendLabel>
                  <TrendLabel titleTrend={'OTL Terbanyak :'} maxTrend={this.countTrendTerbanyak(3)[0]} bulanMaxTrend={this.countTrendTerbanyak(3)[1]}></TrendLabel>
              </View>
            </View>
            <LineChartIcon data={this.state.data}></LineChartIcon>
            {/* <Text onPress={() => this.countGrafikChart()}>TEST PRINT</Text> */}
            {/* <LineChartIcon></LineChartIcon> */}

            {/* <LineChart
                data={this.state.data}
                width={Dimensions.get("window").width} // from react-native
                height={220}
                // yAxisLabel="$"
                // yAxisSuffix="k"
                yAxisInterval={1} // optional, defaults to 1
                chartConfig={{
                        backgroundColor: "#e26a00",
                        backgroundGradientFrom: "#008c45",
                        backgroundGradientTo: "#ffa726",
                        decimalPlaces: 0, // optional, defaults to 2dp, 0 untuk Int
                        color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                        labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                        style: {
                          borderRadius: 16
                        },
                        propsForDots: {
                          r: "3",
                          strokeWidth: "0.5",
                          stroke: "#36c9a9"
                        }
                      }}
                        bezier
                        style={{
                          marginVertical: 8,
                          borderRadius: 16,
                          margin: 10,
                        }}
                bezier
                onDataPointClick={(data)=> {console.log(data, data.value)}}
              /> */}

            <View style={{flex:1, flexDirection: 'row', margin: 10, marginTop: 5}}>
              <ColorIcon titleColor={'On Time Delivery'} bgColor={'#7fff59'}/>
              <ColorIcon titleColor={'On Time Loading'} bgColor={'#2c65f7'}/>
              <ColorIcon titleColor={'On Time Arrival'} bgColor={'#e92cf7'}/>
            </View>
            <View style={{flex:1, flexDirection: 'row', margin: 10, marginTop: 5}}>
              <ColorIcon titleColor={'Jumlah Trip'} bgColor={'#ffff63'}/>
              <ColorIcon titleColor={'Jumlah Order'} bgColor={'#ffad5b'}/>
              <ColorIcon/>
            </View>

            <ModalCalendar {...this.state} {...this.props} onSelectDate={this.onSelectDate} closeModal={this.closeModal}/>
          </Content>
          <Toast ref={(ref) => Toast.setRef(ref)} />
          </>
        );
    }
}

export default ReportPage;