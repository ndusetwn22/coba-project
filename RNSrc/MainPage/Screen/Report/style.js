import { StyleSheet, PixelRatio } from 'react-native';


// sebelumnya elevation: 1
const styles = StyleSheet.create({
    flexGrid:{
        flexDirection: 'row',
        padding:10
    },
    flexGridNoPadding:{
        flexDirection: 'row',
    },
    flexGridPLPR:{
        flexDirection: 'row',
        // paddingRight:10,
        // paddingLeft:10
    },
    lineStyle:{
        borderWidth: 0.5,
        borderColor:'#008c45'
    },
    countStyle:{
        width:'100%',
        textAlign:'center',
        color: '#fafafa', 
        fontSize: 16,
        fontWeight: 'bold'
    },
    headerViewStyleKiri2:{
        flex: 1, 
        flexDirection: "row", 
        backgroundColor: '#fafafa', 
        marginLeft:10,
        // borderRadius:10,
        height: 50, 
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.48,
        shadowRadius: 11.95,
        elevation: 1, 
        zIndex: 0
    },
    headerViewStyle:{
        flex: 1, 
        flexDirection: "row", 
        backgroundColor: '#fafafa', 
        // borderRadius:10,
        marginRight:10,
        height: 50, 
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.48,
        shadowRadius: 11.95,
        elevation: 1, 
        zIndex: 0
    },
    headerViewStyleRight:{
        backgroundColor: '#fff0', 
        marginTop:10,
        marginRight:10,
        // borderRadius:10,
        height: 50, 
        zIndex: 0
    },
    mtdViewStyleKanan:{
        flex: 1,
        padding:5,
        backgroundColor: '#fafafa', 
        margin:10, 
        // borderRadius:10, 
        height: 150,
        width: '70%', 
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 9,
        },
        shadowOpacity: 0.48,
        shadowRadius: 11.95,
        elevation: 1, 
        zIndex: 0
    },
});

export default styles;
