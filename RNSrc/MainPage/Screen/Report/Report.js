import React,{Component} from 'react'
import {BackHandler,TouchableOpacity, Text,View} from "react-native";
import { useNavigation  } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
import CustomNavbar from '../../Header/Navbar/Navbar'

import Loading,{getProfile, setUrl, getUrl, decrypt} from '../../../../APIProp/ApiConstants'
import ReportPage from './ReportPage'
import moment from 'moment';
import {getOF, getOTA, getOTL, getOTD, getTrend} from './ReportAxios'
import messaging from '@react-native-firebase/messaging';
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import PushNotification from 'react-native-push-notification';
import NetInfo from "@react-native-community/netinfo";
import LostConnection from '../../Component/LostConnection';


class Report extends Component {
    constructor(props) {
        super(props);
        this.state = {
          paramLogin:{
            username:'',
            password:'',
          },
          OF:{
            of_hit: 0,
            of_no_hit: 0,
          },
          OTA: {
            ota_hit: 0,
            ota_no_hit: 0,
          },
          OTL:{
            otl_hit: 0,
            otl_no_hit: 0,
          },
          OTD:{
            otd_hit: 0,
            otd_no_hit: 0,
          },
          isLoading:false,
          url:null,
          profile: null,
          profileStr: null,
          isConnected: null,
          isConnectedChange: null,
          startDate: moment(),
          endDate: moment(),
          displayedDate: moment(),
          allTrend: [],
          data : {
            labels: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agus", "Sep", "Okt", "Nov", "Des"],
            datasets: [
              {
                data: [0,0,0,0,0,100,0,0,0,0,0,0],
                color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})`, // optional
                strokeWidth: 2 // optional
              },
              {
                data: [40, 25, 98, 10, 69, 50,0,0,0,0,0,0],
                color: (opacity = 1) => `rgba(65, 134, 244, ${opacity})`, // optional
                strokeWidth: 2 // optional
              },
              {
                data: [10, 35, 18, 20, 79, 30,0,0,0,0,0,0],
                color: (opacity = 1) => `rgba(20, 134, 244, ${opacity})`, // optional
                strokeWidth: 2 // optional
              },
              {
                data: [20, 15, 58, 80, 34, 20,0,0,0,0,0,0],
                color: (opacity = 1) => `rgba(65, 40, 244, ${opacity})`, // optional
                strokeWidth: 2 // optional
              },
              {
                data: [24, 32, 40, 25, 96, 10,0,0,0,0,0,0],
                color: (opacity = 1) => `rgba(65, 134, 50, ${opacity})`, // optional
                strokeWidth: 2 // optional
              },
            ],
            // legend: ["Rainy Days", "Sunny Days", "Snowy Days"] // optional
          }
        };
      }
    
      componentDidMount= async()=>{
        this.setState({isLoading:true});
        await setUrl();
        await getUrl().then(async(result)=>{
          this.setState({url:result})
        });
        await this.funcGetProfile();
        await this.pushNotif();
        await this.cekConnection();
        console.log('year',moment().year())

        var _getOF = await getOF(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate);
        var _getOTA = await getOTA(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate);
        var _getOTL = await getOTL(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate);
        var _getOTD = await getOTD(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate);
        var _getTrend = await getTrend(this.state.url, this.state.profile.id_user, moment().year())

        await this.setState({
          allTrend: _getTrend,
          OF: _getOF[0],
          OTA: _getOTA[0],
          OTL: _getOTL[0],
          OTD: _getOTD[0],
          isLoading:false,
        });


      }

      cekConnection = async()=>{
        // Cek Connection
        NetInfo.addEventListener(state => {
          if (this.state.isConnected == null) {
            this.setState({isConnected: state.isConnected})
          }
          if (state.isConnected == false) {
            this.setState({isConnected: state.isConnected, isConnectedChange: state.isConnected})
          }
          if (state.isConnected == true) {
            this.setState({isConnectedChange: state.isConnected})
          }      
          console.log("Is connected?", state.isConnected);
        });
      }

      restartConnection = async() =>{
        console.log('Restart Connection', this.state.isConnected)
        if (this.state.isConnected == false && this.state.isConnectedChange == true) {
          console.log('HARUSNYA INI OFF', this.state.isConnected)
          this.props.navigation.replace('MainRouter', {screen: 'Report'})
        }
      }

      funcGetProfile=async()=>{
        await getProfile().then(result=>{
          if(result!=null){
            this.setState({profileStr:result})
          }
        })
        if(this.state.profileStr==null){
          this.props.navigation.replace('Login')
        }
        var tmpStr = await decrypt(this.state.profileStr);
        this.setState({profile:JSON.parse(tmpStr)})
      }

      setStartEndDate=async(s,e)=>{
        await this.setState({
          startDate:moment(s, 'YYYY-MM-DD'),
          endDate:moment(e, 'YYYY-MM-DD')
        })
      }

      onSearchButtonClicked=async()=>{
        await this.setState({isLoading:true});
        
        var startDate =this.state.startDate;
        var endDate;
  
        if(this.state.endDate==null){
          endDate = this.state.startDate;
        }else{
          endDate = this.state.endDate;
        }

        var _getOF = await getOF(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate);
        var _getOTA = await getOTA(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate);
        var _getOTL = await getOTL(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate);
        var _getOTD = await getOTD(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate);

        //set state disini
        await this.setState({
          OF: _getOF[0],
          OTA: _getOTA[0],
          OTL: _getOTL[0],
          OTD: _getOTD[0],
          isLoading:false,
        });
      }

      refreshContent = async(_getOF, _getOTA, _getOTL, _getOTD, _getTrend) => {
        await this.setState({isLoading: true})
        await this.setState({isLoading:false, OF: _getOF, OTA: _getOTA, OTL: _getOTL, OTD: _getOTD, allTrend: _getTrend});
      }

      pushNotif = async () => {
        // Must be outside of any component LifeCycle (such as `componentDidMount`).
        PushNotification.configure({
          // (optional) Called when Token is generated (iOS and Android)
          onRegister: function (token) {
            console.log("TOKEN:", token);
          },
        
          // (required) Called when a remote is received or opened, or local notification is opened
          onNotification: async (notification) => {
            console.log("NOTIFICATION:", notification);
            //Utk dpt Notif
            // process the notification
            //mau apa disini
            //disini setelah ada userInteraction/di klik
            if (notification.userInteraction) {
              console.log('DO SOMETHING NOTIFICATION IN REPORT') 
              // this.props.navigation.navigate('ListTrip',{
              //   screen: 'ListTrip',
              //   id_trip: notification.data.id_trip,
              //   id: notification.data.id
              // })
              this.props.navigation.navigate('Dashboard', {screen: 'Dashboard'})
            }
        
            // (required) Called when a remote is received or opened, or local notification is opened
            notification.finish(PushNotificationIOS.FetchResult.NoData);
          },
  
          
          // // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
          // onAction: function (notification) {
          //   console.log("ACTION:", notification.action);
          //   console.log("NOTIFICATION ACTION:", notification);
        
          //   // process the action
  
          //   console.log('waiwawa')
          // },
        
          // // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
          // onRegistrationError: function(err) {
          //   console.error(err.message, err);
          // },
        
          // // IOS ONLY (optional): default: all - Permissions to register.
          // permissions: {
          //   alert: true,
          //   badge: true,
          //   sound: true,
          // },
        
          // Should the initial notification be popped automatically
          // default: true
          popInitialNotification: true,
        
          /**
           * (optional) default: true
           * - Specified if permissions (ios) and token (android and ios) will requested or not,
           * - if not, you must call PushNotificationsHandler.requestPermissions() later
           * - if you are not using remote notification or do not have Firebase installed, use this:
           *     requestPermissions: Platform.OS === 'ios'
           */
          requestPermissions: true,
        });
      }

      render(){
        return(
          <>
            <CustomNavbar title={'Laporan'}/>
            {/* <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text>Report</Text>
            </View> */}
            {
            this.state.isConnected == false?
                <LostConnection restartConnection={this.restartConnection}/> 
            : 
              
              <ReportPage 
                {...this.state} 
                setStartEndDate={this.setStartEndDate}
                // onTripSelected={this.onTripSelected}
                onSearchButtonClicked={this.onSearchButtonClicked}
                refreshContent={this.refreshContent}
              />
            }
            
            {this.state.isLoading ? <Loading/> : false}
          </>
        );
      }
}

ReportFunc= (props) => {
  const navigation = useNavigation();
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      BackHandler.addEventListener("hardwareBackPress", ()=>props.backAction('Report', navigation));
    });

    return unsubscribe;
  }, [navigation]);

  return <Report navigation={navigation}/>;
}

export default ReportFunc;