import React,{Component} from 'react'
import {Text, View} from 'native-base'
import styles from './style'
import moment from 'moment'
import { Dimensions} from "react-native";

import {
    LineChart,
  } from "react-native-chart-kit";
  


const screenWidth = Dimensions.get("window").width;

class LineChartIcon extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : {
                labels: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agus", "Sep", "Okt", "Nov", "Des"],
                datasets: [
                  {
                    data: [0,0,0,0,0,100,0,0,0,0,0,0],
                    color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})`, // optional
                    strokeWidth: 2 // optional
                  },
                  {
                    data: [40, 25, 98, 10, 69, 50,0,0,0,0,0,0],
                    color: (opacity = 1) => `rgba(65, 134, 244, ${opacity})`, // optional
                    strokeWidth: 2 // optional
                  },
                  {
                    data: [10, 35, 18, 20, 79, 30,0,0,0,0,0,0],
                    color: (opacity = 1) => `rgba(20, 134, 244, ${opacity})`, // optional
                    strokeWidth: 2 // optional
                  },
                  {
                    data: [20, 15, 58, 80, 34, 20,0,0,0,0,0,0],
                    color: (opacity = 1) => `rgba(65, 40, 244, ${opacity})`, // optional
                    strokeWidth: 2 // optional
                  },
                  {
                    data: [24, 32, 40, 25, 96, 10,0,0,0,0,0,0],
                    color: (opacity = 1) => `rgba(65, 134, 50, ${opacity})`, // optional
                    strokeWidth: 2 // optional
                  },
                ],
                // legend: ["Rainy Days", "Sunny Days", "Snowy Days"] // optional
              }

        };
    }

    componentDidMount= async()=>{

    }

    render(){
        return(
          <View>
            <LineChart
                data={this.props.data}
                width={Dimensions.get("window").width} // from react-native
                height={220}
                // yAxisLabel="$"
                // yAxisSuffix=" order"
                yAxisInterval={1} // optional, defaults to 1
                chartConfig={{
                        backgroundColor: "white",
                        backgroundGradientFrom: "white",
                        backgroundGradientTo: "white",
                        decimalPlaces: 0, // optional, defaults to 2dp
                        color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                        labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                        style: {
                          borderRadius: 16,
                        },
                        propsForDots: {
                          r: "3",
                          strokeWidth: "0.5",
                          stroke: "#36c9a9"
                        }
                      }}
                        bezier
                        style={{
                          marginVertical: 8,
                          // borderRadius: 16,
                          margin: 0,
                          padding: 0
                        }}
                bezier
                onDataPointClick={(data)=> {console.log(data, data.value)}}
              />

              {/* <View style={{flex:1, flexDirection: 'row', margin: 10, marginTop: 5}}>
                <View style={{flex:1, flexDirection: 'row', justifyContent: 'center'}}>
                  <Text style={styles.textPoint2}>x = bulan</Text>
                </View>
                <View style={{flex:1, flexDirection: 'row', justifyContent:'center'}}>
                  <Text style={styles.textPoint2}>y = jumlah order</Text>
                </View>
              </View> */}
          </View>
        );
    }
}

export default LineChartIcon;