import { StyleSheet, Platform, Dimensions } from 'react-native';

const styles = StyleSheet.create({
  
    
  lineStyle:{
    borderWidth: 0.5,
    borderColor:'#7d8d8c'
  },

  search:{
    padding: 10,
    borderColor: '#CCC',
    borderWidth: 2,
    borderRadius: 10,
    width: '40%',
    height: 40,
    marginLeft: 10,
    marginTop: 5
  },

  flexGrid:{
    flex: 1,
    flexDirection: 'row'
  },

  dateSetelected:{
    backgroundColor: '#008c45'
  },

  dateBackdrop:{
    elevation: 1,
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    zIndex: 20000000000,
    margin: -10
    // marginLeft: -121
  },

  dateContainer:{
    zIndex: 300000000000
  },
  headerViewStyleKanan:{
    // width: '60%',
    // flex: 1,
    flexDirection: 'row',
    margin:10, 
    borderRadius:10,
    height: 50,
  },
  headerViewStyleGradientDate:{
    borderRadius: 10,
    height: 50,
    backgroundColor: 'white',
    width: Dimensions.get('window').width - 20,
    borderWidth: 2,
    borderColor: '#008c45',
    justifyContent: 'center',
    // marginRight: 10,
  },

  circleViewStyle:{
    flex: 1,
    backgroundColor: '#fafafa', 
    margin: 10, 
    borderRadius:10, 
    height: 110,
    width: '50%', 
    shadowColor: "#000",
    alignItems:'center', 
    alignSelf: 'center', 
    justifyContent: 'center',

    shadowOffset: {
    width: 0,
    height: 9,
  },
    shadowOpacity: 0.48,
    shadowRadius: 11.95,

    elevation: 1, 
    zIndex: 0
  },

  textStyleCircle:{
    textAlign: 'center', 
    margin: 10, 
    marginBottom: 20, 
    marginTop: 5, 
    fontSize: 12, 
    color: '#7d8d8c'
  },

  textStyleCircle2:{
    textAlign: 'center', 
    fontSize: 10, 
    color: '#7d8d8c'
  },

  maxStyle:{
    textAlign: 'center', 
    fontSize: 8, 
    color: '#7d8d8c'
  },

  pointStyle:{
    width: 10, 
    height: 10, 
    borderRadius: 10/2, 
    marginLeft: 5, 
    marginRight: 5
  },

  textPoint:{
    textAlign: 'left', 
    fontSize: 8, 
    color: '#7d8d8c'
  }, 
  textPoint2:{
    textAlign: 'left', 
    fontSize: 12, 
    color: '#7d8d8c'
  }

});

export default styles;