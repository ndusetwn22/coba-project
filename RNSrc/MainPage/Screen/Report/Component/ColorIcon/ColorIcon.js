import React,{Component} from 'react'
import {Text, View} from 'native-base'
import styles from './style'
import moment from 'moment'

class ColorIcon extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount= async()=>{

    }

    render(){
        return(
            <View style={{flex:1, flexDirection: 'row'}}>
                <View style={[{backgroundColor: this.props.bgColor}, styles.pointStyle]}></View>
                <Text style={styles.textPoint}>{this.props.titleColor}</Text>
            </View>
        );
    }
}

export default ColorIcon;