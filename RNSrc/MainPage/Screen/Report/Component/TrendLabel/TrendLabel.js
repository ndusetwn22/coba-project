import React,{Component} from 'react'
import {Text, View} from 'native-base'
import styles from './style'
import moment from 'moment'

class TrendLabel extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount= async()=>{

    }

    render(){
        return(
            <View style={{flex:1, flexDirection: 'column'}}>
                <Text style={[styles.maxStyle,{fontWeight: 'bold'}]}>{this.props.titleTrend} {this.props.maxTrend}</Text>
                <Text style={styles.maxStyle}>{this.props.bulanMaxTrend} {moment().year()}</Text>
            </View>
        );
    }
}

export default TrendLabel;