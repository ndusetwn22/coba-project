import Axios from 'axios';
import {encrypt} from '../../../../APIProp/ApiConstants';


export const getOF = async (url,transporter_id, _startDate,_endDate) =>{
  var startDate = _startDate
  var endDate = _endDate
  startDate = startDate.format('YYYY-MM-DD')
  endDate = endDate.format('YYYY-MM-DD')
  startDate += ' 00:00:00';
  endDate += ' 24:00:00';
  // let dataQry = `
  //       select 
  //       coalesce(count(distinct b.id_trip),0) of_hit,
  //       coalesce(sum(case when ( b.reason_cancel_order_role='A' or  b.status_order = 'cancel' ) then 1 else 0 end ),0) 
  //       of_no_hit 
  //       from brosky_laporan_bulanan b 
  //       where b.jadwal_penjemputan between '`+startDate+`' and ('`+endDate+`'::date + interval '1 day')  and b.transporter_id = '`+transporter_id+`' 
  //       and b.status_trip = 'close'
  // `
  let dataQry = `
        select * from mobiletra_getreportof(`+transporter_id+`, '`+startDate+`', '`+endDate+`')
  `
  let x = await encrypt(dataQry);
  return await Axios.post(url.select,{
    query: x
  }).then(value=>{
    let x = value.data.data;
    console.log('getOF : ', x)
    if(x==""){
      return [];
    }
    return x;
  }).catch(err=>{
    return [];
  });
}


export const getOTA = async (url,transporter_id, _startDate,_endDate) =>{
    var startDate = _startDate
    var endDate = _endDate
    startDate = startDate.format('YYYY-MM-DD')
    endDate = endDate.format('YYYY-MM-DD')
    startDate += ' 00:00:00';
    endDate += ' 24:00:00';
    // let dataQry = `
    //     select coalesce(sum (case when (x.jemput - interval '1 hours')  >= x.arrival then 1 else 0 end),0) ota_hit,
    //     coalesce(sum (case when (x.jemput - interval '1 hours') < x.arrival then 1 else 0 end),0) ota_no_hit
    //     from (select min(b.arrival_date) arrival, b.id_trip, min(b.jadwal_penjemputan) jemput
    //     from brosky_laporan_bulanan b 
    //     where b.jadwal_penjemputan between '`+startDate+`' and ('`+endDate+`'::date + interval '1 day')
    //     and b.transporter_id = '`+transporter_id+`' and b.status_trip = 'close' group by b.id_trip ) x
    // `

    let dataQry = `
        select * from mobiletra_getreportota(`+transporter_id+`, '`+startDate+`', '`+endDate+`')
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('getOTA : ', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
    });
  }

  export const getOTL = async (url,transporter_id, _startDate,_endDate) =>{
    var startDate = _startDate
    var endDate = _endDate
    startDate = startDate.format('YYYY-MM-DD')
    endDate = endDate.format('YYYY-MM-DD')
    startDate += ' 00:00:00';
    endDate += ' 24:00:00';
    // let dataQry = `
    //         select coalesce (sum (case when bb.jadwal_penjemputan + interval '30 minutes' >= bb.startloaded_date then 1 else 0 end ) ,0) otl_hit,
    //         coalesce (sum (case when bb.jadwal_penjemputan + interval '30 minutes' < bb.startloaded_date then 1 else 0 end ) ,0) otl_no_hit 
    //         from brosky_laporan_bulanan bb 
    //         where 
    //         bb.id_trip in (
    //         select (case when x.jemput >= x.arrival then x.id_trip else '' end) hit
    //         from (select min(b.arrival_date) arrival, b.id_trip, min(b.jadwal_penjemputan - interval '1 hours') jemput
    //         from brosky_laporan_bulanan b 
    //         where b.jadwal_penjemputan between '`+startDate+`' and ('`+endDate+`'::date + interval '1 day')
    //         and b.transporter_id = '`+transporter_id+`' and b.status_trip = 'close'  group by b.id_trip) x ) 
    //         and bb.jadwal_penjemputan between '`+startDate+`' and ('`+endDate+`'::date + interval '1 day')  
    //         and bb.transporter_id = '`+transporter_id+`' and bb.status_trip = 'close'   
    // `

    let dataQry = `
        select * from mobiletra_getreportotl(`+transporter_id+`, '`+startDate+`', '`+endDate+`')
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('getOTL : ', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
    });
  }

  export const getOTD = async (url,transporter_id, _startDate,_endDate) =>{
    var startDate = _startDate
    var endDate = _endDate
    startDate = startDate.format('YYYY-MM-DD')
    endDate = endDate.format('YYYY-MM-DD')
    startDate += ' 00:00:00';
    endDate += ' 24:00:00';
    // let dataQry = `
    //     select (select (count (distinct b.id_order))
    //     from brosky_laporan_bulanan b
    //     where b.jadwal_penjemputan between '`+startDate+`' and ('`+endDate+`'::date + interval '1 day')  
    //     and b.transporter_id = '`+transporter_id+`' and b.status_trip = 'close' and b.new_estimasi_tiba + interval '1 hours' >= b.dropoff_date) as otd_hit,
    //     (select (count (distinct b.id_order))
    //     from brosky_laporan_bulanan b
    //     where b.jadwal_penjemputan between '`+startDate+`' and ('`+endDate+`'::date + interval '1 day')  
    //     and b.transporter_id = '`+transporter_id+`' and b.status_trip = 'close' and b.new_estimasi_tiba + interval '1 hours' < b.dropoff_date) otd_no_hit
    // `

    let dataQry = `
        select * from mobiletra_getreportotd(`+transporter_id+`, '`+startDate+`', '`+endDate+`')
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('getOTD : ', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
    });
  }


  export const getTrend = async (url,user_id, year) =>{
    // var year = moment().year();
    // let dataQry = `
    //         select coalesce(jml_OTD,0) as jml_otd, 
    //         coalesce(jml_OTL,0) as jml_otl, 
    //         coalesce(jml_OTA,0) as jml_ota, 
    //         coalesce(jum_order,0) jum_order, 
    //         coalesce(jum_trip,0) jum_trip, 
    //         bulan from		 (select (extract(month from a.jadwal_penjemputan)) mon, 
    //         (sum(stat_OTD)) jml_OTD, 
    //         (sum(stat_OTL)) jml_OTL, 
    //         (sum(stat_OTA)) jml_OTA  ,		 
    //         count(distinct id_order) jum_order, 
    //         count(distinct id_trip) jum_trip		
    //         from (select b.jadwal_penjemputan, id_order, id_trip,		 
    //         (case when b.jadwal_penjemputan - interval '1 hours' > b.arrival_date then 1 else 0 end) as stat_OTA, 		  
    //         (case when  b.new_estimasi_tiba > b.dropoff_date then 1 else 0 end) as stat_OTD, 		   
    //         (case when b.jadwal_penjemputan > b.startloaded_date then 1 else 0 end) as stat_OTL		
    //         from brosky_laporan_bulanan b left join mt_master_transporter t on t.nama_transporter = b.nama_transporter		 
    //         left join mt_master_user u on u.transporter_id = t.id		 
    //         where extract(year from b.jadwal_penjemputan) = '`+year+`' and u.id = '`+user_id+`'		 ) a 
    //         group by mon) tbl		 
    //         right join ( select * from (values (1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12)) x (bulan) ) tbl2  on tbl2.bulan  = tbl.mon
    // `
    let dataQry = `
        select * from mobiletra_getreporttrend(`+user_id+`, `+year+`)
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('getTrend : ', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
    });
  }