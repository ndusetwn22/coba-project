import React,{Component} from 'react'
import { AppState,Platform,Dimensions, Image, FlatList , TouchableOpacity,ScrollView, ImageBackground, BackHandler, Alert, PixelRatio, StyleSheet, TextInput, Picker } from "react-native";
import { useNavigation  } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
import CustomNavbar from '../../../Header/Navbar/Navbar'
import Loading,{getProfile, encrypt, setUrl, getUrl, decrypt} from '../../../../../APIProp/ApiConstants'
import Axios from 'axios';
import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Left,
    Right,
    Body,
    Text,
    Icon,
    Card,
    CardItem,
    Grid,
    Row,
    View,
    Col, 
    List,
    ListItem,
    Thumbnail,
    Form,
    Label,
    Input,
    Item,
    DatePicker
} from "native-base";
import styles from "./style";
import moment from 'moment'

//image picker
import ImagePicker from 'react-native-image-picker';

//Google Places
import RNGooglePlaces from 'react-native-google-places';

//firebase
import { utils } from '@react-native-firebase/app';
import storage from '@react-native-firebase/storage';

//function
import {insertDriver} from './DriverAddAxios'


class DriverAdd extends Component {
    constructor(props) {
            super(props);
            this.state = {
            paramLogin:{
                username:'',
                password:'',
            },
            isLoading:false,
            url:null,
            profile: null,
            fotoFileName: 'Pilih foto pengemudi',
            fotoSource: null,
            ktpFileName: 'Pilih foto KTP',
            ktpSource: null,
            simFileName: 'Pilih foto SIM',
            simSource: null,
            namaTransporter: '',
            role: 'DRIVER',
            namaPengemudi: '',
            noHp: '',
            email: '',
            validatedemail: false,
            validatePassword: false,
            validateKonfirmasiPassword: false,
            setSelectedValue: 'A',
            password: '',
            konfirmasiPassword: '',
            refId: '',
            noKtp: '',
            noSim: '',
            
            
        };
      }
    
      componentDidMount= async()=>{
        await this.setState({isLoading:true});
        await setUrl();
        await getUrl().then(async(result)=>{
          this.setState({url:result})
        });

        await this.funcGetProfile();
        // var _listKendaraan = await getListKendaraan(this.state.url, this.state.profile.id_transporter)
        // var _allPool = await getPool(this.state.url, this.state.profile.id_transporter)
        var _getTransporter = await this.getTransporter(this.state.url, this.state.profile.id_transporter)
        await this.setState({isLoading:false});
      }

      funcGetProfile=async()=>{
        await getProfile().then(result=>{
          if(result!=null){
            this.setState({profileStr:result})
          }
        })
        if(this.state.profileStr==null){
          this.props.navigation.replace('Login')
        }
        var tmpStr = await decrypt(this.state.profileStr);
        this.setState({profile:JSON.parse(tmpStr)})
        console.log('profile', JSON.parse(tmpStr))
      }

      getTransporter = async(url, trid) => {
        let query = `
        select * from mt_master_transporter where id = `+trid+`
      `
            await encrypt(query).then(result => {
              query = result;
            });
            Axios.post(url.select, {
              query: query 
            })
              .then(result => {
                
                let res = result.data.data[0]
  
                console.log('get nama transporter : ', res.nama_transporter)
                this.setState({
                  namaTransporter: res.nama_transporter
                })
  
              })
              .catch(error => {
                console.log(error);
              });
     }


     validateemail=(text)=>{
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
        console.log(reg);
        if(reg.test(text) === false)
        {
          // console.log("Email is Not Correct");
    
          this.setState({email:text, validatedemail: false})
          return false;
          }
        else {
          console.log('email correct');
          this.setState({email:text, validatedemail: true})
        }
      }

      validatePassword=(text)=>{
        let reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/ ;
        console.log(reg);
        if(reg.test(text) === false)
        {
          // console.log("password is Not Correct");
    
          this.setState({password: text, validatePassword: false})
          return false;
          }
        else {
          console.log('password correct');
          this.setState({password: text, validatePassword: true})
        }
      }

      validateKonfirmasiPassword=(text)=>{
        let reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/ ;
        console.log(reg);
        if(reg.test(text) === false)
        {
          // console.log("password is Not Correct");
    
          this.setState({konfirmasiPassword: text, validateKonfirmasiPassword: false})
          return false;
          }
        else {
          console.log('password correct');
          this.setState({konfirmasiPassword: text, validateKonfirmasiPassword: true})
        }
      }



      selectKtpTapped = () => {
        const options = {
          quality: 1.0,
          maxWidth: 500,
          maxHeight: 500,
          storageOptions: {
            skipBackup: true,
          },
        };
    
        ImagePicker.showImagePicker(options, response => {
          console.log('Response = ', response);
    
          if (response.didCancel) {
            console.log('User cancelled photo picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            let source = {uri: response.uri};
    
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
            
            console.log('options : ', options)
            console.log('uri :', response.uri)
            this.setState({
              // avatarSource: response.uri,
              ktpSource: source,
              ktpFileName: 'KTP'
            });
    
          }
        });
      }
    
      selectSimTapped = () => {
        const options = {
          quality: 1.0,
          maxWidth: 500,
          maxHeight: 500,
          storageOptions: {
            skipBackup: true,
          },
        };
    
        ImagePicker.showImagePicker(options, response => {
          console.log('Response = ', response);
    
          if (response.didCancel) {
            console.log('User cancelled photo picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            let source = {uri: response.uri};
    
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
            
            console.log('options : ', options)
            console.log('uri :', response.uri)
            this.setState({
              // avatarSource: response.uri,
              simSource: source,
              simFileName: 'SIM'
            });
    
          }
        });
      }
    
    
      selectFotoTapped = () => {
        const options = {
          quality: 1.0,
          maxWidth: 500,
          maxHeight: 500,
          storageOptions: {
            skipBackup: true,
          },
        };
    
        ImagePicker.showImagePicker(options, response => {
          console.log('Response = ', response);
    
          if (response.didCancel) {
            console.log('User cancelled photo picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            let source = {uri: response.uri};
    
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
            
            console.log('options : ', options)
            console.log('uri :', response.uri)
            this.setState({
              // avatarSource: response.uri,
              fotoSource: source,
              fotoFileName: 'Driver'
            });
    
          }
        });
      }


      tambahPengemudi=async ()=>{
        // this.setState({buttondisabled: true});
        let title = 'Invalid Input';

        if (this.state.role == 'DRIVER') {
          
            if(this.state.namaPengemudi === '' ) Alert.alert(title, 'Nama Pengemudi tidak boleh kosong!');
            else if(this.state.noHp === '') Alert.alert(title, 'No HP tidak boleh kosong!');
            else if(this.state.validatedemail === false) Alert.alert(title, 'Format Email tidak sesuai!');
            else if(this.state.setSelectedValue === '') Alert.alert(title, 'Status tidak boleh kosong!');
            // else if(this.state.password === '') Alert.alert(title, 'Password tidak boleh kosong!');
            // else if(this.state.konfirmasiPassword === '') Alert.alert(title, 'Konfirmasi Password tidak boleh kosong!');
            else if(this.state.validatePassword === false) Alert.alert(title, 'Password harus memiliki minimal 6 karakter, 1 digit angka, 1 huruf besar, 1 huruf kecil');
            else if(this.state.password !== this.state.konfirmasiPassword) Alert.alert(title, 'Konfirmasi Password tidak sesuai, harap isi kembali!');
            else if(this.state.noKtp === '') Alert.alert(title, 'No KTP tidak boleh kosong!');
            else if(this.state.noKtp.length <= 15) Alert.alert(title, 'Format KTP harus 16 digit!'); //dari index, total KTP = 16
            else if(this.state.noSim === '') Alert.alert(title, 'No SIM tidak boleh kosong!');
            else if(this.state.noSim.length <= 12) Alert.alert(title, 'Format SIM Harus 13 digit!'); //dari index total SIM = 13
            else if(this.state.fotoSource === null) Alert.alert(title, 'Foto pengemudi tidak boleh kosong!');
            else if(this.state.ktpSource === null) Alert.alert(title, 'Foto KTP tidak boleh kosong!');
            else if(this.state.simSource === null) Alert.alert(title, 'Foto SIM tidak boleh kosong!');
            else{
                
                this.setState({isLoading:true});
                let trid = this.state.profile.id_transporter
                let uid = this.state.profile.id_user

                let _insertDriver = await insertDriver(this.state.url, this.state.namaPengemudi, this.state.role, this.state.noHp, this.state.email, this.state.password, this.state.setSelectedValue, this.state.profile.id_transporter, this.state.profile.id_user, this.state.fotoFileName, this.state.ktpFileName, this.state.simFileName, this.state.refId, this.state.noKtp, this.state.noSim, this.state.fotoSource, this.state.ktpSource, this.state.simSource)
                this.setState({isLoading:false}); 
                this.props.funcSweetAlert('status',_insertDriver[0], _insertDriver[1])
            }
        } 
        else{

            if(this.state.namaPengemudi === '' ) Alert.alert(title, 'Nama Pengemudi tidak boleh kosong!');
            else if(this.state.noHp === '') Alert.alert(title, 'No HP tidak boleh kosong!');
            else if(this.state.validatedemail === false) Alert.alert(title, 'Format Email tidak sesuai!');
            else if(this.state.setSelectedValue === '') Alert.alert(title, 'Status tidak boleh kosong!');
            // else if(this.state.password === '') Alert.alert(title, 'Password tidak boleh kosong!');
            // else if(this.state.konfirmasiPassword === '') Alert.alert(title, 'Konfirmasi Password tidak boleh kosong!');
            else if(this.state.validatePassword === false) Alert.alert(title, 'Password harus memiliki minimal 6 karakter, 1 digit angka, 1 huruf besar, 1 huruf kecil');
            else if(this.state.password !== this.state.konfirmasiPassword) Alert.alert(title, 'Konfirmasi Password tidak sesuai, harap isi kembali!');
            else if(this.state.noKtp === '') Alert.alert(title, 'No KTP tidak boleh kosong!');
            else if(this.state.noKtp.length <= 15) Alert.alert(title, 'Format KTP harus 16 digit!'); //dari index, total KTP = 16
            else if(this.state.fotoSource === null) Alert.alert(title, 'Foto pengemudi tidak boleh kosong!');
            else if(this.state.ktpSource === null) Alert.alert(title, 'Foto KTP tidak boleh kosong!');
            else{
                
                this.setState({isLoading:true});
                let trid = this.state.profile.id_transporter
                let uid = this.state.profile.id_user

                let _insertDriver = await insertDriver(this.state.url, this.state.namaPengemudi, this.state.role, this.state.noHp, this.state.email, this.state.password, this.state.setSelectedValue, this.state.profile.id_transporter, this.state.profile.id_user, this.state.fotoFileName, this.state.ktpFileName, this.state.simFileName, this.state.refId, this.state.noKtp, this.state.noSim, this.state.fotoSource, this.state.ktpSource, this.state.simSource)
                this.setState({isLoading:false}); 
                this.props.funcSweetAlert('status',_insertDriver[0], _insertDriver[1])
            }

        }
      }

      onValueChangeTipePengemudi = async(itemValue) =>{
        await this.setState({role: itemValue})
        if (itemValue == 'KERANI') {
            await this.setState({noSim: '', simFileName: 'Pilih foto SIM', simSource: ''})
            console.log('KERANI', this.state.noSim)
        }else{
          console.log('DRIVER',this.state.noSim)
        }
      }

      render(){

         //Image Picker
        this.selectFotoTapped = this.selectFotoTapped.bind(this);
        this.selectKtpTapped = this.selectKtpTapped.bind(this);
        this.selectSimTapped = this.selectSimTapped.bind(this);


        return(
          <>
            <Content>

                {/* <Text style={styles.title}>TAMBAH PENGEMUDI</Text> */}

                  {/* Nama Transporter */}
                <View style={styles.viewTextInput}>
                  <Label style={styles.label}>Nama Transporter</Label>
                    <TextInput
                      style={styles.textInputTransporter}
                      underlineColorAndroid='rgba(0,0,0,0)'
                      caretHidden={true}
                      editable ={false}
                      selectTextOnFocus = {false}
                      // placeholder="PT INSTRANS"
                      // placeholderTextColor = "#CACCD7"
                      selectionColor="#008c45"
                      keyboardType="email-address"
                      value = {this.state.namaTransporter}
                      
                    />
                </View>

                {/* Tipe Driver */}
                <View style={{flex: 1, flexDirection: 'column'}}>   
                  <Label style={{fontSize: 14, marginLeft: 10, marginTop: 10, alignSelf: 'flex-start', alignItems: 'flex-start'}}>Tipe Pengemudi</Label>
                    <View 
                        style=
                        {{flex: 1,  
                          alignItems: 'center',  
                          justifyContent: 'center',
                          borderColor: 'gray', 
                          borderWidth: 2, 
                          borderRadius: 5, 
                          marginTop: 10, 
                          marginLeft: 20, 
                          marginRight: 20, 
                          height: 40, 
                          color: '#494A4A', 
                          backgroundColor: '#E9EDF2', 
                          padding: 10 }}>

                              
                                      <Picker
                                        style={{ 
                                          width: "100%",
                                          color: '#344953',  
                                          justifyContent: 'center',
                                        }}
                                        enabled={false}
                                        selectedValue={this.state.role}
                                        onValueChange={(itemValue, itemIndex) => 
                                          // this.setState({role: itemValue})
                                          this.onValueChangeTipePengemudi(itemValue)
                                          }
                                      >
                                        <Picker.Item label="DRIVER" value="DRIVER" />
                                        <Picker.Item label="KERANI" value="KERANI" />
                                      </Picker>

                    </View>
                </View>


                    {/* Nama Pengemudi */}
                <View style={styles.viewTextInput}>   
                    <Label style={styles.label}>Nama Pengemudi</Label>
                        <TextInput
                        style={styles.textInput}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder="Nama Pengemudi"
                        placeholderTextColor = "#CACCD7"
                        selectionColor="#008c45"
                        onChangeText={input=>this.setState({namaPengemudi: input})}
                        
                        />
                </View>


                <View style={styles.viewTextInput}>   
                    <Label style={styles.label}>No Handphone</Label>
                        <TextInput
                        style={styles.textInput}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder="No Handphone"
                        placeholderTextColor = "#CACCD7"
                        selectionColor="#008c45"
                        keyboardType="phone-pad"
                        onChangeText={input=>this.setState({noHp: input})}
                        
                        />
                </View>


                <View style={styles.viewTextInput}>   
                    <Label style={styles.label}>Email</Label>
                        <TextInput
                        style={styles.textInput}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder="Email"
                        placeholderTextColor = "#CACCD7"
                        selectionColor="#008c45"
                        keyboardType="email-address"
                        // onChangeText={input=>this.setState({email: input})}
                        onChangeText={(text)=>this.validateemail(text)}
                        value={this.state.email}
                        
                        />
                </View>

                <View style={{flex: 1, flexDirection: 'column'}}>   
                  <Label style={{fontSize: 14, marginLeft: 10, marginTop: 10, alignSelf: 'flex-start', alignItems: 'flex-start'}}>Status</Label>
                    <View 
                        style={{flex: 1,  
                        alignItems: 'center',  
                        justifyContent: 'center',
                        borderColor: 'gray', 
                        borderWidth: 2, 
                        borderRadius: 5, 
                        marginTop: 10, 
                        marginLeft: 20, 
                        marginRight: 20, height: 40, color: '#494A4A', padding: 10 }}>

                              
                                      <Picker
                                        style={{ 
                                          width: "100%",
                                          color: '#344953',  
                                          justifyContent: 'center',
                                        }}
                                        selectedValue={this.state.setSelectedValue}
                                        onValueChange={(itemValue, itemIndex) => 
                                          this.setState({setSelectedValue: itemValue})
                                          }
                                      >
                                        <Picker.Item label="Active" value="A" />
                                        <Picker.Item label="In Active" value="I" />
                                      </Picker>

                    </View>
                </View>


                
                <View style={styles.viewTextInput}>   
                    <Label style={styles.label}>Password</Label>
                        <TextInput
                                style={styles.textInput}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                placeholder="Password"
                                placeholderTextColor = "#CACCD7"
                                selectionColor="#008c45"
                                secureTextEntry={true}
                                onChangeText={(text)=>this.validatePassword(text)}
                                
                            />
                
                </View>

                <View style={styles.viewTextInput}>   
                    <Label style={styles.label}>Konfirmasi Password</Label>      
                        <TextInput
                                style={styles.textInput}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                placeholder="Konfirmasi Password"
                                placeholderTextColor = "#CACCD7"
                                selectionColor="#008c45"
                                secureTextEntry={true}
                                onChangeText={(text)=>this.validateKonfirmasiPassword(text)}
                                
                            />
                </View>

                <View style={styles.viewTextInput}>   
                  <Label style={styles.label}>Ref. ID Pengemudi (optional)</Label>
                    <TextInput
                      style={styles.textInput}
                      underlineColorAndroid='rgba(0,0,0,0)'
                      placeholder="Ref. ID Pengemudi"
                      placeholderTextColor = "#CACCD7"
                      selectionColor="#008c45"
                      onChangeText={input=>this.setState({refId: input})}
                      
                    />
                </View>


                <View style={styles.viewTextInput}>   
                    <Label style={styles.label}>No KTP</Label>
                        <TextInput
                        style={styles.textInput}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder="No KTP"
                        placeholderTextColor = "#CACCD7"
                        selectionColor="#008c45"
                        keyboardType="number-pad"
                        maxLength={16}
                        onChangeText={input=>this.setState({noKtp: input})}
                        
                        />
                </View>

                
                <View style={styles.viewTextInput}>   
                    <Label style={styles.label}>No SIM</Label>
                        <TextInput
                        style={this.state.role == 'KERANI'? styles.textInputTransporter: styles.textInput}
                        caretHidden={this.state.role == 'KERANI' ? true: false}
                        editable ={this.state.role == 'KERANI' ? false: true}
                        selectTextOnFocus = {this.state.role == 'KERANI' ? false:true}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder="No SIM"
                        placeholderTextColor = "#CACCD7"
                        selectionColor="#008c45"
                        keyboardType="number-pad"
                        maxLength={13}
                        value={this.state.noSim}
                        onChangeText={input=>this.setState({noSim: input})}
                        
                        />
                </View>


                <View style={styles.viewButton}>   
                  <Label style={styles.label}>Foto Pengemudi</Label>
                    <Button
                      style={styles.button}
                        onPress={this.selectFotoTapped.bind(this)}
            
                    >
                      <Text uppercase={false} style={{fontSize: 14}} numberOfLines={1}>{this.state.fotoFileName}</Text>
                      <Icon type="MaterialIcons" name='photo' />
                    </Button>
                </View>

                <View style={styles.viewButton}>   
                    <Label style={styles.label}>Foto KTP</Label>
                        <Button
                        style={styles.button}
                            onPress={this.selectKtpTapped.bind(this)}
                
                        >
                        <Text uppercase={false} style={{fontSize: 14}} numberOfLines={1}>{this.state.ktpFileName}</Text>
                        <Icon type="MaterialIcons" name='photo' />
                        </Button>
                </View>

                <View style={styles.viewButton}>   
                    <Label style={styles.label}>Foto SIM</Label>
                        <Button
                        style={this.state.role == 'KERANI'? styles.buttonDisabled: styles.button}
                            onPress={this.selectSimTapped.bind(this)}
                            disabled={this.state.role == 'KERANI' ? true: false}
                        >
                        <Text uppercase={false} style={{fontSize: 14}} numberOfLines={1}>{this.state.simFileName}</Text>
                        <Icon type="MaterialIcons" name='photo' />
                        </Button>
                </View>

                <View style={{flex: 1, flexDirection: 'column', marginBottom: 15, marginTop: 10}}>   
                  {/* <Label style={{fontSize: 14, 
                                // marginLeft: 10, 
                                marginTop: 10, alignSelf: 'center', alignItems: 'center'}}>Tambah Pengemudi</Label> */}
                    <Button
                      style={[styles.button, {justifyContent: 'center', alignItems: 'center'}]}
                        onPress={this.tambahPengemudi}
            
                    >
                      <Text uppercase={false} style={{fontSize: 14, textAlign: "center"}}>Tambah Pengemudi</Text>
                    </Button>
                </View> 

            </Content>

            {this.state.isLoading ? <Loading/> : false}
          </>
        );
      }
}

export default DriverAdd;