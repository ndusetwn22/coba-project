import React,{Component} from 'react'
import {
    View
} from "react-native";
import styles from './style'
import {
    Text,  
    Card,
    CardItem,
    Body,
    Left,
    Right,
    Button,
    Icon,
    Thumbnail
} from 'native-base'
import { TouchableOpacity } from 'react-native-gesture-handler';

class CardDriver extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount= async()=>{

    } 

    render(){
        const hak_akses = this.props.hak_akses
        return(
            // <Card style={{marginLeft: 10, marginRight: 10}}>
            //     <CardItem>
            //         <Body>
            //             <View style={{flex: 1, flexDirection: 'row'}}>
            //                 <View style={{flex:1, marginLeft: 10}}>
            //                     <Text style={{width: "100%", fontSize: 14, fontWeight: 'bold'}}>
            //                         {this.props.value.nama}
            //                     </Text>
            //                     <Text note style={{width: "100%", fontSize: 12}}>
            //                         Nomor hp :{'\n'}{this.props.value.hp}
            //                     </Text>
            //                     <Text note style={{width: "100%", fontSize: 12}}>
            //                         Email :{'\n'}{this.props.value.email}
            //                     </Text>
            //                 </View>
            //                 <View style={{flex:1, justifyContent: 'center'}}>
            //                     <Text note style={{width: "100%", fontSize: 14, fontStyle: 'italic', textAlign: 'center'}}>
            //                     {this.props.value.role}
            //                     </Text>

            //                     {/* harusnya ada Block A = active, I = Inactive, B = Block */}
            //                     <Text note style={{width: "100%", fontSize: 14, fontStyle: 'italic', textAlign: 'center'}}>
            //                         {this.props.value.active == 'A' ? 'Active' : 
            //                          this.props.value.active == 'I' ? 'Inactive':
            //                          'Block'   
            //                     }
            //                     </Text>
            //                 </View>
            //             {hak_akses == 'ADMIN' ?
            //                 <View style={{justifyContent: 'center'}}>
            //                     {this.props.value.active != 'B'?
            //                         <Button icon primary rounded small onPress={()=>this.props.onDriverEdit(this.props.value)} style={{margin: 5, backgroundColor: '#008c45'}}>
            //                             <Icon type="MaterialCommunityIcons" name='pencil-outline'/>
            //                         </Button>
            //                     :
            //                         <Button icon danger rounded small style={{margin: 5, backgroundColor: '#ee595b'}}>
            //                             <Icon type="MaterialCommunityIcons" name='pencil-outline'/>
            //                         </Button>
            //                     }
            //                 </View>
            //             : null}
            //             </View>
            //         </Body>
            //     </CardItem>
            // </Card>

            <Card style={{marginLeft: 10, marginRight: 10, borderLeftWidth: 8, borderColor: '#008c45'}}>
                    <CardItem>
                    <Body>
                            
                            <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', alignContent:'center', alignSelf: 'center'}}>
                                <Text style={{fontSize: 16, fontWeight: 'bold', textAlign: 'center'}}>
                                    {this.props.value.nama}
                                </Text>
                                <Text note style={{fontSize: 12, textAlign: 'center'}}>
                                    {this.props.value.email}
                                </Text>
                            </View>
                            <View style = {{borderWidth: 0.5, marginTop: 5, marginBottom: 5, borderColor: '#008c45', width: '100%'}} />

                        <View style={{flex: 1, flexDirection: 'row'}}>
                            <View style={{flex:1, marginLeft: 10}}>
                                <Text note style={{width: "100%", fontSize: 12}}>
                                    <Text style={{fontWeight:'bold', fontSize: 12}}>HP :</Text> {this.props.value.hp}
                                </Text>
                                <Text note style={{width: "100%", fontSize: 12}}>
                                    <Text style={{fontWeight:'bold', fontSize: 12}}>Last Update :</Text> {'\n'}{this.props.value.tgl_update}
                                </Text>
                            </View>

                            <View style={{flex:1}}>
                                
                            


                                {/* harusnya ada Block A = active, I = Inactive, B = Block */}
                                {this.props.value.active == 'A'?
                                <Text note style={{width: "100%", fontSize: 12, fontWeight: 'bold', color: 'green'}}>
                                    <Text style={{fontWeight:'bold', fontSize: 12}}>Status :</Text> Active
                                </Text>
                                : this.props.value.active == 'I'?
                                <Text note style={{width: "100%", fontSize: 12, fontWeight: 'bold', color: 'red'}}>
                                    <Text style={{fontWeight:'bold', fontSize: 12}}>Status :</Text> Non Active
                                </Text>
                                :
                                <Text note style={{width: "100%", fontSize: 12, fontWeight: 'bold', color: 'red'}}>
                                    <Text style={{fontWeight:'bold', fontSize: 12}}>Status :</Text>  Block
                                </Text>
                                }  


                                <Text note style={{width: "100%", fontSize: 12}}>
                                    <Text style={{fontWeight:'bold', fontSize: 12}}>Role :</Text> {'\n'}{this.props.value.role}
                                </Text>
                            </View>



                            {hak_akses == 'ADMIN' ?
                            <View style={{justifyContent: 'center'}}>
                                {this.props.value.active != 'B'?
                                    <Button color="#008c45" icon  rounded small onPress={()=>this.props.onDriverEdit(this.props.value)} style={{margin: 5, backgroundColor: '#008c45'}}>
                                        <Icon type="MaterialCommunityIcons" name='pencil-outline'/>
                                    </Button>
                                :
                                    <Button color="#ee595b" icon rounded small style={{margin: 5, backgroundColor: '#ee595b'}}>
                                        <Icon type="MaterialCommunityIcons" name='pencil-outline'/>
                                    </Button>
                                }
                            </View>
                        : null}
                        </View>
                    </Body>
                </CardItem>
             </Card>
        );
    }
}

export default CardDriver;