import { StyleSheet, Platform } from 'react-native';

const styles = StyleSheet.create({
  
    
  lineStyle:{
    borderWidth: 0.5,
    borderColor:'#7d8d8c'
  },

  search:{
    padding: 10,
    borderColor: '#CCC',
    borderWidth: 2,
    borderRadius: 10,
    width: '40%',
    height: 40,
    marginLeft: 10,
    marginTop: 10
  }

});

export default styles;