import React,{Component} from 'react'
import { AppState,Platform,Dimensions, Image, FlatList , TouchableOpacity,ScrollView, ImageBackground, BackHandler, Alert, PixelRatio, StyleSheet, TextInput, Picker } from "react-native";
import { useNavigation  } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
import CustomNavbar from '../../../Header/Navbar/Navbar'
import Loading,{getProfile, encrypt, setUrl, getUrl, decrypt} from '../../../../../APIProp/ApiConstants'
import Axios from 'axios';
import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Left,
    Right,
    Body,
    Text,
    Icon,
    Card,
    CardItem,
    Grid,
    Row,
    View,
    Col, 
    List,
    ListItem,
    Thumbnail,
    Form,
    Label,
    Input,
    Item,
    DatePicker
} from "native-base";
import styles from "./style";
import moment from 'moment'

//image picker
import ImagePicker from 'react-native-image-picker';

//Google Places
import RNGooglePlaces from 'react-native-google-places';

//axios
import {getDataUser, getDataDriver, editPengemudi} from './DriverEditAxios'

//firebase
import { utils } from '@react-native-firebase/app';
import storage from '@react-native-firebase/storage';


class DriverEdit extends Component {
    constructor(props) {
            super(props);
            this.state = {
            paramLogin:{
                username:'',
                password:'',
            },
            isLoading:false,
            url:null,
            profile: null,
            namaTransporter: '',
            tempFotoFileName: '',
            fotoFileName: 'Pilih foto pengemudi',
            fotoSource: null,
            tempKtpFileName: '',
            ktpFileName: 'Pilih foto KTP',
            ktpSource: null,
            tempSimFileName: '',
            simFileName: 'Pilih foto SIM',
            simSource: null,
            namaTransporter: '',
            role: 'DRIVER',
            namaPengemudi: '',
            noHp: '',
            email: '',
            validatedemail: false,
            validatePassword: false,
            status: 'A',
            password: '',
            konfirmasiPassword: '',
            refId: '',
            noKtp: '',
            noSim: '',
            changeTextFoto: '',
            changeTextKtp: '',
            changeTextSim: '',
            
            
        };
      }
    
      componentDidMount= async()=>{
        await this.setState({isLoading:true});
        await setUrl();
        await getUrl().then(async(result)=>{
          this.setState({url:result})
        });

        await this.funcGetProfile();
        var _getDataUser = await getDataUser(this.state.url, this.props.driverSelected.value.id)
        var _getDataDriver = await getDataDriver(this.state.url, this.props.driverSelected.value.id)

        this.setState({
          namaPengemudi: _getDataUser.nama,
          role: _getDataUser.role,
          noHp: _getDataUser.hp,
          email: _getDataUser.email,
          status: _getDataUser.active,
          password: _getDataUser.pass,
          konfirmasiPassword: _getDataUser.pass,
          refId: _getDataDriver.external_id,
          noKtp: _getDataDriver.no_ktp,
          noSim: _getDataDriver.no_sim,
          fotoFileName: _getDataDriver.fotoFileName,
          ktpFileName: _getDataDriver.ktpFileName,
          simFileName: _getDataDriver.simFileName,
          tempFotoFileName: _getDataDriver.foto_driver,
          tempKtpFileName: _getDataDriver.foto_ktp,
          tempSimFileName: _getDataDriver.foto_sim,
        })

        if (_getDataUser.role == 'KERANI') {
          await this.setState({noSim: '', simFileName: 'Pilih foto SIM', simSource: ''})
        }

        
        setTimeout(() => {this.validateemail(this.state.email)}, 3000)
        setTimeout(() => {this.validatePassword(this.state.password)}, 3000)

        await this.setState({isLoading:false, namaTransporter: this.state.profile.nama_transporter});
      }

      funcGetProfile=async()=>{
        await getProfile().then(result=>{
          if(result!=null){
            this.setState({profileStr:result})
          }
        })
        if(this.state.profileStr==null){
          this.props.navigation.replace('Login')
        }
        var tmpStr = await decrypt(this.state.profileStr);
        this.setState({profile:JSON.parse(tmpStr)})
        // console.log('profile', JSON.parse(tmpStr))
      }


     validateemail=(text)=>{
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
        console.log(reg);
        if(reg.test(text) === false)
        {
          // console.log("Email is Not Correct");
    
          this.setState({email:text, validatedemail: false})
          return false;
          }
        else {
          console.log('email correct');
          this.setState({email:text, validatedemail: true})
        }
      }

      validatePassword=(text)=>{
        let reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/ ;
        console.log(reg);
        if(reg.test(text) === false)
        {
          // console.log("password is Not Correct");
    
          this.setState({password: text, validatePassword: false})
          return false;
          }
        else {
          console.log('password correct');
          this.setState({password: text, validatePassword: true})
        }
      }


      selectKtpTapped = () => {
        const options = {
          quality: 1.0,
          maxWidth: 500,
          maxHeight: 500,
          storageOptions: {
            skipBackup: true,
          },
        };
    
        ImagePicker.showImagePicker(options, response => {
          console.log('Response = ', response);
    
          if (response.didCancel) {
            console.log('User cancelled photo picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            let source = {uri: response.uri};
    
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
            var changeText = "(*foto sudah dirubah)"
            console.log('options : ', options)
            console.log('uri :', response.uri)
            this.setState({
              // avatarSource: response.uri,
              ktpSource: source,
              ktpFileName: 'KTP',
              changeTextKtp: changeText
            });
    
          }
        });
      }
    
      selectSimTapped = () => {
        const options = {
          quality: 1.0,
          maxWidth: 500,
          maxHeight: 500,
          storageOptions: {
            skipBackup: true,
          },
        };
    
        ImagePicker.showImagePicker(options, response => {
          console.log('Response = ', response);
    
          if (response.didCancel) {
            console.log('User cancelled photo picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            let source = {uri: response.uri};
    
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
            var changeText = "(*foto sudah dirubah)"
            console.log('options : ', options)
            console.log('uri :', response.uri)
            this.setState({
              // avatarSource: response.uri,
              simSource: source,
              simFileName: 'SIM',
              changeTextSim: changeText
            });
    
          }
        });
      }
    
    
      selectFotoTapped = () => {
        const options = {
          quality: 1.0,
          maxWidth: 500,
          maxHeight: 500,
          storageOptions: {
            skipBackup: true,
          },
        };
    
        ImagePicker.showImagePicker(options, response => {
          console.log('Response = ', response);
    
          if (response.didCancel) {
            console.log('User cancelled photo picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            let source = {uri: response.uri};
    
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
            var changeText = "(*foto sudah dirubah)"
            console.log('options : ', options)
            console.log('uri :', response.uri)
            this.setState({
              // avatarSource: response.uri,
              fotoSource: source,
              fotoFileName: 'Driver',
              changeTextFoto: changeText
            });
    
          }
        });
      }


      editPengemudi=async ()=>{
        // this.setState({buttondisabled: true});
        let title = 'Invalid Input';

        if (this.state.role == 'DRIVER') {
          
            if(this.state.namaPengemudi === '' ) Alert.alert(title, 'Nama Pengemudi tidak boleh kosong!');
            else if(this.state.noHp === '') Alert.alert(title, 'No HP tidak boleh kosong!');
            else if(this.state.validatedemail === false) Alert.alert(title, 'Format Email tidak sesuai!');
            else if(this.state.status === '') Alert.alert(title, 'Status tidak boleh kosong!');
            // else if(this.state.password === '') Alert.alert(title, 'Password tidak boleh kosong!');
            // else if(this.state.konfirmasiPassword === '') Alert.alert(title, 'Konfirmasi Password tidak boleh kosong!');
            else if(this.state.validatePassword === false) Alert.alert(title, 'Password harus memiliki minimal 6 karakter, 1 digit angka, 1 huruf besar, 1 huruf kecil');
            else if(this.state.password !== this.state.konfirmasiPassword) Alert.alert(title, 'Konfirmasi Password tidak sesuai, harap isi kembali!');
            else if(this.state.noKtp === '') Alert.alert(title, 'No KTP tidak boleh kosong!');
            else if(this.state.noKtp.length <= 15) Alert.alert(title, 'Format KTP harus 16 digit!'); //dari index, total KTP = 16
            else if(this.state.noSim === '') Alert.alert(title, 'No SIM tidak boleh kosong!');
            else if(this.state.noSim.length <= 12) Alert.alert(title, 'Format SIM Harus 13 digit!'); //dari index total SIM = 13
            else if(this.state.tempFotoFileName === null) Alert.alert(title, 'Foto pengemudi tidak boleh kosong!');
            else if(this.state.tempKtpFileName === null) Alert.alert(title, 'Foto KTP tidak boleh kosong!');
            else if(this.state.tempSimFileName === null || this.state.simFileName === 'Pilih foto SIM') Alert.alert(title, 'Foto SIM tidak boleh kosong!');
            else{
                
                this.setState({isLoading:true});
                let trid = this.state.profile.id_transporter
                let uid = this.state.profile.id_user

                //query
                var _editDriver = await editPengemudi(this.state.url, this.props.driverSelected.value.id, trid, uid, this.state.namaPengemudi, this.state.role, this.state.noHp, this.state.email, this.state.status, this.state.password, this.state.refId, this.state.noKtp, this.state.noSim, this.state.fotoFileName, this.state.ktpFileName, this.state.simFileName, this.state.fotoSource, this.state.ktpSource, this.state.simSource, this.state.tempFotoFileName, this.state.tempKtpFileName, this.state.tempSimFileName)
                this.setState({isLoading:false}); 

                this.props.funcSweetAlert('status',_editDriver[0], _editDriver[1])

        
            }

        }
        // EDIT KERANI
        else{
          if(this.state.namaPengemudi === '' ) Alert.alert(title, 'Nama Pengemudi tidak boleh kosong!');
          else if(this.state.noHp === '') Alert.alert(title, 'No HP tidak boleh kosong!');
          else if(this.state.validatedemail === false) Alert.alert(title, 'Format Email tidak sesuai!');
          else if(this.state.status === '') Alert.alert(title, 'Status tidak boleh kosong!');
          // else if(this.state.password === '') Alert.alert(title, 'Password tidak boleh kosong!');
          // else if(this.state.konfirmasiPassword === '') Alert.alert(title, 'Konfirmasi Password tidak boleh kosong!');
          else if(this.state.validatePassword === false) Alert.alert(title, 'Password harus memiliki minimal 6 karakter, 1 digit angka, 1 huruf besar, 1 huruf kecil');
          else if(this.state.password !== this.state.konfirmasiPassword) Alert.alert(title, 'Konfirmasi Password tidak sesuai, harap isi kembali!');
          else if(this.state.noKtp === '') Alert.alert(title, 'No KTP tidak boleh kosong!');
          else if(this.state.noKtp.length <= 15) Alert.alert(title, 'Format KTP harus 16 digit!'); //dari index, total KTP = 16
          else if(this.state.tempFotoFileName === null) Alert.alert(title, 'Foto pengemudi tidak boleh kosong!');
          else if(this.state.tempKtpFileName === null) Alert.alert(title, 'Foto KTP tidak boleh kosong!');
          else{
              
              this.setState({isLoading:true});
              let trid = this.state.profile.id_transporter
              let uid = this.state.profile.id_user

              //query
              var _editDriver = await editPengemudi(this.state.url, this.props.driverSelected.value.id, trid, uid, this.state.namaPengemudi, this.state.role, this.state.noHp, this.state.email, this.state.status, this.state.password, this.state.refId, this.state.noKtp, this.state.noSim, this.state.fotoFileName, this.state.ktpFileName, this.state.simFileName, this.state.fotoSource, this.state.ktpSource, this.state.simSource, this.state.tempFotoFileName, this.state.tempKtpFileName, this.state.tempSimFileName)
              this.setState({isLoading:false}); 

              this.props.funcSweetAlert('status',_editDriver[0], _editDriver[1])
      
          }
        }
      }

      onValueChangeTipePengemudi = async(itemValue) =>{
        await this.setState({role: itemValue})
        if (itemValue == 'KERANI') {
            await this.setState({noSim: '', simFileName: 'Pilih foto SIM', simSource: ''})
            console.log('KERANI', this.state.noSim)
        }else{
          console.log('DRIVER',this.state.noSim)
        }
      }



      render(){

         //Image Picker
        this.selectFotoTapped = this.selectFotoTapped.bind(this);
        this.selectKtpTapped = this.selectKtpTapped.bind(this);
        this.selectSimTapped = this.selectSimTapped.bind(this);

        return(
          <>
            <Content>

                {/* <Text style={styles.title}>EDIT PENGEMUDI</Text> */}

                  {/* Nama Transporter */}
                <View style={styles.viewTextInput}>
                  <Label style={styles.label}>Nama Transporter</Label>
                    <TextInput
                      style={styles.textInputTransporter}
                      underlineColorAndroid='rgba(0,0,0,0)'
                      caretHidden={true}
                      editable ={false}
                      selectTextOnFocus = {false}
                      // placeholder="PT INSTRANS"
                      // placeholderTextColor = "#CACCD7"
                      selectionColor="#008c45"
                      keyboardType="email-address"
                      value = {this.state.namaTransporter}
                      
                    />
                </View>

                {/* Tipe Driver */}
                <View style={{flex: 1, flexDirection: 'column'}}>   
                  <Label style={{fontSize: 14, marginLeft: 10, marginTop: 10, alignSelf: 'flex-start', alignItems: 'flex-start'}}>Tipe Pengemudi</Label>
                    <View 
                        style={{flex: 1,  
                        alignItems: 'center',  
                        justifyContent: 'center',
                        borderColor: 'gray', 
                        borderWidth: 2, 
                        borderRadius: 5, 
                        marginTop: 10, 
                        marginLeft: 20, 
                        marginRight: 20, 
                        height: 40, 
                        color: '#494A4A', 
                        backgroundColor: '#E9EDF2', 
                        padding: 10 }}
                        >

                              
                                      <Picker
                                        style={{ 
                                          width: "100%",
                                          color: '#344953',  
                                          justifyContent: 'center',
                                        }}
                                        enabled={false}
                                        selectedValue={this.state.role}
                                        onValueChange={(itemValue, itemIndex) => 
                                          // this.setState({role: itemValue})
                                          this.onValueChangeTipePengemudi(itemValue)
                                          }
                                      >
                                        <Picker.Item label="DRIVER" value="DRIVER" />
                                        <Picker.Item label="KERANI" value="KERANI" />
                                      </Picker>

                    </View>
                </View>


                    {/* Nama Pengemudi */}
                <View style={styles.viewTextInput}>   
                  <Label style={styles.label}>Nama Pengemudi</Label>
                    <TextInput
                      style={styles.textInputTransporter}
                      underlineColorAndroid='rgba(0,0,0,0)'
                      placeholder="Nama Pengemudi"
                      placeholderTextColor = "#CACCD7"
                      selectionColor="#008c45"
                      onChangeText={input=>this.setState({namaPengemudi: input})}
                      value = {this.state.namaPengemudi}
                      caretHidden={true}
                      editable ={false}
                      selectTextOnFocus = {false}
                    />
                </View>


                <View style={styles.viewTextInput}>   
                    <Label style={styles.label}>No Handphone</Label>
                        <TextInput
                        style={styles.textInput}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder="No Handphone"
                        placeholderTextColor = "#CACCD7"
                        selectionColor="#008c45"
                        keyboardType="phone-pad"
                        onChangeText={input=>this.setState({noHp: input})}
                        value = {this.state.noHp}
                        
                        />
                </View>


                <View style={styles.viewTextInput}>   
                    <Label style={styles.label}>Email</Label>
                        <TextInput
                        style={styles.textInput}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder="Email"
                        placeholderTextColor = "#CACCD7"
                        selectionColor="#008c45"
                        keyboardType="email-address"
                        // onChangeText={input=>this.setState({email: input})}
                        onChangeText={(text)=>this.validateemail(text)}
                        value={this.state.email}
                        
                        />
                </View>

                <View style={{flex: 1, flexDirection: 'column'}}>   
                  <Label style={{fontSize: 14, marginLeft: 10, marginTop: 10, alignSelf: 'flex-start', alignItems: 'flex-start'}}>Status</Label>
                    <View 
                        style={{flex: 1,  
                        alignItems: 'center',  
                        justifyContent: 'center',
                        borderColor: 'gray', 
                        borderWidth: 2, 
                        borderRadius: 5, 
                        marginTop: 10, 
                        marginLeft: 20, 
                        marginRight: 20, height: 40, color: '#494A4A', padding: 10 }}>

                              
                                      <Picker
                                        style={{ 
                                          width: "100%",
                                          color: '#344953',  
                                          justifyContent: 'center',
                                        }}
                                        selectedValue={this.state.status}
                                        onValueChange={input=>this.setState({status: input})}
                                      >
                                        <Picker.Item label="Active" value="A" />
                                        <Picker.Item label="In Active" value="I" />
                                      </Picker>

                    </View>
                </View>


                
                <View style={styles.viewTextInput}>   
                    <Label style={styles.label}>Password</Label>
                        <TextInput
                                style={styles.textInput}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                placeholder="Password"
                                placeholderTextColor = "#CACCD7"
                                selectionColor="#008c45"
                                secureTextEntry={true}
                                onChangeText={(text)=>this.validatePassword(text)}
                                value = {this.state.password}
                            />
                
                </View>

                <View style={styles.viewTextInput}>   
                    <Label style={styles.label}>Konfirmasi Password</Label>      
                        <TextInput
                                style={styles.textInput}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                placeholder="Konfirmasi Password"
                                placeholderTextColor = "#CACCD7"
                                selectionColor="#008c45"
                                secureTextEntry={true}
                                onChangeText={this.onvalueChangeKonfirmasiPassword}
                                value = {this.state.konfirmasiPassword}
                                
                            />
                </View>

                <View style={styles.viewTextInput}>   
                  <Label style={styles.label}>Ref. ID Pengemudi (optional)</Label>
                    <TextInput
                      style={styles.textInput}
                      underlineColorAndroid='rgba(0,0,0,0)'
                      placeholder="Ref. ID Pengemudi"
                      placeholderTextColor = "#CACCD7"
                      selectionColor="#008c45"
                      onChangeText={input=>this.setState({refId: input})}
                      value = {this.state.refId}
                      caretHidden={true}
                      editable ={false}
                      selectTextOnFocus = {false}
                      
                    />
                </View>


                <View style={styles.viewTextInput}>   
                    <Label style={styles.label}>No KTP</Label>
                        <TextInput
                        style={styles.textInput}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder="No KTP"
                        placeholderTextColor = "#CACCD7"
                        selectionColor="#008c45"
                        keyboardType="number-pad"
                        maxLength={16}
                        onChangeText={input=>this.setState({noKtp: input})}
                        value = {this.state.noKtp}
                        
                        />
                </View>

                
                <View style={styles.viewTextInput}>   
                    <Label style={styles.label}>No SIM</Label>
                        <TextInput
                        style={this.state.role == 'KERANI'? styles.textInputTransporter: styles.textInput}
                        caretHidden={this.state.role == 'KERANI' ? true: false}
                        editable ={this.state.role == 'KERANI' ? false: true}
                        selectTextOnFocus = {this.state.role == 'KERANI' ? false:true}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder="No SIM"
                        placeholderTextColor = "#CACCD7"
                        selectionColor="#008c45"
                        keyboardType="number-pad"
                        maxLength={13}
                        onChangeText={input=>this.setState({noSim: input})}
                        value = {this.state.noSim}
                        />
                </View>


                <View style={styles.viewButton}>   
                  <Label style={styles.label}>Foto Pengemudi {this.state.changeTextFoto}</Label>
                    <Button
                      style={styles.button}
                        onPress={this.selectFotoTapped.bind(this)}
            
                    >
                      <Text uppercase={false} style={{fontSize: 14}} numberOfLines={1}>{this.state.fotoFileName}</Text>
                      <Icon type="MaterialIcons" name='photo' />
                    </Button>
                </View>

                <View style={styles.viewButton}>   
                    <Label style={styles.label}>Foto KTP {this.state.changeTextKtp}</Label>
                        <Button
                        style={styles.button}
                            onPress={this.selectKtpTapped.bind(this)}
                
                        >
                        <Text uppercase={false} style={{fontSize: 14}} numberOfLines={1}>{this.state.ktpFileName}</Text>
                        <Icon type="MaterialIcons" name='photo' />
                        </Button>
                </View>

                <View style={styles.viewButton}>   
                    <Label style={styles.label}>Foto SIM {this.state.changeTextSim}</Label>
                        <Button
                        style={this.state.role == 'KERANI'? styles.buttonDisabled: styles.button}
                            onPress={this.selectSimTapped.bind(this)}
                            disabled={this.state.role == 'KERANI' ? true: false}
                
                        >
                        <Text uppercase={false} style={{fontSize: 14}} numberOfLines={1}>{this.state.simFileName}</Text>
                        <Icon type="MaterialIcons" name='photo' />
                        </Button>
                </View>

                <View style={{flex: 1, flexDirection: 'column', marginBottom: 15, marginTop: 10}}>   
                  {/* <Label style={{fontSize: 14, 
                                // marginLeft: 10, 
                                marginTop: 10, alignSelf: 'center', alignItems: 'center'}}>Edit Pengemudi</Label> */}
                    <Button
                      style={[styles.button, {justifyContent: 'center', alignItems: 'center'}]}
                        onPress={this.editPengemudi}
            
                    >
                      <Text uppercase={false} style={{fontSize: 14, textAlign: "center"}}>Edit Pengemudi</Text>
                    </Button>
                </View> 

            </Content>

            {this.state.isLoading ? <Loading/> : false}
          </>
        );
      }
}

export default DriverEdit;