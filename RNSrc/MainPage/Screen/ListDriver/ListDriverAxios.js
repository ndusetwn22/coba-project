import Axios from 'axios';
import {encrypt} from '../../../../APIProp/ApiConstants';

  export const getListDriver = async (url,trans_id) =>{
    // let dataQry = `
    // select mmu.id, mmu.nama, mmu.hp, mmu.active, mmu.email, mmu.role, mdi.foto_driver, to_char(mmu.update_date, 'dd Mon yyyy') as tgl_update from mt_master_user mmu 
    // left join mt_driver_info mdi
    // on mmu.id = mdi.user_id 
    // where mmu.transporter_id = '`+trans_id+`' 
    // and mmu.role = 'DRIVER'
    // order by mmu.id desc
    // `

    let dataQry = `
        select * from mobiletra_getlistdrivermenu(`+trans_id+`)
    `
    console.log('query list driver : ', dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('listDriver', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
      // console.log('error', err)
    });
  }


  export const getDataUser = async (url,id) =>{
    let dataQry = `
      select *, epm_decrypt(mmu.password) as pass from mt_master_user mmu 
      where mmu.id = `+id+`
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      // console.log('getDataUserDriver', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
      // console.log('error', err)
    });
  }

  export const getDataDriverInfo = async (url,id) =>{
    let dataQry = `
      select * from mt_driver_info mdi 
      where mdi.user_id = `+id+`
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      // console.log('getDataDriverInfo', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
      // console.log('error', err)
    });
  }