import React,{Component} from 'react'
import {BackHandler,TouchableOpacity, Text,View} from "react-native";
import Loading,{getProfile, setUrl, getUrl, decrypt, firebase} from '../../../../APIProp/ApiConstants'
import { useNavigation  } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
import CustomNavbar from '../../Header/Navbar/Navbar'
import {Container} from 'native-base'
import SweetAlert from 'react-native-sweet-alert';
import {
  getListDriver, getDataUser, getDataDriverInfo,
} from './ListDriverAxios'
import ListDriverPage from './ListDriverPage'
import DriverAdd from './DriverAdd/DriverAdd'
import DriverEdit from './DriverEdit/DriverEdit'
import messaging from '@react-native-firebase/messaging';
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import PushNotification from 'react-native-push-notification'
import Toast from 'react-native-toast-message'
import NetInfo from "@react-native-community/netinfo";
import LostConnection from '../../Component/LostConnection';


class ListDriver extends Component {
    constructor(props) {
        super(props);
        this.state = {
          paramLogin:{
            username:'',
            password:'',
          },
          isLoading:false,
          url:null,
          profile: null,
          isConnected: null,
          isConnectedChange: null,
          listDriver:[],
          driverAdd: false,
          driverEdit: false,
          driverEditData: {},
          driverSelected: null,
          hak_akses: ''
          
        };
      }
    
      componentDidMount= async()=>{

        await this.setState({isLoading:true});
        // this.funcSweetAlert('status', 'sukses tambah pengemudi!', 'success')
        await setUrl();
        await getUrl().then(async(result)=>{
          this.setState({url:result})
        });
        await this.funcGetProfile();
        var _listDriver = await getListDriver(this.state.url, this.state.profile.id_transporter)
        await this.setState({isLoading:false, listDriver: _listDriver});
        await this.pushNotif();
        await this.cekConnection();


      }

      cekConnection = async()=>{
        // Cek Connection
        NetInfo.addEventListener(state => {
          if (this.state.isConnected == null) {
            this.setState({isConnected: state.isConnected})
          }
          if (state.isConnected == false) {
            this.setState({isConnected: state.isConnected, isConnectedChange: state.isConnected})
          }
          if (state.isConnected == true) {
            this.setState({isConnectedChange: state.isConnected})
          }      
          console.log("Is connected?", state.isConnected);
        });
      }

      restartConnection = async() =>{
        console.log('Restart Connection', this.state.isConnected)
        if (this.state.isConnected == false && this.state.isConnectedChange == true) {
          console.log('HARUSNYA INI OFF', this.state.isConnected)
          this.props.navigation.replace('MainRouter', {screen: 'ListDriver'})
        }
      }

      funcGetProfile=async()=>{
        await getProfile().then(result=>{
          if(result!=null){
            this.setState({profileStr:result})
          }
        })
        if(this.state.profileStr==null){
          this.props.navigation.replace('Login')
        }
        var tmpStr = await decrypt(this.state.profileStr);
        await this.setState({profile:JSON.parse(tmpStr)})
        await this.setState({hak_akses: JSON.parse(tmpStr).hak_akses}) // tambah ini utk hak_akses dan state
        console.log('profile', JSON.parse(tmpStr))
      }

      funcSweetAlert = async(title, subTitle, style) =>{
        SweetAlert.showAlertWithOptions({
            title: title,
            subTitle: subTitle,
            confirmButtonTitle: 'OK',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: style,
            cancellable: true
          },
            callback => {
              console.log('callback') 
              this.backToList()
            }
          );
    }

      onDriverAdd=async()=>{
        await this.setState({isLoading:true, driverAdd:false});
        // var _tripSelected = {};
        // _tripSelected.infoTrip = infoTrip;
        // _tripSelected.infoOrder = await getDetailOrder(this.state.url,infoTrip.id);
        // _tripSelected.location = await getLatestLocation(this.state.url,infoTrip.id);
        await this.setState({isLoading:false, driverAdd: true});
      }

      onDriverEdit=async(value)=>{
        await this.setState({isLoading:true, driverEdit: false});
        var _driverSelected = {};
        //id user dari mt_master_user
        // console.log('value driver', value.id)
        // console.log('value driver', value)
        _driverSelected.value = value
        // _driverSelected.getDataUser = await getDataUser(this.state.url, value.id)
        // _driverSelected.getDataDriverInfo = await getDataDriverInfo(this.state.url, value.id)

        // var depan = firebase.linkStorage

        //   //foto driver logic
        //   var _fotoFilename = _driverSelected.getDataDriverInfo[0].foto_driver
        //   _fotoFilename = _fotoFilename.substr(depan.length);
        //   _fotoFilename = _fotoFilename.split('?alt=media')
        //   _fotoFilename = _fotoFilename[0]

        //   //foto ktp logic
        //   var _ktpFilename = _driverSelected.getDataDriverInfo[0].foto_ktp
        //   _ktpFilename = _ktpFilename.substr(depan.length);
        //   _ktpFilename = _ktpFilename.split('?alt=media')
        //   _ktpFilename = _ktpFilename[0]

        //   //foto sim logic
        //   var _simFilename = _driverSelected.getDataDriverInfo[0].foto_sim
        //   _simFilename = _simFilename.substr(depan.length);
        //   _simFilename = _simFilename.split('?alt=media')
        //   _simFilename = _simFilename[0]

        //   _driverSelected.foto_driver = _fotoFilename
        //   _driverSelected.foto_ktp = _ktpFilename
        //   _driverSelected.foto_sim = _simFilename


        await this.setState({isLoading:false, driverEdit: true, driverSelected: _driverSelected});
        // console.log('Hasil tarik data : ', _driverSelected)
      }

      onInputChange=async(factor,value)=>{
        // var tmpParamLogin = this.state.paramLogin;
        var tmpDriverEditData = this.state.driverEditData
        // if(factor=='1'){
        //   tmpDriverEditData.namaPengemudi=value;
        // }else{
        //   tmpParamLogin.password=value;
        // }

        switch (factor) {
          case 1:
            this.state.driverSelected.getDataUser[0].nama=value;
            break;

          case 2:
            this.state.driverSelected.getDataUser[0].hp=value;
            break;

          default:
            break;
        }

        await this.setState({driverEditData:tmpDriverEditData});
        console.log('edit data', this.state.driverSelected)
      }

      backToList=async()=>{
        await this.setState({isLoading:true});
        var _listDriver = await getListDriver(this.state.url, this.state.profile.id_transporter)
        await this.setState({isLoading:false, listDriver: _listDriver, driverAdd: false, driverEdit: false});
      }

      backToListWithoutLoad=async()=>{
        await this.setState({driverAdd: false, driverEdit: false});
      }

      refreshContent = async(_listDriver) => {
        await this.setState({isLoading: true})
        await this.setState({isLoading:false, listDriver: _listDriver});
      }

      pushNotif = async () => {
        // Must be outside of any component LifeCycle (such as `componentDidMount`).
        PushNotification.configure({
          // (optional) Called when Token is generated (iOS and Android)
          onRegister: function (token) {
            console.log("TOKEN:", token);
          },
        
          // (required) Called when a remote is received or opened, or local notification is opened
          onNotification: async (notification) => {
            console.log("NOTIFICATION:", notification);
            //Utk dpt Notif
            // process the notification
            //mau apa disini
            //disini setelah ada userInteraction/di klik
            if (notification.userInteraction) {
              // console.log('DO SOMETHING NOTIFICATION IN DRIVER') 
              // this.props.navigation.navigate('ListTrip',{
              //   screen: 'ListTrip',
              //   id_trip: notification.data.id_trip,
              //   id: notification.data.id
              // })
              this.props.navigation.navigate('Dashboard', {screen: 'Dashboard'})
            }
        
            // (required) Called when a remote is received or opened, or local notification is opened
            notification.finish(PushNotificationIOS.FetchResult.NoData);
          },
  
          
          // // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
          // onAction: function (notification) {
          //   console.log("ACTION:", notification.action);
          //   console.log("NOTIFICATION ACTION:", notification);
        
          //   // process the action
  
          //   console.log('waiwawa')
          // },
        
          // // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
          // onRegistrationError: function(err) {
          //   console.error(err.message, err);
          // },
        
          // // IOS ONLY (optional): default: all - Permissions to register.
          // permissions: {
          //   alert: true,
          //   badge: true,
          //   sound: true,
          // },
        
          // Should the initial notification be popped automatically
          // default: true
          popInitialNotification: true,
        
          /**
           * (optional) default: true
           * - Specified if permissions (ios) and token (android and ios) will requested or not,
           * - if not, you must call PushNotificationsHandler.requestPermissions() later
           * - if you are not using remote notification or do not have Firebase installed, use this:
           *     requestPermissions: Platform.OS === 'ios'
           */
          requestPermissions: true,
        });
      }

      render(){
        return(
          <>
          <Container>
            {/* <CustomNavbar title={'List Driver'}/> */}

            {this.state.driverAdd == true ?  
              <CustomNavbar buttonBack={this.backToListWithoutLoad} title={'Tambah Pengemudi'}/> : 
              this.state.driverEdit == true ? 
              <CustomNavbar buttonBack={this.backToListWithoutLoad} title={'Edit Pengemudi'}/> :
              <CustomNavbar title={'Daftar Pengemudi'}/>
            }

          {
            this.state.isConnected == false?
              <LostConnection restartConnection={this.restartConnection}/> 
            :
            this.state.driverAdd ? 

            <DriverAdd
              {...this.state}
              backToList={this.backToList}
              funcSweetAlert={this.funcSweetAlert}
              // {...this.props}
              />
              
              :

              this.state.driverEdit ? 

              <DriverEdit
              {...this.state}
              onInputChange={this.onInputChange}
              backToList={this.backToList}
              funcSweetAlert={this.funcSweetAlert}
              // {...this.props}
              /> 
              
              :

              <ListDriverPage 
                  {...this.state}
                  {...this.props}
                  onDriverAdd = {this.onDriverAdd}
                  onDriverEdit = {this.onDriverEdit}
                  refreshContent = {this.refreshContent}
                />

              
            }

                {/* <ListDriverPage 
                  {...this.state}
                  {...this.props}
                /> */}

          </Container>

            {this.state.isLoading ? <Loading/> : false}
          </>
        );
      }
}

ListDriverFunc= (props) => {
  const navigation = useNavigation();
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      BackHandler.addEventListener("hardwareBackPress", ()=>props.backAction('ListDriver', navigation));
    });

    return unsubscribe;
  }, [navigation]);

  return <ListDriver navigation={navigation}/>;
}

export default ListDriverFunc;