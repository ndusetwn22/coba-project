import React,{Component} from 'react'
import {View, ScrollView, RefreshControl, ToastAndroid} from "react-native";
import styles from './style'
import {Text,Button, Content, Container, Icon} from 'native-base'
import FlatListJadwalKapal from './Component/FlatListJadwalKapal/FlatListJadwalKapal';
import DateRange from './Component/DateRange/DateRange'
import ModalCalendar from './Component/DateRange/ModalCalendar'
import SearchInput,{ createFilter } from 'react-native-search-filter';
import Toast from 'react-native-toast-message'
import { getListJadwalKapal } from './ListJadwalKapalAxios';

const KEYS_TO_FILTERS = ['vessel_name'];


class ListJadwalKapalPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paramSearch:'',
            modalCalenderVisible: false,
            refreshing: false
        };
    }

    componentDidMount= async()=>{

    }

    onSelectDate=async (start,end)=>{
        await this.props.setStartEndDate(start,end);
        await this.setState({modalCalenderVisible: false})
        await this.props.onSearchButtonClicked();
    }

    openModalCalendar=(typeTrip)=>{
        this.setState({modalCalenderVisible: true})
    }

    closeModal=()=>{
        this.setState({modalCalenderVisible: false})
    }

    onRefresh= async() => {
        this.setState({refreshing: true})
        console.log('REFRESHHHHHH')
        var _listJadwalKapal = await getListJadwalKapal(this.props.url, this.props.profile.id_transporter, this.props.startDate, this.props.endDate)


            await this.props.refreshContent(_listJadwalKapal)
            
            // Toast.show({
            //     type: 'info',
            //     position: 'bottom',
            //     text1: 'Refreshing',
            //     text2: 'Refresh data ...',
            //     visibilityTime: 3000,
            //     autoHide: true,
            //     topOffset: 30,
            //     bottomOffset: 40,
            //     onShow: () => {},
            //     onHide: () => {}
            //   })
  
            ToastAndroid.show('Refreshing', ToastAndroid.SHORT);
            this.setState({refreshing: false})
  
      };

    render(){
        const filteredJadwalKapal = this.props.listJadwalKapal.filter(createFilter(this.state.paramSearch, KEYS_TO_FILTERS))
        return(
          <>
          {/* <View style={{flex: 1}}>
                <Text>
                    Jadwal Kapal
                </Text>
          </View> */}


                {/* <View style={styles.flexGridNoPadding}>
                    <View style={{flex:1}}>
                        <DateRange openModalCalendar={this.openModalCalendar} {...this.props}/>
                    </View>
                </View>
                <Text>WKWKWKWK</Text>
                    <FlatListJadwalKapal {...this.state} {...this.props}/> */}

                <View style={styles.flexGridNoPadding}>
                    <View style={{flex:1}}>
                        <DateRange openModalCalendar={this.openModalCalendar} {...this.props}/>
                    </View>
                </View>
                
                
                <FlatListJadwalKapal {...this.state} {...this.props} onRefresh={this.onRefresh}/>
            <ModalCalendar {...this.state} {...this.props} onSelectDate={this.onSelectDate} closeModal={this.closeModal}/>
            <Toast ref={(ref) => Toast.setRef(ref)} />
          </>
        );
    }
}

export default ListJadwalKapalPage;