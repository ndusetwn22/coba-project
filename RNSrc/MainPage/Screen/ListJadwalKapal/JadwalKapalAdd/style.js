import { StyleSheet, Platform } from 'react-native';

const styles = StyleSheet.create({
  
    
  title:{
    fontSize: 18,
    fontWeight: 'bold', 
    textAlign: 'center', 
    marginTop: 15, 
    // marginBottom: 5, 
    color: '#7d8d8c'
  },

  viewTextInput:{
    flex: 1, 
    flexDirection: 'column'
  },

  label:{
    fontSize: 14, 
    marginLeft: 10, 
    marginTop: 10, 
    alignSelf: 'flex-start', 
    alignItems: 'flex-start'
  },

  textInputTransporter:{
    flex:1, 
    borderColor: 'gray', 
    borderWidth: 2, 
    borderRadius: 5, 
    marginTop: 10, 
    marginLeft: 20, 
    marginRight: 20, 
    height: 40, 
    backgroundColor: '#E9EDF2', 
    color: '#494A4A', 
    paddingLeft: 10, 
    paddingRight: 10, 
    fontSize: 14 
  },

  textInput:{
    flex:1, 
    borderColor: 'gray', 
    borderWidth: 2, 
    borderRadius: 5, 
    marginTop: 10, 
    marginLeft: 20, 
    marginRight: 20, 
    height: 40, 
    color: '#494A4A', 
    paddingLeft: 10, 
    paddingRight: 10, 
    fontSize: 14
  },

  viewButton:{
    flex: 1, 
    flexDirection: 'column'
  },

  button:{
    width: '89%', 
    // borderColor: 'gray', 
    // borderWidth: 2, 
    borderRadius: 5, 
    marginTop: 10, 
    marginLeft: 20, 
    marginRight: 20, 
    height: 40, 
    // color: '#494A4A',
    backgroundColor: '#008c45', 
    padding: 10 
  },
  
  mapStyle: {
    width: '100%',
    height: 250,
    flex: 1,
    padding:10,
    // margin: 10,

  },





});

export default styles;