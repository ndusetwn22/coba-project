import Axios from 'axios';
import {encrypt, firebase} from '../../../../../APIProp/ApiConstants';
import {Alert} from 'react-native'
//firebase
import { utils } from '@react-native-firebase/app';
import storage from '@react-native-firebase/storage';

  export const getKotaPelabuhan = async (url) =>{
    // let dataQry = `
    //     select * from mt_master_pelabuhan
    // `

    let dataQry = `
        select * from mobiletra_getlistpelabuhan()
    `

    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('get Kota Pelabuhan : ', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
      console.log('error', err)
    });
  }

  export const getPelayaran = async (url, trans_id) =>{
    let dataQry = `
        select * from mobiletra_getlistpelayaran(`+trans_id+`)
    `

    // let dataQry = `
    //   select * from mt_master_pelayaran mmp 
    //   left join mt_mapping_pelayaran mmp2
    //   on mmp.id = mmp2.id_pelayaran 
    //   where mmp2.id_transporter = `+trans_id+`
    // `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('get Pelayaran : ', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
      console.log('error', err)
    });
  }



  export const insertJadwalKapal = async (url, trid, uid, asal, tujuan, namaKapal, pelayaran, tipeRute, etd, eta, dooring, closing_date) =>{

    // let dataQry = `
    //     insert into mt_master_schedule 
    //     (vessel_name, ship_id, asal, tujuan, type, etd, eta, est_dooring, closing_date, create_date, create_by, update_date, update_by)
    //     values('`+namaKapal+`', `+pelayaran+`, `+asal+`, `+tujuan+`, '`+tipeRute+`', '`+etd+`', '`+eta+`', '`+dooring+`', '`+closing_date+`', now(), `+uid+`, now(), `+uid+`)
    //     returning id as id_return
    // `

    let dataQry = `
        select * from mobiletra_insertjadwalkapal('`+namaKapal+`', `+pelayaran+`, `+asal+`, `+tujuan+`, '`+tipeRute+`', '`+etd+`', '`+eta+`', '`+dooring+`', '`+closing_date+`', `+uid+`)
    `

    //test sama
    // let dataQry = `
    // insert into mt_master_schedule 
    //     (vessel_name, ship_id, asal, tujuan, type, etd, eta, est_dooring, closing_date, create_date, create_by, update_date, update_by)
    //     values('TEST122', 1, 2, 1, 'D', '2020-11-19', '2020-11-29', '2020-12-01', '2020-12-24', now(), 18, now(), 18)
    //     returning id as id_return
    //     `

    // let dataQry = `
    // select * from mobiletra_insertjadwalkapal('TEST122', 1, 2, 1, 'D', '2020-11-19', '2020-11-29', '2020-12-01', '2020-12-24', 18)
    // `

    // let dataQry = `
    //     select * from mobiletra_insertkendaraan('`+refId+`', '`+platNomor+`', `+jenisKendaraan+`, '`+status+`', `+pool+`, '`+dedicated+`', '`+nomorKir+`', '`+jatuhTempoKir+`', `+tahunPembuatan+`, `+uid+`)
    // `
    console.log('insert jadwal kapal : ', dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;

      console.log('sukses query', value)


      return ['Sukses tambah jadwal kapal!', 'success'];

    }).catch(err=>{
    //   return [];
      console.log('error', err)
      return ['Gagal tambah jadwal kapal!', 'warning'];
    });
  }
