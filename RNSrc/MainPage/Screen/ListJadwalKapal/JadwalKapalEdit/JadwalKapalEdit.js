import React,{Component} from 'react'
import { AppState,Platform,Dimensions, Image, FlatList , TouchableOpacity,ScrollView, ImageBackground, BackHandler, Alert, PixelRatio, StyleSheet, TextInput, Picker } from "react-native";
import { useNavigation  } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
import CustomNavbar from '../../../Header/Navbar/Navbar'
import Loading,{getProfile, encrypt, setUrl, getUrl, decrypt} from '../../../../../APIProp/ApiConstants'
import Axios from 'axios';
import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Left,
    Right,
    Body,
    Text,
    Icon,
    Card,
    CardItem,
    Grid,
    Row,
    View,
    Col, 
    List,
    ListItem,
    Thumbnail,
    Form,
    Label,
    Input,
    Item,
    DatePicker
} from "native-base";
import styles from "./style";
import moment from 'moment'

//image picker
import ImagePicker from 'react-native-image-picker';

//Google Places
import RNGooglePlaces from 'react-native-google-places';

//Axios
import { getKotaPelabuhan, getPelayaran, editJadwalKapal, getDataJadwalKapal } from './JadwalKapalEditAxios';

//firebase
import { utils } from '@react-native-firebase/app';
import storage from '@react-native-firebase/storage';

var { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE = -6.117664;
const LONGITUDE = 106.906349;
const LATITUDE_DELTA = 1;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const id = 1;

const arrMonth =['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus','September','Oktober','November', 'Desember'];


class JadwalKapalEdit extends Component {
    constructor(props) {
            super(props);
            this.state = {
            paramLogin:{
                username:'',
                password:'',
            },
            isLoading:false,
            url:null,
            profile: null,
            id: null,
            allPelayaran: [],
            pelayaranSelected: '',
            allKotaPelabuhan: [],
            asal: '',
            tujuan: '',
            namaKapal: '',
            tipePelayaran: 'D',
            chosenDate: new Date(),
            now: 0,
            month: 0,
            year: 0,
            chosenDate_eta: new Date(),
            now_eta: 0,
            month_eta: 0,
            year_eta: 0,
            chosenDate_dooring: new Date(),
            now_dooring: 0,
            month_dooring: 0,
            year_dooring: 0,
            chosenDate_closing: new Date(),
            now_closing: 0,
            month_closing: 0,
            year_closing: 0,
            isVisibleDate: true,
        };
      }
    
      componentDidMount= async()=>{
        await this.setState({isLoading:true});
        await setUrl();
        await getUrl().then(async(result)=>{
          this.setState({url:result})
        });

        await this.funcGetProfile();

        var curDate = new Date(); //Date
        var date= curDate.getDate();
        var month = curDate.getMonth(); //Current Month
        var year = curDate.getFullYear(); //Current Year
        var nowDate = year+"-"+month+"-"+date;

        await this.setState({
            now: date,
            month: month,
            year: year,
            now_eta: date,
            month_eta: month,
            year_eta: year,
            now_dooring: date,
            month_dooring: month,
            year_dooring: year,
            now_closing: date,
            month_closing: month,
            year_closing: year
        })

        var _getKotaPelabuhan = await getKotaPelabuhan(this.state.url);
        var _getPelayaran = await getPelayaran(this.state.url, this.state.profile.id_transporter);

        var _getDataJadwalKapal = await getDataJadwalKapal(this.state.url, this.props.jadwalKapalSelected.value.id)
        // await this.getJenisKendaraan()
        // await this.getPool()
        // await this.getData()

        await this.setState({
          allKotaPelabuhan: _getKotaPelabuhan, 
          allPelayaran: _getPelayaran,

          id: _getDataJadwalKapal.id,
          asal: _getDataJadwalKapal.asal,
          tujuan : _getDataJadwalKapal.tujuan,
          namaKapal: _getDataJadwalKapal.vessel_name,
          pelayaranSelected: _getDataJadwalKapal.ship_id,
          tipePelayaran: _getDataJadwalKapal.type,
        //   etd: _getDataJadwalKapal.etd,
        //   eta: _getDataJadwalKapal.eta,
        //   est_dooring: _getDataJadwalKapal.est_dooring,
          chosenDate: new Date(_getDataJadwalKapal._yearEtd, _getDataJadwalKapal._monthEtd, _getDataJadwalKapal._nowEtd),
          chosenDate_eta: new Date(_getDataJadwalKapal._yearEta, _getDataJadwalKapal._monthEta, _getDataJadwalKapal._nowEta),
          chosenDate_dooring: new Date(_getDataJadwalKapal._yearDooring, _getDataJadwalKapal._monthDooring, _getDataJadwalKapal._nowDooring),
          chosenDate_closing: new Date(_getDataJadwalKapal._yearClosing, _getDataJadwalKapal._monthClosing, _getDataJadwalKapal._nowClosing)

        })

        console.log('tgl date : ', this.state.chosenDate_dooring.getDate())
        console.log('tgl day : ', this.state.chosenDate_dooring.getDay())


        // await this.setState({});
        setTimeout(() => {this.setState({isLoading: false})}, 3000)

      }


      convDateString =(param)=>{
        var today = new Date(param);
        // console.log('param', param)
        today = new Date(today.setHours(today.getHours())); //jgn pake -1 klo add/edit --> nanti tgl 9 bakal ilang
        return today.getDate() + " " + arrMonth[today.getMonth()] + " " + today.getFullYear();
        }

      setDate = (newDate) => {
        this.setState({ chosenDate: newDate});
        // this.setState({ chosenDate: newDate, isVisibleDate: false });

        var _temp = newDate

        var _convert = this.convDateString(_temp.getFullYear() + "-" + (_temp.getMonth()+1) + "-" + _temp.getDate())
    
        // return _convert
        console.log('chosen date: ', newDate)
        console.log('convert :',  _convert)
      }

      setDate_eta = (newDate) => {
        this.setState({ chosenDate_eta: newDate});

        var _temp = newDate

        var _convert = this.convDateString(_temp.getFullYear() + "-" + (_temp.getMonth()+1) + "-" + _temp.getDate())
    
        // return _convert
        console.log('chosen date: ', newDate)
        console.log('convert :',  _convert)
      }

      setDate_dooring = (newDate) => {
        this.setState({ chosenDate_dooring: newDate});

        var _temp = newDate

        var _convert = this.convDateString(_temp.getFullYear() + "-" + (_temp.getMonth()+1) + "-" + _temp.getDate())
    
        // return _convert
        console.log('chosen date: ', newDate)
        console.log('convert :',  _convert)
      }

      setDate_closing = (newDate) => {
        this.setState({ chosenDate_closing: newDate});

        var _temp = newDate

        var _convert = this.convDateString(_temp.getFullYear() + "-" + (_temp.getMonth()+1) + "-" + _temp.getDate())
    
        // return _convert
        console.log('chosen date: ', newDate)
        console.log('convert closing:',  _convert)
      }

      funcGetProfile=async()=>{
        await getProfile().then(result=>{
          if(result!=null){
            this.setState({profileStr:result})
          }
        })
        if(this.state.profileStr==null){
          this.props.navigation.replace('Login')
        }
        var tmpStr = await decrypt(this.state.profileStr);
        this.setState({profile:JSON.parse(tmpStr)})
        console.log('profile', JSON.parse(tmpStr))
      }
    
      editJadwalKapal=async ()=>{
        // this.setState({buttondisabled: true});
        let title = 'Invalid Input';
        if(this.state.asal === '' ) Alert.alert(title, 'Asal pelabuhan tidak boleh kosong!');
        else if(this.state.tujuan === '') Alert.alert(title, 'Tujuan pelabuhan tidak boleh kosong!');
        else if(this.state.namaKapal === '') Alert.alert(title, 'Nama Kapal tidak boleh kosong!');
        else if(this.state.pelayaranSelected === '') Alert.alert(title, 'Pelayaran tidak boleh kosong!');
        else if(this.state.tipePelayaran === '') Alert.alert(title, 'Tipe rute tidak boleh kosong!');
        else if(this.state.chosenDate === null) Alert.alert(title, 'Estimasi keberangkatan tidak boleh kosong!');
        else if(this.state.chosenDate_eta === null) Alert.alert(title, 'Estimasi tiba tidak boleh kosong!');
        else if(this.state.chosenDate_dooring === null) Alert.alert(title, 'Estimasi dooring tidak boleh kosong!');
        else if(this.state.chosenDate_closing === null) Alert.alert(title, 'Closing date tidak boleh kosong!');
        else{
          
        //  console.warn('masuk query')   
          this.setState({isLoading:true});
    
        let id = this.state.id
        let asal = this.state.asal
        let tujuan = this.state.tujuan
        let namaKapal = this.state.namaKapal.toUpperCase()
        let pelayaran = this.state.pelayaranSelected
        let tipeRute = this.state.tipePelayaran
        let etd = this.state.chosenDate.getFullYear() + "-" + (this.state.chosenDate.getMonth()+1) + "-" + this.state.chosenDate.getDate()
        let eta = this.state.chosenDate_eta.getFullYear() + "-" + (this.state.chosenDate_eta.getMonth()+1) + "-" + this.state.chosenDate_eta.getDate()
        let dooring = this.state.chosenDate_dooring.getFullYear() + "-" + (this.state.chosenDate_dooring.getMonth()+1) + "-" + this.state.chosenDate_dooring.getDate()
        let closing_date = this.state.chosenDate_closing.getFullYear() + "-" + (this.state.chosenDate_closing.getMonth()+1) + "-" + this.state.chosenDate_closing.getDate()

        // console.warn('nama kapal : ', namaKapal.toUpperCase())
        // console.warn('etd', etd)
        // console.warn('eta', eta)
        // console.warn('dooring', dooring)

        let _editJadwalKapal = await editJadwalKapal(this.state.url, this.state.profile.id_user, id ,asal, tujuan, namaKapal, pelayaran, tipeRute, etd, eta, dooring, closing_date)




        this.props.funcSweetAlert('status',_editJadwalKapal[0], _editJadwalKapal[1])

        // this.props.funcSweetAlert('status','test callback', 'success')

        
        this.setState({isLoading:false});


        }
      }



      render(){

        // var day_dooring = this.state.chosenDate_dooring.getDate()
        // day_dooring = parseInt(day_dooring)
        // if (day_dooring < 10) {
        //     day_dooring = day_dooring+1
        // }
        // console.warn('day : ', day_dooring)
        return(
          <>
            <Content>

            {/* <Text style={{flex: 1}}>JADWAL EDIT</Text> */}

            <View style={{flex: 1, flexDirection: 'column'}}>   
                    <Label style={{fontSize: 14, marginLeft: 10, marginTop: 10, alignSelf: 'flex-start', alignItems: 'flex-start'}}>Pelabuhan Asal</Label>
                        <View 
                            style={{flex: 1,  
                            alignItems: 'center',  
                            justifyContent: 'center',
                            borderColor: 'gray', 
                            borderWidth: 2, 
                            borderRadius: 5, 
                            marginTop: 10, 
                            marginLeft: 20, 
                            marginRight: 20, height: 40, color: '#494A4A', padding: 10 }}>

                                
                                        <Picker
                                            style={{ 
                                            width: "100%",
                                            color: '#344953',  
                                            justifyContent: 'center',
                                            }}
                                            selectedValue={this.state.asal}
                                            onValueChange={(itemValue, itemIndex) => 
                                            this.setState({asal: itemValue})
                                            }
                                        >
                                            {this.state.allKotaPelabuhan.map((value, index) => {
                                                return (
                                                    <Picker.Item
                                                    label={value.nama}
                                                    value={value.id}
                                                    key={index}
                                                    />
                                                );
                                                })}
                                        </Picker>

                        </View>
                </View>

                <View style={{flex: 1, flexDirection: 'column'}}>   
                    <Label style={{fontSize: 14, marginLeft: 10, marginTop: 10, alignSelf: 'flex-start', alignItems: 'flex-start'}}>Pelabuhan Tujuan</Label>
                        <View 
                            style={{flex: 1,  
                            alignItems: 'center',  
                            justifyContent: 'center',
                            borderColor: 'gray', 
                            borderWidth: 2, 
                            borderRadius: 5, 
                            marginTop: 10, 
                            marginLeft: 20, 
                            marginRight: 20, height: 40, color: '#494A4A', padding: 10 }}>

                                
                                        <Picker
                                            style={{ 
                                            width: "100%",
                                            color: '#344953',  
                                            justifyContent: 'center',
                                            }}
                                            selectedValue={this.state.tujuan}
                                            onValueChange={(itemValue, itemIndex) => 
                                            this.setState({tujuan: itemValue})
                                            }
                                        >
                                            {this.state.allKotaPelabuhan.map((value, index) => {
                                                return (
                                                    <Picker.Item
                                                    label={value.nama}
                                                    value={value.id}
                                                    key={index}
                                                    />
                                                );
                                                })}
                                        </Picker>

                        </View>
                </View>


                <View style={styles.viewTextInput}>
                    <Label style={styles.label}>Nama Kapal</Label>
                    <TextInput
                        style={styles.textInput}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder="Nama Kapal"
                        placeholderTextColor = "#CACCD7"
                        selectionColor="#008c45"
                        onChangeText={input=>this.setState({namaKapal: input})}
                        value={this.state.namaKapal}
                        
                        />
                </View>


                <View style={{flex: 1, flexDirection: 'column'}}>   
                    <Label style={{fontSize: 14, marginLeft: 10, marginTop: 10, alignSelf: 'flex-start', alignItems: 'flex-start'}}>Nama Pelayaran</Label>
                        <View 
                            style={{flex: 1,  
                            alignItems: 'center',  
                            justifyContent: 'center',
                            borderColor: 'gray', 
                            borderWidth: 2, 
                            borderRadius: 5, 
                            marginTop: 10, 
                            marginLeft: 20, 
                            marginRight: 20, height: 40, color: '#494A4A', padding: 10 }}>

                                
                                        <Picker
                                            style={{ 
                                            width: "100%",
                                            color: '#344953',  
                                            justifyContent: 'center',
                                            }}
                                            selectedValue={this.state.pelayaranSelected}
                                            onValueChange={(itemValue, itemIndex) => 
                                            this.setState({pelayaranSelected: itemValue})
                                            }
                                        >
                                            {this.state.allPelayaran.map((value, index) => {
                                                return (
                                                    <Picker.Item
                                                    label={value.nama}
                                                    value={value.id}
                                                    key={index}
                                                    />
                                                );
                                                })}
                                        </Picker>

                        </View>
                </View>


                <View style={{flex: 1, flexDirection: 'column'}}>   
                    <Label style={{fontSize: 14, marginLeft: 10, marginTop: 10, alignSelf: 'flex-start', alignItems: 'flex-start'}}>Tipe rute</Label>
                        <View 
                            style={{flex: 1,  
                            alignItems: 'center',  
                            justifyContent: 'center',
                            borderColor: 'gray', 
                            borderWidth: 2, 
                            borderRadius: 5, 
                            marginTop: 10, 
                            marginLeft: 20, 
                            marginRight: 20, height: 40, color: '#494A4A', padding: 10 }}>
                                
                                    <Picker
                                        style={{ 
                                          width: "100%",
                                          color: '#344953',  
                                          justifyContent: 'center',
                                        }}
                                        selectedValue={this.state.tipePelayaran}
                                        onValueChange={(itemValue, itemIndex) => 
                                          this.setState({tipePelayaran: itemValue})
                                          }
                                      >
                                        <Picker.Item label="Direct" value="D" />
                                        <Picker.Item label="Transit" value="T" />
                                      </Picker>

                        </View>
                </View>


                <View style={styles.viewTextInput}>   
                        <Label style={styles.label}>Closing Date</Label>

                        <View style={styles.textInput}>
                                <DatePicker
                                    defaultDate={new Date(this.state.year_closing, this.state.month_closing, this.state.now_closing)}
                                    minimumDate={new Date(this.state.year_closing, this.state.month_closing, this.state.now_closing)}
                                    // maximumDate={new Date(2018, 12, 31)}
                                    locale={"en"}
                                    timeZoneOffsetInMinutes={undefined}
                                    modalTransparent={false}
                                    animationType={"fade"}
                                    androidMode={"default"}
                                    // placeHolderText={ this.state.chosenDate.getDate()+ "-" + (this.state.chosenDate.getMonth()+1) + "-" + this.state.chosenDate.getFullYear()}
                                    placeHolderText={ this.convDateString(this.state.chosenDate_closing.getFullYear() + "-" + (this.state.chosenDate_closing.getMonth()+1) + "-" +(this.state.chosenDate_closing.getDate()))}
                                    textStyle={{ color: "#008c45", marginTop: -5, marginLeft: -5}}
                                    placeHolderTextStyle={{ color: "black", marginTop: -5, marginLeft: -5 }}
                                    onDateChange={this.setDate_closing}
                                    disabled={false}
                                />
                        </View>
                </View>



                <View style={styles.viewTextInput}>   
                        <Label style={styles.label}>Estimasi Keberangkatan (ETD)</Label>

                        <View style={styles.textInput}>
                                <DatePicker
                                    defaultDate={new Date(this.state.year, this.state.month, this.state.now)}
                                    minimumDate={new Date(this.state.year, this.state.month, this.state.now)}
                                    // maximumDate={new Date(2018, 12, 31)}
                                    locale={"en"}
                                    timeZoneOffsetInMinutes={undefined}
                                    modalTransparent={false}
                                    animationType={"fade"}
                                    androidMode={"default"}
                                    // placeHolderText={ this.state.chosenDate.getDate()+ "-" + (this.state.chosenDate.getMonth()+1) + "-" + this.state.chosenDate.getFullYear()}
                                    placeHolderText={ this.convDateString(this.state.chosenDate.getFullYear() + "-" + (this.state.chosenDate.getMonth()+1) + "-" + this.state.chosenDate.getDate())}
                                    textStyle={{ color: "#008c45", marginTop: -5, marginLeft: -5}}
                                    placeHolderTextStyle={{ color: "black", marginTop: -5, marginLeft: -5 }}
                                    onDateChange={this.setDate}
                                    disabled={false}
                                />
                        </View>
                </View>

                <View style={styles.viewTextInput}>   
                        <Label style={styles.label}>Estimasi Tiba (ETA)</Label>

                        <View style={styles.textInput}>
                                <DatePicker
                                    defaultDate={new Date(this.state.year_eta, this.state.month_eta, this.state.now_eta)}
                                    minimumDate={new Date(this.state.year_eta, this.state.month_eta, this.state.now_eta)}
                                    // maximumDate={new Date(2018, 12, 31)}
                                    locale={"en"}
                                    timeZoneOffsetInMinutes={undefined}
                                    modalTransparent={false}
                                    animationType={"fade"}
                                    androidMode={"default"}
                                    // placeHolderText={ this.state.chosenDate.getDate()+ "-" + (this.state.chosenDate.getMonth()+1) + "-" + this.state.chosenDate.getFullYear()}
                                    placeHolderText={ this.convDateString(this.state.chosenDate_eta.getFullYear() + "-" + (this.state.chosenDate_eta.getMonth()+1) + "-" + this.state.chosenDate_eta.getDate())}
                                    textStyle={{ color: "#008c45", marginTop: -5, marginLeft: -5}}
                                    placeHolderTextStyle={{ color: "black", marginTop: -5, marginLeft: -5 }}
                                    onDateChange={this.setDate_eta}
                                    disabled={false}
                                />
                        </View>
                </View>

                <View style={styles.viewTextInput}>   
                        <Label style={styles.label}>Estimasi Dooring</Label>

                        <View style={styles.textInput}>
                                <DatePicker
                                    defaultDate={new Date(this.state.year_dooring, this.state.month_dooring, this.state.now_dooring)}
                                    minimumDate={new Date(this.state.year_dooring, this.state.month_dooring, this.state.now_dooring)}
                                    // maximumDate={new Date(2018, 12, 31)}
                                    locale={"en"}
                                    timeZoneOffsetInMinutes={undefined}
                                    modalTransparent={false}
                                    animationType={"fade"}
                                    androidMode={"default"}
                                    // placeHolderText={ this.state.chosenDate.getDate()+ "-" + (this.state.chosenDate.getMonth()+1) + "-" + this.state.chosenDate.getFullYear()}
                                    placeHolderText={ this.convDateString(this.state.chosenDate_dooring.getFullYear() + "-" + (this.state.chosenDate_dooring.getMonth()+1) + "-" +(this.state.chosenDate_dooring.getDate()))}
                                    textStyle={{ color: "#008c45", marginTop: -5, marginLeft: -5}}
                                    placeHolderTextStyle={{ color: "black", marginTop: -5, marginLeft: -5 }}
                                    onDateChange={this.setDate_dooring}
                                    disabled={false}
                                />
                        </View>
                </View>

                



                <View style={{flex: 1, flexDirection: 'column', marginBottom: 15, marginTop: 10}}>   
                        <Button
                        style={[styles.button, {justifyContent: 'center', alignItems: 'center'}]}
                            onPress={this.editJadwalKapal}
                
                        >
                        <Text uppercase={false} style={{fontSize: 14, textAlign: "center"}}>Edit Jadwal Kapal</Text>
                        </Button>
                </View>  
    

            </Content>

            {this.state.isLoading ? <Loading/> : false}
          </>
        );
      }
}

export default JadwalKapalEdit;