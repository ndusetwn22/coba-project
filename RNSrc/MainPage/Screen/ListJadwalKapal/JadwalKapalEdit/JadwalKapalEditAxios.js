import Axios from 'axios';
import {encrypt, firebase} from '../../../../../APIProp/ApiConstants';
import {Alert} from 'react-native'
//firebase
import { utils } from '@react-native-firebase/app';
import storage from '@react-native-firebase/storage';

  export const getKotaPelabuhan = async (url) =>{
    // let dataQry = `
    //     select * from mt_master_pelabuhan
    // `

    let dataQry = `
        select * from mobiletra_getlistpelabuhan()
    `
    
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('get Kota Pelabuhan : ', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
      console.log('error', err)
    });
  }

  export const getPelayaran = async (url, trans_id) =>{
    let dataQry = `
        select * from mobiletra_getlistpelayaran(`+trans_id+`)
    `

    // let dataQry = `
    //   select * from mt_master_pelayaran mmp 
    //   left join mt_mapping_pelayaran mmp2
    //   on mmp.id = mmp2.id_pelayaran 
    //   where mmp2.id_transporter = `+trans_id+`
    // `

    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('get Pelayaran : ', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
      console.log('error', err)
    });
  }




  export const editJadwalKapal = async (url, uid, id, asal, tujuan, namaKapal, pelayaran, tipeRute, etd, eta, dooring, closing) =>{
  
    // let dataQry = `
    //     update mt_master_schedule SET
    //     vessel_name = '`+namaKapal+`',
    //     ship_id = `+pelayaran+`,
    //     asal = `+asal+`,
    //     tujuan = `+tujuan+`,
    //     type = '`+tipeRute+`',
    //     etd = '`+etd+`',
    //     eta = '`+eta+`',
    //     est_dooring = '`+dooring+`',
    //     closing_date = '`+closing+`',
    //     update_date = now(),
    //     update_by = '`+uid+`'
    //     WHERE id = `+id+`
    //     returning id as return_id
    // `

    let dataQry = `
        select * from mobiletra_editjadwalkapal(`+id+`, '`+namaKapal+`', `+pelayaran+`, `+asal+`, `+tujuan+`, '`+tipeRute+`', '`+etd+`', '`+eta+`', '`+dooring+`', '`+closing+`', `+uid+`)
    `

    console.log('edit schedule query : ', dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data[0];
      console.log('sukses edit kendaraan', x)

      if (x.return_id == id) {
          return ['Sukses edit jadwal!', 'success'];
      }else{
          return ['Gagal edit jadwal!', 'warning'];
      }
    
    }).catch(err=>{
    //   return [];
      console.log('error', err)
      return ['Gagal edit jadwal!', 'warning'];
    });
  }


  export const getDataJadwalKapal = async (url, id) =>{

    let dataQry = `
        select * from mt_master_schedule mms where mms.id = `+id+`
    `

    // let dataQry = `
    //     select * from mobiletra_insertkendaraan('`+refId+`', '`+platNomor+`', `+jenisKendaraan+`, '`+status+`', `+pool+`, '`+dedicated+`', '`+nomorKir+`', '`+jatuhTempoKir+`', `+tahunPembuatan+`, `+uid+`)
    // `
    console.log('get jadwal kapal : ', dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
        let x = value.data.data[0]

        let _eta = x.eta
        var _tglEta = _eta.split("-")
        var _yearEta = _tglEta[0]
        var _monthEta = _tglEta[1]-1 //minus 1 karena pickernya pakai index array-_-
        var _nowEta = _tglEta[2].substring(0,2) //salah disini;(
      
        let _etd = x.etd
        var _tglEtd = _etd.split("-")
        var _yearEtd = _tglEtd[0]
        var _monthEtd = _tglEtd[1]-1 //minus 1 karena pickernya pakai index array-_-
        var _nowEtd = _tglEtd[2].substring(0,2) //salah disini;(


        let _dooring = x.est_dooring
        var _tglDooring= _dooring.split("-")
        var _yearDooring = _tglDooring[0]
        var _monthDooring = _tglDooring[1]-1 //minus 1 karena pickernya pakai index array-_-
        var _nowDooring = _tglDooring[2].substring(0,2) //salah disini;(

        let _closing = x.closing_date
        var _tglClosing= _closing.split("-")
        var _yearClosing = _tglClosing[0]
        var _monthClosing = _tglClosing[1]-1 //minus 1 karena pickernya pakai index array-_-
        var _nowClosing = _tglClosing[2].substring(0,2) //salah disini;(

        x._nowEta = _nowEta
        x._monthEta = _monthEta
        x._yearEta = _yearEta

        x._nowEtd = _nowEtd
        x._monthEtd = _monthEtd
        x._yearEtd = _yearEtd

        x._nowDooring = _nowDooring
        x._monthDooring = _monthDooring
        x._yearDooring = _yearDooring

        x._nowClosing = _nowClosing
        x._monthClosing = _monthClosing
        x._yearClosing = _yearClosing
        // x.tahunPembuatan = tahunPembuatan
      

        console.log('sukses data jadwal kapal : ', x)
        if(x==""){
          return [];
        }
        return x;

    }).catch(err=>{
    //   return [];
      console.log('error', err)
      return [];
    });
  }
