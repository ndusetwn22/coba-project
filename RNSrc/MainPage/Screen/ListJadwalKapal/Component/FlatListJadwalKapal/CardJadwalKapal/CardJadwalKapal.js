import React,{Component} from 'react'
import {
    View
} from "react-native";
import styles from './style'
import {
    Text,  
    Card,
    CardItem,
    Body,
    Left,
    Right,
    Button,
    Icon,
    Thumbnail,
    Badge
} from 'native-base'
import { TouchableOpacity } from 'react-native-gesture-handler';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';

const arrMonth =['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus','September','Oktober','November', 'Desember'];


class CardJadwalKapal extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount= async()=>{

    } 

    convDateString =(param)=>{
        var today = new Date(param);
        // console.log('param', param)
        today = new Date(today.setHours(today.getHours()-1));
        return today.getDate() + " " + arrMonth[today.getMonth()] + " " + today.getFullYear();
    }

    render(){
        const hak_akses = this.props.hak_akses

        if (this.props.value.est_dooring != null && this.props.value.est_dooring != undefined && this.props.value.est_dooring != '') {

            var est_dooring = this.props.value.est_dooring.split('T')[0]
            // var __est_dooring = _est_dooring.split('-')
            // var est_dooring = __est_dooring[0]+':'+__est_dooring[1]

        }

        if (this.props.value.etd != null && this.props.value.etd != undefined && this.props.value.etd != '') {

            var etd = this.props.value.etd.split('T')[0]
            // var __etd = _etd.split(':')
            // var etd = __etd[0]+':'+__etd[1]

        }

        if (this.props.value.eta != null && this.props.value.eta != undefined && this.props.value.eta != '') {

            var eta = this.props.value.eta.split('T')[0]
            // var __eta = _eta.split(':')
            // var eta = __eta[0]+':'+__eta[1]

        }

        if (this.props.value.closing_date != null && this.props.value.closing_date != undefined && this.props.value.closing_date != '') {

            var closing_date = this.props.value.closing_date.split('T')[0]
            // var __eta = _eta.split(':')
            // var eta = __eta[0]+':'+__eta[1]

        }

        return(

            <Card style={{marginLeft: 10, marginRight: 10, borderLeftWidth: 8, borderColor: 'green'}}>
                    <CardItem>
                    <Body>
                            
                        <View style={{flex: 1, flexDirection: 'row'}}>
                            <Left>
                                <View style={{flexDirection: 'column'}}>
                                    <Text style={{fontSize: 16, fontWeight: 'bold', textAlign: 'left'}}>
                                        {this.props.value.vessel_name}
                                    </Text>
                                    <View style={{flexDirection: 'row', marginTop: 5}}>
                                        {this.props.value.type == 'D' ? 
                                            <Badge style={{ backgroundColor: '#008c45', height: '60%', justifyContent: 'center'}}>
                                                <Text style={{ color: 'white', fontSize: 10 , padding: 2}}>DIRECT</Text>
                                            </Badge>
                                        :
                                            <Badge style={{ backgroundColor: 'orange', height: '60%', justifyContent: 'center'}}>
                                                <Text style={{ color: 'white', fontSize: 10 , padding: 2}}>TRANSIT</Text>
                                            </Badge>
                                        }
                                        {/* <Text style={{fontSize: 12, textAlign: 'left', marginLeft: 5}}>
                                            {this.props.value.type}
                                            {" - "}
                                            {this.props.value.est_dooring.split('T')[0]}
                                        </Text> */}
                                    </View>
                                </View>
                            </Left>
                        {this.props.value.create_by == this.props.profile.id_user ? 
                            <Right>
                                <View style={{flexDirection: 'column'}}>
                                        {hak_akses == 'ADMIN' ?
                                            <View style={{justifyContent: 'center'}}>
                                                <Button color="#008c45" icon rounded small onPress={()=>this.props.onJadwalKapalEdit(this.props.value)} style={{margin: 5, backgroundColor: '#008c45'}}>
                                                    <Icon type="MaterialCommunityIcons" name='pencil-outline'/>
                                                </Button>
                                            </View>
                                        : null }
                                </View>
                            </Right>
                        : null}
                        </View>


                        {/* LINE */}
                        <View style = {{borderWidth: 0.5, marginTop: 5, marginBottom: 5, borderColor: '#008c45', width: '100%'}} />

                        <View style={{flex: 1, flexDirection: 'row'}}>
                            <View style={{flex:1}}>
                                <Text style={{width: "100%", fontSize: 12, textAlign: 'center', fontWeight: 'bold'}}>
                                    {/* {this.props.value.pelayaran} {' \n'} */}
                                    Closing Date: {' '}
                                    {this.convDateString(closing_date)}
                                    {'\n'} 
                                    Estimasi Dooring :  {' '}                                    
                                    {this.convDateString(est_dooring)}
                                    {/* {est_dooring} */}
                                    {/* {this.props.value.est_dooring} */}
                                </Text>
                            </View>
                        </View>
                        
                        {/* LINE */}
                        <View style = {{borderWidth: 0.5, marginTop: 5, marginBottom: 5, borderColor: '#008c45', width: '100%'}} />

                        <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                            <View style={{flex:1}}>
                                <Text style={{width: "100%", fontSize: 12, textAlign: 'center'}}>
                                    {this.props.value.asal} {'\n'} 
                                    {this.convDateString(etd)}
                                    {/* {this.props.value.etd} */}
                                </Text>
                            </View>

                            <View style={{flex:1}}>
                                {/* <Text style={{width: "100%", fontSize: 12, textAlign: 'center', marginBottom: 5}}>
                                    Rute
                                </Text> */}
                                <IconFontAwesome 
                                    name='arrow-right' 
                                    size={16} 
                                    style={{
                                        textAlign: 'center'
                                    }}
                                />
                            </View>

                            <View style={{flex:1}}>
                                <Text style={{width: "100%", fontSize: 12, textAlign: 'center'}}>
                                    {this.props.value.tujuan} {'\n'} 
                                    {this.convDateString(eta)}
                                    {/* {this.props.value.eta} */}
                                </Text>
                            </View>
                        </View>

                    </Body>
                </CardItem>
             </Card>
        );
    }
}

export default CardJadwalKapal;