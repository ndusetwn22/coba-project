import React,{Component} from 'react'
import {Text, View,Icon, Content, Button, Container} from 'native-base'
import {
    TouchableOpacity,
    ScrollView,FlatList, Image, Dimensions, RefreshControl, ToastAndroid
} from "react-native";
import styles from './style'
import CardJadwalKapal from './CardJadwalKapal/CardJadwalKapal'
import SearchInput,{ createFilter } from 'react-native-search-filter';
import { getListJadwalKapal } from '../../ListJadwalKapalAxios';
var listKosong = require('../../../../../../Assets/list_kosong.png');
const width = Dimensions.get('window').width;

const KEYS_TO_FILTERS = ['vessel_name', 'asal', 'tujuan', 'type'];


class FlatListJadwalKapal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paramSearch:'',
            refreshing: false,
        };
    }

    componentDidMount= async()=>{
        // setTimeout(() => {this.onRefresh}, 3000)
        // this.onRefresh()
    } 

    onRefresh= async() => {
        this.setState({refreshing: true})
        var _tempLength = this.props.listJadwalKapal.length
        var _listJadwalKapal = await getListJadwalKapal(this.props.url, this.props.profile.id_transporter)
        if (_listJadwalKapal.length > _tempLength || _listJadwalKapal.length < _tempLength) {
            
            if (_listJadwalKapal.length > _tempLength) {
                console.log('refresh', this.props.listJadwalKapal.length)
                this.props.refreshContent(_listJadwalKapal)
                this.setState({refreshing: false})
    
                var selisih = parseInt(_listJadwalKapal.length) - parseInt(_tempLength)
                ToastAndroid.show('New data added '+selisih, ToastAndroid.SHORT);
            }else{
                console.log('refresh', this.props.listJadwalKapal.length)
                this.props.refreshContent(_listJadwalKapal)
                this.setState({refreshing: false})
    
                ToastAndroid.show('Refresh ...', ToastAndroid.SHORT);
            }


        }
        else{
          ToastAndroid.show('No more new data available', ToastAndroid.SHORT);
          this.setState({refreshing: false})
        }
      };
    

    render(){

        const filteredJadwalKapal = this.props.listJadwalKapal.filter(createFilter(this.state.paramSearch, KEYS_TO_FILTERS))
        const hak_akses = this.props.hak_akses

        return(
            <>
                {/* adjust content dan scrollview */}
                    <View style={{flexDirection: 'row', backgroundColor: 'white', top: 1}}>
                    {hak_akses == 'ADMIN' ? 
                        <Button onPress={()=>this.props.onJadwalKapalAdd()} style={{marginTop: 10, marginLeft: 10, borderRadius: 10, padding: 0, height: 40, backgroundColor: '#008c45'}}>
                            <Icon type="MaterialCommunityIcons" name="calendar-plus" />
                        </Button>
                    : 
                        <Button disabled onPress={()=>this.props.onJadwalKapalAdd()} style={{marginTop: 10, marginLeft: 10, borderRadius: 10, padding: 0, height: 40, backgroundColor: '#008c45'}}>
                            <Icon type="MaterialCommunityIcons" name="calendar-plus" />
                        </Button>
                    }
                        <SearchInput 
                            onChangeText={(term) => { this.setState({paramSearch:term}) }} 
                            style={{
                                paddingLeft:10,
                                paddingTop:5,
                                marginTop: 10,
                                marginLeft:10,
                                marginRight:10,
                                marginBottom:5,
                                paddingBottom:5,
                                borderColor: '#008c45',
                                borderWidth: 1,
                                backgroundColor:"white",
                                borderRadius: 10,
                                width: '218%',
                            }}
                            placeholder="Search Jadwal"
                            inputFocus={true}
                            removeClippedSubviews={true} // Unmount components when outside of window 
                            initialNumToRender={2} // Reduce initial render amount
                            maxToRenderPerBatch={1} // Reduce number in each render batch
                            maxToRenderPerBatch={10} // Increase time between renders
                            windowSize={7} // Reduce the window size
                        />
                    </View>
                <Content
                    refreshControl={
                        <RefreshControl
                          refreshing={this.state.refreshing}
                          onRefresh={this.props.onRefresh}
                        />}
                >
                {this.props.listJadwalKapal.length == 0 ? 
                    <View>
                        <Image resizeMode='cover' source={listKosong} style={{marginTop: 50, width: '50%', height: '50%', opacity: 0.5, alignSelf: 'center'}}></Image>
                        <Text note style={{marginBottom: width, textAlign: 'center', fontStyle:'italic'}}>Jadwal Kapal Kosong</Text>
                    </View>
                :            
                    <ScrollView>
                            <FlatList
                                data={
                                    // filteredJadwalKapal

                                    this.props.listJadwalKapal.filter((element)=>{
                                        var boolFilter = false;
    
        
                                        var _est_dooring = element.est_dooring;
                                        if(_est_dooring!=null && _est_dooring!=''){
                                            _est_dooring = _est_dooring.toUpperCase();
                                        }
                                        var _vessel_name = element.vessel_name;
                                        if(_vessel_name!=null && _vessel_name!=''){
                                            _vessel_name = _vessel_name.toUpperCase();
                                        }
                                        var _asal = element.asal;
                                        if(_asal!=null && _asal!=''){
                                            _asal = _asal.toUpperCase();
                                        }
                                        var _tujuan = element.tujuan;
                                        if(_tujuan!=null && _tujuan!=''){
                                            _tujuan = _tujuan.toUpperCase();
                                        }
                                        var _paramSearch = this.state.paramSearch;
                                        if(_paramSearch!=null || _paramSearch!=''){
                                            _paramSearch = _paramSearch.toUpperCase();
                                        }

                                        var _type = element.type;
                                        if (_type == 'D') {
                                            _type = 'Direct'.toUpperCase();
                                        }
                                        if(_type == 'T'){
                                            _type = 'Transit'.toUpperCase();
                                        }
        
                                        if(
                                            element.id == element.id
                                        ){
                                            if(element.id.indexOf(_paramSearch) >-1){
                                                boolFilter=true;
                                            }
                                            if(_est_dooring!=null){
                                                if(
                                                    _est_dooring.indexOf(_paramSearch) >-1
                                                ){
                                                    boolFilter = true;
                                                }
                                            }

                                            if(_vessel_name!=null){
                                                if(
                                                    _vessel_name.indexOf(_paramSearch) >-1
                                                ){
                                                    boolFilter = true;
                                                }
                                            }

                                            if(_asal!=null){
                                                if(
                                                    _asal.indexOf(_paramSearch) >-1
                                                ){
                                                    boolFilter = true;
                                                }
                                            }

                                            if(_tujuan!=null){
                                                if(
                                                    _tujuan.indexOf(_paramSearch) >-1
                                                ){
                                                    boolFilter = true;
                                                }
                                            }

                                            if(_type!=null){
                                                if(
                                                    _type.indexOf(_paramSearch) >-1
                                                ){
                                                    boolFilter = true;
                                                }
                                            }
                                        }
                                        return boolFilter;
                                    })
                                }
                                style={{marginBottom:10}}
                                renderItem={({ item: value, index }) => {
                                    return (
                                        <CardJadwalKapal {...this.props} index={index} value={value} />
                                    );
                                }}
                                keyExtractor={(item, index) => index.toString()}
                            /> 
                    </ScrollView>
                }
                </Content>
            </>
        );
    }
}

export default FlatListJadwalKapal;