import Axios from 'axios';
import {encrypt} from '../../../../APIProp/ApiConstants';

  export const getListJadwalKapal = async (url,trans_id, _startDate, _endDate) =>{

    var startDate = _startDate
    var endDate = _endDate
    startDate = startDate.format('YYYY-MM-DD')
    endDate = endDate.format('YYYY-MM-DD')


    // let dataQry = `
    // select
    //     mms.id,
    //     mms.vessel_name,
    //     mms.etd,
    //     mms.eta,
    //     mms.type,
    //     pelabuhan1.nama as asal,
    //     pelabuhan2.nama as tujuan,
    //     mmp_pelayaran.nama as pelayaran,
    //     mms.create_date,
    //     mms.create_by,
    //     mms.update_date,
    //     mms.update_by,
    //     mms.transit_point,
    //     mms.est_dooring,
    //     mms.closing_date 
    // from mt_master_schedule mms 
    // left join mt_master_pelayaran mmp_pelayaran
    // on mmp_pelayaran.id = mms.ship_id 
    // left join mt_master_pelabuhan pelabuhan1
    // on pelabuhan1.id = mms.asal 
    // left join mt_master_pelabuhan pelabuhan2
    // on pelabuhan2.id = mms.tujuan 
    // where 
    // mms.ship_id in (select mmp2.id_pelayaran from mt_mapping_pelayaran mmp2 where mmp2.id_transporter = `+trans_id+`) 
    // and mms.etd >= '`+startDate+`'
    // and mms.etd  <= '`+endDate+`'
    // `

    let dataQry = `
        select * from mobiletra_getlistjadwalkapalmenu(`+trans_id+`,'`+startDate+`', '`+endDate+`')
    `
    console.log('query list jadwal kapal menu :', dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('listJadwal', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
      // console.log('error', err)
    });
  }
