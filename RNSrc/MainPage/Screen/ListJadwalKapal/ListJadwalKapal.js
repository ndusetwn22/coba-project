import React,{Component} from 'react'
import {BackHandler,TouchableOpacity, Text,View} from "react-native";
import { useNavigation  } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
import CustomNavbar from '../../Header/Navbar/Navbar'
import {Container} from 'native-base'
import Loading,{getProfile, setUrl, getUrl, decrypt} from '../../../../APIProp/ApiConstants'
import SweetAlert from 'react-native-sweet-alert';
import {
    getListJadwalKapal,
  } from './ListJadwalKapalAxios'
import ListJadwalKapalPage from './ListJadwalKapalPage'
import JadwalKapalAdd from './JadwalKapalAdd/JadwalKapalAdd'
import JadwalKapalEdit from './JadwalKapalEdit/JadwalKapalEdit';
import messaging from '@react-native-firebase/messaging';
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import PushNotification from 'react-native-push-notification'
import NetInfo from "@react-native-community/netinfo";
import LostConnection from '../../Component/LostConnection';
import moment from 'moment';


class ListJadwalKapal extends Component {
    constructor(props) {
        super(props);
        this.state = {
          paramLogin:{
            username:'',
            password:'',
          },
          isLoading:false,
          url:null,
          profile: null,
          isConnected: null,
          isConnectedChange: null,
          startDate: moment(),
          endDate: moment(),
          displayedDate: moment(),
          listJadwalKapal: [],
          jadwalKapalAdd: false,
          jadwalKapalEdit: false,
          jadwalKapalSelected: null
        };
      }
    
      componentDidMount= async()=>{
        // this.props.changeHeaderTitle("List Trip")
        await this.setState({isLoading:true});
        await setUrl();
        await getUrl().then(async(result)=>{
          this.setState({url:result})
        });
        await this.funcGetProfile();

        var _listJadwalKapal = await getListJadwalKapal(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate)
        await this.setState({isLoading:false, listJadwalKapal: _listJadwalKapal});


        await this.pushNotif();
        await this.cekConnection();
        await this.setState({isLoading:false});
      }

      cekConnection = async()=>{
        // Cek Connection
        NetInfo.addEventListener(state => {
          if (this.state.isConnected == null) {
            this.setState({isConnected: state.isConnected})
          }
          if (state.isConnected == false) {
            this.setState({isConnected: state.isConnected, isConnectedChange: state.isConnected})
          }
          if (state.isConnected == true) {
            this.setState({isConnectedChange: state.isConnected})
          }      
          console.log("Is connected?", state.isConnected);
        });
      }

      restartConnection = async() =>{
        console.log('Restart Connection', this.state.isConnected)
        if (this.state.isConnected == false && this.state.isConnectedChange == true) {
          console.log('HARUSNYA INI OFF', this.state.isConnected)
          this.props.navigation.replace('MainRouter', {screen: 'ListJadwalKapal'})
        }
      }

      funcGetProfile=async()=>{
        await getProfile().then(result=>{
          if(result!=null){
            this.setState({profileStr:result})
          }
        })
        if(this.state.profileStr==null){
          this.props.navigation.replace('Login')
        }
        var tmpStr = await decrypt(this.state.profileStr);
        await this.setState({profile:JSON.parse(tmpStr)})
        await this.setState({hak_akses: JSON.parse(tmpStr).hak_akses}) // tambah ini utk hak_akses dan state
        console.log('profile', JSON.parse(tmpStr))
      }

      funcSweetAlert = async(title, subTitle, style) =>{
        SweetAlert.showAlertWithOptions({
            title: title,
            subTitle: subTitle,
            confirmButtonTitle: 'OK',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: style,
            cancellable: true
          },
            callback => {
              console.log('callback') 
              this.backToList()
            }
          );
      }

      backToList=async()=>{
        await this.setState({isLoading:true});
        var _listJadwalKapal = await getListJadwalKapal(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate)
        await this.setState({isLoading:false, listJadwalKapal: _listJadwalKapal, jadwalKapalAdd: false, jadwalKapalEdit: false});
      }

      backToListWithoutLoad=async()=>{
        await this.setState({jadwalKapalAdd: false, jadwalKapalEdit: false});
      }

      refreshContent = async(_listJadwalKapal) => {
        await this.setState({isLoading: true})
        await this.setState({isLoading:false, listJadwalKapal: _listJadwalKapal});
      }

      pushNotif = async () => {
        // Must be outside of any component LifeCycle (such as `componentDidMount`).
        PushNotification.configure({
          // (optional) Called when Token is generated (iOS and Android)
          onRegister: function (token) {
            console.log("TOKEN:", token);
          },
        
          // (required) Called when a remote is received or opened, or local notification is opened
          onNotification: async (notification) => {
            console.log("NOTIFICATION:", notification);
            //Utk dpt Notif
            // process the notification
            //mau apa disini
            //disini setelah ada userInteraction/di klik
            if (notification.userInteraction) {
              // console.log('DO SOMETHING NOTIFICATION KENDARAAN') 
              // this.props.navigation.navigate('ListTrip',{
              //   screen: 'ListTrip',
              //   id_trip: notification.data.id_trip,
              //   id: notification.data.id
              // })
              this.props.navigation.navigate('Dashboard', {screen: 'Dashboard'})
            }
        
            // (required) Called when a remote is received or opened, or local notification is opened
            notification.finish(PushNotificationIOS.FetchResult.NoData);
          },
  
          
          // // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
          // onAction: function (notification) {
          //   console.log("ACTION:", notification.action);
          //   console.log("NOTIFICATION ACTION:", notification);
        
          //   // process the action
  
          //   console.log('waiwawa')
          // },
        
          // // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
          // onRegistrationError: function(err) {
          //   console.error(err.message, err);
          // },
        
          // // IOS ONLY (optional): default: all - Permissions to register.
          // permissions: {
          //   alert: true,
          //   badge: true,
          //   sound: true,
          // },
        
          // Should the initial notification be popped automatically
          // default: true
          popInitialNotification: true,
        
          /**
           * (optional) default: true
           * - Specified if permissions (ios) and token (android and ios) will requested or not,
           * - if not, you must call PushNotificationsHandler.requestPermissions() later
           * - if you are not using remote notification or do not have Firebase installed, use this:
           *     requestPermissions: Platform.OS === 'ios'
           */
          requestPermissions: true,
        });
      }

      setStartEndDate=async(s,e)=>{
        // console.log('start-dates: ', s)
        await this.setState({
          startDate:moment(s, 'YYYY-MM-DD'),
          endDate:moment(e, 'YYYY-MM-DD')
        })
      }

      onSearchButtonClicked=async()=>{
        await this.setState({isLoading:true});
        
        var startDate =this.state.startDate;
        var endDate;
    
        if(this.state.endDate==null){
          endDate = this.state.startDate;
        }else{
          endDate = this.state.endDate;
        }

        var _listJadwalKapal = await getListJadwalKapal(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate)


        await this.setState({isLoading: false, listJadwalKapal: _listJadwalKapal})

        }

        onJadwalKapalAdd=async()=>{
          await this.setState({isLoading:true,jadwalKapalAdd:false, jadwalKapalEdit: false});
            
          await this.setState({isLoading:false, jadwalKapalAdd: true, jadwalKapalEdit: false});
        }

        onJadwalKapalEdit=async(value)=>{
          await this.setState({isLoading:true, jadwalKapalEdit: false, jadwalKapalAdd: false});
          var _jadwalKapalSelected = {};
          _jadwalKapalSelected.value = value
          await this.setState({isLoading:false, jadwalKapalEdit: true, jadwalKapalAdd: false, jadwalKapalSelected: _jadwalKapalSelected});
          console.log('ISI JADWAL : ', value)
        }

      render(){
        return(
          <>
          <Container>

          {this.state.jadwalKapalAdd == true ?  
              <CustomNavbar buttonBack={this.backToListWithoutLoad} title={'Tambah Jadwal'}/> : 
              this.state.jadwalKapalEdit == true ?  
              <CustomNavbar buttonBack={this.backToListWithoutLoad} title={'Edit Jadwal'}/> :
              <CustomNavbar title={'Jadwal Kapal'}/>
          }


        {
          this.state.isConnected == false?
            <LostConnection restartConnection={this.restartConnection}/> 
          : 
          
          this.state.jadwalKapalAdd ?
            <JadwalKapalAdd
                {...this.state}
                backToList={this.backToList}
                funcSweetAlert={this.funcSweetAlert}
                // {...this.props}
                />
            :
          
            this.state.jadwalKapalEdit ?

              <JadwalKapalEdit
                {...this.state}
                backToList={this.backToList}
                funcSweetAlert={this.funcSweetAlert}
                // {...this.props}
                /> 
              
            :
              <ListJadwalKapalPage 
                  {...this.state}
                  {...this.props}
                  refreshContent={this.refreshContent}
                  setStartEndDate={this.setStartEndDate}
                  onSearchButtonClicked={this.onSearchButtonClicked}
                  onTripSelected={this.onTripSelected}
                  onJadwalKapalAdd={this.onJadwalKapalAdd}
                  onJadwalKapalEdit = {this.onJadwalKapalEdit}

                />

              
            }



          </Container>
            {this.state.isLoading ? <Loading/> : false}
          </>
        );
      }
}

ListJadwalKapalFunc= (props) => {
  const navigation = useNavigation();
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      BackHandler.addEventListener("hardwareBackPress", ()=>props.backAction('ListJadwalKapal', navigation));
    });

    return unsubscribe;
  }, [navigation]);

  return <ListJadwalKapal navigation={navigation}/>;
}

export default ListJadwalKapalFunc;