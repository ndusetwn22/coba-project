import React,{Component} from 'react'
import {BackHandler,TouchableOpacity, Text,View} from "react-native";
import { useNavigation  } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
import CustomNavbar from '../../Header/Navbar/Navbar'
import Loading,{getProfile, setUrl, getUrl, decrypt} from '../../../../APIProp/ApiConstants'
import {Container} from 'native-base'
import { WebView } from 'react-native-webview';
import SweetAlert from 'react-native-sweet-alert';
import { getlistUser } from './ListUserAxios';
import ListUserPage from './ListUserPage';
import UserAdd from './UserAdd/UserAdd'
import UserEdit from './UserEdit/UserEdit'
import messaging from '@react-native-firebase/messaging';
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import PushNotification from 'react-native-push-notification'
import Toast from 'react-native-toast-message'
import NetInfo from "@react-native-community/netinfo";
import LostConnection from '../../Component/LostConnection';



class ListUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
          paramLogin:{
            username:'',
            password:'',
          },
          isLoading:false,
          url:null,
          profile: null,
          profileStr: null,
          isConnected: null,
          isConnectedChange: null,
          listUser: [],
          userAdd: false,
          userEdit: false,
          userEditData: [],
          userSelected: null,
          hak_akses: ''
        };
      }
    
      componentDidMount= async()=>{
        await this.setState({isLoading:true});
        await setUrl();
        await getUrl().then(async(result)=>{
          this.setState({url:result})
        });
        await this.funcGetProfile();
        var _listUser = await getlistUser(this.state.url, this.state.profile.id_transporter)
        await this.setState({isLoading:false, listUser: _listUser});
        await this.pushNotif();

        await this.cekConnection();
        
      }

      cekConnection = async()=>{
        // Cek Connection
        NetInfo.addEventListener(state => {
          if (this.state.isConnected == null) {
            this.setState({isConnected: state.isConnected})
          }
          if (state.isConnected == false) {
            this.setState({isConnected: state.isConnected, isConnectedChange: state.isConnected})
          }
          if (state.isConnected == true) {
            this.setState({isConnectedChange: state.isConnected})
          }      
          console.log("Is connected?", state.isConnected);
        });
      }

      restartConnection = async() =>{
        console.log('Restart Connection', this.state.isConnected)
        if (this.state.isConnected == false && this.state.isConnectedChange == true) {
          console.log('HARUSNYA INI OFF', this.state.isConnected)
          this.props.navigation.replace('MainRouter', {screen: 'ListUser'})
        }
      }

      funcGetProfile=async()=>{
        await getProfile().then(result=>{
          if(result!=null){
            this.setState({profileStr:result})
          }
        })
        if(this.state.profileStr==null){
          this.props.navigation.replace('Login')
        }
        var tmpStr = await decrypt(this.state.profileStr);
        await this.setState({profile:JSON.parse(tmpStr)})
        await this.setState({hak_akses: JSON.parse(tmpStr).hak_akses}) // tambah ini utk hak_akses dan state
        console.log('profile', JSON.parse(tmpStr).hak_akses)
      }

      funcSweetAlert = async(title, subTitle, style) =>{
        SweetAlert.showAlertWithOptions({
            title: title,
            subTitle: subTitle,
            confirmButtonTitle: 'OK',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: style,
            cancellable: true
          },
            callback => {
              console.log('callback') 
              this.backToList()
            }
          );
    }

    funcToast = async(type, text1, text2) =>{
      Toast.show({
            type: type,
            position: 'bottom',
            text1: text1,
            text2: text2,
            visibilityTime: 3000,
            autoHide: true,
            topOffset: 30,
            bottomOffset: 40,
            onShow: () => {},
            onHide: () => {}
          })
    }

      onUserAdd=async()=>{
        await this.setState({isLoading:true,userAdd:false});

        await this.setState({isLoading:false, userAdd: true, userEdit: false});
      }

      onUserEdit=async(value)=>{
        await this.setState({isLoading:true, userEdit: false});
        var _userSelected = {};
        _userSelected.value = value
        await this.setState({isLoading:false, userEdit: true, userAdd: false, userSelected: _userSelected});
      }

      backToList=async()=>{
        await this.setState({isLoading:true});
        var _listUser = await getlistUser(this.state.url, this.state.profile.id_transporter)
        await this.setState({isLoading:false, listUser: _listUser, userAdd: false, userEdit: false});
        // this.props.navigation.replace('MainRouter', { screen:'ListUser'})
      }

      backToListWithoutLoad=async()=>{
        await this.setState({userAdd: false, userEdit: false});
      }

      refreshContent = async(_listUser) => {
        await this.setState({isLoading: true})
        await this.setState({isLoading:false, listUser: _listUser});
      }

      pushNotif = async () => {
        // Must be outside of any component LifeCycle (such as `componentDidMount`).
        PushNotification.configure({
          // (optional) Called when Token is generated (iOS and Android)
          onRegister: function (token) {
            console.log("TOKEN:", token);
          },
        
          // (required) Called when a remote is received or opened, or local notification is opened
          onNotification: async (notification) => {
            console.log("NOTIFICATION:", notification);
            if (notification.userInteraction) {
              console.log('DO SOMETHING NOTIFICATION IN USER') 
              // this.props.navigation.navigate('ListTrip',{
              //   screen: 'ListTrip',
              //   id_trip: notification.data.id_trip,
              //   id: notification.data.id
              // })
              this.props.navigation.navigate('Dashboard', {screen: 'Dashboard'})
            }
        
            // (required) Called when a remote is received or opened, or local notification is opened
            notification.finish(PushNotificationIOS.FetchResult.NoData);
          },
  
          
          // // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
          // onAction: function (notification) {
          //   console.log("ACTION:", notification.action);
          //   console.log("NOTIFICATION ACTION:", notification);
        
          //   // process the action
  
          //   console.log('waiwawa')
          // },
        
          // // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
          // onRegistrationError: function(err) {
          //   console.error(err.message, err);
          // },
        
          // // IOS ONLY (optional): default: all - Permissions to register.
          // permissions: {
          //   alert: true,
          //   badge: true,
          //   sound: true,
          // },
        
          // Should the initial notification be popped automatically
          // default: true
          popInitialNotification: true,
        
          /**
           * (optional) default: true
           * - Specified if permissions (ios) and token (android and ios) will requested or not,
           * - if not, you must call PushNotificationsHandler.requestPermissions() later
           * - if you are not using remote notification or do not have Firebase installed, use this:
           *     requestPermissions: Platform.OS === 'ios'
           */
          requestPermissions: true,
        });
      }

      render(){
        const { navigation } = this.props;
        return(
          <>
            <Container>
              {/* <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                  <Text>List Pengguna Screen Baru mudah"an bisa!</Text>
              </View> */}
              {/* <CustomNavbar title={'List Pengguna'}/> */}

              {this.state.userAdd == true ?  
              <CustomNavbar buttonBack={this.backToListWithoutLoad} title={'Tambah Pengguna'}/> : 
              this.state.userEdit == true ? 
              <CustomNavbar buttonBack={this.backToListWithoutLoad} title={'Edit Pengguna'}/> :
              <CustomNavbar title={'Daftar Pengguna'}/>
              }          

              <Toast ref={(ref) => Toast.setRef(ref)} style={{zIndex: 1000}}/>           
              

              {
                this.state.isConnected == false?
                <LostConnection restartConnection={this.restartConnection}/> 
              :
              this.state.userAdd ? 
                <UserAdd
                    {...this.state}
                    backToList={this.backToList}
                    funcSweetAlert={this.funcSweetAlert}
                    backToListWithoutLoad={this.backToListWithoutLoad}
                    // {...this.props}
                  />
                  
                  :

                  this.state.userEdit ? 
                // null
                  <UserEdit
                    {...this.state}
                    backToList={this.backToList}
                    funcSweetAlert={this.funcSweetAlert}
                    // {...this.props}
                  /> 
                  
                  :
                 <ListUserPage
                    {...this.state}
                    {...this.props}
                    onUserAdd = {this.onUserAdd}
                    onUserEdit = {this.onUserEdit}
                    refreshContent = {this.refreshContent}
                    funcToast = {this.funcToast}
                  />
                  

                  // <WebView source={{ uri: 'https://reactnative.dev/' }} />;
                  
              }

              {/* <Text onPress={()=>this.props.navigation.replace('MainRouter', { screen:'Dashboard'})}>test</Text> */}
            </Container>
            {this.state.isLoading ? <Loading/> : false}
          </>
        );
      }
}

ListUserFunc= (props) => {
  const navigation = useNavigation();
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      BackHandler.addEventListener("hardwareBackPress", ()=>props.backAction('ListUser', navigation));
    });

    return unsubscribe;
  }, [navigation]);

  return <ListUser navigation={navigation} />;
}

export default ListUserFunc;