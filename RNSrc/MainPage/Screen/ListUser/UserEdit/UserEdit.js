import React,{Component} from 'react'
import { AppState,Platform,Dimensions, Image, FlatList , TouchableOpacity,ScrollView, ImageBackground, BackHandler, Alert, PixelRatio, StyleSheet, TextInput, Picker } from "react-native";
import { useNavigation  } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
import CustomNavbar from '../../../Header/Navbar/Navbar'
import Loading,{getProfile, encrypt, setUrl, getUrl, decrypt} from '../../../../../APIProp/ApiConstants'
import Axios from 'axios';
import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Left,
    Right,
    Body,
    Text,
    Icon,
    Card,
    CardItem,
    Grid,
    Row,
    View,
    Col, 
    List,
    ListItem,
    Thumbnail,
    Form,
    Label,
    Input,
    Item,
    DatePicker
} from "native-base";
import styles from "./style";
import moment from 'moment'

//image picker
import ImagePicker from 'react-native-image-picker';

//Google Places
import RNGooglePlaces from 'react-native-google-places';

//firebase
import { utils } from '@react-native-firebase/app';
import storage from '@react-native-firebase/storage';
import { editUser, getHakAkses } from './UserEditAxios';



class UserEdit extends Component {
    constructor(props) {
            super(props);
            this.state = {
            paramLogin:{
                username:'',
                password:'',
            },
            isLoading:false,
            url:null,
            profile: null,
            profileStr: null,
            namaTransporter: '',
            id: this.props.userSelected.value.id,
            namaPengguna: this.props.userSelected.value.nama,
            noHp: this.props.userSelected.value.hp,
            akses: this.props.userSelected.value.hak_akses,
            allAkses: [],
            email: this.props.userSelected.value.email,
            status: this.props.userSelected.value.active,
            validatedemail: false,
            validatePassword: false,
            validateKonfirmasiPassword: false,
            password: this.props.userSelected.value.pass,
            konfirmasiPassword: this.props.userSelected.value.pass,
            userSelected : this.props.userSelected.value
        };
      }
    
      componentDidMount= async()=>{
        await this.setState({isLoading:true});
        await setUrl();
        await getUrl().then(async(result)=>{
          this.setState({url:result})
        });

        await this.funcGetProfile();
        // console.log('user selected', this.state.userSelected)

        setTimeout(() => {this.validateemail(this.state.email)}, 2000) //tambah timeout karena butuh dari validate
        setTimeout(() => {this.validatePassword(this.state.password)}, 2000) //tambah timeout karena butuh dari validate
        var _allAkses = await getHakAkses(this.state.url)
        await this.setState({isLoading:false, namaTransporter: this.state.profile.nama_transporter, allAkses: _allAkses});
      }

      funcGetProfile=async()=>{
        await getProfile().then(result=>{
          if(result!=null){
            this.setState({profileStr:result})
          }
        })
        if(this.state.profileStr==null){
          this.props.navigation.replace('Login')
        }
        var tmpStr = await decrypt(this.state.profileStr);
        this.setState({profile:JSON.parse(tmpStr)})
        console.log('profile', JSON.parse(tmpStr))
      }



     validateemail=(text)=>{
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
        console.log(reg);
        if(reg.test(text) === false)
        {
          // console.log("Email is Not Correct");
    
          this.setState({email:text, validatedemail: false})
          return false;
          }
        else {
          console.log('email correct');
          this.setState({email:text, validatedemail: true})
        }
      }


      validatePassword=(text)=>{
        let reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/ ;
        console.log(reg);
        if(reg.test(text) === false)
        {
          // console.log("password is Not Correct");
    
          this.setState({password: text, validatePassword: false})
          return false;
          }
        else {
          console.log('password correct');
          this.setState({password: text, validatePassword: true})
        }
      }


      validateKonfirmasiPassword=(text)=>{
        let reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/ ;
        console.log(reg);
        if(reg.test(text) === false)
        {
          // console.log("password is Not Correct");
    
          this.setState({konfirmasiPassword: text, validateKonfirmasiPassword: false})
          return false;
          }
        else {
          console.log('password correct');
          this.setState({konfirmasiPassword: text, validateKonfirmasiPassword: true})
        }
      }

      onValueChangeNamaPengguna = value => {
        this.setState({
          namaPengguna: value
        });
      };

      editPengguna=async ()=>{
        // this.setState({buttondisabled: true});
        let title = 'Invalid Input';
        if(this.state.namaPengguna === '' ) Alert.alert(title, 'Nama Pengguna tidak boleh kosong!');
        else if(this.state.noHp === '') Alert.alert(title, 'No HP tidak boleh kosong!');
        else if(this.state.akses === '') Alert.alert(title, 'Akses tidak boleh kosong!');
        else if(this.state.validatedemail === false) Alert.alert(title, 'Format Email tidak sesuai!');
        else if(this.state.status === '') Alert.alert(title, 'Status tidak boleh kosong!');
        // else if(this.state.password === '') Alert.alert(title, 'Password tidak boleh kosong!');
        // else if(this.state.konfirmasiPassword === '') Alert.alert(title, 'Konfirmasi Password tidak boleh kosong!');
        else if(this.state.validatePassword === false) Alert.alert(title, 'Password harus memiliki minimal 6 karakter, 1 digit angka, 1 huruf besar, 1 huruf kecil');
        else if(this.state.password !== this.state.konfirmasiPassword) Alert.alert(title, 'Konfirmasi Password tidak sesuai, harap isi kembali!');
        else{
            
            this.setState({isLoading:true});
            let trid = this.state.profile.id_transporter
            let uid = this.state.profile.id_user

            //QUERY
            var _editUser = await editUser(this.state.url, this.state.id, this.state.namaPengguna, this.state.noHp, this.state.email, this.state.password, this.state.akses, this.state.status, this.state.profile.id_transporter, this.state.profile.id_user)

            // Alert.alert("Status", _editUser,[
            //     {
            //       text: 'Ok',
            //       onPress: ()=>this.props.backToList()
            //     }
            //   ]);

              this.props.funcSweetAlert('status',_editUser[0], _editUser[1])
              // this.props.navigation.replace('MainRouter', { screen:'Dashboard'})}
            // this.props.backToList()

            this.setState({isLoading: false})
        }
      }




      render(){

        return(
          <>
            <Content>
                {/* <Text style={styles.title}>{this.props.userSelected.value.nama}</Text> */}

                {/* <Text style={styles.title}>EDIT PENGGUNA</Text> */}

                <View style={styles.viewTextInput}>
                  <Label style={styles.label}>Nama Transporter</Label>
                    <TextInput
                      style={styles.textInputTransporter}
                      underlineColorAndroid='rgba(0,0,0,0)'
                      caretHidden={true}
                      editable ={false}
                      selectTextOnFocus = {false}
                      // placeholder="PT INSTRANS"
                      // placeholderTextColor = "#CACCD7"
                      selectionColor="#008c45"
                      keyboardType="email-address"
                      value = {this.state.namaTransporter}
                      
                    />
                </View>


                {/* Nama Pengguna */}
                <View style={styles.viewTextInput}>   
                    <Label style={styles.label}>Nama Pengguna</Label>
                        <TextInput
                        style={styles.textInput}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder="Nama Pengguna"
                        placeholderTextColor = "#CACCD7"
                        selectionColor="#008c45"
                        onChangeText={this.onValueChangeNamaPengguna}
                        value = {this.state.namaPengguna}
                        
                        />
                </View>

                {/* No HP */}
                <View style={styles.viewTextInput}>   
                    <Label style={styles.label}>No Handphone</Label>
                        <TextInput
                        style={styles.textInput}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder="No Handphone"
                        placeholderTextColor = "#CACCD7"
                        selectionColor="#008c45"
                        keyboardType="phone-pad"
                        onChangeText={input=>this.setState({noHp: input})}
                        value = {this.state.noHp}
                        />
                </View>


                {/* Akses */}
                <View style={{flex: 1, flexDirection: 'column'}}>   
                  <Label style={{fontSize: 14, marginLeft: 10, marginTop: 10, alignSelf: 'flex-start', alignItems: 'flex-start'}}>Akses</Label>
                    <View 
                        style={{flex: 1,  
                        alignItems: 'center',  
                        justifyContent: 'center',
                        borderColor: 'gray', 
                        borderWidth: 2, 
                        borderRadius: 5, 
                        marginTop: 10, 
                        marginLeft: 20, 
                        marginRight: 20, height: 40, color: '#494A4A', padding: 10 }}>

                              
                                      <Picker
                                        style={{ 
                                          width: "100%",
                                          color: '#344953',  
                                          justifyContent: 'center',
                                        }}
                                        selectedValue={this.state.akses}
                                        onValueChange={(itemValue, itemIndex) => 
                                          this.setState({akses: itemValue})
                                          }
                                      >
                                        {/* <Picker.Item label="Admin" value="ADMIN" />
                                        <Picker.Item label="User" value="USER" />
                                        <Picker.Item label="Finance" value="FINANCE" /> */}
                                        {this.state.allAkses.map((value, index) => {
                                            return (
                                                <Picker.Item
                                                label={value.hak_akses}
                                                value={value.hak_akses}
                                                key={index}
                                                />
                                            );
                                            })}
                                      </Picker>

                    </View>
                </View>

                {/* Email */}
                <View style={styles.viewTextInput}>   
                    <Label style={styles.label}>Email</Label>
                        <TextInput
                        style={styles.textInput}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder="Email"
                        placeholderTextColor = "#CACCD7"
                        selectionColor="#008c45"
                        keyboardType="email-address"
                        // onChangeText={input=>this.setState({email: input})}
                        onChangeText={(text)=>this.validateemail(text)}
                        value={this.state.email}
                        />
                </View>

                
                <View style={{flex: 1, flexDirection: 'column'}}>   
                  <Label style={{fontSize: 14, marginLeft: 10, marginTop: 10, alignSelf: 'flex-start', alignItems: 'flex-start'}}>Status</Label>
                    <View 
                        style={{flex: 1,  
                        alignItems: 'center',  
                        justifyContent: 'center',
                        borderColor: 'gray', 
                        borderWidth: 2, 
                        borderRadius: 5, 
                        marginTop: 10, 
                        marginLeft: 20, 
                        marginRight: 20, height: 40, color: '#494A4A', padding: 10 }}>

                              
                                      <Picker
                                        style={{ 
                                          width: "100%",
                                          color: '#344953',  
                                          justifyContent: 'center',
                                        }}
                                        selectedValue={this.state.status}
                                        onValueChange={(itemValue, itemIndex) => 
                                          this.setState({status: itemValue})
                                          }
                                      >
                                        <Picker.Item label="Active" value="A" />
                                        <Picker.Item label="In Active" value="I" />
                                      </Picker>

                    </View>
                </View>

                {/* Password */}
                <View style={styles.viewTextInput}>   
                    <Label style={styles.label}>Password</Label>
                        <TextInput
                                style={styles.textInput}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                placeholder="Password"
                                placeholderTextColor = "#CACCD7"
                                selectionColor="#008c45"
                                secureTextEntry={true}
                                onChangeText={(text)=>this.validatePassword(text)}
                                value = {this.state.password}
                                
                            />
                
                </View>

                {/* validatepassword */}
                <View style={styles.viewTextInput}>   
                    <Label style={styles.label}>Konfirmasi Password</Label>      
                        <TextInput
                                style={styles.textInput}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                placeholder="Konfirmasi Password"
                                placeholderTextColor = "#CACCD7"
                                selectionColor="#008c45"
                                secureTextEntry={true}
                                onChangeText={(text)=>this.validateKonfirmasiPassword(text)}
                                value = {this.state.konfirmasiPassword}
                                
                            />
                </View>

                {/* Tambah Pengguna */}
                <View style={{flex: 1, flexDirection: 'column', marginBottom: 15, marginTop: 10}}>   
                  {/* <Label style={{fontSize: 14, 
                                // marginLeft: 10, 
                                marginTop: 10, alignSelf: 'center', alignItems: 'center'}}>Edit Pengguna</Label> */}
                    <Button
                      style={[styles.button, {justifyContent: 'center', alignItems: 'center'}]}
                        onPress={this.editPengguna}
            
                    >
                      <Text uppercase={false} style={{fontSize: 14, textAlign: "center"}}>Edit Pengguna</Text>
                    </Button>
                </View> 

                  

            </Content>

            {this.state.isLoading ? <Loading/> : false}
          </>
        );
      }
}

export default UserEdit;