import Axios from 'axios';
import {encrypt} from '../../../../../APIProp/ApiConstants';

  export const editUser = async (url, id, nama, hp, email, password, hak_akses, active, transporter_id, user_id) =>{
      // console.log('id', id, nama, hp, email, password, hak_akses, active, transporter_id, user_id, id)
    // let dataQry = `
    //     update mt_master_user SET
    //     nama = '`+nama+`',
    //     hp = '`+hp+`',
    //     email = '`+email+`',
    //     password = epm_encrypt('`+password+`'),
    //     role = 'TRANSPORTER',
    //     hak_akses = '`+hak_akses+`',
    //     active = '`+active+`',
    //     shipper_id = 0,
    //     transporter_id = `+transporter_id+`,
    //     update_date = now(),
    //     update_by = `+user_id+`
    //     where id = `+id+`
    // `

    let dataQry = `
      select * from mobiletra_edituser(`+id+`, '`+nama+`', '`+hp+`', '`+email+`', '`+password+`', '`+hak_akses+`', '`+active+`', `+transporter_id+`, `+user_id+`)
    `
    console.log('query', dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data[0];
    //   console.log('insert', x)
      console.log('sukses', x)
      if (x.id_user == id) {
        return ['Sukses edit pengguna!', 'success'];
      }
    else{
        return ['Gagal edit pengemudi!', 'warning'];
    }
    //   if(x==""){
    //     return [];
    //   }
    //   return x;
    }).catch(err=>{
    //   return [];
      console.log('error', err)
      return ['Gagal edit pengguna!', 'warning'];
    });
  }

  export const getHakAkses = async (url) =>{
    let dataQry = `
        select * from mt_master_hak_akses
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('List Hak Akses: ', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      // return [];
      console.log('error', err)
    });
  }