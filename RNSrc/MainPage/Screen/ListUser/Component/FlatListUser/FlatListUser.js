import React,{Component} from 'react'
import {Text, View,Icon, Content, Button} from 'native-base'
import {
    TouchableOpacity,
    ScrollView,FlatList, Image, Dimensions, RefreshControl, ToastAndroid
} from "react-native";
import styles from './style'
import CardUser from './CardUser/CardUser'
import SearchInput,{ createFilter } from 'react-native-search-filter';
import { getlistUser } from '../../ListUserAxios';
const KEYS_TO_FILTERS = ['nama', 'email','hp','active', 'hak_akses'];
var listKosong = require('../../../../../../Assets/list_kosong.png');
import Toast from 'react-native-toast-message'
// import Toast from 'react-native-simple-toast';
const width = Dimensions.get('window').width;

class FlatListUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paramSearch:'',
            refreshing: false,
        };
    }

    componentDidMount= async()=>{
        //dari listUser
        // await this.setState({hak_akses : this.props.hak_akses})
    } 

    onRefresh= async() => {
        this.setState({refreshing: true})
        var _tempLength = this.props.listUser.length
        var _listUser = await getlistUser(this.props.url, this.props.profile.id_transporter)

        if (_listUser.length > _tempLength || _listUser.length < _tempLength) {
            
            if (_listUser.length > _tempLength) {
                console.log('refresh', this.props.listUser.length)
                this.props.refreshContent(_listUser)
                this.setState({refreshing: false})
    
                var selisih = parseInt(_listUser.length) - parseInt(_tempLength)
                // Toast.show({
                //     type: 'success',
                //     position: 'bottom',
                //     text1: 'Refreshing',
                //     text2: 'New data added '+selisih,
                //     visibilityTime: 3000,
                //     autoHide: true,
                //     topOffset: 30,
                //     bottomOffset: 40,
                //     onShow: () => {},
                //     onHide: () => {}
                //   })

                ToastAndroid.show('New data added '+selisih, ToastAndroid.SHORT);
            }else{
                console.log('refresh', this.props.listUser.length)
                this.props.refreshContent(_listUser)
                this.setState({refreshing: false})

                // Toast.show({
                //     type: 'info',
                //     position: 'bottom',
                //     text1: 'Refreshing',
                //     text2: 'Refresh data ...',
                //     visibilityTime: 3000,
                //     autoHide: true,
                //     topOffset: 30,
                //     bottomOffset: 40,
                //     onShow: () => {},
                //     onHide: () => {}
                //   })
    
                ToastAndroid.show('Refresh ...', ToastAndroid.SHORT);
            }


        }
        else{
            // Toast.show({
            //     type: 'info',
            //     position: 'bottom',
            //     text1: 'Refreshing',
            //     text2: 'No more new data available ...',
            //     visibilityTime: 3000,
            //     autoHide: true,
            //     topOffset: 30,
            //     bottomOffset: 40,
            //     onShow: () => {},
            //     onHide: () => {}
            //   })

            // this.props.funcToast('info', 'refresh', 'refreshhh pliss!')

          ToastAndroid.show('No more new data available', ToastAndroid.SHORT);
          this.setState({refreshing: false})
        }
      };

    render(){
        console.log('list user', this.props.listUser.length)
        const filteredUser = this.props.listUser.filter(createFilter(this.state.paramSearch, KEYS_TO_FILTERS))
        const hak_akses = this.props.hak_akses
        return(
            <>
            {/* <Toast ref={(ref) => Toast.setRef(ref)} style={{zIndex: 1000}}/>            */}
                    <View style={{flexDirection: 'row', backgroundColor: 'white', top: 1}}>
                        {hak_akses == 'ADMIN' ? 
                            <Button onPress={()=>this.props.onUserAdd()} style={{marginTop: 10, marginLeft: 10, borderRadius: 10, padding: 0, height: 40, backgroundColor: '#008c45'}}>
                                <Icon type="MaterialIcons" name='person-add' />
                            </Button>
                        : 
                            <Button disabled onPress={()=>this.props.onUserAdd()} style={{marginTop: 10, marginLeft: 10, borderRadius: 10, padding: 0, height: 40, backgroundColor: '#008c45'}}>
                                <Icon type="MaterialIcons" name='person-add' />
                            </Button>
                        }
                        <SearchInput 
                            onChangeText={(term) => { this.setState({paramSearch:term}) }} 
                            style={{
                                paddingLeft:10,
                                paddingTop:5,
                                marginTop: 10,
                                marginLeft:10,
                                marginRight:10,
                                marginBottom:5,
                                paddingBottom:5,
                                borderColor: '#008c45',
                                borderWidth: 1,
                                backgroundColor:"white",
                                borderRadius: 10,
                                width: '248%',
                            }}
                            placeholder="Search User"
                            inputFocus={true}
                            removeClippedSubviews={true} // Unmount components when outside of window 
                            initialNumToRender={2} // Reduce initial render amount
                            maxToRenderPerBatch={1} // Reduce number in each render batch
                            maxToRenderPerBatch={10} // Increase time between renders
                            windowSize={7} // Reduce the window size
                        />
                    </View>
                <Content
                    refreshControl={
                        <RefreshControl
                          refreshing={this.state.refreshing}
                          onRefresh={this.onRefresh.bind(this)}
                        />}
                >
                {this.props.listUser.length == 0 ? 
                    <View>
                        <Image resizeMode='cover' source={listKosong} style={{marginTop: 50, width: '50%', height: '50%', opacity: 0.5, alignSelf: 'center'}}></Image>
                        <Text note style={{marginBottom: width, textAlign: 'center', fontStyle:'italic'}}>Pengguna kosong</Text>
                    </View>
                :
                    <ScrollView>
                            <FlatList
                                data={
                                    filteredUser
                                }
                                style={{marginBottom:10}}
                                renderItem={({ item: value, index }) => {
                                    return (
                                        <CardUser {...this.props} index={index} value={value} />
                                    );
                                }}
                                keyExtractor={(item, index) => index.toString()}
                            /> 
                    </ScrollView>
                }                
                </Content>
            </>
        );
    }
}

export default FlatListUser;