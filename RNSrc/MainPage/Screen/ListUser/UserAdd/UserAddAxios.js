import Axios from 'axios';
import {encrypt} from '../../../../../APIProp/ApiConstants';

  export const insertUser = async (url,nama, hp, email, password, hak_akses, active, transporter_id, user_id) =>{
    // let dataQry = `
    // insert into  mt_master_user (nama, hp, email, password, role, hak_akses, active, shipper_id, transporter_id, create_date, create_by, update_date, update_by)
    // values ('`+nama+`', '`+hp+`', '`+email+`', epm_encrypt('`+password+`'), 'TRANSPORTER', '`+hak_akses+`', '`+active+`', 0, `+transporter_id+`, now(), `+user_id+`, now(), `+user_id+`)
    // `

    let dataQry = `
      select * from mobiletra_insertuser('`+nama+`', '`+hp+`', '`+email+`', '`+password+`', '`+hak_akses+`', '`+active+`', `+transporter_id+`, `+user_id+`)
    `
    console.log('query', dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.insert,{
      query: x
    }).then(value=>{
      let x = value.data.data;
    //   console.log('insert', x)
      console.log('sukses', x)
      return ['Sukses tambah pengguna!', 'success'];
    //   if(x==""){
    //     return [];
    //   }
    //   return x;
    }).catch(err=>{
    //   return [];
      console.log('error', err)
      return ['Gagal tambah pengguna!', 'warning'];
    });
  }

  export const getHakAkses = async (url) =>{
    let dataQry = `
        select * from mt_master_hak_akses
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('List Hak Akses: ', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      // return [];
      console.log('error', err)
    });
  }