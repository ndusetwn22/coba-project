import React,{Component} from 'react'
import {Text, View,Icon, Content, Button, Container} from 'native-base'
import {
    TouchableOpacity,
    ScrollView,FlatList, Image, Dimensions, RefreshControl, ToastAndroid
} from "react-native";
import SweetAlert from 'react-native-sweet-alert';
import styles from './style'
import CardKendaraan from './CardKendaraan/CardKendaraan'
import SearchInput,{ createFilter } from 'react-native-search-filter';
import { getListKendaraan } from '../../ListKendaraanAxios';
const KEYS_TO_FILTERS = ['ref_id', 'plat_nomor', 'jenis_kendaraan', 'active', 'pool', 'dedicated'];
var listKosong = require('../../../../../../Assets/list_kosong.png');
const width = Dimensions.get('window').width;

class FlatListKendaraan extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paramSearch:''
        };
    }

    componentDidMount= async()=>{
        // this.props.listKendaraan == 0 ? this.funcSweetAlert('status', 'Pool masih kosong, harap tambah pool dahulu!', 'warning'):null;
    } 

    onRefresh= async() => {
        this.setState({refreshing: true})
        var _tempLength = this.props.listKendaraan.length
        var _listKendaraan = await getListKendaraan(this.props.url, this.props.profile.id_transporter)

        if (_listKendaraan.length > _tempLength || _listKendaraan.length < _tempLength) {
            
            if (_listKendaraan.length > _tempLength) {
                console.log('refresh', this.props.listKendaraan.length)
                this.props.refreshContent(_listKendaraan)
                this.setState({refreshing: false})
    
                var selisih = parseInt(_listKendaraan.length) - parseInt(_tempLength)
                ToastAndroid.show('New data added '+selisih, ToastAndroid.SHORT);
            }else{
                console.log('refresh', this.props.listKendaraan.length)
                this.props.refreshContent(_listKendaraan)
                this.setState({refreshing: false})
    
                ToastAndroid.show('Refresh ...', ToastAndroid.SHORT);
            }


        }
        else{
          ToastAndroid.show('No more new data available', ToastAndroid.SHORT);
          this.setState({refreshing: false})
        }
      };

    funcSweetAlert = async(title, subTitle, style) =>{
        SweetAlert.showAlertWithOptions({
            title: title,
            subTitle: subTitle,
            confirmButtonTitle: 'OK',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: style,
            cancellable: true
          },
            callback => console.log('callback'));
    }

    render(){

        const filteredKendaraan = this.props.listKendaraan.filter(createFilter(this.state.paramSearch, KEYS_TO_FILTERS))
        const hak_akses = this.props.hak_akses

        return(
            <>
            {/* adjust content dan scrollview */}
            <View style={{flexDirection: 'row', backgroundColor: 'white', top: 1}}>
            {hak_akses == 'ADMIN' ?     
                <Button onPress={()=>this.props.onKendaraanAdd()} style={{marginTop: 10, marginLeft: 10, borderRadius: 10, padding: 0, height: 40, backgroundColor: '#008c45'}}>
                    <Icon type="MaterialCommunityIcons" name="plus-circle" />
                </Button>
            :
                <Button disabled onPress={()=>this.props.onKendaraanAdd()} style={{marginTop: 10, marginLeft: 10, borderRadius: 10, padding: 0, height: 40, backgroundColor: '#008c45'}}>
                    <Icon type="MaterialCommunityIcons" name="plus-circle" />
                </Button>
            }
                <SearchInput 
                    onChangeText={(term) => { this.setState({paramSearch:term}) }} 
                    style={{
                        paddingLeft:10,
                        paddingTop:5,
                        marginTop: 10,
                        marginLeft:10,
                        marginRight:10,
                        marginBottom:5,
                        paddingBottom:5,
                        borderColor: '#008c45',
                        borderWidth: 1,
                        backgroundColor:"white",
                        borderRadius: 10,
                        width: '185%',
                    }}
                    placeholder="Search Kendaraan"
                    inputFocus={true}
                    removeClippedSubviews={true} // Unmount components when outside of window 
                    initialNumToRender={2} // Reduce initial render amount
                    maxToRenderPerBatch={1} // Reduce number in each render batch
                    maxToRenderPerBatch={10} // Increase time between renders
                    windowSize={7} // Reduce the window size
                />
            </View>
            <Content
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh.bind(this)}
                    />}
            >
                {this.props.listKendaraan.length == 0 ? 
                    <View>
                        <Image resizeMode='cover' source={listKosong} style={{marginTop: 50, width: '50%', height: '50%', opacity: 0.5, alignSelf: 'center'}}></Image>
                        <Text note style={{marginBottom: width, textAlign: 'center', fontStyle:'italic'}}>Kendaraan kosong</Text>
                    </View>
                    
                    
                :        
                    <ScrollView>
                            <FlatList
                                data={
                                    filteredKendaraan
                                }
                                style={{marginBottom:10}}
                                renderItem={({ item: value, index }) => {
                                    return (
                                        <CardKendaraan {...this.props} index={index} value={value} />
                                    );
                                }}
                                keyExtractor={(item, index) => index.toString()}
                            /> 
                    </ScrollView>
                }
                </Content>
            </>
        );
    }
}

export default FlatListKendaraan;