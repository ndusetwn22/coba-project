import React,{Component} from 'react'
import {
    View
} from "react-native";
import styles from './style'
import {
    Text,  
    Card,
    CardItem,
    Body,
    Left,
    Right,
    Button,
    Icon,
    Thumbnail
} from 'native-base'
import { TouchableOpacity } from 'react-native-gesture-handler';

class CardKendaraan extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount= async()=>{

    } 

    render(){
        const hak_akses = this.props.hak_akses
        return(
            <Card style={{marginLeft: 10, marginRight: 10, borderLeftWidth: 8, borderColor: 'green'}}>
                    <CardItem>
                    <Body>
                            
                            <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', alignContent:'center', alignSelf: 'center'}}>
                                <Text style={{fontSize: 16, fontWeight: 'bold', textAlign: 'center'}}>
                                    {this.props.value.plat_nomor}
                                </Text>
                                <Text note style={{fontSize: 12, textAlign: 'center'}}>
                                    {this.props.value.jenis_kendaraan}
                                </Text>
                            </View>
                            <View style = {{borderWidth: 0.5, marginTop: 5, marginBottom: 5, borderColor: '#008c45', width: '100%'}} />

                        <View style={{flex: 1, flexDirection: 'row'}}>
                            <View style={{flex:1, marginLeft: 10}}>
                                {/* <Text style={{width: "100%", fontSize: 14, fontWeight: 'bold'}}>
                                    {this.props.value.nama}
                                </Text> */}
                                {/* <Text note style={{width: "100%", fontSize: 12}}>
                                    Jenis Kendaraan :{'\n'}{this.props.value.jenis_kendaraan}
                                </Text> */}
                                {/* <Text note style={{width: "100%", fontSize: 12}}>
                                    Status : {this.props.value.active}
                                </Text> */}
                                <Text note style={{width: "100%", fontSize: 12}}>
                                    <Text style={{fontWeight:'bold', fontSize: 12}}>Pool :</Text> {this.props.value.pool}
                                </Text>
                                <Text note style={{width: "100%", fontSize: 12}}>
                                    <Text style={{fontWeight:'bold', fontSize: 12}}>Ref ID :</Text> {this.props.value.ref_id}
                                </Text>
                            </View>

                            <View style={{flex:1}}>
                                


                                {this.props.value.dedicated == 'Y'?
                                <Text note style={{width: "100%", fontSize: 12}}>
                                    <Text style={{fontWeight:'bold', fontSize: 12}}>Dedicated :</Text> Yes
                                </Text>
                                : 
                                <Text note style={{width: "100%", fontSize: 12}}>
                                    <Text style={{fontWeight:'bold', fontSize: 12}}>Dedicated :</Text> No
                                </Text>
                                }  


                                {/* harusnya ada Block A = active, I = Inactive, B = Block */}
                                {this.props.value.active == 'A'?
                                <Text note style={{width: "100%", fontSize: 12, fontWeight: 'bold', color: 'green'}}>
                                    <Text style={{fontWeight:'bold', fontSize: 12}}>Status :</Text> Active
                                </Text>
                                : this.props.value.active == 'I'?
                                <Text note style={{width: "100%", fontSize: 12, fontWeight: 'bold', color: 'red'}}>
                                    <Text style={{fontWeight:'bold', fontSize: 12}}>Status :</Text> Non Active
                                </Text>
                                :
                                <Text note style={{width: "100%", fontSize: 12, fontWeight: 'bold', color: 'red'}}>
                                    <Text style={{fontWeight:'bold', fontSize: 12}}>Status :</Text>  Block
                                </Text>
                                }  
                            </View>
                        {hak_akses == 'ADMIN' ?
                            <View style={{justifyContent: 'center'}}>
                                {this.props.value.active != 'B'?
                                    <Button color="#008c45" icon rounded small onPress={()=>this.props.onKendaraanEdit(this.props.value)} style={{margin: 5, backgroundColor: '#008c45'}}>
                                        <Icon type="MaterialCommunityIcons" name='pencil-outline'/>
                                    </Button>
                                :
                                    <Button color="#ee595b" icon rounded small style={{margin: 5, backgroundColor: '#ee595b'}}>
                                        <Icon type="MaterialCommunityIcons" name='pencil-outline'/>
                                    </Button>
                                }
                            </View>
                        : null }
                        </View>
                    </Body>
                </CardItem>
             </Card>
        );
    }
}

export default CardKendaraan;