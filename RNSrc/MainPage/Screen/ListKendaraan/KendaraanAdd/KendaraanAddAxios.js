import Axios from 'axios';
import {encrypt} from '../../../../../APIProp/ApiConstants';
import {Alert} from 'react-native'

  export const getJenisKendaraan = async (url) =>{
    let dataQry = `
        select * from mt_master_jenis_kendaraan
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('List Jenis Kendaraan : ', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      // return [];
      console.log('error', err)
    });
  }

  export const getPool = async (url, trans_id) =>{
    let dataQry = `
        select * from mt_master_pool
        where transporter_id = `+trans_id+`
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('List Pool : ', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
      console.log('error', err)
    });
  }

 
  export const insertKendaraanIntrans = async (url, trid, uid, refId, platNomor, jenisKendaraan, status, pool, dedicated, nomorKir, jatuhTempoKir, tahunPembuatan, tglBerlakuHargaSewa, statusHargaSewa, hargaSewa) =>{

    // let dataQry = `
    //     with ins1 as(insert into mt_master_kendaraan 
    //     (external_id , plat_nomor , jenis_kendaraan , active , pool_id , dedicated , nomor_kir , jatuh_tempo_kir, tahun_kendaraan, create_date , create_by , update_date , update_by )
    //     values('`+refId+`', '`+platNomor+`', '`+jenisKendaraan+`', '`+status+`', '`+pool+`', '`+dedicated+`', '`+nomorKir+`', '`+jatuhTempoKir+`', '`+tahunPembuatan+`', now(), `+uid+`, now(), `+uid+`)
    //     returning id)

    //     insert into harga_sewa_kendaraan_perbulan 
    //     (start_date, status, transporter_id, kendaraan_id, harga_sewa, create_date, create_by, update_date, update_by)
    //     values('`+tglBerlakuHargaSewa+`', '`+statusHargaSewa+`', '`+trid+`', (select id from ins1), `+hargaSewa+`, now(), `+uid+`, now(), `+uid+`)
    // `

    let dataQry = `
      select * from mobiletra_insertkendaraanintrans('`+refId+`', '`+platNomor+`', `+jenisKendaraan+`, '`+status+`', `+pool+`, '`+dedicated+`', '`+nomorKir+`', '`+jatuhTempoKir+`', `+tahunPembuatan+`, '`+tglBerlakuHargaSewa+`', '`+statusHargaSewa+`', `+hargaSewa+`, `+trid+`, `+uid+`)
    `
    console.log('query : ', dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.insert,{
      query: x
    }).then(value=>{
      let x = value.data.data;

      console.log('sukses query', value)


      return ['Sukses tambah kendaraan!', 'success'];

    }).catch(err=>{
    //   return [];
      console.log('error', err)
      return ['Gagal tambah kendaraan!', 'warning'];
    });
  }


  export const insertKendaraan = async (url, trid, uid, refId, platNomor, jenisKendaraan, status, pool, dedicated, nomorKir, jatuhTempoKir, tahunPembuatan) =>{

    // let dataQry = `
    //     insert into mt_master_kendaraan 
    //     (external_id , plat_nomor , jenis_kendaraan , active , pool_id , dedicated , nomor_kir , jatuh_tempo_kir, tahun_kendaraan, create_date , create_by , update_date , update_by )
    //     values('`+refId+`', '`+platNomor+`', '`+jenisKendaraan+`', '`+status+`', '`+pool+`', '`+dedicated+`', '`+nomorKir+`', '`+jatuhTempoKir+`', '`+tahunPembuatan+`', now(), `+uid+`, now(), `+uid+`)
    // `

    let dataQry = `
        select * from mobiletra_insertkendaraan('`+refId+`', '`+platNomor+`', `+jenisKendaraan+`, '`+status+`', `+pool+`, '`+dedicated+`', '`+nomorKir+`', '`+jatuhTempoKir+`', `+tahunPembuatan+`, `+uid+`)
    `
    console.log('insert kendaraan non intrans: ', dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.insert,{
      query: x
    }).then(value=>{
      let x = value.data.data;

      console.log('sukses query', value)


      return ['Sukses tambah kendaraan!', 'success'];

    }).catch(err=>{
    //   return [];
      console.log('error', err)
      return ['Gagal tambah kendaraan!', 'warning'];
    });
  }
