import React,{Component} from 'react'
import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Left,
    Right,
    Body,
    Text,
    Icon,
    Card,
    CardItem,
    Grid,
    Row,
    View,
    Col, 
    List,
    ListItem,
    Thumbnail,
    Form,
    Label,
    Input,
    Item,
    DatePicker
} from "native-base";
import { AppState,Platform,Dimensions, Image, FlatList , TouchableOpacity,ScrollView, ImageBackground, BackHandler, Alert, PixelRatio, StyleSheet, TextInput, Picker } from "react-native";
import styles from './style'

class TextFieldKendaraanAdd extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount= async()=>{

    } 

    render(){
        return(
            <>
                <View style={styles.viewTextInput}>
                  <Label style={styles.label}>{this.props.title}</Label>
                  <TextInput
                      style={styles.textInput}
                      underlineColorAndroid='rgba(0,0,0,0)'
                      placeholder={this.props.placeholder}
                      placeholderTextColor = "#CACCD7"
                      selectionColor="#008c45"
                      onChangeText={input=>this.props.onValueChange(input)}
                      
                    />
                </View>
            </>
        );
    }
}

export default TextFieldKendaraanAdd;