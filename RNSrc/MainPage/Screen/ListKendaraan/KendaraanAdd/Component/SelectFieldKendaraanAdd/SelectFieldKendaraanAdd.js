import React,{Component} from 'react'
import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Left,
    Right,
    Body,
    Text,
    Icon,
    Card,
    CardItem,
    Grid,
    Row,
    View,
    Col, 
    List,
    ListItem,
    Thumbnail,
    Form,
    Label,
    Input,
    Item,
    DatePicker
} from "native-base";
import { AppState,Platform,Dimensions, Image, FlatList , TouchableOpacity,ScrollView, ImageBackground, BackHandler, Alert, PixelRatio, StyleSheet, TextInput, Picker } from "react-native";
import styles from './style'

class SelectFieldKendaraanAdd extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount= async()=>{

    } 

    render(){
        return(
            <>
                <View style={{flex: 1, flexDirection: 'column'}}>   
                  <Label style={{fontSize: 14, marginLeft: 10, marginTop: 10, alignSelf: 'flex-start', alignItems: 'flex-start'}}>{this.props.Title}</Label>
                    <View 
                        style={{flex: 1,  
                        alignItems: 'center',  
                        justifyContent: 'center',
                        borderColor: 'gray', 
                        borderWidth: 2, 
                        borderRadius: 5, 
                        marginTop: 10, 
                        marginLeft: 20, 
                        marginRight: 20, height: 40, color: '#494A4A', padding: 10 }}>

                              
                                      <Picker
                                        style={{ 
                                          width: "100%",
                                          color: '#344953',  
                                          justifyContent: 'center',
                                        }}
                                        // selectedValue={this.props.jenisKendaraan}
                                        onValueChange={(itemValue, itemIndex) => 
                                          this.props.onValueChange(itemValue)
                                          }
                                      >
                                        

                                        {this.props.allItem.map((value, index) => {
                                            return (
                                                <Picker.Item
                                                label={value.jenis_kendaraan}
                                                value={value.id}
                                                key={index}
                                                />
                                            );
                                            })}

                                      </Picker>

                      </View>
              </View>
            </>
        );
    }
}

export default SelectFieldKendaraanAdd;