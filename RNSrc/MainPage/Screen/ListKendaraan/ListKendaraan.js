import React,{Component} from 'react'
import {BackHandler,TouchableOpacity, Text,View} from "react-native";
import { useNavigation  } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
import CustomNavbar from '../../Header/Navbar/Navbar'
import SweetAlert from 'react-native-sweet-alert';
import {Container} from 'native-base'
import Loading,{getProfile, setUrl, getUrl, decrypt} from '../../../../APIProp/ApiConstants'
import {
  getListKendaraan,
} from './ListKendaraanAxios'

import ListKendaraanPage from './ListKendaraanPage'
import KendaraanAdd from './KendaraanAdd/KendaraanAdd';
import KendaraanEdit from './KendaraanEdit/KendaraanEdit';

import messaging from '@react-native-firebase/messaging';
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import PushNotification from 'react-native-push-notification'
import NetInfo from "@react-native-community/netinfo";
import LostConnection from '../../Component/LostConnection';

class ListKendaraan extends Component {
    constructor(props) {
        super(props);
        this.state = {
          paramLogin:{
            username:'',
            password:'',
          },
          isLoading:false,
          url:null,
          isConnected: null,
          isConnectedChange: null,
          profile: null,
          listKendaraan: [],
          kendaraanAdd: false,
          kendaraanEdit: false,
          kendaraanSelected: null,
        };
      }
    
      componentDidMount= async()=>{
        await this.setState({isLoading:true});
        await setUrl();
        await getUrl().then(async(result)=>{
          this.setState({url:result})
        });
        await this.funcGetProfile();
        var _listKendaraan = await getListKendaraan(this.state.url, this.state.profile.id_transporter)
        await this.setState({isLoading:false, listKendaraan: _listKendaraan});

        await this.pushNotif();
        await this.cekConnection();

      }
      
      cekConnection = async()=>{
        // Cek Connection
        NetInfo.addEventListener(state => {
          if (this.state.isConnected == null) {
            this.setState({isConnected: state.isConnected})
          }
          if (state.isConnected == false) {
            this.setState({isConnected: state.isConnected, isConnectedChange: state.isConnected})
          }
          if (state.isConnected == true) {
            this.setState({isConnectedChange: state.isConnected})
          }      
          console.log("Is connected?", state.isConnected);
        });
      }

      restartConnection = async() =>{
        console.log('Restart Connection', this.state.isConnected)
        if (this.state.isConnected == false && this.state.isConnectedChange == true) {
          console.log('HARUSNYA INI OFF', this.state.isConnected)
          this.props.navigation.replace('MainRouter', {screen: 'ListKendaraan'})
        }
      }

      funcGetProfile=async()=>{
        await getProfile().then(result=>{
          if(result!=null){
            this.setState({profileStr:result})
          }
        })
        if(this.state.profileStr==null){
          this.props.navigation.replace('Login')
        }
        var tmpStr = await decrypt(this.state.profileStr);
        await this.setState({profile:JSON.parse(tmpStr)})
        await this.setState({hak_akses: JSON.parse(tmpStr).hak_akses}) // tambah ini utk hak_akses dan state
        console.log('profile', JSON.parse(tmpStr))
      }

      funcSweetAlert = async(title, subTitle, style) =>{
        SweetAlert.showAlertWithOptions({
            title: title,
            subTitle: subTitle,
            confirmButtonTitle: 'OK',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: style,
            cancellable: true
          },
            callback => {
              console.log('callback') 
              this.backToList()
            }
          );
    }

      onKendaraanAdd=async()=>{
        await this.setState({isLoading:true,kendaraanAdd:false});
        // var _tripSelected = {};
        // _tripSelected.infoTrip = infoTrip;
        // _tripSelected.infoOrder = await getDetailOrder(this.state.url,infoTrip.id);
        // _tripSelected.location = await getLatestLocation(this.state.url,infoTrip.id);
        await this.setState({isLoading:false, kendaraanAdd: true});
      }

      onKendaraanEdit=async(value)=>{
        await this.setState({isLoading:true, kendaraanEdit: false});
        var _kendaraanSelected = {};
        _kendaraanSelected.value = value
        await this.setState({isLoading:false, kendaraanEdit: true, kendaraanSelected: _kendaraanSelected});
        // console.log('Hasil tarik data : ', _kendaraanSelected)
      }

      // funcSweetAlert=(style,title,subTitle)=>{
      //   var context =this;
      //   SweetAlert.showAlertWithOptions({
      //     title: title,
      //     subTitle: subTitle,
      //     confirmButtonTitle: 'OK',
      //     confirmButtonColor: '#000',
      //     otherButtonTitle: 'Cancel',
      //     otherButtonColor: '#dedede',
      //     style: style,
      //     cancellable: false
      //   },
      //   callback => context.backToList(context));
      // }

      backToList=async()=>{
        await this.setState({isLoading:true});
        var _listKendaraan = await getListKendaraan(this.state.url, this.state.profile.id_transporter)
        await this.setState({isLoading:false, listKendaraan: _listKendaraan, kendaraanAdd: false, kendaraanEdit: false});
      }

      backToListWithoutLoad=async()=>{
        await this.setState({kendaraanAdd: false, kendaraanEdit: false});
      }

      refreshContent = async(_listKendaraan) => {
        await this.setState({isLoading: true})
        await this.setState({isLoading:false, listKendaraan: _listKendaraan});
      }

      pushNotif = async () => {
        // Must be outside of any component LifeCycle (such as `componentDidMount`).
        PushNotification.configure({
          // (optional) Called when Token is generated (iOS and Android)
          onRegister: function (token) {
            console.log("TOKEN:", token);
          },
        
          // (required) Called when a remote is received or opened, or local notification is opened
          onNotification: async (notification) => {
            console.log("NOTIFICATION:", notification);
            //Utk dpt Notif
            // process the notification
            //mau apa disini
            //disini setelah ada userInteraction/di klik
            if (notification.userInteraction) {
              // console.log('DO SOMETHING NOTIFICATION KENDARAAN') 
              // this.props.navigation.navigate('ListTrip',{
              //   screen: 'ListTrip',
              //   id_trip: notification.data.id_trip,
              //   id: notification.data.id
              // })
              this.props.navigation.navigate('Dashboard', {screen: 'Dashboard'})
            }
        
            // (required) Called when a remote is received or opened, or local notification is opened
            notification.finish(PushNotificationIOS.FetchResult.NoData);
          },
  
          
          // // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
          // onAction: function (notification) {
          //   console.log("ACTION:", notification.action);
          //   console.log("NOTIFICATION ACTION:", notification);
        
          //   // process the action
  
          //   console.log('waiwawa')
          // },
        
          // // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
          // onRegistrationError: function(err) {
          //   console.error(err.message, err);
          // },
        
          // // IOS ONLY (optional): default: all - Permissions to register.
          // permissions: {
          //   alert: true,
          //   badge: true,
          //   sound: true,
          // },
        
          // Should the initial notification be popped automatically
          // default: true
          popInitialNotification: true,
        
          /**
           * (optional) default: true
           * - Specified if permissions (ios) and token (android and ios) will requested or not,
           * - if not, you must call PushNotificationsHandler.requestPermissions() later
           * - if you are not using remote notification or do not have Firebase installed, use this:
           *     requestPermissions: Platform.OS === 'ios'
           */
          requestPermissions: true,
        });
      }

      render(){
        return(
          <>
          {/* <CustomNavbar title={'List Kendaraan'}/> */}

          <Container>
            {this.state.kendaraanAdd == true ?  
            <CustomNavbar buttonBack={this.backToListWithoutLoad} title={'Tambah Kendaraan'}/> : 
            this.state.kendaraanEdit == true ?  
            <CustomNavbar buttonBack={this.backToListWithoutLoad} title={'Edit Kendaraan'}/> :
            <CustomNavbar title={'Daftar Kendaraan'}/>
            }
            

              {
              this.state.isConnected == false?
                <LostConnection restartConnection={this.restartConnection}/> 
              : 
              this.state.kendaraanAdd ? 
                <KendaraanAdd
                {...this.state}
                backToList={this.backToList}
                funcSweetAlert={this.funcSweetAlert}
                // {...this.props}
                />
                
                :

                this.state.kendaraanEdit ? 

                <KendaraanEdit
                {...this.state}
                backToList={this.backToList}
                funcSweetAlert={this.funcSweetAlert}
                // {...this.props}
                /> 
              
                :

                <ListKendaraanPage 
                  {...this.state}
                  {...this.props}
                  onKendaraanAdd={this.onKendaraanAdd}
                  onKendaraanEdit={this.onKendaraanEdit}
                  refreshContent={this.refreshContent}
                />

                
              }
            </Container>

            {this.state.isLoading ? <Loading/> : false}
          </>
        );
      }
}

ListKendaraanFunc= (props) => {
  const navigation = useNavigation();
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      BackHandler.addEventListener("hardwareBackPress", ()=>props.backAction('ListKendaraan', navigation));
    });

    return unsubscribe;
  }, [navigation]);

  return <ListKendaraan navigation={navigation}/>;
}

export default ListKendaraanFunc;