import React,{Component} from 'react'
import { AppState,Platform,Dimensions, Image, FlatList , TouchableOpacity,ScrollView, ImageBackground, BackHandler, Alert, PixelRatio, StyleSheet, TextInput, Picker } from "react-native";
import { useNavigation  } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
import CustomNavbar from '../../../Header/Navbar/Navbar'
import Loading,{getProfile, encrypt, setUrl, getUrl, decrypt} from '../../../../../APIProp/ApiConstants'
import Axios from 'axios';
import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Left,
    Right,
    Body,
    Text,
    Icon,
    Card,
    CardItem,
    Grid,
    Row,
    View,
    Col, 
    List,
    ListItem,
    Thumbnail,
    Form,
    Label,
    Input,
    Item,
    DatePicker
} from "native-base";
import styles from "./style";
import moment from 'moment'
import { getJenisKendaraan, getPool, getDataKendaraan, getHargaSewa, editKendaraan } from './KendaraanEditAxios';


class KendaraanEdit extends Component {
    constructor(props) {
            super(props);
            this.state = {
            paramLogin:{
                username:'',
                password:'',
            },
            isLoading:false,
            url:null,
            profile: null,
            refId: '',
            jenisKendaraan: '',
            allJenisKendaraan: [],
            platNomor: '',
            status: 'A',
            nomorKir: '',
            // jatuhTempoKir: null,
            // tglBerlakuHargaSewa: null,
            hargaSewa: 0,
            statusHargaSewa: 'A',
            chosenDateSewa: new Date (),
            now_sewa: 0,
            month_sewa: 0,
            year_sewa: 0,
            allPool: [],
            pool: '',
            dedicated: 'Y',
            tahunPembuatan: '2020',
            date: null,
            displayedDate: moment(),
            chosenDate: new Date(),
            now: 0,
            month: 0,
            year: 0,
        };
      }
    
      componentDidMount= async()=>{
        await this.setState({isLoading:true});
        await setUrl();
        await getUrl().then(async(result)=>{
          this.setState({url:result})
        });
        var curDate = new Date(); //Date
        var date= curDate.getDate();
        var month = curDate.getMonth(); //Current Month
        var year = curDate.getFullYear(); //Current Year
        var nowDate = year+"-"+month+"-"+date;

        this.setState({
            now: date,
            month: month,
            year: year,
            now_sewa: date,
            month_sewa: month,
            year_sewa: year
        })


        await this.funcGetProfile();
        var _allJenisKendaraan = await getJenisKendaraan(this.state.url)
        var _allPool = await getPool(this.state.url, this.state.profile.id_transporter)
        var _getData = await getDataKendaraan(this.state.url, this.props.kendaraanSelected.value.id)
        // await this.getJenisKendaraan()
        // await this.getPool()
        // await this.getData()

        await this.setState({
          allJenisKendaraan: _allJenisKendaraan,
          allPool: _allPool,

          allData: _getData, 
          refId: _getData.external_id,
          tahunPembuatan : _getData.tahunPembuatan,
          jenisKendaraan: _getData.jenis_kendaraan,
          platNomor: _getData.plat_nomor,
          status: _getData.active,
          nomorKir: _getData.nomor_kir,
          pool: _getData.pool_id,
          dedicated: _getData.dedicated,
          id: _getData.kendaraan_id,

          now_kir: _getData._nowKir,
          month_kir: _getData._monthKir,
          year_kir: _getData._yearKir,
          chosenDate: new Date(_getData._yearKir, _getData._monthKir, _getData._nowKir)
        })
        
        if (this.state.profile.id_transporter == '2') {
            let _getHargaSewa = await getHargaSewa(this.state.url, this.props.kendaraanSelected.value.id)
            await this.setState({ 
              hargaSewa: _getHargaSewa.harga_sewa,
              statusHargaSewa: _getHargaSewa.status,
              now_kir: _getHargaSewa._now,
              month_kir: _getHargaSewa._month,
              year_kir: _getHargaSewa._year,
              chosenDateSewa: new Date(_getHargaSewa._year, _getHargaSewa._month, _getHargaSewa._now),
              
            });
        }

        setTimeout(() => {this.setState({isLoading: false})}, 3000)
        // await this.setState({isLoading:false});
      }

      funcGetProfile=async()=>{
        await getProfile().then(result=>{
          if(result!=null){
            this.setState({profileStr:result})
          }
        })
        if(this.state.profileStr==null){
          this.props.navigation.replace('Login')
        }
        var tmpStr = await decrypt(this.state.profileStr);
        this.setState({profile:JSON.parse(tmpStr)})
        // console.log('profile', JSON.parse(tmpStr))
      }

      setDate = (newDate) => {
        this.setState({ chosenDate: newDate });
    
        console.log('chosen date: ', newDate)
        console.log('type of: ', typeof(newDate))
      }
    
      setDateSewa = (newDate) => {
        this.setState({ chosenDateSewa: newDate });
    
        console.log('chosen date: ', newDate)
        console.log('type of: ', typeof(newDate))
      }


    editKendaraan = async ()=>{
      // this.setState({buttondisabled: true});
      let title = 'Invalid Input';
      if(this.state.refId === '' ) Alert.alert(title, 'Ref ID tidak boleh kosong!');
      else if(this.state.jenisKendaraan === '') Alert.alert(title, 'Jenis Kendaraan tidak boleh kosong!');
      else if(this.state.platNomor === '') Alert.alert(title, 'Plat Nomor tidak boleh kosong!');
      else if(this.state.status === '') Alert.alert(title, 'Status tidak boleh kosong!');
      else if(this.state.nomorKir === '') Alert.alert(title, 'Nomor KIR tidak boleh kosong!');
      else if(this.state.chosenDate === null) Alert.alert(title, 'Jatuh tempo KIR tidak boleh kosong!');
      else if(this.state.pool === '') Alert.alert(title, 'Pool tidak boleh kosong!');
      else if(this.state.dedicated === '') Alert.alert(title, 'Dedicated tidak boleh kosong!');
      else if(this.state.tahunPembuatan === '') Alert.alert(title, 'Tahun pembuatan tidak boleh kosong!');
      else{
  
        this.setState({isLoading: true})

        let jatuhTempoKir = this.state.chosenDate.getFullYear() + "-" + (this.state.chosenDate.getMonth()+1) + "-" + this.state.chosenDate.getDate()
        let nomorKir = this.state.nomorKir
        let jenisKendaraan = parseInt(this.state.jenisKendaraan)
        let pool = parseInt(this.state.pool)
        let tahunPembuatan = parseInt(this.state.tahunPembuatan)


        //query
        let _editKendaraan = await editKendaraan(this.state.url, this.props.kendaraanSelected.value.id, this.state.profile.id_transporter, this.state.profile.id_user, this.state.refId, jenisKendaraan, this.state.platNomor, this.state.status, nomorKir, jatuhTempoKir, pool, this.state.dedicated, tahunPembuatan, null, null, null)
          
        this.setState({isLoading:false}); 
        // Alert.alert("Status", _editKendaraan ,[
        //   {
        //     text: 'Ok',
        //     onPress: ()=>this.props.backToList()
        //   }
        // ]);

        this.props.funcSweetAlert('status',_editKendaraan[0], _editKendaraan[1])


      }
    }

    editKendaraanIntrans = async ()=>{
        // this.setState({buttondisabled: true});
        let title = 'Invalid Input';
        if(this.state.refId === '' ) Alert.alert(title, 'Ref ID tidak boleh kosong!');
        else if(this.state.jenisKendaraan === '') Alert.alert(title, 'Jenis Kendaraan tidak boleh kosong!');
        else if(this.state.platNomor === '') Alert.alert(title, 'Plat Nomor tidak boleh kosong!');
        else if(this.state.status === '') Alert.alert(title, 'Status tidak boleh kosong!');
        else if(this.state.chosenDateSewa === null) Alert.alert(title, 'Tanggal berlaku harga tidak boleh kosong!');
        else if(this.state.hargaSewa === '') Alert.alert(title, 'Harga sewa kendaraan tidak boleh kosong!');
        else if(this.state.statusHargaSewa === '') Alert.alert(title, 'Status harga sewa tidak boleh kosong!');
        else if(this.state.nomorKir === '') Alert.alert(title, 'Nomor KIR tidak boleh kosong!');
        else if(this.state.chosenDate === null) Alert.alert(title, 'Jatuh tempo KIR tidak boleh kosong!');
        else if(this.state.pool === '') Alert.alert(title, 'Pool tidak boleh kosong!');
        else if(this.state.dedicated === '') Alert.alert(title, 'Dedicated tidak boleh kosong!');
        else if(this.state.tahunPembuatan === '') Alert.alert(title, 'Tahun pembuatan tidak boleh kosong!');
        else{
            
          this.setState({isLoading:true});
          let uid = this.state.profile.id_user
          
          //master kendaraan
          let jatuhTempoKir = this.state.chosenDate.getFullYear() + "-" + (this.state.chosenDate.getMonth()+1) + "-" + this.state.chosenDate.getDate()
          let nomorKir = this.state.nomorKir
          let jenisKendaraan = parseInt(this.state.jenisKendaraan)
          let pool = parseInt(this.state.pool)
          let tahunPembuatan = parseInt(this.state.tahunPembuatan)

          //harga sewa khusus instrans
          let tglBerlakuHargaSewa = this.state.chosenDateSewa.getFullYear() + "-" + (this.state.chosenDateSewa.getMonth()+1) + "-" + this.state.chosenDateSewa.getDate()
          let statusHargaSewa = this.state.statusHargaSewa
          let hargaSewa = parseInt(this.state.hargaSewa)
       
          //query
          let _editKendaraan = await editKendaraan(this.state.url, this.props.kendaraanSelected.value.id, this.state.profile.id_transporter, this.state.profile.id_user, this.state.refId, jenisKendaraan, this.state.platNomor, this.state.status, nomorKir, jatuhTempoKir, pool, this.state.dedicated, tahunPembuatan, tglBerlakuHargaSewa, statusHargaSewa, hargaSewa)
          
          this.setState({isLoading:false}); 
          // Alert.alert("Status", _editKendaraan ,[
          //   {
          //     text: 'Ok',
          //     onPress: ()=>this.props.backToList()
          //   }
          // ]);

          this.props.funcSweetAlert('status',_editKendaraan[0], _editKendaraan[1])

        }
      }
    

      render(){
        return(
          <>
            {/* <CustomNavbar title={'Kendaraan Add'}/> */}



                {/* <Text style={{flex: 1}} onPress={()=>this.props.backToList()}>wkwk new</Text> */}

                <Content>
                    {/* <Text style={styles.title}>EDIT KENDARAAN</Text> */}

                    <View style={styles.viewTextInput}>
                        <Label style={styles.label}>Ref. ID Kendaraan</Label>
                        <TextInput
                            style={styles.textInputDisabled}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder="Masukan ref.ID Kendaraan"
                            placeholderTextColor = "#CACCD7"
                            selectionColor="#008c45"
                            onChangeText={input=>this.setState({refId: input})}
                            value = {this.state.refId}
                            caretHidden={true}
                            editable ={false}
                            selectTextOnFocus = {false}
                            
                            />
                    </View>


                    <View style={{flex: 1, flexDirection: 'column'}}>   
                        <Label style={{fontSize: 14, marginLeft: 10, marginTop: 10, alignSelf: 'flex-start', alignItems: 'flex-start'}}>Jenis Kendaraan</Label>
                        <View 
                            style={{flex: 1,  
                            alignItems: 'center',  
                            justifyContent: 'center',
                            borderColor: 'gray', 
                            borderWidth: 2, 
                            borderRadius: 5, 
                            marginTop: 10, 
                            marginLeft: 20, 
                            marginRight: 20, height: 40, color: '#494A4A', padding: 10 }}>

                              
                                      <Picker
                                        style={{ 
                                          width: "100%",
                                          color: '#344953',  
                                          justifyContent: 'center',
                                        }}
                                        selectedValue={this.state.jenisKendaraan}
                                        onValueChange={input=>this.setState({jenisKendaraan: input})}
                                      >
                                        

                                        {this.state.allJenisKendaraan.map((value, index) => {
                                            return (
                                                <Picker.Item
                                                label={value.jenis_kendaraan}
                                                value={value.id}
                                                key={index}
                                                />
                                            );
                                            })}

                                      </Picker>

                        </View>
                    </View>


                    <View style={styles.viewTextInput}>
                        <Label style={styles.label}>Plat Nomor</Label>
                        <TextInput
                        style={[styles.textInput,{textTransform: 'uppercase'}]}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder="Plat Nomor"
                        placeholderTextColor = "#CACCD7"
                        selectionColor="#008c45"
                        autoCapitalize= 'characters'
                        onChangeText={input=>this.setState({platNomor: input})}
                        value = {this.state.platNomor}
                        />
                    </View>


                    <View style={{flex: 1, flexDirection: 'column'}}>   
                        <Label style={{fontSize: 14, marginLeft: 10, marginTop: 10, alignSelf: 'flex-start', alignItems: 'flex-start'}}>Status</Label>
                            <View 
                                style={{flex: 1,  
                                alignItems: 'center',  
                                justifyContent: 'center',
                                borderColor: 'gray', 
                                borderWidth: 2, 
                                borderRadius: 5, 
                                marginTop: 10, 
                                marginLeft: 20, 
                                marginRight: 20, height: 40, color: '#494A4A', padding: 10 }}>

                                    
                                            <Picker
                                                style={{ 
                                                width: "100%",
                                                color: '#344953',  
                                                justifyContent: 'center',
                                                }}
                                                selectedValue={this.state.status}
                                                onValueChange={input=>this.setState({status: input})}
                                            >
                                                <Picker.Item label="Active" value="A" />
                                                <Picker.Item label="In Active" value="I" />
                                            </Picker>

                            </View>
                    </View>


                    {/* Khusus PT Intrans */}
                    {this.state.profile == null || this.state.profile.id_transporter == '2' ?
                    <View>
                        <View style={styles.viewTextInput}>   
                            <Label style={styles.label}>Harga Sewa Kendaraan</Label>
                            <TextInput
                                style={styles.textInput}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                placeholder="Harga Sewa Kendaraan"
                                placeholderTextColor = "#CACCD7"
                                selectionColor="#008c45"
                                keyboardType="number-pad"
                                onChangeText={input=>this.setState({hargaSewa: input})}
                                value = {this.state.hargaSewa}
                                
                            />
                        </View>

                        <View style={styles.viewTextInput}>   
                            <Label style={styles.label}>Tanggal berlaku harga</Label>
                            <View style={styles.textInput}>
                            <DatePicker
                                defaultDate={this.state.chosenDateSewa}
                                // minimumDate={new Date(this.state.year_sewa, this.state.month_sewa, this.state.now_sewa)}
                                locale={"en"}
                                timeZoneOffsetInMinutes={undefined}
                                modalTransparent={false}
                                animationType={"fade"}
                                androidMode={"default"}
                                placeHolderText={this.state.chosenDateSewa.getFullYear() + "-" + (this.state.chosenDateSewa.getMonth()+1) + "-" + this.state.chosenDateSewa.getDate()}
                                textStyle={{ color: "#008c45", marginTop: -5, marginLeft: -5}}
                                placeHolderTextStyle={{ color: "#d3d3d3", marginTop: -5, marginLeft: -5 }}
                                onDateChange={this.setDateSewa}
                                disabled={false}
                        />
                            </View>
                        </View>
                        

                        <View style={{flex: 1, flexDirection: 'column'}}>   
                            <Label style={{fontSize: 14, marginLeft: 10, marginTop: 10, alignSelf: 'flex-start', alignItems: 'flex-start'}}>Status Harga Sewa</Label>
                            <View 
                                style={{flex: 1,  
                                alignItems: 'center',  
                                justifyContent: 'center',
                                borderColor: 'gray', 
                                borderWidth: 2, 
                                borderRadius: 5, 
                                marginTop: 10, 
                                marginLeft: 20, 
                                marginRight: 20, height: 40, color: '#494A4A', padding: 10 }}>

                                        
                                                <Picker
                                                style={{ 
                                                    width: "100%",
                                                    color: '#344953',  
                                                    justifyContent: 'center',
                                                }}
                                                selectedValue={this.state.statusHargaSewa}
                                                onValueChange={input=>this.setState({statusHargaSewa: input})}
                                                >
                                                <Picker.Item label="Active" value="A" />
                                                <Picker.Item label="In Active" value="I" />
                                                </Picker>

                                </View>
                        </View>
                    </View>
                    : null  
                    }

                    <View style={styles.viewTextInput}>   
                        <Label style={styles.label}>Nomor KIR</Label>
                            <TextInput
                            style={styles.textInput}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder="Nomor KIR"
                            placeholderTextColor = "#CACCD7"
                            selectionColor="#008c45"
                            keyboardType="phone-pad"
                            onChangeText={input=>this.setState({nomorKir: input})}
                            value = {this.state.nomorKir}
                            
                            />
                    </View>


                    <View style={styles.viewTextInput}>   
                        <Label style={styles.label}>Jatuh tempo KIR</Label>
                        <View style={styles.textInput}>
                        <DatePicker
                            defaultDate={this.state.chosenDate}
                            // defaultDate = {new Date(this.state.year_kir, this.state.month_kir, this.state.now_kir)}
                            // minimumDate={new Date(this.state.year, this.state.month, this.state.now)}
                            // maximumDate={new Date(2018, 12, 31)}
                            locale={"en"}
                            timeZoneOffsetInMinutes={undefined}
                            modalTransparent={false}
                            animationType={"fade"}
                            androidMode={"default"}
                            placeHolderText={this.state.chosenDate.getFullYear() + "-" + (this.state.chosenDate.getMonth()+1) + "-" + this.state.chosenDate.getDate()}
                            // placeHolderText = {!this.state.chosenDate ? placeholder : null}
                            textStyle={{ color: "#008c45", marginTop: -5, marginLeft: -5}}
                            placeHolderTextStyle={{ color: "#d3d3d3", marginTop: -5, marginLeft: -5 }}
                            onDateChange={this.setDate}
                            disabled={false}
                        />
                        </View>
                    </View>

                    <View style={{flex: 1, flexDirection: 'column'}}>   
                        <Label style={{fontSize: 14, marginLeft: 10, marginTop: 10, alignSelf: 'flex-start', alignItems: 'flex-start'}}>Pool</Label>
                        <View 
                            style={{flex: 1,  
                            alignItems: 'center',  
                            justifyContent: 'center',
                            borderColor: 'gray', 
                            borderWidth: 2, 
                            borderRadius: 5, 
                            marginTop: 10, 
                            marginLeft: 20, 
                            marginRight: 20, height: 40, color: '#494A4A', padding: 10 }}>

                                
                                        <Picker
                                            style={{ 
                                            width: "100%",
                                            color: '#344953',  
                                            justifyContent: 'center',
                                            }}
                                            selectedValue={this.state.pool}
                                            onValueChange={input=>this.setState({pool: input})}
                                        >
                                            
                                            {this.state.allPool.map((value, index) => {
                                                return (
                                                    <Picker.Item
                                                    label={value.nama}
                                                    value={value.id}
                                                    key={index}
                                                    />
                                                );
                                                })}


                                        </Picker>

                        </View>
                    </View> 



                    <View style={{flex: 1, flexDirection: 'column'}}>   
                        <Label style={{fontSize: 14, marginLeft: 10, marginTop: 10, alignSelf: 'flex-start', alignItems: 'flex-start'}}>Dedicated</Label>
                            <View 
                                style={{flex: 1,  
                                alignItems: 'center',  
                                justifyContent: 'center',
                                borderColor: 'gray', 
                                borderWidth: 2, 
                                borderRadius: 5, 
                                marginTop: 10, 
                                marginLeft: 20, 
                                marginRight: 20, height: 40, color: '#494A4A', padding: 10 }}>

                                    
                                            <Picker
                                                style={{ 
                                                width: "100%",
                                                color: '#344953',  
                                                justifyContent: 'center',
                                                }}
                                                selectedValue={this.state.dedicated}
                                                onValueChange={input=>this.setState({dedicated: input})}
                                            >
                                                <Picker.Item label="Yes" value="Y" />
                                                <Picker.Item label="No" value="N" />
                                            </Picker>

                            </View>
                    </View> 

                    <View style={{flex: 1, flexDirection: 'column'}}>   
                        <Label style={{fontSize: 14, marginLeft: 10, marginTop: 10, alignSelf: 'flex-start', alignItems: 'flex-start'}}>Tahun Pembuatan</Label>
                            <View 
                                style={{flex: 1,  
                                alignItems: 'center',  
                                justifyContent: 'center',
                                borderColor: 'gray', 
                                borderWidth: 2, 
                                borderRadius: 5, 
                                marginTop: 10, 
                                marginLeft: 20, 
                                marginRight: 20, height: 40, color: '#494A4A', padding: 10 }}>

                                    
                                            <Picker
                                                style={{ 
                                                width: "100%",
                                                color: '#344953',  
                                                justifyContent: 'center',
                                                }}
                                                selectedValue={this.state.tahunPembuatan}
                                                onValueChange={input=>this.setState({tahunPembuatan: input})}
                                            >
                                                <Picker.Item label="2020" value="2020" />
                                                <Picker.Item label="2019" value="2019" />
                                                <Picker.Item label="2018" value="2018" />
                                                <Picker.Item label="2017" value="2017" />
                                                <Picker.Item label="2016" value="2016" />
                                                <Picker.Item label="2015" value="2015" />
                                                <Picker.Item label="2014" value="2014" />
                                                <Picker.Item label="2013" value="2013" />
                                                <Picker.Item label="2012" value="2012" />
                                                <Picker.Item label="2011" value="2011" />
                                                <Picker.Item label="2010" value="2010" />
                                                <Picker.Item label="2009" value="2009" />
                                                <Picker.Item label="2008" value="2008" />
                                                <Picker.Item label="2007" value="2007" />
                                                <Picker.Item label="2006" value="2006" />
                                            </Picker>

                            </View>
                    </View>

                    <View style={{flex: 1, flexDirection: 'column', marginBottom: 15, marginTop: 10}}>   
                        {/* <Label style={{fontSize: 14, 
                                        // marginLeft: 10, 
                                        marginTop: 10, alignSelf: 'center', alignItems: 'center'}}>Edit Kendaraan</Label> */}
                            <Button
                                style={[styles.button, {justifyContent: 'center', alignItems: 'center'}]}
                                onPress={this.state.profile == null || this.state.profile.id_transporter == '2'? this.editKendaraanIntrans: this.editKendaraan}
                                //Handle Submit PT Intrans
                            >
                            <Text uppercase={false} style={{fontSize: 14, textAlign: "center"}}>Edit Kendaraan</Text>
                            </Button>
                    </View>  


        

                </Content>

            {this.state.isLoading ? <Loading/> : false}
          </>
        );
      }
}

export default KendaraanEdit;