import Axios from 'axios';
import {encrypt} from '../../../../../APIProp/ApiConstants';
import {Alert} from 'react-native'

  export const getJenisKendaraan = async (url) =>{
    let dataQry = `
        select * from mt_master_jenis_kendaraan
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('List Jenis Kendaraan : ', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      console.log('error', err)
      return [];    
    });
  }

  export const getPool = async (url, trans_id) =>{
    let dataQry = `
        select * from mt_master_pool
        where transporter_id = `+trans_id+`
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('List Pool : ', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      console.log('error', err)
      return [];
    });
  }


    export const getDataKendaraan = async (url, id) =>{
      let dataQry = `
          select *, mmk.id as kendaraan_id from mt_master_kendaraan mmk 
          where mmk.id = `+id+`
      `
      let x = await encrypt(dataQry);
      return await Axios.post(url.select,{
        query: x
      }).then(value=>{
        let x = value.data.data[0]
      
        let tahunPembuatan = x.tahun_kendaraan.toString()
        var kir = x.jatuh_tempo_kir
        var tgl = kir.split("-")
        var _yearKir = tgl[0]
        var _monthKir = tgl[1]-1 //minus 1 karena pickernya pakai index array-_-
        var _nowKir = tgl[2].substring(0,2) //salah disini;(

        x.tahunPembuatan = tahunPembuatan
        x._nowKir = _nowKir
        x._monthKir = _monthKir
        x._yearKir = _yearKir
        // x.tahunPembuatan = tahunPembuatan
      

        console.log('sukses data kendaraan : ', x)
        if(x==""){
          return [];
        }
        return x;
      }).catch(err=>{
        console.log('error', err)
        return [];
      });
    }


    export const getHargaSewa = async (url, id) =>{
      let dataQry = `
        select * from harga_sewa_kendaraan_perbulan hksp 
        where hksp.kendaraan_id = `+id+`
      `
      let x = await encrypt(dataQry);
      return await Axios.post(url.select,{
        query: x
      }).then(value=>{
        let x = value.data.data[0]
      
        var tglBerlakuHarga = x.start_date
        var tgl = tglBerlakuHarga.split("-")
        var _year = tgl[0]
        var _month = tgl[1]-1
        var _now = tgl[2].substring(0,2)

        x._year = _year
        x._month = _month
        x._now = _now
        // x.tahunPembuatan = tahunPembuatan
      

        console.log('sukses data harga sewa kendaraan : ', x)
        if(x==""){
          return [];
        }
        return x;
      }).catch(err=>{
        console.log('error', err)
        return [];
      });
    }


    export const editKendaraan = async (url, id, trid, uid, refId, jenisKendaraan, platNomor, status, nomorKir, jatuhTempoKir, pool, dedicated, tahunPembuatan, tglBerlakuHargaSewa, statusHargaSewa, hargaSewa) =>{
  
      // let dataQry = `
      //     update mt_master_kendaraan SET
      //     external_id = '`+refId+`',
      //     jenis_kendaraan = `+jenisKendaraan+`,
      //     plat_nomor = '`+platNomor+`',
      //     active = '`+status+`',
      //     nomor_kir = '`+nomorKir+`',
      //     jatuh_tempo_kir = '`+jatuhTempoKir+`',
      //     pool_id = `+pool+`,
      //     dedicated = '`+dedicated+`',
      //     update_date = now(),
      //     update_by = '`+uid+`',
      //     tahun_kendaraan = `+tahunPembuatan+`
      //     WHERE id = `+id+`
      //     and active != 'B'
      //     returning id as return_id
      // `
      let dataQry = `
          select * from mobiletra_editkendaraan(`+id+`, '`+refId+`', `+jenisKendaraan+`, '`+platNomor+`', '`+status+`', '`+nomorKir+`', '`+jatuhTempoKir+`', `+pool+`, '`+dedicated+`', `+tahunPembuatan+`, '`+uid+`')
      `
      console.log('edit kendaraan query : ', dataQry)
      let x = await encrypt(dataQry);
      return await Axios.post(url.select,{
        query: x
      }).then(value=>{
        let x = value.data.data[0];
        console.log('sukses edit kendaraan', x)

        if (x.return_id == id) {
          if (trid == '2') {
            let _editHargaSewa = editHargaSewa(url, id, trid, uid, refId, jenisKendaraan, platNomor, status, nomorKir, jatuhTempoKir, pool, dedicated, tahunPembuatan, tglBerlakuHargaSewa, statusHargaSewa, hargaSewa)
            return _editHargaSewa
          }else{
            return ['Sukses edit kendaraan!', 'success']; 
          }
        }else{
            return ['Gagal edit kendaraan!', 'warning'];
        }
      
      }).catch(err=>{
      //   return [];
        console.log('error', err)
        return ['Gagal edit kendaraan!', 'warning'];
      });
    }

    export const editHargaSewa = async (url, id, trid, uid, refId, jenisKendaraan, platNomor, status, nomorKir, jatuhTempoKir, pool, dedicated, tahunPembuatan, tglBerlakuHargaSewa, statusHargaSewa, hargaSewa) =>{
  
      // let dataQry = `
      //     update harga_sewa_kendaraan_perbulan SET
      //     start_date = '`+tglBerlakuHargaSewa+`',
      //     status = '`+statusHargaSewa+`',
      //     harga_sewa = '`+hargaSewa+`',
      //     update_date = now(),
      //     update_by = '`+uid+`'
      //     WHERE kendaraan_id = `+id+`
      // `
      let dataQry = `
          select * from mobiletra_edithargasewa(`+id+`, '`+tglBerlakuHargaSewa+`', '`+statusHargaSewa+`', `+hargaSewa+`, '`+uid+`')
      `
      console.log('edit harga sewa : ', dataQry)
      let x = await encrypt(dataQry);
      return await Axios.post(url.select,{
        query: x
      }).then(value=>{
        let x = value.data.data;
        console.log('sukses edit harga sewa')

        return ['Sukses edit kendaraan!', 'success'];
      }).catch(err=>{
      //   return [];
        console.log('error', err)
        return ['Gagal edit kendaraan!', 'warning'];
      });
    }