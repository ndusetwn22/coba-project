import Axios from 'axios';
import {encrypt} from '../../../../APIProp/ApiConstants';

  export const getListKendaraan = async (url,trans_id) =>{
    // let dataQry = `
    //         select mmk.id, mmk.external_id as ref_id, mmk.plat_nomor, mmjk.jenis_kendaraan, mmk.active, mmp.nama as pool, mmk.dedicated, mmk.id_vendor
    //         from mt_master_kendaraan mmk 
    //         left join mt_master_pool mmp
    //         on mmp.id = mmk.pool_id 
    //         left join mt_master_jenis_kendaraan mmjk
    //         on mmjk.id = mmk.jenis_kendaraan 
    //         where mmp.transporter_id = '`+trans_id+`' 
    //         order by mmk.id desc
    // `

    let dataQry = `
          select * from mobiletra_getlistkendaraanmenu(`+trans_id+`)
    `
    console.log('data query list kendaraan menu :', dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      console.log('listKendaraan', x)
      if(x==""){
        return [];
      }
      return x;
    }).catch(err=>{
      return [];
      // console.log('error', err)
    });
  }
