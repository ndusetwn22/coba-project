import Axios from 'axios';
import {encrypt} from '../../../../APIProp/ApiConstants';

export const getListManagementFee = async (url,transporter_id, _startDate,_endDate) =>{
    var startDate = _startDate
    var endDate = _endDate
    // startDate = startDate.format('YYYY-MM-DD')
    // endDate = endDate.format('YYYY-MM-DD')
    // startDate = startDate.format('DD-MMM-YYYY')
    // endDate = endDate.format('DD-MMM-YYYY')
    // console.log('start', endDate)

    // gabisa pakai date karena di view_list_tagihan_trans trx_date = varchar
  
    let dataQry = `
      select * from view_list_tagihan_trans vltt
      where vltt.id = '`+transporter_id+`'
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;

      console.log('list nagih', x)
      if(x==""){
        return [];
      }else{
        for(let i=0;i<x.length;i++){
          x[i].isChecked = false;
          // x[i].amount_due_remaining = Math.ceil(0.98 * x[i].amount_due_remaining);
        }
        return x;
      }
    }).catch(err=>{
      return [];
    });
  }

  // select * from mt_list_tagihan mlt 
  //   where mlt.customer_number = '8888-EXP-0174'
  //   and mlt.trx_date  >= '`+startDate+`'
  //   and mlt.trx_date  <= '`+endDate+`'  

  export const getCountOP = async (url,transporter_id, _startDate,_endDate) =>{
    var startDate = _startDate
    var endDate = _endDate
    startDate = startDate.format('YYYY-MM-DD')
    endDate = endDate.format('YYYY-MM-DD')
    // console.log('start', startDate)
  
    let dataQry = `
      select count(*) from view_list_tagihan_trans vltt
      where vltt.id = '`+transporter_id+`'
      and vltt.status = 'OP'
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data[0].count;
      console.log('OP', x)
    //   if(x==""){
    //     return [];
    //   }
      return x;
    }).catch(err=>{
      return [];
    });
  }

  // select count(*) from mt_list_tagihan mlt 
  // where mlt.customer_number = '8888-EXP-0174'
  // and mlt.trx_date  >= '`+startDate+`'
  // and mlt.trx_date  <= '`+endDate+`'  
  // and mlt.status = 'OP'

  export const getCountCL = async (url,transporter_id, _startDate,_endDate) =>{
    var startDate = _startDate
    var endDate = _endDate
    startDate = startDate.format('YYYY-MM-DD')
    endDate = endDate.format('YYYY-MM-DD')
    // console.log('start', startDate)
  
    let dataQry = `
      select count(*) from view_list_tagihan_trans vltt
      where vltt.id = '`+transporter_id+`'
      and vltt.status = 'CL'
    `
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data[0].count;
      console.log('CL', x)
    //   if(x==""){
    //     return [];
    //   }
      return x;
    }).catch(err=>{
      return [];
    });
  }

  // select count(*) from mt_list_tagihan mlt 
  //   where mlt.customer_number = '8888-EXP-0174'
  //   and mlt.trx_date  >= '`+startDate+`'
  //   and mlt.trx_date  <= '`+endDate+`'  
  //   and mlt.status = 'CL'
