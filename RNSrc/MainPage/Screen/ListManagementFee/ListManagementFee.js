import React,{Component} from 'react'
import {BackHandler,TouchableOpacity, View} from "react-native";
import { useNavigation  } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
import CustomNavbar from '../../Header/Navbar/Navbar'
import {Header, Text,Left, Body, Right, Container} from 'native-base'
import {BurgerMenu} from '../../Component/BurgerMenu'
import {ChattingIcon} from '../../Component/Chatting'
import SweetAlert from 'react-native-sweet-alert';
import Loading,{getProfile, setUrl, getUrl, decrypt} from '../../../../APIProp/ApiConstants'
import moment from 'moment'
import ListManagementFeePage from './ListManagementFeePage'
import { getCountOP, getCountCL, getListManagementFee } from './ListManagementFeeAxios';
import messaging from '@react-native-firebase/messaging';
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import PushNotification from 'react-native-push-notification';
import NetInfo from "@react-native-community/netinfo";
import LostConnection from '../../Component/LostConnection';

class ListManagementFee extends Component {
    constructor(props) {
        super(props);
        this.state = {
          paramLogin:{
            username:'',
            password:'',
          },
          isLoading:false,
          url:null,
          profile: null,
          profileStr:null,
          isConnected: null,
          isConnectedChange: null,
          startDate: moment(),
          endDate: moment(),
          displayedDate: moment(),
          listManagementFee: [],
          countOP: 0,
          countCL: 0,
        };
      }
    
      componentDidMount= async()=>{
        await this.setState({isLoading:true});
        await setUrl();
        await getUrl().then(async(result)=>{
          this.setState({url:result})
        });
        await this.funcGetProfile();
        await this.cekConnection();
        // var _listActiveTrip= await getActiveTrip(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate);
        var _listManagementFee = await getListManagementFee(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate)
        // _listManagementFee.isChecked = false;
        var _getCountOP = await getCountOP(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate)
        var _getCountCL = await getCountCL(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate)
        await this.setState({isLoading:false, listManagementFee: _listManagementFee, countOP: _getCountOP, countCL: _getCountCL});
        // console.log('active trip', _listActiveTrip)

        // await this.pushNotif();

      }

      cekConnection = async()=>{
        // Cek Connection
        NetInfo.addEventListener(state => {
          if (this.state.isConnected == null) {
            this.setState({isConnected: state.isConnected})
          }
          if (state.isConnected == false) {
            this.setState({isConnected: state.isConnected, isConnectedChange: state.isConnected})
          }
          if (state.isConnected == true) {
            this.setState({isConnectedChange: state.isConnected})
          }      
          console.log("Is connected?", state.isConnected);
        });
      }

      restartConnection = async() =>{
        console.log('Restart Connection', this.state.isConnected)
        if (this.state.isConnected == false && this.state.isConnectedChange == true) {
          console.log('HARUSNYA INI OFF', this.state.isConnected)
          this.props.navigation.replace('MainRouter', {screen: 'ListManagementFee'})
        }
      }

      funcGetProfile=async()=>{
        await getProfile().then(result=>{
          if(result!=null){
            this.setState({profileStr:result})
          }
        })
        if(this.state.profileStr==null){
          this.props.navigation.replace('Login')
        }
        var tmpStr = await decrypt(this.state.profileStr);
        this.setState({profile:JSON.parse(tmpStr)})
      }

      funcSweetAlert = async(title, subTitle, style) =>{
        SweetAlert.showAlertWithOptions({
            title: title,
            subTitle: subTitle,
            confirmButtonTitle: 'OK',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: style,
            cancellable: true
          },
            callback => {
              console.log('callback') 
              this.backToList()
            }
          );
    }

      setStartEndDate=async(s,e)=>{
        await this.setState({
          startDate:moment(s, 'YYYY-MM-DD'),
          endDate:moment(e, 'YYYY-MM-DD')
        })
      }

      onSearchButtonClicked=async()=>{
        await this.setState({isLoading:true});
        
        var startDate =this.state.startDate;
        var endDate;
  
        if(this.state.endDate==null){
          endDate = this.state.startDate;
        }else{
          endDate = this.state.endDate;
        }

        var _listManagementFee = await getListManagementFee(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate)
        // _listManagementFee.isChecked = false;
        var _getCountOP = await getCountOP(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate)
        var _getCountCL = await getCountCL(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate)
        //set state disini
        await this.setState({
          isLoading:false,
          listManagementFee: _listManagementFee,
          countOP: _getCountOP,
          countCL: _getCountCL
        });
      }

      backToList=async()=>{
        // await this.setState({isLoading:true});
        // await this.funcGetProfile();
        // // var _listActiveTrip= await getActiveTrip(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate);
        // var _listManagementFee = await getListManagementFee(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate)
        // // _listManagementFee.isChecked = false;
        // var _getCountOP = await getCountOP(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate)
        // var _getCountCL = await getCountCL(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate)
        // await this.setState({isLoading:false, listManagementFee: _listManagementFee, countOP: _getCountOP, countCL: _getCountCL});

        await this.setState({isLoading:true});
        await setUrl();
        await getUrl().then(async(result)=>{
          this.setState({url:result})
        });
        await this.funcGetProfile();
        // var _listActiveTrip= await getActiveTrip(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate);
        var _listManagementFee = await getListManagementFee(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate)
        // _listManagementFee.isChecked = false;
        var _getCountOP = await getCountOP(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate)
        var _getCountCL = await getCountCL(this.state.url, this.state.profile.id_transporter, this.state.startDate, this.state.endDate)
        await this.setState({isLoading:false, listManagementFee: _listManagementFee, countOP: _getCountOP, countCL: _getCountCL});
        // console.log('active trip', _listActiveTrip)
      }

      refreshContent = async(_listManagementFee, _getCountOP, _getCountCL) => {
        await this.setState({isLoading: true})
        await this.setState({isLoading:false, listManagementFee: _listManagementFee, countOP: _getCountOP, countCL: _getCountCL});
      }

      pushNotif = async () => {
        // Must be outside of any component LifeCycle (such as `componentDidMount`).
        PushNotification.configure({
          // (optional) Called when Token is generated (iOS and Android)
          onRegister: function (token) {
            console.log("TOKEN:", token);
          },
        
          // (required) Called when a remote is received or opened, or local notification is opened
          onNotification: async (notification) => {
            console.log("NOTIFICATION:", notification);
            //Utk dpt Notif
            // process the notification
            //mau apa disini
            //disini setelah ada userInteraction/di klik
            if (notification.userInteraction) {
              console.log('DO SOMETHING NOTIFICATION IN MFEE') 
              this.props.navigation.navigate('ListTrip',{
                screen: 'ListTrip',
                id_trip: notification.data.id_trip,
                id: notification.data.id
              })
            }
        
            // (required) Called when a remote is received or opened, or local notification is opened
            notification.finish(PushNotificationIOS.FetchResult.NoData);
          },
  
          
          // // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
          // onAction: function (notification) {
          //   console.log("ACTION:", notification.action);
          //   console.log("NOTIFICATION ACTION:", notification);
        
          //   // process the action
  
          //   console.log('waiwawa')
          // },
        
          // // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
          // onRegistrationError: function(err) {
          //   console.error(err.message, err);
          // },
        
          // // IOS ONLY (optional): default: all - Permissions to register.
          // permissions: {
          //   alert: true,
          //   badge: true,
          //   sound: true,
          // },
        
          // Should the initial notification be popped automatically
          // default: true
          popInitialNotification: true,
        
          /**
           * (optional) default: true
           * - Specified if permissions (ios) and token (android and ios) will requested or not,
           * - if not, you must call PushNotificationsHandler.requestPermissions() later
           * - if you are not using remote notification or do not have Firebase installed, use this:
           *     requestPermissions: Platform.OS === 'ios'
           */
          requestPermissions: true,
        });
      }

      render(){
        return(
          <>
            {/* <CustomNavbar title={'Management Fee'}/> */}
        <Container>
            <Header style={{backgroundColor:"#fff"}} androidStatusBarColor="#000">
              <Left>
                <BurgerMenu/>
              </Left>
              <Body>
                <Text style={{color:'#008c45', fontSize:18, fontWeight:"bold"}}>Management Fee</Text>
              </Body>
              <Right>
                <ChattingIcon/>
              </Right>
            </Header>

            { 
              this.state.isConnected == false?
                <LostConnection restartConnection={this.restartConnection}/> 
              :
                <ListManagementFeePage 
                    {...this.state}
                    {...this.props}
                    setStartEndDate={this.setStartEndDate}
                    onSearchButtonClicked={this.onSearchButtonClicked}
                    backToList={this.backToList}
                    funcSweetAlert={this.funcSweetAlert}
                    refreshContent={this.refreshContent}
                />
            }
            
        </Container>

      {this.state.isLoading ? <Loading/> : false}
          </>
        );
      }
}

ListManagementFeeFunc= (props) => {
  const navigation = useNavigation();
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      BackHandler.addEventListener("hardwareBackPress", ()=>props.backAction('ListManagementFee', navigation));
    });

    return unsubscribe;
  }, [navigation]);

  return <ListManagementFee navigation={navigation}/>;
}

export default ListManagementFeeFunc;