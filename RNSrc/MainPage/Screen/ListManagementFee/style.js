import { StyleSheet, PixelRatio } from 'react-native';


// sebelumnya elevation: 1
const styles = StyleSheet.create({
    flexGrid:{
        flexDirection: 'row',
        padding:10
    },
    flexGridNoPadding:{
        flexDirection: 'row',
    },
    flexGridPLPR:{
        flexDirection: 'row',
        paddingRight:10,
        paddingLeft:10
    },
    labelBoxViewStyle:{
        flex: 1,
        backgroundColor: '#fafafa', 
        margin: 10, 
        borderRadius:10, 
        height: 100,
        width: '50%', 
        shadowColor: "#000",
        alignItems:'center', 
        alignSelf: 'center', 
        justifyContent: 'center',
    
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.48,
        shadowRadius: 11.95,
        elevation: 1, 
        zIndex: 0
    },

    labelPriceBoxStyle:{
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#fafafa', 
        margin: 10, 
        borderRadius:10, 
        height: 60,
        width: '50%', 
        shadowColor: "#000",
        alignItems:'center', 
        // alignSelf: 'center', 
        justifyContent: 'center',
    
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.48,
        shadowRadius: 11.95,
        elevation: 1, 
        zIndex: 0
    },
});

export default styles;
