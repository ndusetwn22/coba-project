import React,{Component} from 'react'
import {
    View,
    CheckBox
} from "react-native";
import styles from './style'
import {
    Text,  
    Card,
    CardItem,
    Body,
    Left,
    Right,
    Button,
    Icon,
    Thumbnail
} from 'native-base'
import { TouchableOpacity } from 'react-native-gesture-handler';

class CardManagementFee extends Component {
    constructor(props) {
        super(props);
        this.state = {
            test: null,
            data: [this.props.value],
            // data: []
        };
    }

    componentDidMount= async()=>{
        // let temp = {}
        // temp.index = this.props.index
        // temp.isChecked = this.props.value.isChecked
        // // console.log(this.props.index)
        // // console.log(this.props.value.isChecked)
        // await this.setState({
        //     test: temp
        // })
        // let temp = []
        // for (let i = 0; i < this.props.value.length; i++) {
        //     const element = array[i];
        //     temp.push(this.props.value[i])
        // }
        // await this.setState({
        //     data: temp
        // })
        // console.log('test data',this.state.data)
    } 

    onValueChange= async(value,index)=>{
        console.log(value)
        console.log(index)
        let temp = this.state.data
        temp[index].isChecked = value
        this.setState({
            data: temp
        })
    }

    numberWithCommas=(x)=>{
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      }

    render(){
        return(
            <Card style={{marginLeft: 10, marginRight: 10}}>
                <CardItem>
                    <Body>
                    <CheckBox value={this.props.value.isChecked} 
                            // onValueChange={e=>{
                            // let temp = this.state.data;
                            // temp[this.props.index].isChecked = e;
                            // this.setState({ data: temp});
                            // }}
                            // console.log(temp[this.props.index].isChecked);}}
                            onValueChange={(hasil)=>this.props.gantiValue(hasil, this.props.index)}
                            />
                        <View style={{flex: 1, flexDirection: 'column', alignContent:'center', alignSelf: 'center'}}>
                            <Text note style={{fontSize: 8, textAlign: 'center', marginBottom: -2, marginLeft: 20}}>
                                Nomor Transaksi
                            </Text>
                            <Text style={{fontSize: 16, fontWeight: 'bold', textAlign: 'center', marginBottom: 5}}>
                                {this.props.value.trx_number}
                            </Text>
                        </View>

                        <View style={{flex: 1, flexDirection: 'row'}}>
                            <View style={{flex:1, marginLeft: 10, marginTop: 5}}>
                                {/* <Text style={{width: "100%", fontSize: 14, fontWeight: 'bold'}}>
                                    {this.props.value.nama}
                                </Text> */}
                                <Text note style={{width: "100%", fontSize: 12}}>
                                    Tanggal Transaksi :{'\n'}{this.props.value.trx_date.substr(0,11)}
                                </Text>
                                <Text note style={{width: "100%", fontSize: 12}}>
                                    No Faktur Pajak :{'\n'}{this.props.value.no_fpajak}
                                </Text>
                                {/* <Text note style={{width: "100%", fontSize: 12}}>
                                    Kode pos :{'\n'}{this.props.value.kode_pos}
                                </Text>
                                <Text note style={{width: "100%", fontSize: 12}}>
                                    Ref ID :{'\n'}{this.props.value.external_id}
                                </Text> */}
                            </View>
                            <View style={{flex:1, marginLeft: 10, marginTop: 5}}>
                                {/* <Text note style={{width: "100%", fontSize: 12, color: '#f6b247'}}>
                                    Due Date :{'\n'}{this.props.value.due_date}
                                </Text>
                                <Text note style={{width: "100%", fontSize: 12, color: '#78e4a9'}}>
                                    Harga :{'\n'}{this.props.value.amount_due_remaining}
                                </Text> */}

                                <View style={{flexDirection: 'column'}}>
                                    <Text note style={{width: "100%", fontSize: 12}}>
                                        Due Date :
                                    </Text>
                                    <Text note style={{width: "100%", fontSize: 12, color: '#f6b247'}}>
                                        {this.props.value.due_date.substr(0,11)}
                                    </Text>
                                </View>
                                <View style={{flexDirection: 'column'}}>
                                    <Text note style={{width: "100%", fontSize: 12}}>
                                        Harga :
                                    </Text>
                                    <Text note style={{width: "100%", fontSize: 12, color: '#78e4a9'}}>
                                        {this.numberWithCommas(this.props.value.amount_due_remaining)}
                                    </Text>
                                </View>
                            </View>
                        </View>
                    </Body>
                </CardItem>
            </Card>
        );
    }
}

export default CardManagementFee;