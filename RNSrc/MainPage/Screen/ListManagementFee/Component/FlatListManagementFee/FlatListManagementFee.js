import React,{Component} from 'react'
import {Text, View,Icon, Content, Button, Container} from 'native-base'
import {
    TouchableOpacity,
    ScrollView,FlatList,
    Alert, Linking, CheckBox, RefreshControl, ToastAndroid
} from "react-native";
import styles from './style'
import CardManagementFee from './CardManagementFee/CardManagementFee'
import SearchInput,{ createFilter } from 'react-native-search-filter';
import Loading,{getProfile, setUrl, getUrl, decrypt, encrypt} from '../../../../../../APIProp/ApiConstants'
import Axios from 'axios'
import moment from 'moment'
import md5 from 'md5'
import { getListManagementFee, getCountOP, getCountCL } from '../../ListManagementFeeAxios';
import Toast from 'react-native-toast-message';

class FlatListManagementFee extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paramLogin:{
                username:'',
                password:'',
              },
            isLoading:false,
            url:null,
            profile: null,
            paramSearch:'',
            dataList: this.props.listManagementFee,
            link: 'https://www.google.co.id',
            pilihSemua: false,
            refreshing: false,
        };
    }

    componentDidMount= async()=>{
        // this.props.changeHeaderTitle("List Trip")
        await this.setState({isLoading:true});
        await setUrl();
        await getUrl().then(async(result)=>{
            this.setState({url:result})
        });
        await this.funcGetProfile();
        await this.setState({isLoading:false});
        console.log('profileeee', this.state.profile.nama_transporter)
        // this.props.funcSweetAlert('status','Sukses melakukan checkout!', 'success')
        // console.log('dataList', this.state.dataList)
    } 

    funcGetProfile=async()=>{
        await getProfile().then(result=>{
          if(result!=null){
            this.setState({profileStr:result})
          }
        })
        if(this.state.profileStr==null){
          this.props.navigation.replace('Login')
        }
        var tmpStr = await decrypt(this.state.profileStr);
        this.setState({profile:JSON.parse(tmpStr)})
        console.log('profile', JSON.parse(tmpStr))
      }

    numberWithCommas=(x)=>{
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      }


    format_date=(d)=>{
        let x = d.getFullYear() + '-';
        //nambahin 0 didepan
        if((d.getMonth() + 1) < 10) x += '0'+(d.getMonth() + 1);
        else x+=(d.getMonth() + 1);
        x+= '-';
        if((d.getDate()) <10) x+= '0'+(d.getDate());
        else x += d.getDate();
        return x;
    }

    gantiPilihSemua = async(value)=>{
        this.setState({
            pilihSemua: !this.state.pilihSemua
        })

    }

    gantiValue = async(value, index)=>{
      // console.log(value)
      // console.log(index)
      let temp = this.state.dataList
      temp[index].isChecked = value
      this.setState({
          dataList: temp
      })
      console.log('dataList', this.state.dataList)
  }


    proses=async()=>{
        let count=0;
        let data=[];
        let total=0;
        let datacheckout=[];
        for(let i=0;i<this.state.dataList.length;i++){
            //ngecek setiap data apakah ceklist atau tidak
          if(this.state.dataList[i].isChecked == true) {
            count = +count + +1;
            let trxDate = new Date(this.state.dataList[i].trx_date);
            let dueDate =  new Date(this.state.dataList[i].due_date);
            // console.log('trx', this.format_date(trxDate))
            // console.log('due', this.format_date(dueDate))
            // dari list object Faktur(liat di dokumentasi mospay) bisa beberapa faktur(ini merupakan prosesnya, ada berapa faktur yg di ceklist)
            let x={
              "Invoice": this.state.dataList[i].trx_number,
              "KodeCust": this.state.profile.id_transporter,
              "TrxDate": this.format_date(trxDate),
              "DueDate": this.format_date(dueDate),
              "Amount": this.state.dataList[i].amount_due_original,
              "RemainingAmount": this.state.dataList[i].amount_due_remaining,
              "Status": "N",
              "FakturPajak": this.state.dataList[i].no_fpajak,
            }
            //dari list object Detail(liat di dokumentasinya mospay), bisa beberapa faktur (ini merupakan proses yg ceklist)
            let y={
              "Product": this.state.dataList[i].trx_number,
              "Price": this.state.dataList[i].amount_due_remaining,
              "Qty": 1,
              "GrandTotal": this.state.dataList[i].amount_due_remaining,
              "TotalDisc": 0,
              "TrxDate": this.format_date(trxDate)
            }
            total = +total + +this.state.dataList[i].amount_due_remaining;
            datacheckout.push(y);
            data.push(x);
          }
        }
        if(count==0) {
          Alert.alert('','Anda belum memilih Invoice!');
        }
        else{
        this.send_faktur(data, datacheckout, total);
        console.log('data_cekout', datacheckout)
        console.log('data_cekout lenght', datacheckout.length)
        console.log('data', data)
        // console.log('total', total)
        console.log('otw send faktur')
        }
      }

      send_faktur=async(data, datacheckout, total)=>{
        this.setState({isLoading: true});
        let query = `
            select * from mt_master_transporter where id = '`+this.state.profile.id_transporter+`'
        `
        let x = await encrypt(query);
        
        return await Axios.post(url.select,{
        query: x
        }).then(result=>{
            let res = result.data.data[0]
            
            let tgl_invoice = moment().format('YYYYMMDDHHmm')

            // let test_tgl = moment().subtract(7, 'hours').format('YYYY-MM-DDTHH:mm:ss.SSSz')+'Z'
            // let besok = moment().subtract(7, 'hours').add(1, 'days').format('YYYY-MM-DDTHH:mm:ss.SSSz')+'Z'
            // let now = moment().format('YYYY-MM-DDTHH:mm:ss.SSSz')
            // let besok = moment().add(1, 'days').format('YYYY-MM-DDTHH:mm:ss.SSSz')

            let now = moment.utc().format('YYYY-MM-DDTHH:mm:ss.SSS')+'Z'
            let besok = moment.utc().add(1, 'days').format('YYYY-MM-DDTHH:mm:ss.SSS')+'Z'
            console.log('now', now)
            console.log('besok', besok)
            // console.log('test_tgl', test_tgl)
            // console.log('besok', besok)
            let reqId = 'INV-'+res.kode_transporter+tgl_invoice


            let key = 'D176ZEWVSIZ5L6V';
            let reqSendFaktur = {
                "ReqID": reqId,
                "MerchantID": "TRANS001",
                "ReqDate": now,
                "JumlahFaktur": data.length,
                "Faktur": data,
                "signature": "e3606ca320cc095a9185733665de3f8b"
            }
            let signature = "SENDFAKTURTRANS001"+reqId+now+data.length+key;
            signature = md5(signature);
            reqSendFaktur.signature=signature;
            console.log(JSON.stringify(reqSendFaktur));
            console.log('reqSendFaktur', reqSendFaktur)
//1
            Axios.post("https://www.mospay.id/sandbox/faktur/sendfaktur",reqSendFaktur).then(result=>{
                console.log('response sendfaktur :',result)
                if(result.data.success.jumlah>0){
                    this.checkout(reqId, now, besok, datacheckout, total);
                    // Alert.alert('','Sukses mengirim faktur');
                }
                else{
                    this.setState({isLoading: false});
                    Alert.alert('','Gagal Mengirim Faktur, silahkan ulangi kembali.');
                }
            }).catch(err2=>{
                console.log(err2);
                this.setState({isLoading: false});
                Alert.alert('','Error Saat Mengirim Faktur, silahkan ulangi kembali.');
            })
          }).catch(err=>{
          console.log(err);
          Alert.alert('','Error, silahkan ulangi kembali.');
          this.setState({isLoading: false});
        });
        
      }



      checkout=async(transactionId, now, besok, datacheckout, total)=>{
        let key = 'D176ZEWVSIZ5L6V';
        let reqCheckOut={
          "Header": {
            "MerchantId": "TRANS001",
            "TransactionId": transactionId,
            "TransactionDate": now
          },
          "Body": {
            "Customer": {
              "CustomerCode": this.state.profile.id_transporter,
              "CustomerName": this.state.profile.nama_transporter,
              "CustomerEmail": this.state.profile.email,
              "CustomerPhone": this.state.profile.hp
            },
            "Destination": {
              "SubCode": "123",
              "SubName": "TESTING MOSTRANS"
            },
            "TotalPrice": total,
            "ExpiredDate": besok,
            "UrlNotif": "",
            "Detail": datacheckout,
            "Signature": "ce5ec112ee287cdcfcab92426f1fb96e"
          }
        }
        let signature = "CREATEPAYMENTTRANS001" + transactionId + now + total + this.state.profile.id_transporter + key;
        signature = md5(signature);
        reqCheckOut.Body.Signature = signature;
        console.log(JSON.stringify(reqCheckOut));
        console.log('request Chekcout:', reqCheckOut)
//2
        Axios.post("https://www.mospay.id/sandbox/CheckOut",reqCheckOut).then(async(result)=>{
          this.setState({isLoading: false});
          console.log('response cekout', result)
          console.log('CHeck Out Berhasil');
          if(result.data.body.checkURL == "")Alert.alert('',result.data.body.errorDesc);
          else {

            // let query=`with return_id_header as(
            //   insert into mt_header_payment (payment_id, customer_number, total_harga, status, source, url_mospay, role) 
            //   values('${transactionId}', '${this.state.profile.id_transporter}', ${total}, 'N', 'W', '${result.data.body.checkURL}', 'TRA')
            //   returning header_id)
            //   insert into mt_line_payment(id_header, id_payment, trx_number, amount )
            //   select header_id,'${transactionId}', '${datacheckout[0].Product}', ${datacheckout[0].GrandTotal} from return_id_header`;

            // for(let i=1;i<datacheckout.length;i++){
            //   query += ` union all select header_id,'${transactionId}', '${datacheckout[i].Product}', ${datacheckout[1].GrandTotal} from return_id_header`;
            // }

            //ini kalo hit api nya sukses, maka insert data
            //with dan return itu untuk langsung ngambil balikannya dari insert into

            // with ins1 as ( insert into mt_header_payment (payment_id, customer_number, total_harga, status, source, url_mospay) 
            //     values ( '`+transactionId+`', '`+this.state.profile.id_transporter+`', `+total+` , 'N','W' , '`+result.data.body.checkURL+`') returning header_id )	
            //     insert into mt_line_payment(id_header, id_payment, trx_number, amount, created ) 
            //     values ((select header_id from ins1), '`+transactionId+`', '`+datacheckout[0].Product+`', '`+datacheckout[0].GrandTotal+`', now())
//3
            let query = `
                with ins1 as ( insert into mt_header_payment (payment_id, customer_number, total_harga, status, source, url_mospay, role) 
                values ( '`+transactionId+`', '`+this.state.profile.id_transporter+`', `+total+` , 'N','W' , '`+result.data.body.checkURL+`', 'TRA') 
                returning header_id )	
                insert into mt_line_payment(id_header, id_payment, trx_number, amount, created ) 
                select header_id, '${transactionId}', '${datacheckout[0].Product}', ${datacheckout[0].GrandTotal}, now() from ins1
            `

            for(let i=1;i<datacheckout.length;i++){
                  query += `union all select header_id,'${transactionId}', '${datacheckout[i].Product}', ${datacheckout[i].GrandTotal}, now() from ins1`;
                }

            console.log(query);
            await encrypt(query)
                .then(value=>query=value);
                //insert/select
            return await Axios.post(url.select,{
                query: query
                }).then(result2=>{
                    // console.log(result2.data);
                    console.log('sukses insert query',result2)
                    this.setState({link: result.data.body.checkURL});
                    Linking.openURL(result.data.body.checkURL)
                    // Alert.alert("Status","Sukses melakukan checkout!",[
                    //     {
                    //       text: 'Ok',
                    //       onPress: ()=>this.props.backToList()
                    //     }
                    //   ]);
                    this.props.funcSweetAlert('status','Sukses melakukan checkout!', 'success')
                  }).catch(err=>{
                    console.log(err);
                    this.setState({isLoading: false});
                    Alert.alert('','Gagal insert query header & line payment!');
                  })         
            };
          this.setState({isLoading: false})
          Linking.openURL(result.data.body.checkURL)
        }).catch(err=>{
          console.log(err);
          this.setState({isLoading: false});
          Alert.alert('','Gagal saat melakukan Check Out');
        })
      }

      onRefresh= async() => {
        this.setState({refreshing: true})
  
        var _listManagementFee = await getListManagementFee(this.props.url, this.props.profile.id_transporter, this.props.startDate, this.props.endDate);
        var _getCountOP = await getCountOP(this.props.url, this.props.profile.id_transporter, this.props.startDate, this.props.endDate);
        var _getCountCL = await getCountCL(this.props.url, this.props.profile.id_transporter, this.props.startDate, this.props.endDate);

        // await this.setState({isLoading:false, listManagementFee: _listManagementFee, countOP: _getCountOP, countCL: _getCountCL});
  
  
        await this.props.refreshContent(_listManagementFee, _getCountOP, _getCountCL)
  
        // Toast.show({
        //   type: 'info',
        //   position: 'bottom',
        //   text1: 'Refreshing',
        //   text2: 'Refresh data ...',
        //   visibilityTime: 3000,
        //   autoHide: true,
        //   topOffset: 30,
        //   bottomOffset: 40,
        //   onShow: () => {},
        //   onHide: () => {}
        // })

          ToastAndroid.show('Refreshing', ToastAndroid.SHORT);
          this.setState({refreshing: false})
  
      };



    render(){
        let count=0;
        let total=0;
        let total_belum_bayar = 0
        let jumlah_tagihan = 0
        for(let i=0;i<this.state.dataList.length;i++){
          total_belum_bayar = +total_belum_bayar+ +this.state.dataList[i].amount_due_remaining;
          jumlah_tagihan++
            if(this.state.dataList[i].isChecked) {
                count++;
                total = +total + +this.state.dataList[i].amount_due_remaining;
                // this.props.total(total)
            }
        }

        let checkall = count!=0 && count==this.state.dataList.length;


        

        // const filteredManagementFee = this.props.listPenagihan.filter(createFilter(this.state.paramSearch, KEYS_TO_FILTERS))

        return(
            <>
                <Content
                    refreshControl={
                      <RefreshControl
                          refreshing={this.state.refreshing}
                          onRefresh={this.onRefresh.bind(this)}
                      />}
                >
                    {/* <View style={styles.flexGridNoPadding}>
                        <View style={styles.labelBoxViewStyle}>
                            <View style={{backgroundColor: '#ccf5f8', height: 50, width: 50, borderRadius: 30, marginTop: 5, alignItems:'center', alignSelf: 'center', justifyContent: 'center'}}>
                                <Icon type="MaterialCommunityIcons" name='pencil-outline' style={{color: '#58d0de'}}/>
                            </View>
                            <View>
                                <Text style={{fontSize: 12, textAlign: 'center', marginTop: 0, color: '#7d8d8c'}}>Process</Text>
                                <Text style={{textAlign: 'center', marginBottom: 0, marginTop: 0, fontSize: 12, color: '#7d8d8c'}}>{this.props.countOP}</Text>
                            </View>
                        </View>

                        <View style={styles.labelBoxViewStyle}>
                            <View style={{backgroundColor: '#fdeed9', height: 50, width: 50, borderRadius: 30, marginTop: 5, alignItems:'center', alignSelf: 'center', justifyContent: 'center'}}>
                                <Icon type="MaterialCommunityIcons" name='file-document-outline' style={{color: '#f6b247'}}/>
                            </View>
                            <View>
                                <Text style={{fontSize: 12, textAlign: 'center', marginTop: 0, color: '#7d8d8c'}}>Close</Text>
                                <Text style={{textAlign: 'center', marginBottom: 0, marginTop: 0, fontSize: 12, color: '#7d8d8c'}}>{this.props.countCL}</Text>
                            </View>
                        </View>
                    </View> */}

                    {/* <View style={styles.flexGridNoPadding}>
                        <View style={styles.labelPriceBoxStyle}>
                            <View style={{backgroundColor: '#FADDDE', height: 40, width: 40, borderRadius: 30, alignItems:'center', alignSelf: 'center', justifyContent: 'center'}}>
                                <Icon type='MaterialIcons' name='attach-money' style={{color: '#EE5E5F', fontSize: 20}}/>
                            </View>
                            <View style={{marginLeft: 10}}>
                                <Text style={{fontSize: 12, textAlign: 'center', marginTop: 0, color: '#7d8d8c'}}>Total tagihan yang belum dibayarkan</Text>
                                <Text style={{textAlign: 'center', marginBottom: 0, marginTop: 0, fontSize: 12, color: '#EE5E5F'}}>
                                {total_belum_bayar == 0?'-':'Rp. '+this.numberWithCommas(total_belum_bayar)+',-'}</Text>
                            </View>
                        </View>
                    </View> */}

                    <View style={styles.flexGridNoPadding}>
                        <View style={styles.labelPriceBoxStyle}>
                            <View style={{backgroundColor: '#FADDDE', height: 40, width: 40, borderRadius: 30, alignItems:'center', alignSelf: 'center', justifyContent: 'center'}}>
                                <Icon type='MaterialIcons' name='attach-money' style={{color: '#EE5E5F', fontSize: 20}}/>
                            </View>
                            <View style={{marginLeft: 10}}>
                                <Text style={{fontSize: 12, textAlign: 'center', marginTop: 0, color: '#7d8d8c'}}>Total belum bayar</Text>
                                <Text style={{textAlign: 'center', marginBottom: 0, marginTop: 0, fontSize: 12, color: '#EE5E5F'}}>
                                {total_belum_bayar == 0?'-':'Rp. '+this.numberWithCommas(total_belum_bayar)+',-'}</Text>
                            </View>
                        </View>
                        <View style={styles.labelPriceBoxStyle}>
                            <View style={{backgroundColor: '#d4f6e7', height: 40, width: 40, borderRadius: 30, alignItems:'center', alignSelf: 'center', justifyContent: 'center'}}>
                                <Icon type='MaterialIcons' name='description' style={{color: '#78e4a9', fontSize: 20}}/>
                            </View>
                            <View style={{marginLeft: 10}}>
                                <Text style={{fontSize: 12, textAlign: 'center', marginTop: 0, color: '#7d8d8c'}}>Jumlah tagihan</Text>
                                <Text style={{textAlign: 'center', marginBottom: 0, marginTop: 0, fontSize: 12, color: '#78e4a9'}}>
                                  {jumlah_tagihan}
                                </Text>
                            </View>
                        </View>
                    </View>


                    <View style={{flexDirection: 'row', marginLeft: 5}}>
                      <CheckBox
                          value={checkall}
                          onValueChange={()=>{
                            if(count!=0 && count==this.state.dataList.length){
                              let temp = this.state.dataList;
                              for(let i=0;i<temp.length;i++){
                                temp[i].isChecked = false;
                              }
                              this.setState({dataList: temp});
                            }
                            else{
                              let temp = this.state.dataList;
                              for(let i=0;i<temp.length;i++){
                                temp[i].isChecked = true;
                              }
                              this.setState({dataList: temp});
                            }}}        
                          style={{alignSelf: 'center'}}
                      />
                      <Text style={{marginTop: 5}}>Pilih semua tagihan</Text>
                    </View>
                    
                    <ScrollView>
                            <FlatList
                                data={
                                    this.state.dataList
                                }
                                style={{marginBottom:10}}
                                renderItem={({ item: value, index }) => {
                                    return (
                                        <CardManagementFee {...this.props} index={index} value={value} gantiValue={this.gantiValue} />
                                    );
                                }}
                                keyExtractor={(item, index) => index.toString()}
                            /> 
                    </ScrollView>
                </Content>

              <View style={styles.flexGridNoPadding}>
                {/* <TouchableOpacity onPress={this.proses}> */}
                        <View style={styles.labelPriceBoxStyle}>
                            <View style={{backgroundColor: '#d4f6e7', height: 40, width: 40, borderRadius: 30, alignItems:'center', alignSelf: 'center', justifyContent: 'center'}}>
                                <Icon type='MaterialIcons' name='attach-money' style={{color: '#78e4a9', fontSize: 20}}/>
                            </View>
                            <View style={{marginLeft: 10}}>
                                <Text style={{fontSize: 12, textAlign: 'center', marginTop: 0, color: '#7d8d8c'}}>Total tagihan</Text>
                                <Text style={{textAlign: 'center', marginBottom: 0, marginTop: 0, fontSize: 12, color: '#78e4a9'}}>
                                {total == 0?'-':'Rp. '+this.numberWithCommas(total)+',-'}</Text>
                            </View>
                        </View>
                  {/* </TouchableOpacity> */}

                  <TouchableOpacity onPress={this.proses} style={styles.labelPriceBoxStyle2}>
                        {/* <View style={styles.labelPriceBoxStyle2} onTouchStart={this.proses}> */}
                            <View style={{marginLeft: 10}}>
                                <Text style={{fontSize: 12, textAlign: 'center', marginTop: 0, color: 'white'}}>Proses CheckOut 
                                {count == 0 ? null: ' ('+count+')'}</Text>
                            </View>
                        {/* </View> */}
                  </TouchableOpacity>
              </View>
              <Toast ref={(ref) => Toast.setRef(ref)} />
                {this.state.isLoading ? <Loading/> : false}
            </>
        );
    }
}

export default FlatListManagementFee;