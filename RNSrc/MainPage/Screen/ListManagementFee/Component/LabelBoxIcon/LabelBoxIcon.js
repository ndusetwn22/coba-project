import React,{Component} from 'react'
import {Text, View, Icon} from 'native-base'
import ProgressCircle from 'react-native-progress/Circle';
import styles from './style'

class LabelBoxIcon extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount= async()=>{

    }

    render(){
        return(
            <View style={styles.labelBoxViewStyle}>
                <View style={{backgroundColor: '#D1FCE8', height: 50, width: 50, borderRadius: 30, marginTop: 5, alignItems:'center', alignSelf: 'center', justifyContent: 'center'}}>
                    <Icon type="MaterialCommunityIcons" name={this.props.nameIcon} style={{color: '#4D6680'}}/>
                </View>
                <View>
                    <Text style={{fontSize: 12, textAlign: 'center', marginTop: 0, color: '#7d8d8c'}}>{this.props.titleProcess}</Text>
                    <Text style={{textAlign: 'center', marginBottom: 0, marginTop: 0, fontSize: 12, color: '#7d8d8c'}}> 26 {this.props.count}</Text>
                </View>
            </View>
        );
    }
}

export default LabelBoxIcon;