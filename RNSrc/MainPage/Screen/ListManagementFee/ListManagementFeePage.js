import React,{Component} from 'react'
import {View, ScrollView, Image, Dimensions} from "react-native";
import styles from './style'
import {Text,Button, Content, Container, Icon} from 'native-base'
import { NavigationContainer,useNavigation,DrawerActions  } from '@react-navigation/native';

import DateRange from './Component/DateRange/DateRange'
import ModalCalendar from './Component/DateRange/ModalCalendar'
import LabelBoxIcon from './Component/LabelBoxIcon/LabelBoxIcon'
import FlatListManagementFee from './Component/FlatListManagementFee/FlatListManagementFee';
var listKosong = require('../../../../Assets/list_kosong.png');
const width = Dimensions.get('window').width;

class ListManagementFeePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paramSearch:'',
            modalCalenderVisible: false,
            total: 0,
        };
    }

    componentDidMount= async()=>{

    }

    onSelectDate=async (start,end)=>{
        await this.props.setStartEndDate(start,end);
        await this.setState({modalCalenderVisible: false})
        await this.props.onSearchButtonClicked();
    }

    openModalCalendar=(typeTrip)=>{
        this.setState({modalCalenderVisible: true})
    }

    closeModal=()=>{
        this.setState({modalCalenderVisible: false})
    }
    
    hitungTotal=async(total)=>{
        this.setState({
            total: total
        })
    }

    render(){
        return(
          <>
            {/* <View style={styles.flexGridNoPadding}>
               
            </View> */}
            
            {/* <Container> */}
                {/* <Content> */}
                    {/* <Text onPress={()=>navigation.replace('MainRouter', { screen:'KendaraanAdd'})}>KENDARAAN ADD</Text> */}
                    {/* <Text onPress={()=>this.props.onKendaraanAdd()}>Kendaraan Add</Text> */}
                    {/* <FlatListKendaraan {...this.state} {...this.props}/> */}
                {/* </Content> */}
            {/* </Container> */}
            {/* <Content> */}
                {/* <View style={styles.flexGridNoPadding}>
                    <View style={{flex:1}}>
                        <DateRange openModalCalendar={this.openModalCalendar} {...this.props}/>
                    </View>
                </View> */}


                {/* Ini yg ditest */}
                {/* <View style={styles.flexGridNoPadding}>
                    <View style={styles.labelBoxViewStyle}>
                        <View style={{backgroundColor: '#ccf5f8', height: 50, width: 50, borderRadius: 30, marginTop: 5, alignItems:'center', alignSelf: 'center', justifyContent: 'center'}}>
                            <Icon type="MaterialCommunityIcons" name='pencil-outline' style={{color: '#58d0de'}}/>
                        </View>
                        <View>
                            <Text style={{fontSize: 12, textAlign: 'center', marginTop: 0, color: '#7d8d8c'}}>Process</Text>
                            <Text style={{textAlign: 'center', marginBottom: 0, marginTop: 0, fontSize: 12, color: '#7d8d8c'}}>{this.props.countOP}</Text>
                        </View>
                    </View>

                    <View style={styles.labelBoxViewStyle}>
                        <View style={{backgroundColor: '#fdeed9', height: 50, width: 50, borderRadius: 30, marginTop: 5, alignItems:'center', alignSelf: 'center', justifyContent: 'center'}}>
                            <Icon type="MaterialCommunityIcons" name='file-document-outline' style={{color: '#f6b247'}}/>
                        </View>
                        <View>
                            <Text style={{fontSize: 12, textAlign: 'center', marginTop: 0, color: '#7d8d8c'}}>Close</Text>
                            <Text style={{textAlign: 'center', marginBottom: 0, marginTop: 0, fontSize: 12, color: '#7d8d8c'}}>{this.props.countCL}</Text>
                        </View>
                    </View>
                </View> */}
                {/* </Content> */}

                {/* <View style={styles.flexGridNoPadding}>
                    <View style={styles.labelPriceBoxStyle}>
                        <View style={{backgroundColor: '#d4f6e7', height: 40, width: 40, borderRadius: 30, alignItems:'center', alignSelf: 'center', justifyContent: 'center'}}>
                            <Icon type='MaterialIcons' name='attach-money' style={{color: '#78e4a9', fontSize: 20}}/>
                        </View>
                        <View style={{marginLeft: 10}}>
                            <Text style={{fontSize: 12, textAlign: 'center', marginTop: 0, color: '#7d8d8c'}}>Total tagihan yang dibayarkan</Text>
                            <Text style={{textAlign: 'center', marginBottom: 0, marginTop: 0, fontSize: 12, color: '#78e4a9'}}>
                                 {this.state.total}</Text>
                        </View>
                    </View>
                </View> */}
            
                
                {this.props.listManagementFee.length > 0 ? 
                    <FlatListManagementFee {...this.state} {...this.props} total={this.hitungTotal}/>
                : 
                // <Text note style={{flex: 1, textAlign: 'center', fontStyle:'italic', marginTop: 50}}>Transaksi kosong</Text>
                    <View style={{flex:1, flexDirection: 'column'}}>
                        <Image resizeMode= 'contain' source={listKosong} style={{ width: '80%', height: '80%', opacity: 0.5, alignSelf: 'center'}}></Image>
                        <Text note style={{marginTop: -80, textAlign: 'center', fontStyle:'italic'}}>Transaksi kosong</Text>
                    </View>
                }

                <ModalCalendar {...this.state} {...this.props} onSelectDate={this.onSelectDate} closeModal={this.closeModal}/>
            

            {/* <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text>List Management Fee Screen 2</Text>
            </View> */}
          </>
        );
    }
}

export default ListManagementFeePage;