import * as React from 'react';
import { BackHandler,View, Text } from 'react-native';
import { createDrawerNavigator, DrawerNavigator, useIsDrawerOpen } from '@react-navigation/drawer';
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import PushNotification from 'react-native-push-notification'
import { useNavigation  } from '@react-navigation/native';
import DashboardFunc from './Screen/Dashboard/Dashboard'
import ListTripFunc from './Screen/ListTrip/ListTrip'
import ListJadwalKapalFunc from './Screen/ListJadwalKapal/ListJadwalKapal'
import ListUserFunc from './Screen/ListUser/ListUser'
import ListDriverFunc from './Screen/ListDriver/ListDriver'
import ListKeraniFunc from './Screen/ListKerani/ListKerani'
import ListPoolFunc from './Screen/ListPool/ListPool'
import ListKendaraanFunc from './Screen/ListKendaraan/ListKendaraan'
import ReportFunc from './Screen/Report/Report'
import Sidebar from './Header/Sidebar/CustomSidebar'
import ListManagementFeeFunc from './Screen/ListManagementFee/ListManagementFee';
import ListEpodFunc from './Screen/ListEpod/ListEpod'
import ChattingFunc from './Screen/Chatting/Chatting'

//Add
import KendaraanAddFunc from './Screen/ListKendaraan/KendaraanAdd/KendaraanAdd'

import SweetAlert from 'react-native-sweet-alert';

const Drawer = createDrawerNavigator();
// const isDrawerOpen = useIsDrawerOpen();
// const navigation = useNavigation();

export class MainRouter extends React.Component{

    componentDidMount = async() =>{
        // console.log('main router')
        // await this.pushNotif()
        // console.log('all props', this.props)
        // this.props.navigation.navigate('MainRouter', {Screen: 'Dashboard'})
        // console.log('drawer', isDrawerOpen)
    }

    backAction= (value, param)=> {
        SweetAlert.showAlertWithOptions({
          title: 'Close App',
          subTitle: "Come Back Soon, OK?",
          confirmButtonTitle: 'OK',
          confirmButtonColor: '#000',
          otherButtonTitle: 'Cancel',
          otherButtonColor: '#dedede',
          style: 'success',
          cancellable: true
        },
        callback => 
                BackHandler.exitApp()
        );
        
        return true;
    };

    render(){
        return (
            <Drawer.Navigator drawerContent={(props) => <Sidebar {...props} />}>
              <Drawer.Screen name="Dashboard">
                  {props => <DashboardFunc backAction={this.backAction} {...this.props}/>}
              </Drawer.Screen>
              <Drawer.Screen name="ListUser">
                  {props => <ListUserFunc backAction={this.backAction} {...this.props}/>}
              </Drawer.Screen>
              <Drawer.Screen name="ListDriver">
                  {props => <ListDriverFunc backAction={this.backAction} {...this.props}/>}
              </Drawer.Screen>
              <Drawer.Screen name="ListKerani">
                  {props => <ListKeraniFunc backAction={this.backAction} {...this.props}/>}
              </Drawer.Screen>
              <Drawer.Screen name="ListPool">
                  {props => <ListPoolFunc backAction={this.backAction} {...this.props}/>}
              </Drawer.Screen>
              <Drawer.Screen name="ListTrip">
                  {props => <ListTripFunc backAction={this.backAction} {...this.props}/>}
              </Drawer.Screen>
              <Drawer.Screen name="ListKendaraan">
                  {props => <ListKendaraanFunc backAction={this.backAction} {...this.props}/>}
              </Drawer.Screen>
              <Drawer.Screen name="Report">
                  {props => <ReportFunc backAction={this.backAction} {...this.props}/>}
              </Drawer.Screen>
              <Drawer.Screen name="ListManagementFee">
                  {props => <ListManagementFeeFunc backAction={this.backAction} {...this.props}/>}
              </Drawer.Screen>
              <Drawer.Screen name="ListEpod">
                  {props => <ListEpodFunc backAction={this.backAction} {...this.props}/>}
              </Drawer.Screen>
              <Drawer.Screen name="ListJadwalKapal">
                  {props => <ListJadwalKapalFunc backAction={this.backAction} {...this.props}/>}
              </Drawer.Screen>
              <Drawer.Screen name="Chatting">
                  {props => <ChattingFunc backAction={this.backAction} {...this.props}/>}
              </Drawer.Screen>
              {/* <Drawer.Screen name="KendaraanAdd">
                  {props => <KendaraanAddFunc backAction={this.backAction} {...this.props}/>}
              </Drawer.Screen> */}
            </Drawer.Navigator>
          );
    }
}