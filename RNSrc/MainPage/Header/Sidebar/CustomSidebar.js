import React, { Component } from 'react';
import {NavigationActions} from 'react-navigation';
import { ImageBackground, View, StyleSheet, SafeAreaView,Dimensions,Image  } from 'react-native'
import {Thumbnail,Text, ListItem, Left, Body, Icon, Right, Content} from 'native-base';
import { white } from 'ansi-colors';
import StyleSidebar from './StyleSidebar'
import {SidebarPageIcon} from './Component/SidebarPageIcon'
import {getProfile, decrypt,logOut, setUrl, getUrl, encrypt} from '../../../../APIProp/ApiConstants'
import Axios from 'axios';

const width = Dimensions.get('window').width;

var truk = require('../../../../Assets/truc1.png');
var mostrans = require('../../../../Assets/trans3.png')
var mostrans2 = require('../../../../Assets/mostrans_logo_v2.jpg')

export default class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profileStr:null,
      profile:{
        nama:'',
        email:'',
        hp:'',
        nama_transporter:'',
        foto_logo:'',
        hak_akses:'',
        id_user: '',
        token_fcm: '',
        url: '',
        is_laut: '',
        sidebar: null
      }
    };
  }

  componentDidMount=async()=>{
    await setUrl();
      await getUrl().then(async(result)=>{
        this.setState({url:result})
      });
    await getProfile().then(result=>{
      if(result!=null){
        this.setState({profileStr:result})
      }
    })
    if(this.state.profileStr!=null){
      var tmpStr = await decrypt(this.state.profileStr);
      this.setState({profile:JSON.parse(tmpStr)})
      // console.log('sidebar :', this.state.profile)
    }
    let _sidebar = this.sidebar(this.state.url);
    // _sidebar.map((item, index)=>{
    //   console.log('item : ', item)
    // })

    // console.log('CUSTOM SIDEBAR : ')
    // console.log('profile sidebar', this.state.profile.id_user, this.state.profile.token_fcm)
    // console.log('url', this.state.url)
    // console.log('CUSTOM SIDE', this.props)
  }

  navigateToScreen = ( route ) =>(
    () => {
    const navigateAction = NavigationActions.navigate({
        routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  })

  onLogout=async()=>{
    await logOut();
    this.props.navigation.replace('Login');
  }


  sidebar = async (url,trans_id) =>{

    // let dataQry = `
    //     select * from mt_config_mobiletra
    //     where hak_akses = '`+this.state.profile.hak_akses+`'
    // `

    // let dataQry = `
    //     select * from mt_config_mobiletra order by order_number asc
    // `

    let dataQry = `
          select * from mobiletra_getsidebar()
    `
    console.log('query sidebar menu :', dataQry)
    let x = await encrypt(dataQry);
    return await Axios.post(url.select,{
      query: x
    }).then(async(value)=>{
      let x = value.data.data;
      // x.test = x
      console.log('sidebar : ', x)
      if(x==""){
        return [];
      }
      await this.setState({sidebar: x})
      return x;
    }).catch(err=>{
      return [];
      // console.log('error', err)
    });
  }

  render() {
    return (
      <>
      {/* <StatusBar barStyle="light-content"/>  e6e6e6e6  008c45*/}
      <SafeAreaView style={StyleSidebar.container}>
        <View style={StyleSidebar.drawer}>
          <View style={[StyleSidebar.drawerheader, {height: width*0.6}]}>
            <View style={[StyleSidebar.logocontainer,{flex: 1}]}>
              <ImageBackground resizeMode='contain' source={mostrans} style={StyleSidebar.logo}>
                <View style={{ position:'absolute', bottom:-25}}>
                  <View style={{flex:12, marginLeft:30, padding:5, borderRadius:10, backgroundColor:"#e6e6e6", width:width*0.6, flexDirection:"row"}}>
                    <View style={{flex:4, paddingLeft:10}}>
                      {this.state.profile.foto_logo!=null && this.state.profile.foto_logo!=""?<Thumbnail source={{uri: this.state.profile.foto_logo}} onError={()=>console.log('')}/>:null}
                    </View>
                    <View style={{flex:8}}>
                      <Text style={StyleSidebar.nama}>{this.state.profile.nama}</Text>
                      <Text style={StyleSidebar.email}>{this.state.profile.nama_transporter.length > 20 ? this.state.profile.nama_transporter.slice(0, 20)+' ... ':this.state.profile.nama_transporter}  ( {this.state.profile.hak_akses} )</Text>
                      <Text style={StyleSidebar.hp}>{this.state.profile.hp}</Text>
                    </View>
                  </View>
                </View>
              </ImageBackground>
            </View>
          </View>
          <Text style={{marginTop:-10, marginLeft:20, fontSize:18, fontWeight:"bold", fontStyle:"italic", borderBottomWidth:1, borderBottomColor:'#ddd', paddingBottom:10}}>
            <Text style={{fontSize:18, fontWeight:"bold",fontStyle:"normal", color:"#008c45"}}>MOSTRANS </Text> 
            Transporter :
          </Text>
          {/* <SidebarPageIcon onLogout={this.onLogout} profile={"this..profile"}/> */}
          <SidebarPageIcon url={this.state.url} id_user={this.state.profile.id_user} token_fcm={this.state.profile.token_fcm} hak_akses={this.state.profile.hak_akses} startdate_epod={this.state.profile.startdate_epod} is_laut={this.state.profile.is_laut} sidebar={this.state.sidebar}/>
          {/* <ScrollView style={StyleSidebar.drawerbody}>
            {Nav.map((item, index)=>{
              if(item[this.state.hak_akses])
              return(
                <TouchableOpacity style={[StyleSidebar.menu, this.state.active==index ? StyleSidebar.active:'']} key={index} onPress={()=>this.press(index)}>
                  <View style={StyleSidebar.left}>
                    <Icon name={item.icon} style={[StyleSidebar.icon, this.state.active==index ? StyleSidebar.activeFont:'']} type={item.type}/>
                  </View>
                  <View style={StyleSidebar.body}>
                    <Text style={[StyleSidebar.text, this.state.active==index ? StyleSidebar.activeFont:'']}>{item.text}</Text>
                  </View>
                </TouchableOpacity>
              );
            })}
          </ScrollView> */}
        </View>
      </SafeAreaView>
    </>
    )
  }
}
