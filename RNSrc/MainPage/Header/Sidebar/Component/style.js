import { StyleSheet } from 'react-native';


// sebelumnya elevation: 1
const styles = StyleSheet.create({
    styleHeaderPlaceholder:{
        flex:8,
        color: '#3e4747',
        fontSize:15,
    },
});

export default styles;
