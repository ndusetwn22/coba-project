import React from 'react'
import { NavigationContainer,useNavigation,DrawerActions, useRoute  } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/AntDesign';
import IconFeather from 'react-native-vector-icons/Feather';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
// import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import {TouchableOpacity, ScrollView} from "react-native";
import {Text, View} from 'native-base'
import {logOut, encrypt, decrypt, setUrl, getUrl} from '../../../../../APIProp/ApiConstants'
import {StyleSheet} from 'react-native';
import styles from './style'
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import PushNotification from 'react-native-push-notification'
import { deleteTokenFcm } from '../../../../LoginPage/LoginAxios';
import AsyncStorage from '@react-native-community/async-storage';
import Sidebar from '../CustomSidebar';
import Axios from 'axios';



export function SidebarPageIcon(profile) {
    const navigation = useNavigation();
    const route = useRoute();
    const _tempSidebar = null
    if (route.params == undefined) {
        // route.params.screen = 'Dashboard'
        navigation.navigate('MainRouter', { screen:'Dashboard'})
    }

    var _temp = ''
    
    // if (_tempSidebar == null) {

    //     setUrl();
    //     getUrl().then((result)=>{
    //         // this.setState({url:result})
    //         var url = result;
    //     });
        
    //     let dataQry = `
    //         select * from mt_config_mobiletra
    //         where hak_akses = '`+profile.hak_akses+`'
    //     `
    //     console.log('query sidebar menu :', dataQry)
    //     let x = encrypt(dataQry);
    //     return Axios.post(url.select,{
    //       query: x
    //     }).then(async(value)=>{
    //       let x = value.data.data[0];
    //       // x.test = x
    //       console.log('sidebar : ', x)
    //       if(x==""){
    //         return [];
    //       }
    //     //  this.setState({sidebar: x})
    //     _tempSidebar = x;
    //       return x;
    //     }).catch(err=>{
    //       return [];
    //       // console.log('error', err)
    //     });
    //   }
    // console.log('route param screen : ', route.params.screen)
    console.log('SIDEBAR PAGE ICON : ', profile.sidebar)
    console.log('side hak akses : ', profile.hak_akses)
    switch (profile.hak_akses) {
        case 'ADMIN': _temp = 'a_admin'; break;
        case 'USER': _temp = 'a_user'; break;
        case 'FINANCE': _temp = 'a_finance'; break;
    }

    if (profile.sidebar == undefined) {
        
    }else {
    profile.sidebar.map((item, index)=>{
        if (item[_temp]) {
            console.log('masok : ', item[_temp])
            console.log('masokk : ', item)
            console.log('masokkkkk admin : ', item[_temp])
        }else{
            console.log('keluar')
            console.log('keluarr admin : ', item[_temp])
        }
     })
    }
    // if (profile.is_laut == undefined) {
    //     AsyncStorage.clear();
    //     navigation.replace('Login')
    // }

    // console.log('SIDEBAR')
    // console.log('nav', navigation)
    // console.log('route', route)
    // console.log('profile sidebar', profile.is_laut)
    // console.log('akses profile', profile.hak_akses)
    // console.log('startdate_epod', profile.startdate_epod)
    // console.log('sidebar')
    
    return (
        <>

{/* {profile.hak_akses == 'ADMIN' ?    */}
{/* <View> */}
    <ScrollView>

        {
    profile.sidebar == undefined ? null :
    profile.sidebar.map((item, index)=>{
        if (item[_temp]) {
            // // console.log('masok : ', item[_temp])
            // // console.log('masokk : ', item)
            // // console.log('masokkkkk admin : ', item[_temp])
            // <Text>{item.text}</Text>



            if (item.navigation == 'ListJadwalKapal' || item.navigation == 'ListKerani') {
                if (profile.is_laut == 'Y') {
                    return(
                        <TouchableOpacity style={route.params.screen== item.navigation ? style.liststyleSelected:style.liststyle} onPress={()=>navigation.replace('MainRouter', { screen:item.navigation})}>
                            {item.type == 'IconFeather' ? 
                                <IconFeather 
                                    name={item.icon}
                                    size={25} 
                                    style={{
                                        marginLeft:10,
                                        flex:2
                                    }}
                                    color={route.params.screen==item.navigation?'white':'#008c45'}
                                />
                            :
                                <IconFontAwesome 
                                    name={item.icon} 
                                    size={22} 
                                    style={{
                                        marginLeft:10,
                                        flex:2
                                    }}
                                    color={route.params.screen==item.navigation?'white':'#008c45'}
                                />
                            }
                            <Text style={route.params.screen==item.navigation? style.styleHeaderPlaceholderSelected:style.styleHeaderPlaceholder}>{item.text}</Text>
                        </TouchableOpacity>
                        )
                }
                else{
                    return 
                }
            } else if(item.navigation == 'ListDriver'){
                if (profile.is_laut == 'N') {
                    return(
                        <TouchableOpacity style={route.params.screen== item.navigation ? style.liststyleSelected:style.liststyle} onPress={()=>navigation.replace('MainRouter', { screen:item.navigation})}>
                            {item.type == 'IconFeather' ? 
                                <IconFeather 
                                    name={item.icon}
                                    size={25} 
                                    style={{
                                        marginLeft:10,
                                        flex:2
                                    }}
                                    color={route.params.screen==item.navigation?'white':'#008c45'}
                                />
                            :
                                <IconFontAwesome 
                                    name={item.icon} 
                                    size={22} 
                                    style={{
                                        marginLeft:10,
                                        flex:2
                                    }}
                                    color={route.params.screen==item.navigation?'white':'#008c45'}
                                />
                            }
                            <Text style={route.params.screen==item.navigation? style.styleHeaderPlaceholderSelected:style.styleHeaderPlaceholder}>{item.text}</Text>
                        </TouchableOpacity>
                        )
                }
                else{
                    return
                }
            } else if(item.navigation == 'Logout'){
                    return(
                        <TouchableOpacity style={style.liststyle} onPress={async ()=>{await logOut();
                            await deleteTokenFcm(profile.url, profile.id_user, profile.token_fcm); 
                            navigation.replace('Login');}}>
                            <Icon 
                                name='logout' 
                                size={25} 
                                style={{
                                    marginLeft:10,
                                    flex:2
                                }}
                                color='#008c45'
                            />
                            <Text style={styles.styleHeaderPlaceholder}>Logout</Text>
                        </TouchableOpacity>
                    )
            }

            else {
                return(
                    <TouchableOpacity style={route.params.screen== item.navigation? style.liststyleSelected:style.liststyle} onPress={()=>navigation.replace('MainRouter', { screen:item.navigation})}>
                        {item.type == 'IconFeather' ? 
                            <IconFeather 
                                name={item.icon}
                                size={25} 
                                style={{
                                    marginLeft:10,
                                    flex:2
                                }}
                                color={route.params.screen==item.navigation?'white':'#008c45'}
                            />
                        :
                            <IconFontAwesome 
                                name={item.icon} 
                                size={22} 
                                style={{
                                    marginLeft:10,
                                    flex:2
                                }}
                                color={route.params.screen==item.navigation?'white':'#008c45'}
                            />
                        }
                        {/* {console.log('item : ', item.text)} */}
                        <Text style={route.params.screen==item.navigation? style.styleHeaderPlaceholderSelected:style.styleHeaderPlaceholder}>{item.text}</Text>
                    </TouchableOpacity>
                    )
            }

        }
     })
    }
    </ScrollView>
    {/* </View> */}

{/* : null} */}
        </>
    );
}

const style = StyleSheet.create({
    liststyle:{
        // margin:2,
        padding:9,
        borderColor:'#ddd', 
        borderBottomWidth:1, 
        flexDirection: 'row'
      },
      liststyleSelected:{
        // margin:2,
        padding:9,
        borderColor:'#008c45', 
        borderBottomWidth:1, 
        flexDirection: 'row',
        backgroundColor: '#008c45'
      },
      styleHeaderPlaceholderSelected:{
        flex:8,
        color: 'white',
        fontSize:15,
    },
      styleHeaderPlaceholder:{
        flex:8,
        color: '#3e4747',
        fontSize:15,
     },
  });

