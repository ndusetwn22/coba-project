import {StyleSheet} from 'react-native';
import COLORS from './StyleColor';
import {Dimensions } from "react-native";

const StyleSidebar = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor: COLORS.BACKGROUND
    },
    image:{
      flex: 1,
      width: '100%',
      opacity: 0.3
    },
    full:{
      width: '100%',
      height: '100%',
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0
    },
    logo:{
      height: '95%',
      width: '100%',
      opacity: 1,
      // top: '0%',
      left: '0%',
      right: 0,
      bottom: 0,
      position: 'absolute',
      top: -30
      
    },
    logocontainer:{
      height: '100%',
      width: '100%',
      textAlign: 'center',
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
    },
    drawerheader:{
      backgroundColor: COLORS.BACKGROUND,
      width: '100%',
      alignItems: 'flex-start',
      justifyContent: 'flex-end',
      borderBottomEndRadius: 30,
      borderBottomStartRadius: 30,
    },
    drawerbody:{
      width: '100%',
    },
    full:{
      alignSelf: 'stretch',
      textAlign: 'center'
    },
    profile:{
      marginLeft: '3%',
      marginBottom: '3%'
    },
    nama:{
      fontWeight: 'bold',
      color:'black'
    },
    nama_shipper:{
      fontWeight: 'bold',
      fontSize: 16
    },
    email:{
      color: COLORS.TEXT,
      fontSize: 10,
      fontStyle:"italic"
    },
    hp: {
      color: COLORS.TEXT,
      fontSize: 10
    },
    menu:{
      flex: 1,
      // borderBottomWidth: 1,
      // borderBottomColor: COLORS.TEXT,
      // borderBottomStartRadius: 16,
      // borderBottomEndRadius: 16,
      flexDirection: 'row',
      paddingHorizontal: 16,
      paddingVertical: 16,
      marginRight: '5%',
    },
    left:{
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      paddingRight: 5
    },
    body:{
      flex: 9,
      justifyContent: 'center',
      paddingHorizontal: 5
      // backgroundColor: 'red'
    },
    icon:{
      fontSize: 18,
      color: COLORS.TEXT
    },
    active:{
      backgroundColor: COLORS.PRIMARY_LIGHT_TRANSPARENT,
      borderBottomEndRadius: 20,
      borderTopEndRadius: 20,
      
    },
    activeFont:{
      color: COLORS.PRIMARY_LIGHT,
      fontWeight: 'bold'
    },
    text:{
      color: COLORS.TEXT,
    },
    drawer:{
      flex: 1
    },
    background:{
      backgroundColor: COLORS.BACKGROUND
    }
  });
  
export default StyleSidebar;