import React,{Component} from 'react'
import {Header, Text,Left, Body, Right,Icon} from 'native-base'
import {View} from "react-native";
import {BurgerMenu} from '../../Component/BurgerMenu'
import {ChattingIcon} from '../../Component/Chatting'
import { NavigationContainer,useNavigation,DrawerActions  } from '@react-navigation/native';
import { TouchableOpacity } from 'react-native-gesture-handler';

function CustomNavbar(props) {
  const navigation = useNavigation();
  return(
    <Header style={{backgroundColor:"#fff"}} androidStatusBarColor="#000">
      <Left>
        {props.buttonBack == null ? 
          <BurgerMenu/> :               
        <Icon onPress={props.buttonBack} type="MaterialCommunityIcons" name='arrow-left' style={{color: '#008c45'}}/>
        }
      </Left>
      <Body>
        <Text style={{color:'#008c45', fontSize:17, fontWeight:"bold"}}>
          {props.title}
        </Text>
      </Body>
      <Right>
        <TouchableOpacity onPress={()=>{navigation.replace('MainRouter', { screen:'Chatting'})}}>
          <ChattingIcon />
        </TouchableOpacity>
      </Right>
    </Header>
  );
}


export default CustomNavbar;