import React from 'react'
import { NavigationContainer,useNavigation,DrawerActions  } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Octicons';
import {TouchableOpacity} from "react-native";

export function BurgerMenu() {
    const navigation = useNavigation();
    return (
        <TouchableOpacity style={{
            width:40
        }} onPress={()=>{navigation.dispatch(DrawerActions.toggleDrawer())}}>
            <Icon 
                name='three-bars' 
                size={25} 
                style={{marginLeft:10
                }}
                color='#008c45' 
            />
        </TouchableOpacity>
    );
  }