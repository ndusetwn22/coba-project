import React from 'react'
import { NavigationContainer,useNavigation,DrawerActions  } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {TouchableOpacity} from "react-native";

export function ChattingIcon() {
    const navigation = useNavigation();
    return (
        <TouchableOpacity style={{
            width:40
        }} >
            <Icon 
                name='envelope' 
                size={25} 
                style={{
                    marginRight:10,
                    fontWeight:"bold"
                }}
                color='#F5AC41' 
            />
        </TouchableOpacity>
    );
  }