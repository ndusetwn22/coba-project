import React,{Component} from 'react'
import {Header, Text,Left, Body, Right,Icon, Button} from 'native-base'
import {View, Image, Dimensions} from "react-native";
import { NavigationContainer,useNavigation,DrawerActions  } from '@react-navigation/native';
import { TouchableOpacity } from 'react-native-gesture-handler';
var lost = require('../../../Assets/lostConnection.png');
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
function LostConnection(props) {
//   const navigation = useNavigation();
  return(
      <>
        <View style={{flex: 1, justifyContent: 'center', backgroundColor: 'white'}}>
          <View style={{flexDirection: 'column', justifyContent: 'center'}}>
            <Image resizeMode='contain' source={lost} style={{width: width, height: height, marginBottom: width/2, opacity: 0.5, alignSelf: 'center'}}></Image>
            <Text note style={{alignSelf: 'center', position: 'absolute', top: width+30, textAlign: 'center', fontSize: 14, paddingLeft: 50, paddingRight: 50}}> 
              Ooopps... koneksi internet kamu tidak stabil/lambat! 
            </Text>
            <Button onPress={props.restartConnection} style={{backgroundColor: '#008c45', justifyContent: 'center', alignSelf: 'center', position: 'absolute', top: width+90, textAlign: 'center', height: 40, width: 200}}>
              <Text>
                Coba lagi
              </Text>
            </Button>
          </View>
        </View>
    </>
  );
}


export default LostConnection;