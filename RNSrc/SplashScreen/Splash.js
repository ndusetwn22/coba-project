import {Text, View, Image,BackHandler,PermissionsAndroid} from 'react-native';
import React, {Component} from 'react';
import {Container, Content, Spinner} from 'native-base';
import styles from './css/style.js'
import {version, getProfile} from "../../APIProp/ApiConstants";

export default class Splash extends Component{
  constructor(props) {
    super(props);
    this.state = {
      profile:null
    };
  }
  async componentDidMount(){
      try{
          const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
              {
                'title': 'Mostrans Transporter',
                'message': 'Locate Your Company Location'
              }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {}
            else{
              alert("Active Location Permission For Using This Application");
          }

          const granted_camera = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA,
            {
              'title': 'Mostrans Transporter',
              'message': 'Entry Driver/Kerani'
            }
          )
          if (granted_camera === PermissionsAndroid.RESULTS.GRANTED) {}
          else{
            alert("Active Camera Permission For Using This Application");
        }
      }catch(e){
          alert(e.message)
      }
      
      await getProfile().then(result=>{
        this.setState({profile:result})
      })

      this.interval = setInterval(()=>{
        if(this.state.profile==null){
          this.props.navigation.replace('Login')
        }else{
          this.props.navigation.replace('MainRouter')
        }
      }, 2000);
  }
  componentWillUnmount(){
      clearInterval(this.interval);
  }

  render() {
    return (
      <Container>
        <Content contentContainerStyle={styles.container} style={{flex: 1}}>
        <View style={{
                        width: '100%',
                        height: '100%',
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: 0
                }}>
                    <Image source={require('../../Assets/Background2a.png')} style={[ {      flex: 1,
                    width: '100%',
                    right: -10,
                    opacity: 0.4}]}/>
                </View>
          <Image source={require('../../Assets/logo.png')} style={styles.logo}/>
          <Spinner style={styles.spinner}/>
          <Text style={{ fontStyle: 'italic'}}>Version : {version.version}</Text>
        </Content>
      </Container>
    );
  }
}