/* App config for apis
 */

import AesCrypto from 'react-native-aes-kit';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

import {deleteTokenFcm} from '../../mostrans_transporter/RNSrc/LoginPage/LoginAxios'
import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {Spinner} from 'native-base';

const secretKey = '(1xkfh*2.zl^23-]';
const iv = "43i9{tdk'14045xw";

const secretKeyChat = 'mostrans12345678';
const ivChat = 'mostrans12345678';

export const encrypt = (data) => new Promise((resolve)=>{
    AesCrypto.encrypt(data,secretKey, iv).then(cipher=>{
        resolve(cipher)
    }).catch(err=>{
        resolve(false)
    });
});

export const decrypt = (data) => new Promise((resolve)=>{
    AesCrypto.decrypt(data,secretKey, iv).then(plaintext=>{
        resolve(plaintext)
    }).catch(err=>{
        resolve(false)
    });
});

export const encryptChat = (data) => new Promise((resolve)=>{
    AesCrypto.encrypt(data,secretKeyChat, ivChat).then(cipher=>{
        resolve(cipher)
    }).catch(err=>{
        resolve(false)
    });
});

export const decryptChat = (data) => new Promise((resolve)=>{
    AesCrypto.decrypt(data,secretKeyChat, ivChat).then(plaintext=>{
        resolve(plaintext)
    }).catch(err=>{
        // alert("Error chat:"+err)
        resolve(false)
    });
});

export const setUrl = () => new Promise(async (resolve)=>{
    // let url = 'https://mostrans.co.id/apimobile/';
    // if (version.type === 'dev') url += 'testing';
    // let encrypted_response = await Axios.get(url);
    // let decrypted_response = await decrypt(encrypted_response.data);
    // await AsyncStorage.setItem('url', decrypted_response).then(value=>{    });
    // resolve('success');

    try {
        let url = 'https://mostrans.co.id/apimobile/';
        if (version.type === 'dev') url += 'testing';
        let encrypted_response = await Axios.get(url);
        let decrypted_response = await decrypt(encrypted_response.data);
        await AsyncStorage.setItem('url', decrypted_response).then(value=>{    });
        resolve('success');
    } catch (error) {
        console.log('ERROR SET URL/JARINGAN', error)
        resolve(false)
    }
})

export const getUrl = () => new Promise(async (resolve, reject)=>{
    await AsyncStorage.getItem('url').then(value=>{
        resolve(JSON.parse(value));
    }).catch(err=>{
        reject('error');
    })
})

export const logOut=async (url, id_user, token_fcm)=>{
    // console.log(id_user, token_fcm)
    //apus Token FCM
    await AsyncStorage.clear();
    // await deleteTokenFcm(url, id_user, token_fcm)
}

export const setProfile = (profile) => new Promise(async (resolve)=>{
    var strProfile = JSON.stringify(profile);
    let encrypted_profile = await encrypt(strProfile);
    await AsyncStorage.setItem('profile', encrypted_profile).then(value=>{    });
    resolve('success');
})

export const getProfile = () => new Promise(async (resolve, reject)=>{
    await AsyncStorage.getItem('profile').then(value=>{
        resolve(value);
    }).catch(err=>{
        reject('error');
    })
})

export const checkVersion = async (url,version,app) =>{
    let id_app = "market://details?id=com.mostrans.transporter";
    let x = await encrypt("select update from mt_master_version where version='" + version + "' and app='" + app + "'");
    await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      if(x.length === 0 ) alert('Error version! '+version, 'Versi tidak sesuai. Silahkan install ulang aplikasi di Play Store',[
        {'text': 'Ok', onPress: ()=>{
          BackHandler.exitApp();
          Linking.openURL(id_app);
        }}
      ], {cancelable: false});
      else {
        if(x[0].update === 'N') return true;
        else alert('New Updates Available', 'Mohon Update Aplikasi untuk menggunakan Mostrans.',[
          {'text': 'Ok', onPress: ()=>{
            BackHandler.exitApp();
            Linking.openURL(id_app);
          }}
        ], {cancelable: false});
      }
    })
}

// Jika mau ke prod, ubah type dan firebase prod
export const version = {
    app: 'TRANSPORTER',
    version: '1.2.6', // dirubah juga
    type: 'dev'// prod | dev
}

export const firebase = {
    linkStorage: 'https://firebasestorage.googleapis.com/v0/b/mostrans-dev.appspot.com/o/' //rubah link dan file json
// prod : 'https://firebasestorage.googleapis.com/v0/b/mostrans-storage.appspot.com/o/'
// pandu/dev : 'https://firebasestorage.googleapis.com/v0/b/mostrans-dev.appspot.com/o/'
}

export default class Loading extends Component{
    render(){
        return(
            <View style={{position: 'absolute',zIndex:0, elevation:10, top: 0, left: 0, right:0, bottom:0, flex:1, backgroundColor: '#00000080', justifyContent: 'center'}}>
                <Spinner size={50}/>
                <Text style={{alignSelf: 'center', color:"#dedede", fontSize:20,fontWeight:"bold", fontStyle:"italic"}}>Loading...</Text>
            </View>
        )
    }
}