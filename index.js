/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import React, { Component } from 'react';
import Routes from './RNSrc/Router/RouterNavigation.js'

// import {drawernav} from './RNSrc/Router/Tmp/drawernav'
class MobileTransporter extends Component {
    render() {
       return (
          <Routes />
       )
    }
 }

AppRegistry.registerComponent(appName, () => MobileTransporter);